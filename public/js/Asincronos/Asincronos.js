//distrito municipio
$(document).on('change', '#entidad', function () {
     if($('#entidad').val() != ''){
     
        var formData = new FormData();
        formData.append("entidad", $('#entidad').val());
        // console.log($('#entidad').val());

        $.ajax({
            url: "obtenerMunicipio", 
            type: "POST",
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (datos) {
            //  console.log(datos);
                let option = '';
                option += '<option value="" selected>Seleccione un municipio</option>';
                $.each(datos, function (index, value) {
                    option += '<option value="' + value.cve_mpioINE + '" >' + value.municipio + '</option>';
                });

                $("#municipio").html(option);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });

        $.ajax({
            url: "obtenerDistritoFederal", 
            type: "POST",
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (datos) {
                // console.log(datos);
                let option = '';
                option += '<option value="" selected>Seleccione un distrito federal</option>';
                $.each(datos, function (index, value) {
                    option += '<option value="' + value.cve_df + '" >' + value.df_nombre + '</option>';
                });

                $("#DF").html(option);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });

        $.ajax({
            url: "obtenerDistritoLocal", 
            type: "POST",
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (datos) {
                console.log(datos);
                let option = '';
                option += '<option value="" selected>Seleccione un distrito local</option>';
                $.each(datos, function (index, value) {
                    option += '<option value="' + value.cve_dl + '" >' + value.dl_nombre + '</option>';
                });

                $("#DL").html(option);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    }
});

//disabled con change de mun, dl o df
    //distrito municipio
    $(document).on('change', '#municipio', function () {
        if($('#municipio').val() != ''){
        
            $("#DL").prop('disabled', true);
            $("#DF").prop('disabled', true);

        }
    });

    //distrito federeal
    $(document).on('change', '#DF', function () {
        if($('#DF').val() != ''){
        
            $("#municipio").prop('disabled', true);
            $("#DL").prop('disabled', true);

        }
    });

    //distrito local
    $(document).on('change', '#DL', function () {
        if($('#DL').val() != ''){
        
            $("#municipio").prop('disabled', true);
            $("#DF").prop('disabled', true);


        }
    });

//btn activar diusabled
$( "#btnLimpiar" ).on( "click", function() {
    $("#municipio").prop('disabled', false).empty();
    $("#DF").prop('disabled', false).empty();
    $("#DL").prop('disabled', false).empty();

    let option = '';
    option += '<option value="" selected>Seleccione una entidad</option>';
    $("#municipio").html(option);
    $("#DF").html(option);
    $("#DL").html(option);
  } );