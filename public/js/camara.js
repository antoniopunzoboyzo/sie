(function () {
  if (
    !"mediaDevices" in navigator ||
    !"getUserMedia" in navigator.mediaDevices
  ) {
    alert("No cuenta con Camara, se requiere para la evidencia fotografica");
    return;
  }
const video = document.querySelector("#video");
const btnScreenshot = document.querySelector("#btnScreenshot");
const btnScreenshot2 = document.querySelector("#btnScreenshot2");

const btnChangeCamera = document.querySelector("#btnChangeCamera");
const screenshotsContainer = document.querySelector("#img1");
const screenshotsContainertxt = document.querySelector("#img1txt");

const screenshotsContainer2 = document.querySelector("#img2");
const canvas = document.querySelector("#canvas");
const devicesSelect = document.querySelector("#devicesSelect");

// video constraints
const constraints = {
  video: {
    width: {
      max: 500,
    },
    height: {
      max: 500,
    },
  },
};

// use front face camera
let useFrontCamera = true;

// current video stream
let videoStream;

// handle events
// play
/*btnPlay.addEventListener("click", function () {
  video.play();
  btnPlay.classList.add("is-hidden");
  btnPause.classList.remove("is-hidden");
});

// pause
btnPause.addEventListener("click", function () {
  video.pause();
  btnPause.classList.add("is-hidden");
  btnPlay.classList.remove("is-hidden");
});*/

// take screenshot
btnScreenshot.addEventListener("click", function () {
  const img = document.createElement("img");
  img.setAttribute('id','productor')
  canvas.width = video.width;
  canvas.height = video.height;
  canvas.getContext("2d").drawImage(video, 0, 0);
  img.src = canvas.toDataURL("image/png");
  $(screenshotsContainer).html(img);
});

btnScreenshot2.addEventListener("click", function () {
  const img = document.createElement("img");
  img.setAttribute('id','ine')
  canvas.width = video.width;
  canvas.height = video.height;
  canvas.getContext("2d").drawImage(video, 0, 0);
  img.src = canvas.toDataURL("image/png");
  $(screenshotsContainer2).html(img);
});

// switch camera
btnChangeCamera.addEventListener("click", function () {
  useFrontCamera = !useFrontCamera;

  initializeCamera();
});

// stop video stream
function stopVideoStream() {
  if (videoStream) {
    videoStream.getTracks().forEach((track) => {
      track.stop();
    });
  }
}

// initialize
async function initializeCamera() {
  stopVideoStream();
  constraints.video.facingMode = useFrontCamera ? "user" : "environment";

  try {
    videoStream = await navigator.mediaDevices.getUserMedia(constraints);
    video.srcObject = videoStream;
  } catch (err) {
    alert("No tienes tu camara habilitada");
  }
}

initializeCamera();
})();