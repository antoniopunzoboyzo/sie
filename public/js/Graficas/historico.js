var options = {
  colors: ["#27A4FA", "#3E6F31", "#E1DF41", "#BB204D","#45DF1F"],
  series: [{
    name: "PAN",
    data: [99, 52, 38, 24, 33, 26, 21, 20, 6, 8]
  },
  {
    name: "PRI",
    data: [50, 41, 62, 42, 13, 18, 29, 37, 36, 51]
  },
  {
    name: 'PRD',
    data: [10, 57, 74, 99, 75, 38, 62, 47, 82, 56]
  }
  ,
  {
    name: 'MORENA',
    data: [10, 70, 74, 110, 87, 65, 90, 67, 95, 69]
  },
  {
    name: "PVEM",
    data: [5, 12, 23, 47, 57, 26, 35, 20, 15, 18]
  },
],
  chart: {
  height: 350,
  type: 'line',
  zoom: {
    enabled: false
  },
},
dataLabels: {
  enabled: false
},
stroke: {
  width: [3, 3, 2],
  curve: 'straight',
  dashArray: [0, 10, 5]
},

legend: {
  tooltipHoverFormatter: function(val, opts) {
    return val + ' - ' + opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] + ''
  }
},
markers: {
  size: 0,
  hover: {
    sizeOffset: 6
  }
},
xaxis: {
  categories: ['DF2018', 'Sen2018', 'P2018', 'AYU2018', 'DL2018', 'DF2021', 'AYU2021', 'DL2021', 'GOB2021',
    'Proyec'
  ],
},
tooltip: {
  y: [
    {
      title: {
        formatter: function (val) {
          return val + " (mins)"
        }
      }
    },
    {
      title: {
        formatter: function (val) {
          return val + " per session"
        }
      }
    },
    {
      title: {
        formatter: function (val) {
          return val;
        }
      }
    }
  ]
},
grid: {
  borderColor: '#f1f1f1',
}
};

  var Delitos = new ApexCharts(document.querySelector("#Delitos"), options);
  Delitos.render();

  var options = {
    series: [44, 55],
    chart: {
      width: "80%",
      height: 200,
      type: 'pie',
    },
    labels: ['Hombres', 'Mujeres'],
    responsive: [{
      breakpoint: 1000,
      options: {
        chart: {
          width: 200
        },
        legend: {
          position: 'bottom'
        }
      }
    }]
  };

  var genero = new ApexCharts(document.querySelector("#genero"), options);
  genero.render();

  var options = {
    series: [67, 33],
    chart: {
      width: "80%",
      height: 200,
      type: 'pie',
    },
    labels: ['Hombres', 'Mujeres'],
    responsive: [{
      breakpoint: 1000,
      options: {
        chart: {
          width: 200
        },
        legend: {
          position: 'bottom'
        }
      }
    }]
  };

  var pea = new ApexCharts(document.querySelector("#pea"), options);
  pea.render();

