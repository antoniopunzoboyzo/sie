// $(document).ready(function () {
//   $('[data-toggle="tooltip"]').tooltip('enable');
// });

$( "#NuevoUsuario" ).on( "click", function() {
  $('#formUsers')[0].reset();
  $('#accion').val('E');
} );

$( ".btnEditar" ).on( "click", function() {
    $('#formUsers')[0].reset();
    $('#name').val(this.dataset.nombre);
    $('#primerApellido').val(this.dataset.primerapellido);
    $('#segundoApellido').val(this.dataset.segundoapellido);
    $('#email').val(this.dataset.email);
    $('#accion').val('E');
    $('#rol').val(this.dataset.rol == 'Administrador' ? '1' : '2').trigger('change');
  } );

$( ".btnEliminar" ).on( "click", function(event) {
    $('#name').val(this.dataset.nombre);
    $('#primerApellido').val(this.dataset.primerapellido);
    $('#segundoApellido').val(this.dataset.segundoapellido);
    $('#email').val(this.dataset.email);
    $('#accion').val('B');
    $('#rol').val(this.dataset.rol == 'Administrador' ? '1' : '2').trigger('change');
    document.getElementById("formUsers").submit();



} );