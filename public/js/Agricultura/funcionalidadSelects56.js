$(document).ready(function () {
    $('.quienCapacito').hide();// step-5
    $('.nivelJefe').hide();
    $('.espeQuien').hide();
    $('.porcentaje').hide();
    $('.porquenoayudo').hide();
    $('.siDieronTriptico').hide();
    $(".temaCapacitacion").hide();

});

// step-5
$("#cve_capacitacion").on("change", function () {
    var capacitacion = $(this).val();

    if (capacitacion == 2 || capacitacion == 3) {
        $('.quienCapacito').show("slow");
        $('#de_quien').attr('required', true);
        $(".temaCapacitacion").show("slow");
        $("#cve_tema").attr('required', true);
    } else {
        $('.quienCapacito').hide("slow");
        $('#de_quien').attr('required', false);
        $("#de_quien").val("");
        $(".temaCapacitacion").hide("slow");
        $("#cve_tema").attr('required', false);

    }
});
// step-5


$("input[name='conoce_recursos']").on("change", function () {
    var conocejefe = $(this).val();

    if (conocejefe == 1) {
        $('.nivelJefe').show("slow");
        $('#cve_recurso').attr('required', true);
    } else {
        $('.nivelJefe').hide();
        $("#cve_recurso option[value='']").attr("selected", true);
        $("#cve_recurso").val("");
        $('#cve_recurso').attr('required', false);

        // tambien oculta especificacion de a quien conoce
        $('.espeQuien').hide();
        $("#quien").val("");
        $('#quien').attr('required', false);
    }
});

$("#cve_recurso").on("change", function () {
    var nivelRecurso = $(this).val();

    if (nivelRecurso == 4) {
        $('.espeQuien').show("slow");
        $('#quien').attr('required', true);
    } else {
        $('.espeQuien').hide();
        $("#quien").val("");
        $('#quien').attr('required', false);
    }

});


$("input[name='entrega_triptico']").on("change", function () {
    var triptico = $(this).val();

    if (triptico == 1) {
        $('.siDieronTriptico').show("slow");
        $("input[name='claro_triptico']").attr('required', true);
    } else {
        $('.siDieronTriptico').hide();
        $("input[name='claro_triptico']").val("");
        $("input[name='claro_triptico']").attr('required', false);
    }

});


$("input[name='mejora_produccion']").on("change", function () {
    var mejoraproduc = $(this).val();

    if (mejoraproduc == 1) {
        $('.porcentaje').show("slow");
        $('#cve_porcentaje').attr('required', true);
        // seccion de porque 
        $('.porquenoayudo').hide();
        $("#porque_satisfaccion").attr('required', false);
        $("#porque_satisfaccion").val("");
    } else {
        $('.porcentaje').hide();
        $("#cve_porcentaje option[value='']").attr("selected", true);
        $("#cve_porcentaje").val("");
        $('#cve_porcentaje').attr('required', false);

        // mostrar el porque no ayudo
        $('.porquenoayudo').show("slow");
        $("#porque_satisfaccion").attr('required', true);
    }
});


function satisfacionNobeneficiario(beneficiario) {
    if (beneficiario == 1) {
        $('#Satisfaccion').show();
        $('#noBeneficiarioSatis').hide();

        inputsSi = $('#Satisfaccion')
            .find("select[name='cedas_nombre'], input[name='conoce_recursos'], input[name='convocado'], select[name='tiempo_espera'], input[name='entrega_formal'],input[name='explicacion_aplicarlo'],input[name='entrega_triptico'],select[name='cve_recibio_fertilizante'], select[name='cve_satisfecho'], textarea[name='porque'],input[name='suficienciafer'], input[name='mejora_produccion']");
        for (var i = 0; i < inputsSi.length; i++) {
            $(inputsSi[i]).attr('required', true);
        }
    } else {
        $('#Satisfaccion').hide();
        $('#noBeneficiarioSatis').show();
        inputsNo = $('#Satisfaccion').find("input[type='text'],input[type='url'],input[type='date'],select,textarea,input[type='radio']");
        for (var i = 0; i < inputsNo.length; i++) {
            $(inputsNo[i]).removeAttr('required');
        }
    }
}


// validar radios


// ############################# RETORNO VALORES SECCION 5 ###############################

// function retornoInformacionBD(form, datos){
//     if (datos != 0){

//         $.map(datos, function (value, key) {
//             var campoName = key;
//             var encontrarCampos = form.find('[name="' + campoName + '"],[id="' + campoName + '"] ');
//             // console.log(encontrarCampos);

//             // si es select
//             if ($(encontrarCampos).is('select')) {
//                 $('[name="' + campoName + '"] option[value="' + value + '"]').attr("selected", true).trigger("change");
//             }
//             // si es select
         
//             // si es selectpicker
//             if ($(encontrarCampos).hasClass('selectpicker')){
                
//                 var isMultiple = $(encontrarCampos).prop('multiple');
//                 // si es opcion multiple
//                 if (isMultiple) {
//                     // console.log(key, value);
//                     var clavesEncontradas = buscarValoresSimilares(datos, campoName);
//                     console.log(campoName);
//                     var arr = clavesEncontradas.toString().split(',');

//                     if(campoName == 'cve_preparacion'){
//                         $('.pickerTerreno').selectpicker('val', arr);
//                         setTimeout(function(){
//                             $('.pickerTerreno').val(arr);
//                         }, 2000);

//                     }
//                     if(campoName == 'laborRealizo'){
//                         $('.laboresRealizo').selectpicker('val', arr);
//                         setTimeout(function(){
//                             $('.laboresRealizo').val(arr).trigger('change');
//                         }, 4000);

//                     }
//                     if(campoName == 'cve_tema'){
//                         $('.cve_tema').selectpicker('val', arr);
//                         setTimeout(function(){
//                             $('.cve_tema').val(arr).trigger('change');
//                         }, 6000);

//                     }



//                     // $(encontrarCampos).selectpicker('val', clavesEncontradas);
//                 }
//                 // sino es opcion multiple
//                 else{
//                     $(encontrarCampos).selectpicker('val', value);
//                 }
//             }
//             // si es selectpicker

//             // si es radio
//             if ($(encontrarCampos).is('input[type="radio"]')) {
//                 $(encontrarCampos).filter('[value="' + value + '"]').attr('checked', true).trigger("change");
//             }
//             // si es radio

//             else{
//                 $(encontrarCampos).val(value);
//             }

//         });


//         // buscar los valores del selectpicker
//         function buscarValoresSimilares(datos, campoName) {
//             var valoresSimilares = [];
//             Object.keys(datos).forEach(function (key) {

//                 var longitudMinima = Math.min(key.length, campoName.length);
//                 if (key.substring(0, longitudMinima) === campoName.substring(0, longitudMinima)) {
//                     // valoresSimilares[key] = datos[key];
//                     valoresSimilares.push(datos[key]);
//                 }
//             });

//             return valoresSimilares;
//         }


  
//     }

// }