$('.nosembroMotivo').hide();
$('.col24').hide();

$("#nosembro, #sembro").change(function () {
    if ($("#sembro").is(":checked")) {
        $('.col24').show("slow");
        $('.nosembroMotivo').hide();
        $('.NoOcupamosSegundoCultivoActual').hide();
        ////pendiente cyuando no va sembrar ocuoltar segundos campos
            //requireds
                $("input[name=motivo_no_sembro]").attr('required',false);

            // ¿CUAL(ES) CULTIVOS SEMBRO EL CICLO ANTERIOR?:
                $("select[name=ciclo_anterior_cultivo_1]").attr('required',true);
                $("input[name=hectarea_ciclo_anterior_1]").attr('required',true);
                $("input[name=area_ciclo_anterior_1]").attr('required',true);
                $("input[name=centiarea_ciclo_anterior_1]").attr('required',true);
                $("select[name=ciclo_anterior_1]").attr('required',true);
                $("select[name=regimen_hidrico_anterior_1]").attr('required',true);
            
            //fertilizante
                $("input[type=radio][name=aplico_fertilizante]").attr('required',true);

            //¿QUÉ SUPERFICIE COSECHO?
                $("input[name=hectarea_cosecho_anterior_1]").attr('required',true);
                $("input[name=area_cosecho_anterior_1]").attr('required',true);
                $("input[name=centiarea_cosecho_anterior_1]").attr('required',true);
                $("input[name=toneladas_cosecho_anterior_1]").attr('required',true);
                $("input[name=rendimiento_tonelada_cosecho_anterior_1]").attr('required',true);
            
            //¿CUÁL FUE EL DESTINO DE LA PRODUCCIÓN OBTENIDA?
                $("input[name=toneladas_autoconsumo_familiar_cult_1]").attr('required',true);
                $("input[name=toneladas_autoconsumo_productivo_cult_1]").attr('required',true);
                $("input[name=toneladas_comercializadora_cult_1]").attr('required',true);
                $("input[name=precio_venta_destino_prod_cult_1]").attr('required',true);
                $("input[name=ingresos_comercializacion_destino_prod_cult_1]").attr('required',true);
    } 
    else if ($("#nosembro").is(":checked")) {

        $('.col24').hide();
        $('.nosembroMotivo').show("slow");
            $('#ciclo_anterior_cultivo_2').val('').trigger('change');
            $('#regimen_hidrico_anterior_2').val('').trigger('change');

            $('.NoOcupamosSegundoCultivoActual').hide();

            //requireds
            $("input[name=motivo_no_sembro]").attr('required',true);

            // ¿CUAL(ES) CULTIVOS SEMBRO EL CICLO ANTERIOR?:
                $("select[name=ciclo_anterior_cultivo_1]").attr('required',false);
                $("input[name=hectarea_ciclo_anterior_1]").attr('required',false);
                $("input[name=area_ciclo_anterior_1]").attr('required',false);
                $("input[name=centiarea_ciclo_anterior_1]").attr('required',false);
                $("select[name=ciclo_anterior_1]").attr('required',false);
                $("select[name=regimen_hidrico_anterior_1]").attr('required',false);

                $("select[name=ciclo_anterior_cultivo_2]").attr('required',false);
                $("input[name=hectarea_ciclo_anterior_2]").attr('required',false);
                $("input[name=area_ciclo_anterior_2]").attr('required',false);
                $("input[name=centiarea_ciclo_anterior_2]").attr('required',false);
                $("select[name=ciclo_anterior_2]").attr('required',false);
                $("select[name=regimen_hidrico_anterior_2]").attr('required',false);

            //fertilizante
                $("input[type=radio][name=aplico_fertilizante]").attr('required',false);
                
            //¿QUÉ SUPERFICIE COSECHO?
                $("input[name=hectarea_cosecho_anterior_1]").attr('required',false);
                $("input[name=area_cosecho_anterior_1]").attr('required',false);
                $("input[name=centiarea_cosecho_anterior_1]").attr('required',false);
                $("input[name=toneladas_cosecho_anterior_1]").attr('required',false);
                $("input[name=rendimiento_tonelada_cosecho_anterior_1]").attr('required',false);
                
                $("input[name=hectarea_cosecho_anterior_2]").attr('required',false);
                $("input[name=area_cosecho_anterior_2]").attr('required',false);
                $("input[name=centiarea_cosecho_anterior_2]").attr('required',false);
                $("input[name=toneladas_cosecho_anterior_2]").attr('required',false);
                $("input[name=rendimiento_tonelada_cosecho_anterior_2]").attr('required',false);

            //¿CUÁL FUE EL DESTINO DE LA PRODUCCIÓN OBTENIDA?
                $("input[name=toneladas_autoconsumo_familiar_cult_1]").attr('required',false);
                $("input[name=toneladas_autoconsumo_productivo_cult_1]").attr('required',false);
                $("input[name=toneladas_comercializadora_cult_1]").attr('required',false);
                $("input[name=precio_venta_destino_prod_cult_1]").attr('required',false);
                $("input[name=ingresos_comercializacion_destino_prod_cult_1]").attr('required',false);
            
                $("input[name=toneladas_autoconsumo_familiar_cult_2]").attr('required',false);
                $("input[name=toneladas_autoconsumo_productivo_cult_2]").attr('required',false);
                $("input[name=toneladas_comercializadora_cult_2]").attr('required',false);
                $("input[name=precio_venta_destino_prod_cult_2]").attr('required',false);
                $("input[name=ingresos_comercializacion_destino_prod_cult_2]").attr('required',false);
    }
});

//////cultivos segundo

    $('.NoOcupamosSegundoCultivo').hide();
    $("select[name=ciclo_anterior_cultivo_2]").change(function () {

        if ($("select[name=ciclo_anterior_cultivo_2]").val() != '') {
            $('.NoOcupamosSegundoCultivo').show();
            $("input[name=area_ciclo_anterior_2]").attr('required',true);
            $("input[name=centiarea_ciclo_anterior_2]").attr('required',true);
            $("select[name=ciclo_anterior_2]").attr('required',true);
            $("select[name=regimen_hidrico_anterior_2]").attr('required',true);

            $("input[name=hectarea_cosecho_anterior_2]").attr('required',true);
            $("input[name=area_cosecho_anterior_2]").attr('required',true);
            $("input[name=centiarea_cosecho_anterior_2]").attr('required',true);
            $("input[name=toneladas_cosecho_anterior_2]").attr('required',true);
            $("input[name=rendimiento_tonelada_cosecho_anterior_2]").attr('required',true);

            $("input[name=toneladas_autoconsumo_familiar_cult_2]").attr('required',true);
            $("input[name=toneladas_autoconsumo_productivo_cult_2]").attr('required',true);
            $("input[name=toneladas_comercializadora_cult_2]").attr('required',true);
            $("input[name=precio_venta_destino_prod_cult_2]").attr('required',true);
            $("input[name=ingresos_comercializacion_destino_prod_cult_2]").attr('required',true);
        } else {

            $('input[name=hectarea_ciclo_anterior_2').val('').trigger('change');
            $('input[name=area_ciclo_anterior_2').val('').trigger('change');
            $('input[name=centiarea_ciclo_anterior_2').val('').trigger('change');
            validarHasUPConLineaBase();

            $('.NoOcupamosSegundoCultivo').hide();
            $("input[name=hectarea_ciclo_anterior_2]").attr('required',false);
            $("input[name=area_ciclo_anterior_2]").attr('required',false);
            $("input[name=centiarea_ciclo_anterior_2]").attr('required',false);
            $("select[name=ciclo_anterior_2]").attr('required',false);
            $("select[name=regimen_hidrico_anterior_2]").attr('required',false);

            $("input[name=hectarea_cosecho_anterior_2]").attr('required',false);
            $("input[name=area_cosecho_anterior_2]").attr('required',false);
            $("input[name=centiarea_cosecho_anterior_2]").attr('required',false);
            $("input[name=toneladas_cosecho_anterior_2]").attr('required',false);
            $("input[name=rendimiento_tonelada_cosecho_anterior_2]").attr('required',false);

            $("input[name=toneladas_autoconsumo_familiar_cult_2]").attr('required',false);
            $("input[name=toneladas_autoconsumo_productivo_cult_2]").attr('required',false);
            $("input[name=toneladas_comercializadora_cult_2]").attr('required',false);
            $("input[name=precio_venta_destino_prod_cult_2]").attr('required',false);
            $("input[name=ingresos_comercializacion_destino_prod_cult_2]").attr('required',false);
            
        }
    });

    $('.NoOcupamosSegundoCultivoActual').hide();

    $("select[name=cultivo_actual_2]").change(function () {
        if ($("select[name=cultivo_actual_2]").val() != '') {
            $('.NoOcupamosSegundoCultivoActual').show();

            $("select[name=cultivo_actual_2]").attr('required',true);
            $("input[name=hectarea_actual_cultivo_2]").attr('required',true);
            $("input[name=area_actual_cultivo_2]").attr('required',true);
            $("input[name=centiarea_actual_cultivo_2]").attr('required',true);
            $("select[name=ciclo_actual_cultivo_2]").attr('required',true);
            $("select[name=regimen_hidrico_actual_cultivo_2]").attr('required',true);

            $("input[name=hectarea_cosecho_actual_2]").attr('required',true);
            $("input[name=area_cosecho_actual_2]").attr('required',true);
            $("input[name=centiarea_cosecho_actual_2]").attr('required',true);
            $("input[name=toneladas_cosecho_actual_2]").attr('required',true);
            $("input[name=rendimiento_tonelada_cosecho_actual_2]").attr('required',true);

            $("input[name=toneladas_autoconsumo_familiar_actual_cult_2]").attr('required',true);
            $("input[name=toneladas_autoconsumo_productivo_actual_cult_2]").attr('required',true);
            $("input[name=toneladas_comercializadora_actual_cult_2]").attr('required',true);
            $("input[name=precio_venta_destino_prod_actual_cult_2]").attr('required',true);
            $("input[name=ingresos_comercializacion_destino_prod_actual_cult_2]").attr('required',true);

            $("input[name=hectareas_fertilizante2]").attr('required',true);
            $("input[name=areas_fertilizante2]").attr('required',true);
            $("input[name=centiareas_fertilizante2]").attr('required',true);
            $("input[name=fecha_aplicacion2]").attr('required',true);
            $("input[name=dosis2]").attr('required',true);
            $("input[name=urea_fecha2]").attr('required',true);
            $("input[name=dosis_urea2]").attr('required',true);

        }else{
            $('#regimen_hidrico_anterior_2').val('').trigger('change');
            $('.NoOcupamosSegundoCultivoActual').hide();

            $('input[name=hectarea_actual_cultivo_2').val('').trigger('change');
            $('input[name=area_actual_cultivo_2').val('').trigger('change');
            $('input[name=centiarea_actual_cultivo_2').val('').trigger('change');
            validarHasUP();

            $("select[name=cultivo_actual_2]").attr('required',false);
            $("input[name=hectarea_actual_cultivo_2]").attr('required',false);
            $("input[name=area_actual_cultivo_2]").attr('required',false);
            $("input[name=centiarea_actual_cultivo_2]").attr('required',false);
            $("select[name=ciclo_actual_cultivo_2]").attr('required',false);
            $("select[name=regimen_hidrico_actual_cultivo_2]").attr('required',false);

            $("input[name=hectarea_cosecho_actual_2]").attr('required',false);
            $("input[name=area_cosecho_actual_2]").attr('required',false);
            $("input[name=centiarea_cosecho_actual_2]").attr('required',false);
            $("input[name=toneladas_cosecho_actual_2]").attr('required',false);
            $("input[name=rendimiento_tonelada_cosecho_actual_2]").attr('required',false);

            $("input[name=toneladas_autoconsumo_familiar_actual_cult_2]").attr('required',false);
            $("input[name=toneladas_autoconsumo_productivo_actual_cult_2]").attr('required',false);
            $("input[name=toneladas_comercializadora_actual_cult_2]").attr('required',false);
            $("input[name=precio_venta_destino_prod_actual_cult_2]").attr('required',false);
            $("input[name=ingresos_comercializacion_destino_prod_actual_cult_2]").attr('required',false);

            $("input[name=hectareas_fertilizante2]").attr('required',false);
            $("input[name=areas_fertilizante2]").attr('required',false);
            $("input[name=centiareas_fertilizante2]").attr('required',false);
            $("input[name=fecha_aplicacion2]").attr('required',false);
            $("input[name=dosis2]").attr('required',false);
            $("input[name=urea_fecha2]").attr('required',false);
            $("input[name=dosis_urea2]").attr('required',false);

            

        }
    });


/////fincultivos 2

$('.aplicoFertilizante').hide();

$("#SiaplicoFertilizanteUP, #noaplicoFertilizanteUP").change(function () {
    if ($("#SiaplicoFertilizanteUP").is(":checked")) {
        ValidarTipoFertilizante();
        $('.aplicoFertilizante').show("slow");
        $("select[name=tipoFertilizanteuso]").attr('required',true);
        $("input[name=kg_fertilizante]").attr('required',true);
    } else 
    if ($("#noaplicoFertilizanteUP").is(":checked")) {
        ValidarTipoFertilizante();
        $('.aplicoFertilizante').hide();
        $("select[name=tipoFertilizanteuso]").attr('required',false);
        $("input[name=kg_fertilizante]").attr('required',false);
        
    }
});

$('.cualFertilizante').hide();

$("#tipoFertilizanteuso").change(function () {
    ValidarTipoFertilizante();
});

function ValidarTipoFertilizante(){
    if ($("#tipoFertilizanteuso").val() == 1) {
        $('.cualFertilizante').show("slow");
        $("select[name=cual_fertilizante_uso]").attr('required',true);
    } else {
        $('.cualFertilizante').hide();
        $("select[name=cual_fertilizante_uso]").attr('required',false);
    }
}

$('.nosembroMotivoEsteCiclo').hide();
$('.col63').hide();

$("#sembroEsteCiclo, #nosembroEsteCiclo").change(function () {
    if ($("#sembroEsteCiclo").is(":checked")) {
        $('.col63').show("slow");
        $('.nosembroMotivoEsteCiclo').hide();
        $("input[name=porque_no_sembro_ciclo_actual]").attr('required',false);

        // ¿CUAL(ES) CULTIVOS SEMBRÓ EN ESTE CICLO?
            $("select[name=cultivo_actual_1]").attr('required',true);
            $("input[name=hectarea_actual_cultivo_1]").attr('required',true);
            $("input[name=area_actual_cultivo_1]").attr('required',true);
            $("input[name=centiarea_actual_cultivo_1]").attr('required',true);
            $("select[name=ciclo_actual_cultivo_1]").attr('required',true);
            $("select[name=regimen_hidrico_actual_cultivo_1]").attr('required',true);
            
            // $("select[name=cultivo_actual_2]").attr('required',true);
            // $("input[name=hectarea_actual_cultivo_2]").attr('required',true);
            // $("input[name=area_actual_cultivo_2]").attr('required',true);
            // $("input[name=centiarea_actual_cultivo_2").attr('required',true);
            // $("select[name=ciclo_actual_cultivo_2]").attr('required',true);
            // $("select[name=regimen_hidrico_actual_cultivo_2]").attr('required',true);

        //¿QUÉ SUPERFICIE COSECHO?
            $("input[name=hectarea_cosecho_actual_1]").attr('required',true);
            $("input[name=area_cosecho_actual_1]").attr('required',true);
            $("input[name=centiarea_cosecho_actual_1]").attr('required',true);
            $("input[name=toneladas_cosecho_actual_1]").attr('required',true);
            $("input[name=rendimiento_tonelada_cosecho_actual_1]").attr('required',true);



        //¿CUÁL FUE EL DESTINO DE LA PRODUCCIÓN OBTENIDA?
            $("input[name=toneladas_autoconsumo_familiar_actual_cult_1]").attr('required',true);
            $("input[name=toneladas_autoconsumo_productivo_actual_cult_1]").attr('required',true);
            $("input[name=toneladas_comercializadora_actual_cult_1]").attr('required',true);
            $("input[name=precio_venta_destino_prod_actual_cult_1]").attr('required',true);
            $("input[name=ingresos_comercializacion_destino_prod_actual_cult_1]").attr('required',true);


        //mostrar datos tecnologicos
        $('#Tecnologias').show();
        $('#nodatosTecnologicos').hide();
        camposTecnologicosObligatorios = 
        $('#Tecnologias').find(
            "select[id='cve_sistema_produccion'], select[id='cve_preparacion'], select[name='cve_tipo_semilla'], select[name='cve_adquisicion_semilla'], select[name='cve_tiempo_semilla'],select[name='cve_tipo_siembra'],input[name='semilla_gramos'],input[name='fecha_siembra'], input[name='resiembra'], select[id='laborRealizo'],input[name='utilizo_herbicidas'], input[name='fertilizante_bienestar'], select[name='cve_temporal'], input[name='plagas'], input[name='cve_enfermedad'], input[name='rendimiento_cultivo']"
        );
        for (var i = 0; i < camposTecnologicosObligatorios.length; i++) {
            $(camposTecnologicosObligatorios[i]).attr('required', true);
        }



            
    } else 
    if ($("#nosembroEsteCiclo").is(":checked")) {
        $('#CultivoActual2').val('').trigger('change');

        $('.col63').hide();
        $('.nosembroMotivoEsteCiclo').show("slow");
        $("input[name=porque_no_sembro_ciclo_actual]").attr('required',true);
        $("#regimenHidricoActualCultivo1").val('').trigger('change');
        $("select[name=regimen_hidrico_actual_cultivo_2]").val('').trigger('change');

            // ¿CUAL(ES) CULTIVOS SEMBRÓ EN ESTE CICLO?
                $("select[name=cultivo_actual_1]").attr('required',false);
                $("input[name=hectarea_actual_cultivo_1]").attr('required',false);
                $("input[name=area_actual_cultivo_1]").attr('required',false);
                $("input[name=centiarea_actual_cultivo_1]").attr('required',false);
                $("select[name=ciclo_actual_cultivo_1]").attr('required',false);
                $("select[name=regimen_hidrico_actual_cultivo_1]").attr('required',false);
            


            //¿QUÉ SUPERFICIE COSECHO?
                $("input[name=hectarea_cosecho_actual_1]").attr('required',false);
                $("input[name=area_cosecho_actual_1]").attr('required',false);
                $("input[name=centiarea_cosecho_actual_1]").attr('required',false);
                $("input[name=toneladas_cosecho_actual_1]").attr('required',false);
                $("input[name=rendimiento_tonelada_cosecho_actual_1]").attr('required',false);
                
                $("input[name=hectarea_cosecho_actual_2]").attr('required',false);
                $("input[name=area_cosecho_actual_2]").attr('required',false);
                $("input[name=centiarea_cosecho_actual_2]").attr('required',false);
                $("input[name=toneladas_cosecho_actual_2]").attr('required',false);
                $("input[name=rendimiento_tonelada_cosecho_actual_2]").attr('required',false);

            //¿CUÁL FUE EL DESTINO DE LA PRODUCCIÓN OBTENIDA?
                $("input[name=toneladas_autoconsumo_familiar_actual_cult_1]").attr('required',false);
                $("input[name=toneladas_autoconsumo_productivo_actual_cult_1]").attr('required',false);
                $("input[name=toneladas_comercializadora_actual_cult_1]").attr('required',false);
                $("input[name=precio_venta_destino_prod_actual_cult_1]").attr('required',false);
                $("input[name=ingresos_comercializacion_destino_prod_actual_cult_1]").attr('required',false);

                $("input[name=toneladas_autoconsumo_familiar_actual_cult_2]").attr('required',false);
                $("input[name=toneladas_autoconsumo_productivo_actual_cult_2]").attr('required',false);
                $("input[name=toneladas_comercializadora_actual_cult_2]").attr('required',false);
                $("input[name=precio_venta_destino_prod_actual_cult_2]").attr('required',false);
                $("input[name=ingresos_comercializacion_destino_prod_actual_cult_2]").attr('required',false);
            
                //ocultar datos tecnologicos
                $('#Tecnologias').hide();
                $('#nodatosTecnologicos').show();
                camposTecnologicosObligatorios = $('#Tecnologias').find("input[type='text'],input[type='url'],input[type='date'],select,textarea,input[type='radio']");
                for (var i = 0; i < camposTecnologicosObligatorios.length; i++) {
                    $(camposTecnologicosObligatorios[i]).removeAttr('required');
                }
    }
});

$(document).on('change', '#ciclo_anterior_cultivo_1', function () {
    constultarCultivo($("#ciclo_anterior_cultivo_1").val(), input = 1);
});

$(document).on('change', '#ciclo_anterior_cultivo_2', function () {
    constultarCultivo($("#ciclo_anterior_cultivo_2").val(), input = 2);
});

$(document).on('change', '#CultivoActual1', function () {
    constultarCultivo($("#CultivoActual1").val(), input = 3);
});

$(document).on('change', '#CultivoActual2', function () {
    constultarCultivo($("#CultivoActual2").val(), input = 4);
});

function constultarCultivo(cicloEntrada, input){
    var formData = new FormData();
    formData.append("ciclo", cicloEntrada);
    var input = input;

    $.ajax({
        url: "consultarCultivo",
        type: "POST",
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function (datos) {
            // console.log(input);

            let option = '';
            $.each(datos, function (index, value) {
                option += '<option value="' + value.cve_cultivo + '" >' + value.cultivo + '</option>';
            });
            
            if(input == 1){
                if(datos.length == 1){
                    $("#cultivoProduccionObtenida1").html(option);
                    $("#cultivocosecho1").html(option);
                }else{
                    $("#cultivoProduccionObtenida1").html('<br> debe seleccionar una opción de cultivo');
                    $("#cultivocosecho1").html('<br> debe seleccionar una opción de cultivo');
                }
            }else
                if(input == 2){
                    if(datos.length == 1){
                        $("#cultivoProduccionObtenida2").html(option);
                        $("#cultivocosecho2").html(option);
                    }else{
                        $("#cultivoProduccionObtenida2").html('');
                        $("#cultivocosecho2").html('');
                    }
                }
            else
                if(input == 3){
                    if(datos.length == 1){
                        $("#cultivoActualCosecho1").html(option);
                        $("#cultivoActualDestino1").html(option);
                        $("#cultivo1AplicoFertilizante").html(option);
                    }else{
                        $("#cultivo1AplicoFertilizante").html('<br> debe seleccionar una opción de cultivo');
                        $("#cultivoActualCosecho1").html('<br> debe seleccionar una opción de cultivo');
                        $("#cultivoActualDestino1").html('<br> debe seleccionar una opción de cultivo');
                    }
                }
            else
                if(input == 4){
                    if(datos.length == 1){
                        $("#cultivo2AplicoFertilizante").html(option);
                        $("#cultivoActualCosecho2").html(option);
                        $("#cultivoActualDestino2").html(option);
                    }else{
                        $("#cultivo2AplicoFertilizante").html('<br> debe seleccionar una opción de cultivo');
                        $("#cultivoActualCosecho2").html('<br> debe seleccionar una opción de cultivo');
                        $("#cultivoActualDestino2").html('<br> debe seleccionar una opción de cultivo');
                    }
                }


        },
        error: function (jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function soloNumeros(evt){
    
    // code is the decimal ASCII representation of the pressed key.
    var code = (evt.which) ? evt.which : evt.keyCode;
    
    if(code==8) { // backspace.
      return true;
    } else if(code>=48 && code<=57) { // is a number.
      return true;
    } else{ // other keys.
      return false;
    }
}

$('.decimal').keyup(function(){
    var val = $(this).val();
    if(isNaN(val)){
         val = val.replace(/[^0-9\.]/g,'');
         if(val.split('.').length>2) 
             val =val.replace(/\.+$/,"");
    }
    $(this).val(val); 
});

// logica de caulculos sobre rendiemientos
        
$( $("input[name=hectarea_cosecho_anterior_1]") ).on( "keyup", function() {
    obtener_rendimiento();
} );

$( $("input[name=toneladas_cosecho_anterior_1]") ).on( "keyup", function() {
    obtener_rendimiento();
} );

$( $("input[name=hectarea_cosecho_anterior_2]") ).on( "keyup", function() {
    obtener_rendimiento();
} );

$( $("input[name=toneladas_cosecho_anterior_2]") ).on( "keyup", function() {
    obtener_rendimiento();
} );

///actual
$( $("input[name=hectarea_cosecho_actual_1]") ).on( "keyup", function() {
    obtener_rendimiento();
} );

$( $("input[name=toneladas_cosecho_actual_1]") ).on( "keyup", function() {
    obtener_rendimiento();
} );

$( $("input[name=hectarea_cosecho_actual_2]") ).on( "keyup", function() {
    obtener_rendimiento();
} );

$( $("input[name=toneladas_cosecho_actual_2]") ).on( "keyup", function() {
    obtener_rendimiento();
} );

function obtener_rendimiento(){
    if($("input[name=hectarea_cosecho_anterior_1]").val() != '' && $("input[name=toneladas_cosecho_anterior_1]").val() != ''){
        $('#rendimiento_tonelada_cosecho_anterior_1').empty();
        var rendimiento = $("input[name=toneladas_cosecho_anterior_1]").val() / $("input[name=hectarea_cosecho_anterior_1]").val();
        $('#rendimiento_tonelada_cosecho_anterior_1').append('<span class="font-weight-bold text-gray-800 h4">'+rendimiento.toLocaleString().substring(0, 8)+'</span>')
    }

    if($("input[name=hectarea_cosecho_anterior_2").val() != '' && $("input[name=toneladas_cosecho_anterior_2]").val() != ''){
        $('#rendimiento_tonelada_cosecho_anterior_2').empty();
        var rendimiento = $("input[name=toneladas_cosecho_anterior_2]").val() / $("input[name=hectarea_cosecho_anterior_2]").val();
        $('#rendimiento_tonelada_cosecho_anterior_2').append('<span class="font-weight-bold text-gray-800 h4">'+rendimiento.toLocaleString().substring(0, 8)+'</span>')
        // console.log($("input[name=toneladas_cosecho_anterior_1]") / $("input[name=hectarea_cosecho_anterior_1]"));
    }

    if($("input[name=toneladas_cosecho_actual_1]").val() != '' && $("input[name=hectarea_cosecho_actual_1]").val() != ''){
        $('#rendimiento_tonelada_cosecho_actual_1').empty();
        var rendimiento = $("input[name=toneladas_cosecho_actual_1]").val() / $("input[name=hectarea_cosecho_actual_1]").val();
        $('#rendimiento_tonelada_cosecho_actual_1').append('<span class="font-weight-bold text-gray-800 h4">'+rendimiento.toLocaleString().substring(0, 8)+'</span>')
        // console.log($("input[name=toneladas_cosecho_anterior_1]") / $("input[name=hectarea_cosecho_anterior_1]"));
    }

    if($("input[name=hectarea_cosecho_actual_2").val() != '' && $("input[name=toneladas_cosecho_actual_2]").val() != ''){
        $('#rendimiento_tonelada_cosecho_actual_2').empty();
        var rendimiento = $("input[name=toneladas_cosecho_actual_2]").val() / $("input[name=hectarea_cosecho_actual_2]").val();
        $('#rendimiento_tonelada_cosecho_actual_2').append('<span class="font-weight-bold text-gray-800 h4">'+rendimiento.toLocaleString().substring(0, 8)+'</span>')
        // console.log($("input[name=toneladas_cosecho_anterior_1]") / $("input[name=hectarea_cosecho_anterior_1]"));
    }
}

//fin de rendiemientos

//clauclar las toneladas de ,mas
    $('#msjToneladasMas').hide();
$('.button_jairo').attr('disabled',false);
    $('#msjToneladasMasLineaBase').hide();
$('.button_jairo').attr('disabled',false);

    $( $("input[name=toneladas_autoconsumo_familiar_cult_1]")).on( "keyup", function() {
        var valor = 1;
        validarToneladasTotales(valor);
    } );
    $( $("input[name=toneladas_autoconsumo_productivo_cult_1]") ).on( "keyup", function() {
        var valor = 1;
        validarToneladasTotales(valor);
    } );
    $( $("input[name=toneladas_comercializadora_cult_1]") ).on( "keyup", function() {
        var valor = 1;
        validarToneladasTotales(valor);
    } );

    $( $("input[name=toneladas_autoconsumo_familiar_cult_2]")  ).on( "keyup", function() {
        var valor = 2;
        validarToneladasTotales(valor);
    } );
    $( $("input[name=toneladas_autoconsumo_productivo_cult_2]") ).on( "keyup", function() {
        var valor = 2;
        validarToneladasTotales(valor);
    } );
    $( $("input[name=toneladas_comercializadora_cult_2]") ).on( "keyup", function() {
        var valor = 2;
        validarToneladasTotales(valor);
    } );

//actual
    $( $("input[name=toneladas_autoconsumo_familiar_actual_cult_1]"),  ).on( "keyup", function() {
        var valor = 3;
        validarToneladasTotales(valor);
    } );
    $( $("input[name=toneladas_autoconsumo_productivo_actual_cult_1]") ).on( "keyup", function() {
        var valor = 3;
        validarToneladasTotales(valor);
    } );
    $( $("input[name=toneladas_comercializadora_actual_cult_1]") ).on( "keyup", function() {
        var valor = 3;
        validarToneladasTotales(valor);
    } );

    $( $("input[name=toneladas_autoconsumo_familiar_actual_cult_2]"),  ).on( "keyup", function() {
        var valor = 4;
        validarToneladasTotales(valor);
    } );
    $( $("input[name=toneladas_autoconsumo_productivo_actual_cult_2]") ).on( "keyup", function() {
        var valor = 4;
        validarToneladasTotales(valor);
    } );
    $( $("input[name=toneladas_comercializadora_actual_cult_2]") ).on( "keyup", function() {
        var valor = 4;
        validarToneladasTotales(valor);
    } );

function validarToneladasTotales(valor){
    if(valor == 1){
        if($("input[name=toneladas_autoconsumo_familiar_cult_1]") != '' && 
            $("input[name=toneladas_autoconsumo_productivo_cult_1]") != '' &&
            $("input[name=toneladas_comercializadora_cult_1]") != '' ){
            var suma =    parseFloat($("input[name=toneladas_autoconsumo_familiar_cult_1]").val()) 
                        + parseFloat($("input[name=toneladas_autoconsumo_productivo_cult_1]").val())
                        + parseFloat($("input[name=toneladas_comercializadora_cult_1]").val());


                        if(suma > $("input[name=toneladas_cosecho_anterior_1]").val()){
                            $('#msjToneladasMasLineaBase').show();
                            $('.button_jairo').attr('disabled',true);
                            // alert('son mas');
                        }else{
                            $('#msjToneladasMasLineaBase').hide();
                            $('.button_jairo').attr('disabled',false);
                        }
        }

    }else if(valor == 2){
        if($("input[name=toneladas_autoconsumo_familiar_cult_2]") != '' && 
            $("input[name=toneladas_autoconsumo_productivo_cult_2]") != '' &&
            $("input[name=toneladas_comercializadora_cult_2]") != '' ){
            var suma =    parseFloat($("input[name=toneladas_autoconsumo_familiar_cult_2]").val()) 
                        + parseFloat($("input[name=toneladas_autoconsumo_productivo_cult_2]").val())
                        + parseFloat($("input[name=toneladas_comercializadora_cult_2]").val());

                        if(suma > $("input[name=toneladas_cosecho_anterior_2]").val()){
                            $('#msjToneladasMasLineaBase').show();
                            $('.button_jairo').attr('disabled',true);
                            // alert('son mas');
                        }else{
                            $('#msjToneladasMasLineaBase').hide();
                            $('.button_jairo').attr('disabled',false);
                        }
        }

    }else if(valor == 3){
        if($("input[name=toneladas_autoconsumo_familiar_actual_cult_1]") != '' && 
            $("input[name=toneladas_autoconsumo_productivo_actual_cult_1]") != '' &&
            $("input[name=toneladas_comercializadora_actual_cult_1]") != '' ){
            var suma =    parseFloat($("input[name=toneladas_autoconsumo_familiar_actual_cult_1]").val()) 
                        + parseFloat($("input[name=toneladas_autoconsumo_productivo_actual_cult_1]").val())
                        + parseFloat($("input[name=toneladas_comercializadora_actual_cult_1]").val());
                        if(suma > $("input[name=toneladas_cosecho_actual_1]").val()){
                            $('#msjToneladasMas').show();
                            $('.button_jairo').attr('disabled',true);
                            // alert('son mas');
                        }else{
                            $('#msjToneladasMas').hide();
                            $('.button_jairo').attr('disabled',false);
                        }

        }
    }else if (valor == 4){
        if($("input[name=toneladas_autoconsumo_familiar_actual_cult_2]") != '' && 
            $("input[name=toneladas_autoconsumo_productivo_actual_cult_2]") != '' &&
            $("input[name=toneladas_comercializadora_actual_cult_2]") != '' ){
            var suma =    parseFloat($("input[name=toneladas_autoconsumo_familiar_actual_cult_2]").val()) 
                        + parseFloat($("input[name=toneladas_autoconsumo_productivo_actual_cult_2]").val())
                        + parseFloat($("input[name=toneladas_comercializadora_actual_cult_2]").val());

                        if(suma > $("input[name=toneladas_cosecho_actual_2]").val()){
                            $('#msjToneladasMas').show();
                            $('.button_jairo').attr('disabled',true);
                            // alert('son mas');
                        }else{
                            $('#msjToneladasMas').hide();
                            $('.button_jairo').attr('disabled',false);
                        }
        }
    }





}

// fin de caluclar toneladas de m<s
//liena base
$( $("input[name=toneladas_comercializadora_cult_1]") ).on( "keyup", function() {
calcularIngresosComercializacion();
} );
$( $("input[name=precio_venta_destino_prod_cult_1]") ).on( "keyup", function() {
calcularIngresosComercializacion();
} );
$( $("input[name=toneladas_comercializadora_cult_2]") ).on( "keyup", function() {
calcularIngresosComercializacion();
} );
$( $("input[name=precio_venta_destino_prod_cult_2]") ).on( "keyup", function() {
calcularIngresosComercializacion();
} );

//actual
$( $("input[name=toneladas_comercializadora_actual_cult_1]") ).on( "keyup", function() {
calcularIngresosComercializacion();
} );
$( $("input[name=precio_venta_destino_prod_actual_cult_1]") ).on( "keyup", function() {
calcularIngresosComercializacion();
} );

$( $("input[name=toneladas_comercializadora_actual_cult_2]") ).on( "keyup", function() {
calcularIngresosComercializacion();
} );
$( $("input[name=precio_venta_destino_prod_actual_cult_2]") ).on( "keyup", function() {
calcularIngresosComercializacion();
} );

function calcularIngresosComercializacion(){
    if($("input[name=toneladas_comercializadora_cult_1]").val() != '' && $("input[name=precio_venta_destino_prod_cult_1]").val() != ''){
        // console.log($("input[name=toneladas_comercializadora_actual_cult_1]").val() * $("input[name=precio_venta_destino_prod_actual_cult_1]").val());
        $('#ingresos_comercializacion_destino_prod_cult_1').empty();
        var ingresos = $("input[name=toneladas_comercializadora_cult_1]").val() * $("input[name=precio_venta_destino_prod_cult_1]").val();
    $("#ingresos_comercializacion_destino_prod_cult_1").append('<span class="font-weight-bold text-gray-800 h4">'+ingresos.toLocaleString ().substring(0, 8)+'</span>')
    }

    if($("input[name=toneladas_comercializadora_cult_2]").val() != '' && $("input[name=precio_venta_destino_prod_cult_2]").val() != ''){
        // console.log($("input[name=toneladas_comercializadora_actual_cult_1]").val() * $("input[name=precio_venta_destino_prod_actual_cult_1]").val());
        $('#ingresos_comercializacion_destino_prod_cult_2').empty();
        var ingresos = $("input[name=toneladas_comercializadora_cult_2]").val() * $("input[name=precio_venta_destino_prod_cult_2]").val();
    $("#ingresos_comercializacion_destino_prod_cult_2").append('<span class="font-weight-bold text-gray-800 h4">'+ingresos.toLocaleString().substring(0, 8)+'</span>')
    }

    if($("input[name=toneladas_comercializadora_actual_cult_1]").val() != '' && $("input[name=precio_venta_destino_prod_actual_cult_1]").val() != ''){
        // console.log($("input[name=toneladas_comercializadora_actual_cult_1]").val() * $("input[name=precio_venta_destino_prod_actual_cult_1]").val());
        $('#ingresos_comercializacion_destino_prod_actual_cult_1').empty();
        var ingresos = $("input[name=toneladas_comercializadora_actual_cult_1]").val() * $("input[name=precio_venta_destino_prod_actual_cult_1]").val();
    $("#ingresos_comercializacion_destino_prod_actual_cult_1").append('<span class="font-weight-bold text-gray-800 h4">'+ingresos.toLocaleString().substring(0, 8)+'</span>')
    }

    if($("input[name=toneladas_comercializadora_actual_cult_2]").val() != '' && $("input[name=precio_venta_destino_prod_actual_cult_2]").val() != ''){
        $('#ingresos_comercializacion_destino_prod_actual_cult_2').empty();
        var ingresos = $("input[name=toneladas_comercializadora_actual_cult_2]").val() * $("input[name=precio_venta_destino_prod_actual_cult_2]").val();
    $("#ingresos_comercializacion_destino_prod_actual_cult_2").append('<span class="font-weight-bold text-gray-800 h4">'+ingresos.toLocaleString().substring(0, 8)+'</span>')
    }

}

$('#superficie_cosecho').hide();
$('.button_jairo').attr('disabled',false);

$( $("input[name=hectarea_cosecho_anterior_1]") ).on( "keyup", function() {
    validarHectareas();
});
$( $("input[name=area_cosecho_anterior_1]") ).on( "keyup", function() {
    validarHectareas();
});
$( $("input[name=centiarea_cosecho_anterior_1]") ).on( "keyup", function() {
    validarHectareas();
});

$( $("input[name=hectarea_ciclo_anterior_1]") ).on( "keyup", function() {
    validarHectareas();
});

$( $("input[name=area_ciclo_anterior_1]") ).on( "keyup", function() {
    validarHectareas();
});

$( $("input[name=centiarea_ciclo_anterior_1]") ).on( "keyup", function() {
    validarHectareas();
});


function validarHectareas(){
    if($("input[name=hectarea_cosecho_anterior_1]").val() != '' && 
        $("input[name=area_cosecho_anterior_1]").val() != '' &&
        $("input[name=centiarea_cosecho_anterior_1]").val() != '' &&
        $("input[name=hectarea_ciclo_anterior_1]").val()!= ''&&
        $("input[name=area_ciclo_anterior_1]").val()!= ''&&
        $("input[name=centiarea_ciclo_anterior_1]").val()!= '' 
    ){
        if(parseInt($("input[name=hectarea_cosecho_anterior_1]").val()) < parseInt($("input[name=hectarea_ciclo_anterior_1]").val())){
            $('#superficie_cosecho').hide();
            $('.button_jairo').attr('disabled',false);
        }else{
            if(parseInt($("input[name=hectarea_cosecho_anterior_1]").val()) > parseInt($("input[name=hectarea_ciclo_anterior_1]").val())){
                // console.log('mayor la hectarea');
                $('#superficie_cosecho').show();
                $('.button_jairo').attr('disabled',true);
            }else{
                if(parseInt($("input[name=hectarea_cosecho_anterior_1]").val()) == parseInt($("input[name=hectarea_ciclo_anterior_1]").val())){
                    if(parseInt($("input[name=area_cosecho_anterior_1]").val()) > parseInt($("input[name=area_ciclo_anterior_1]").val())){
                        // console.log('mayor la area');
                        $('#superficie_cosecho').show();
                        $('.button_jairo').attr('disabled',true);
                    }else

                    if(parseInt($("input[name=centiarea_cosecho_anterior_1]").val()) > parseInt($("input[name=centiarea_ciclo_anterior_1]").val())){
                        // console.log('mayor la area');
                        $('#superficie_cosecho').show();
                        $('.button_jairo').attr('disabled',true);
                    }else{
                        $('#superficie_cosecho').hide();
                        $('.button_jairo').attr('disabled',false);
                    }
                }
            }
        }
    }
}

$('#superficie_cosecho2').hide();
$('.button_jairo').attr('disabled',false);

$( $("input[name=hectarea_cosecho_anterior_2]") ).on( "keyup", function() {
    validarHectarea2();
});
$( $("input[name=area_cosecho_anterior_2]") ).on( "keyup", function() {
    validarHectarea2();
});
$( $("input[name=centiarea_cosecho_anterior_2]") ).on( "keyup", function() {
    validarHectarea2();
});

$( $("input[name=hectarea_ciclo_anterior_2]") ).on( "keyup", function() {
    validarHectarea2();
});

$( $("input[name=area_ciclo_anterior_2]") ).on( "keyup", function() {
    validarHectarea2();
});

$( $("input[name=centiarea_ciclo_anterior_2]") ).on( "keyup", function() {
    validarHectarea2();
});

function validarHectarea2(){
    if($("input[name=hectarea_cosecho_anterior_2]").val() != '' && 
        $("input[name=area_cosecho_anterior_2]").val() != '' &&
        $("input[name=centiarea_cosecho_anterior_2]").val() != '' &&
        $("input[name=hectarea_ciclo_anterior_2]").val()!= ''&&
        $("input[name=area_ciclo_anterior_2]").val()!= ''&&
        $("input[name=centiarea_ciclo_anterior_2]").val()!= '' 
    ){
        if(parseInt($("input[name=hectarea_cosecho_anterior_2]").val()) < parseInt($("input[name=hectarea_ciclo_anterior_2]").val())){
            $('#superficie_cosecho2').hide();
            $('.button_jairo').attr('disabled',false);
        }else{
            if(parseInt($("input[name=hectarea_cosecho_anterior_2]").val()) > parseInt($("input[name=hectarea_ciclo_anterior_2]").val())){
                $('#superficie_cosecho2').show();
                $('.button_jairo').attr('disabled',true);
            }else{
                if(parseInt($("input[name=hectarea_cosecho_anterior_2]").val()) == parseInt($("input[name=hectarea_ciclo_anterior_2]").val())){
                    if(parseInt($("input[name=area_cosecho_anterior_2]").val()) > parseInt($("input[name=area_ciclo_anterior_2]").val())){
                        $('#superficie_cosecho2').show();
                        $('.button_jairo').attr('disabled',true);
                    }else

                    if(parseInt($("input[name=centiarea_cosecho_anterior_1]").val()) > parseInt($("input[name=centiarea_ciclo_anterior_1]").val())){
                        $('#superficie_cosecho2').show();
                        $('.button_jairo').attr('disabled',true);
                    }else{
                        $('#superficie_cosecho2').hide();
                        $('.button_jairo').attr('disabled',false);
                    }
                }
            }
        }
    }
    
}


$('#superficie_cosecho_actual_1').hide();
$('.button_jairo').attr('disabled',false);

$( $("input[name=hectarea_cosecho_actual_1]") ).on( "keyup", function() {
    validarHectarea3();
});
$( $("input[name=area_cosecho_actual_1]") ).on( "keyup", function() {
    validarHectarea3();
});
$( $("input[name=centiarea_cosecho_actual_1]") ).on( "keyup", function() {
    validarHectarea3();
});

$( $("input[name=hectarea_actual_cultivo_1]") ).on( "keyup", function() {
    validarHectarea3();
});

$( $("input[name=area_actual_cultivo_1]") ).on( "keyup", function() {
    validarHectarea3();
});

$( $("input[name=centiarea_actual_cultivo_1]") ).on( "keyup", function() {
    validarHectarea3();
});

function validarHectarea3(){
    if($("input[name=hectarea_cosecho_actual_1]").val() != '' && 
        $("input[name=area_cosecho_actual_1]").val() != '' &&
        $("input[name=centiarea_cosecho_actual_1]").val() != '' &&
        $("input[name=hectarea_actual_cultivo_1]").val()!= ''&&
        $("input[name=area_actual_cultivo_1]").val()!= ''&&
        $("input[name=centiarea_actual_cultivo_1]").val()!= '' 
    ){
        if(parseInt($("input[name=hectarea_cosecho_actual_1]").val()) < parseInt($("input[name=hectarea_actual_cultivo_1]").val())){
            $('#superficie_cosecho_actual_1').hide();
            $('.button_jairo').attr('disabled',false);
        }else{
            if(parseInt($("input[name=hectarea_cosecho_actual_1]").val()) > parseInt($("input[name=hectarea_actual_cultivo_1]").val())){
                $('#superficie_cosecho_actual_1').show();
                $('.button_jairo').attr('disabled',true);
            }else{
                if(parseInt($("input[name=hectarea_cosecho_actual_1]").val()) == parseInt($("input[name=hectarea_actual_cultivo_1]").val())){
                    if(parseInt($("input[name=area_cosecho_actual_1]").val()) > parseInt($("input[name=area_actual_cultivo_1]").val())){
                        $('#superficie_cosecho_actual_1').show();
                        $('.button_jairo').attr('disabled',true);
                    }else

                    if(parseInt($("input[name=centiarea_cosecho_actual_1]").val()) > parseInt($("input[name=centiarea_actual_cultivo_1]").val())){
                        $('#superficie_cosecho_actual_1').show();
                        $('.button_jairo').attr('disabled',true);
                    }else{
                        $('#superficie_cosecho_actual_1').hide();
                        $('.button_jairo').attr('disabled',false);
                    }
                }
            }
        }
    }
    
}

$('#superficie_cosecho_actual_2').hide();
$('.button_jairo').attr('disabled',false);

$( $("input[name=hectarea_cosecho_actual_2]") ).on( "keyup", function() {
    validarHectarea4();
});
$( $("input[name=area_cosecho_actual_2]") ).on( "keyup", function() {
    validarHectarea4();
});
$( $("input[name=centiarea_cosecho_actual_2]") ).on( "keyup", function() {
    validarHectarea4();
});

$( $("input[name=hectarea_actual_cultivo_2]") ).on( "keyup", function() {
    validarHectarea4();
});

$( $("input[name=area_actual_cultivo_2]") ).on( "keyup", function() {
    validarHectarea4();
});

$( $("input[name=centiarea_actual_cultivo_2]") ).on( "keyup", function() {
    validarHectarea4();
});

function validarHectarea4(){
    if($("input[name=hectarea_cosecho_actual_2]").val() != '' && 
        $("input[name=area_cosecho_actual_2]").val() != '' &&
        $("input[name=centiarea_cosecho_actual_2]").val() != '' &&
        $("input[name=hectarea_actual_cultivo_2]").val()!= ''&&
        $("input[name=area_actual_cultivo_2]").val()!= ''&&
        $("input[name=centiarea_actual_cultivo_2]").val()!= '' 
    ){
        if(parseInt($("input[name=hectarea_cosecho_actual_2]").val()) < parseInt($("input[name=hectarea_actual_cultivo_2]").val())){
            $('#superficie_cosecho_actual_2').hide();
            $('.button_jairo').attr('disabled',false);
        }else{
            if(parseInt($("input[name=hectarea_cosecho_actual_2]").val()) > parseInt($("input[name=hectarea_actual_cultivo_2]").val())){
                $('#superficie_cosecho_actual_2').hide();
                $('.button_jairo').attr('disabled',true);
            }else{
                if(parseInt($("input[name=hectarea_cosecho_actual_2]").val()) == parseInt($("input[name=hectarea_actual_cultivo_2]").val())){
                    if(parseInt($("input[name=area_cosecho_actual_2]").val()) > parseInt($("input[name=area_actual_cultivo_2]").val())){
                        $('#superficie_cosecho_actual_2').hide();
                        $('.button_jairo').attr('disabled',true);
                    }else

                    if(parseInt($("input[name=centiarea_cosecho_actual_2]").val()) > parseInt($("input[name=centiarea_actual_cultivo_2]").val())){
                        $('#superficie_cosecho_actual_2').hide();
                        $('.button_jairo').attr('disabled',true);
                    }else{
                        $('#superficie_cosecho_actual_2').hide();
                        $('.button_jairo').attr('disabled',false);
                    }
    o
                }
            }
        }
    }
    
}



$('#SembrarmasdelaUP').hide();

$( $("input[name=hectarea_sup_up]") ).on( "keyup", function() {
    validarHasUP();
});

$( $("input[name=area_sup_up]") ).on( "keyup", function() {
    validarHasUP();
});

$( $("input[name=centiarea_sup_up]") ).on( "keyup", function() {
    validarHasUP();
});

///linea actual

$( $("input[name=hectarea_actual_cultivo_1]") ).on( "keyup", function() {
    validarHasUP();
});

$( $("input[name=area_actual_cultivo_1]") ).on( "keyup", function() {

    validarHasUP();
});

$( $("input[name=centiarea_actual_cultivo_1]") ).on( "keyup", function() {
    validarHasUP();
});

$( $("input[name=hectarea_actual_cultivo_2]") ).on( "keyup", function() {
    validarHasUP();
});

$( $("input[name=area_actual_cultivo_2]") ).on( "keyup", function() {
    validarHasUP();
});

$( $("input[name=centiarea_actual_cultivo_2]") ).on( "keyup", function() {
    validarHasUP();
});


function validarHasUP(){
   if(  $("input[name=hectarea_sup_up]").val() != '' &&
         $("input[name=area_sup_up]").val() != '' &&
         $("input[name=centiarea_sup_up]").val() != '' &&
         $("input[name=hectarea_actual_cultivo_1]").val() != '' &&
         $("input[name=area_actual_cultivo_1]").val() != '' &&
         $("input[name=centiarea_actual_cultivo_1]").val() != ''
   ){
    var sumaHectareasActuales = $("input[name=hectarea_actual_cultivo_2]").val() != '' ? 
                                parseInt($("input[name=hectarea_actual_cultivo_1]").val()) + parseInt($("input[name=hectarea_actual_cultivo_2]").val()) : 
                                parseInt($("input[name=hectarea_actual_cultivo_1]").val());


    if(parseInt($("input[name=hectarea_sup_up]").val()) < sumaHectareasActuales){
        $('#SembrarmasdelaUP').show();
        $('.button_jairo').attr('disabled',true);

    }
    
    else if(parseInt($("input[name=hectarea_sup_up]").val()) >= sumaHectareasActuales){
        $('#SembrarmasdelaUP').hide();
        $('.button_jairo').attr('disabled',false);


        var sumaAreasActuales = $("input[name=area_actual_cultivo_2]").val() != '' ? 
        parseInt($("input[name=area_actual_cultivo_1]").val()) + parseInt($("input[name=area_actual_cultivo_2]").val()) : 
        parseInt($("input[name=area_actual_cultivo_1]").val());

        if(parseInt($("input[name=area_sup_up]").val()) >= sumaAreasActuales){
            
            $('#SembrarmasdelaUP').hide();
            $('.button_jairo').attr('disabled',false);

            var sumaCentiareasAreasActuales = $("input[name=centiarea_actual_cultivo_2]").val() != '' ? 
            parseInt($("input[name=centiarea_actual_cultivo_1]").val()) + parseInt($("input[name=centiarea_actual_cultivo_2]").val()) : 
            parseInt($("input[name=centiarea_actual_cultivo_1]").val());

                if(parseInt($("input[name=centiarea_sup_up]").val()) >= sumaCentiareasAreasActuales){
                    $('#SembrarmasdelaUP').hide();
                    $('.button_jairo').attr('disabled',false);
                }else{
                    $('#SembrarmasdelaUP').show();
                     $('.button_jairo').attr('disabled',true);

                }
        }else{
            $('#SembrarmasdelaUP').show();
            $('.button_jairo').attr('disabled',true);

        }

    }
    

   }
}

    ////linea base
    
    $('#SembrarmasdelaUPLineaBAse').hide();

    $( $("input[name=hectarea_ciclo_anterior_1]") ).on( "keyup", function() {
        validarHasUPConLineaBase();
    });

    $( $("input[name=area_ciclo_anterior_1]") ).on( "keyup", function() {
        validarHasUPConLineaBase();
    });
    
    $( $("input[name=centiarea_ciclo_anterior_1]") ).on( "keyup", function() {
        validarHasUPConLineaBase();
    });
    
    $( $("input[name=hectarea_ciclo_anterior_2]") ).on( "keyup", function() {
        validarHasUPConLineaBase();
    });
    
    $( $("input[name=area_ciclo_anterior_2]") ).on( "keyup", function() {
        validarHasUPConLineaBase();
    });
    
    $( $("input[name=centiarea_ciclo_anterior_2]") ).on( "keyup", function() {
        validarHasUPConLineaBase();
    });

function validarHasUPConLineaBase(){
    // console.log('linea base');
   if(  $("input[name=hectarea_sup_up]").val() != '' &&
         $("input[name=area_sup_up]").val() != '' &&
         $("input[name=centiarea_sup_up]").val() != '' &&
         $("input[name=hectarea_ciclo_anterior_1]").val() != '' &&
         $("input[name=area_ciclo_anterior_1]").val() != '' &&
         $("input[name=centiarea_ciclo_anterior_1]").val() != ''
   ){
    var sumaHectareasAct = $("input[name=hectarea_ciclo_anterior_2]").val() != '' ? 
                                parseInt($("input[name=hectarea_ciclo_anterior_1]").val()) + parseInt($("input[name=hectarea_ciclo_anterior_2]").val()) : 
                                parseInt($("input[name=hectarea_ciclo_anterior_1]").val());


    if(parseInt($("input[name=hectarea_sup_up]").val()) < sumaHectareasAct){
        $('#SembrarmasdelaUPLineaBAse').show();
        $('.button_jairo').attr('disabled',true);
    }
    else if(parseInt($("input[name=hectarea_sup_up]").val()) > sumaHectareasAct){

        $('#SembrarmasdelaUPLineaBAse').hide();
        $('.button_jairo').attr('disabled',false);
    }else if(parseInt($("input[name=hectarea_sup_up]").val()) == sumaHectareasAct){
        // console.log('hect igual');

        var sumaAreasActua = $("input[name=area_ciclo_anterior_2]").val() != '' ? 
        parseInt($("input[name=area_ciclo_anterior_1]").val()) + parseInt($("input[name=area_ciclo_anterior_2]").val()) : 
        parseInt($("input[name=area_ciclo_anterior_1]").val());

        if(parseInt($("input[name=area_sup_up]").val()) >= sumaAreasActua){

            $('#SembrarmasdelaUPLineaBAse').hide();
            $('.button_jairo').attr('disabled',false);

            var sumaCentiareasAreasActuales = $("input[name=centiarea_ciclo_anterior_2]").val() != '' ? 
            parseInt($("input[name=centiarea_ciclo_anterior_1]").val()) + parseInt($("input[name=centiarea_ciclo_anterior_2]").val()) : 
            parseInt($("input[name=centiarea_ciclo_anterior_1]").val());

                if(parseInt($("input[name=centiarea_sup_up]").val()) >= sumaCentiareasAreasActuales){
                    $('#SembrarmasdelaUPLineaBAse').hide();
                    $('.button_jairo').attr('disabled',false);
                }else{
                    $('#SembrarmasdelaUPLineaBAse').show();
                    $('.button_jairo').attr('disabled',true);
                }
        }else{
            $('#SembrarmasdelaUPLineaBAse').show();
            $('.button_jairo').attr('disabled',true);
        }

    }
    

   }
}

$('.colSoloPuntaRiego').hide();

$("#regimenHidricoActualCultivo1, #regimenHidricoActualCultivo2").change(function () {
    if(($("#regimenHidricoActualCultivo1").val() != '' || $("#regimenHidricoActualCultivo2").val())){
        if($("#regimenHidricoActualCultivo1").val() == 3 || $("#regimenHidricoActualCultivo1").val() == 4 ||  $("#regimenHidricoActualCultivo2").val() == 3 || $("#regimenHidricoActualCultivo2").val() == 4 ){
            $('.colSoloPuntaRiego').show();
        }else{
            $('.colSoloPuntaRiego').hide();
        }
    }else{
        $('.colSoloPuntaRiego').hide();
    }

});


