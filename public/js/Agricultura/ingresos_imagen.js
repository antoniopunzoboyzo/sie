function validaEnteros(){
    $('.entero').each(function() {
        if($(this).val()<0)
            $(this).val("");
    });
}

function validaProduccion(){
    pv1=$("#ingresos_comercializacion_destino_prod_actual_cult_1").children().html();
    pv2=$("#ingresos_comercializacion_destino_prod_actual_cult_2").children().html();
    pv1=parseInt(pv1);
    pv1=pv1||0;
   
    pv2=parseInt(pv2);
    pv2=pv2||0;
    suma=pv1+pv2;
    if($("#ingup").val()<suma){
        alert("No puedes ser menor el valor reportado a la suma de la produccion");
        $("#ingup").val("");
    }
}

$("input[name='flexRadioDefault']").on("change", function () {
        var conocejefe = $(this).val();

    if (conocejefe == 1) {
        $('.ingresos').show("slow");
        $('.ingresosCampos').each(function() {
            $(this).attr('required', true);
        });

      }else{
        $('.ingresos').hide();
        $('.ingresosCampos').each(function() {
            $(this).val("");
            
            $(this).attr('required', false);
        });
        $("#total").val(0);
    }
});

$("input[name='produccion']").on("change", function () {
    var conocejefe = $(this).val();
    if (conocejefe == 1) {
        $('.integrantes').show("slow");
         $('.apoyos').each(function() {
                $(this).attr('required', true);
        });
      }else{
        $('.integrantes').hide();
         $('.apoyos').each(function() {
                $(this).attr('required', false);
                $(this).val("");
        });
    }
});

function totali(){
    suma=0;
    $('.ingresosCampos').each(function() {
        num=$(this).val();
        num=num||0;
        num=parseInt(num);
        suma=num+suma;
    });
    $("#total").val(suma);
}

function guardarImagenes(){
    productor=$("#productor").attr('src');
    ine=$("#ine").attr('src');
    console.log(productor);
    if(ine!==undefined && productor!==undefined){
        $.ajax({
            url: 'guardarimagen',
            type: 'POST',
            data: jQuery.param({ ineimg: ine, productorimg : productor}) ,
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            success: function (response) {
                window.open("generaPDF/"+$(".cve_encuesta").val(), "_blank", "top=25, left=250, toolbar=no, width=1000, height=600");
                refrescar();
            },
            error: function () {
                alert("error");
            }
        });
    }else{
        alert("No existe imagen para guardar")
    } 
}

function cerrarsesion(){
    $.ajax({
        url: 'cerrarsesion',
        type: 'POST',
        data: jQuery.param({ sesion:""}) ,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        success: function (response) {
            refrescar();
        },
        error: function () {
            alert("error");
        }
    });
}