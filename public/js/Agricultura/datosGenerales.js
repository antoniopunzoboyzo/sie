$(document).ready(function () {
  $(".agricultor").hide();
  $(".no_contesta").hide();
  $(".pregunta").hide();
  $(".beneficio_boton").hide();
  $('.noporque').hide();
  $("#ocultar_dialecto").hide();
  
}); 
$("#cve_municipio_unidad").change(function () {
//$(document).on('change', '#cve_municipio_unidad', function(){
  //obtener_localidades(this.value);  
  $(".agricultor").hide();
  $(".pregunta").hide();
  $("input[name='si'][value='0']").prop("checked",true);
  $("input[name=cve_localidad_unidad]").attr('required',true);
  $("input[name=productor_contesta]").attr('required',true);
  $("input[name=es_ejidatorio]").attr('required',true);
  $("input[name=dialecto]").attr('required',true);
  $("#folio_estatal").val("");
  $("#curp").val("");
  var formData = new FormData();
  formData.append("cve_encuestador", $("#cve_encuestador").val());
  formData.append("cve_municipio_unidad", $("#cve_municipio_unidad").val());
  $.ajax({
    url: "obtieneAgricultores",
    type: "POST",
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    success: function (datos) {
      //console.log(datos);
      let option = "";
      $.each(datos, function( index, value ) {
        option += '<option value="'+value.curp+'">'+value.nombre+" "+value.paterno+" "+value.materno+'</option>'; 
      });
    $("#nombre").html(option);
    $("#nombre").selectpicker('refresh');
    $("#nombre").attr("disabled", false);
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log(jqXHR);
      console.log(textStatus);
      console.log(errorThrown);
    }
  });
});

  /**Muestra si cambia la encuesta */
  $("input[name='si']").on("change", function () {
      var valor = $(this).val();
      //console.log(valor);
      if(valor == 1){
        $('.agricultor').show("slow");
        }else{
          $('.agricultor').hide();
        }
  });


  /**Muestra si es ejidatario */
//   $(document).on('change', '#es_ejidatorio', function () {
//     var valor = $(this).val();
//     //console.log(valor);
//     if(valor == 1){
//       $('.ejidatario').show("slow");
//       }else{
//         $('.ejidatario').hide();
//       }
// });

/**Muestra si es propietario y contesta la encuesta*/
$(document).on('change', '#productor_contesta', function () {
  var valor = $(this).val();
  //console.log(valor);
  if(valor == 0){
    $('.no_contesta').show("slow");
    $("select[name=cve_quien_contesta]").attr('required',true);
    $("input[name=nombre_quien_contesta]").attr('required',true);
    }else{
      $('.no_contesta').hide();
      $("select[name=cve_quien_contesta]").attr('required',false);
      $("input[name=nombre_quien_contesta]").attr('required',false);
    }
});

$("select[name=dialecto]").change(function () {
//$(document).on('change', '#dialecto', function () {
  var valor = $(this).val();
  //console.log(valor);
  if(valor == 0){
    $("#ocultar_dialecto").hide();
    $("input[name=dialecto_hablado]").attr('required',false)
  }else{
      $("#ocultar_dialecto").show()
      $("input[name=dialecto_hablado]").attr('required',true);
    }
});
  
/**Encuentra al agricultor */
//$("#curp").on("change", 
$("select[name=nombre]").change(function () {
//$(document).on('change', '#nombre', function () {
  var cvNuevo = $("#nombre").val();
  //console.log(cvNuevo);
  //var cve_mpio_encuestador = $("#cve_mpio_encuestador").val();
  //console.log(cve_mpio_encuestador);
  $("div_alert_curp").html("");
  
  if (cvNuevo.length == 18) {
    $(".pregunta").show();
        var formData = new FormData();
        formData.append("curp", $("#nombre").val());
        formData.append("cve_municipio_unidad", $("#cve_municipio_unidad").val());
        $.ajax({
            url: "obtieneAgricultor",
            type: "POST",
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (datos) {
              // console.log(datos);
                if (datos != '') {
                  //let dialecto = "";
                  //let leer_escribir = "";
                  let option = "";
                  let seleccion1 = "", seleccion0= "";
                  //let beneficiario_sagarpa = "";
                  $.each(datos, function( index, value ) {
                    satisfacionNobeneficiario(value.beneficiario);
                    if(value.cve_estatus == 1){ 
                      $("input[name='si'][value='1']").prop("checked",true);
                      $(".agricultor").show();
                    }else {
                      $("input[name='si'][value='0']").prop("checked",true);
                      $(".agricultor").hide();
                      //$(".pregunta").hide();
                      $("div_alert_curp").html("El agricultor no contesto la encuesta");
                    }
                    $("#curp").val(value.curp);
                    $(".cve_encuesta").val(value.cve_encuesta);
                    $("#folio_estatal").val(value.folio_estatal);
                    $("#folio_estatal").attr("disabled", true);
                    $("#celular").val(value.telefono);
                    $("#ejido").val(value.ejido);
                    $("#latitud").val(value.latitud);
                    $("#longitud").val(value.longitud);
                    $("#anio_productor").val(value.anio_productor);
                    $("#dialecto_hablado").val(value.dialecto_hablado);
                    $("#anio_estudio").val(value.anio_estudio);
                    //$("#cve_encuesta").val(value.cve_encuesta);
                    //dialecto = value.dialecto;
                    //leer_escribir = value.leer_escribir;
                    //productor_contesta = value.productor_contesta;
                    if(value.beneficiario == 1){
                      $("#beneficiario_sagarpa").html("¡Es beneficiario!");
                    }else{
                      $("#beneficiario_sagarpa").html("¡No es beneficiario!");  
                    }
                    $(".beneficio_boton").show();
                    /**productor_contesta */
                    option = "<option>Seleccione un opción</option>";
                    if(value.productor_contesta == "1"){
                      $('.no_contesta').hide("slow");
                      seleccion1 =  " selected ";
                      seleccion0 = "";
                    }else if(value.productor_contesta == "0"){
                      $('.no_contesta').show("slow");
                      seleccion0 =  " selected ";
                      seleccion1 = "";
                      obtener_quien_contesta(value.cve_quien_contesta)
                      $("#nombre_quien_contesta").val(value.nombre_quien_contesta);
                      $("#nombre_quien_contesta").attr("disabled", false);
                    }
                    option += '<option '+ seleccion0+'value="0">NO</option>';
                    option += '<option '+seleccion1+'value="1">SI</option>';
                    $("#productor_contesta").html(option);
                    $("#productor_contesta").attr("disabled", false);
                    /** */
                    obtener_localidades(value.cve_municipio_unidad, value.cve_localidad);
                    /** */
                    option = "<option>Seleccione un opción</option>";
                    seleccion = value.es_ejidatorio == "1" ? " selected ": "";
                    seleccion0 = value.es_ejidatorio == "0" ? " selected ": "";
                    option += '<option '+ seleccion0+'value="0">NO</option>';
                    option += '<option '+seleccion1+'value="1">SI</option>';
                    //option = "<option>Seleccione un opción</option>";
                    $("#es_ejidatorio").html(option);
                    obtener_ejidos(value.cve_municipio_unidad, value.ejido);
                    /**Dialecto */
                    option = "<option>Seleccione un opción</option>";
                    seleccion1 = value.dialecto == "1" ? " selected ": "";
                    seleccion0 = value.dialecto == "0" ? " selected ": "";
                    option += '<option '+ seleccion0+'value="0">NO</option>';
                    option += '<option '+seleccion1+'value="1">SI</option>';
                    $("#dialecto").html(option);
                    $("#dialecto").attr("disabled", false);
                    /** */
                    option = "<option>Seleccione un opción</option>";
                    seleccion = value.leer_escribir == "1" ? " selected ": "";
                    seleccion0 = value.leer_escribir == "0" ? " selected ": "";
                    option += '<option '+ seleccion0+'value="0">NO</option>';
                    option += '<option '+seleccion1+'value="1">SI</option>';
                    //option = "<option>Seleccione un opción</option>";
                    $("#leer_escribir").html(option);
                    $("#leer_escribir").attr("disabled", false);
                    /** */
                    /** */
                    option = "<option>Seleccione un opción</option>";
                    seleccion1 = value.beneficiario_bienestar == "1" ? " selected ": "";
                    seleccion0 = value.beneficiario_bienestar == "0" ? " selected ": "";
                    option += '<option '+ seleccion0+'value="0">NO</option>';
                    option += '<option '+seleccion1+'value="1">SI</option>';
                    $("#beneficiario_bienestar").html(option);
                    $("#beneficiario_bienestar").attr("disabled", false);
                  });
                }//else{
                //   $("#div_alert_curp").html("La CURP del agricultor ingresada es incorrecto.");
                //   $("#div_alert_curp").removeClass("d-none");
                // }


            },
        });
      
   } else if (cvNuevo.length  < 18) {
    $("#div_alert_curp").html("Seleccione un nombre de agricultor para continuar.");
    $("#div_alert_curp").removeClass("d-none");

   }
  });

  

function obtener_municipios(cve_municipio){
  //console.log(cve_municipio);
  if(cve_municipio != "" ){
    var formData = new FormData();
    //formData.append("cve_entidad", 16);
    formData.append("cve_municipio", cve_municipio);
      $.ajax({
        url: "getMunicipios",
        type: "POST",
        data: formData,
        dataType: "json",
        processData:false,
        contentType: false,
        success: function(datos){
          //console.log(datos);
          let option = "Seleccione un Municipio";//'<option value="">Seleccione Municipio</option>';
          $.each(datos, function( index, value ) {
              option += '<option value="'+value.cve_mpioINEGI+'">'+value.municipio+'</option>';
          });
          $("#cve_municipio_unidad").html(option);
          $("#cve_municipio_unidad").attr("disabled", false);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(jqXHR);
          console.log(textStatus);
          console.log(errorThrown);
      }
      });
  }
}

function obtener_localidades(cve_municipio, localidad){
  //console.log(cve_municipio);
  if(cve_municipio != "" ){
    var formData = new FormData();
    formData.append("cve_municipio_unidad", cve_municipio);
      $.ajax({
        url: "getLocalidades",
        type: "POST",
        data: formData,
        dataType: "json",
        processData:false,
        contentType: false,
        success: function(datos){
          //console.log(datos);
          //console.log(localidad);
          let seleccion= "";
          let option = "";//"<option>Seleccione un opción</option>";
          //console.log(localidad);
          $.each(datos, function( index, value ) {
            //console.log(parseInt(value.cve_localidad));
            seleccion = localidad == parseInt(value.cve_localidad) ? " selected " : "";
            //console.log(parseInt(value.cve_localidad));
            option+= '<option '+seleccion+' value="'+value.cve_localidad+'">'+value.localidad+'</option>';
          });
          //$("#cve_localidad_unidad").empty();
          $("#cve_localidad_unidad").html(option);
          $("#cve_localidad_unidad").selectpicker('refresh');
          $("#cve_localidad_unidad").attr("disabled", false);
          
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(jqXHR);
          console.log(textStatus);
          console.log(errorThrown);
      }
      });
  }
}

function obtener_ejidos(cve_municipio, ejido){
  //console.log(cve_municipio);
  if(cve_municipio != "" ){
    var formData = new FormData();
    formData.append("cve_municipio", cve_municipio);
      $.ajax({
        url: "getEjidos",
        type: "POST",
        data: formData,
        dataType: "json",
        processData:false,
        contentType: false,
        success: function(datos){
          //console.log(datos);
          let seleccion= "";
          let option = '<option value="">Seleccione un ejido</option>';
          $.each(datos, function( index, value ) {
            seleccion = ejido == value.cve_ejido ? " selected ": "";
            option+= '<option '+seleccion+' value="'+value.cve_ejido+'">'+value.ejido+'</option>';
          });
          $("#ejido").html(option);
          $("#ejido").selectpicker('refresh');
          $("#ejido").attr("disabled", false);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(jqXHR);
          console.log(textStatus);
          console.log(errorThrown);
      }
      });
  }
}


function solonumeros(e) {
  key = e.keyCode || e.which;
  teclado = String.fromCharCode(key);
  numeros = "0123456789";
  especiales = "8-37-38-46";
  tecladoEspecial = false;
  for (var i in especiales) {
      if (key == especiales[i]) {
          tecladoEspecial = true;
      }
  }
  if (numeros.indexOf(teclado) == -1 && !tecladoEspecial) {
      return false;
  }

}

function obtener_quien_contesta(cve_quien_contesta){
  //console.log(cve_municipio);
  if(cve_quien_contesta != "" ){
    var formData = new FormData();
    formData.append("cve_quien_contesta", cve_quien_contesta);
      $.ajax({
        url: "getQuienContesta",
        type: "POST",
        data: formData,
        dataType: "json",
        processData:false,
        contentType: false,
        success: function(datos){
          //console.log(datos);
          let seleccion= "";
          let option = '<option value="">Seleccione una opción</option>';
          $.each(datos, function( index, value ) {
            seleccion = cve_quien_contesta == value.cve_quien_contesta ? " selected ": "";
            option+= '<option '+seleccion+' value="'+value.cve_quien_contesta+'">'+value.quien_contesta+'</option>';
          });
          $("#cve_quien_contesta").html(option);
          $("#cve_quien_contesta").selectpicker('refresh');
          $("#cve_quien_contesta").attr("disabled", false);
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(jqXHR);
          console.log(textStatus);
          console.log(errorThrown);
      }
      });
  }
}