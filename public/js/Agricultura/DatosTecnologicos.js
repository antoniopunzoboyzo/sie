$('.resiembra').hide();

$("#resiembraSI, #resiembraNo").change(function () {
    if ($("#resiembraSI").is(":checked")) {
        $('.resiembra').show("slow");
        $("input[name=fecha_resiembra]").attr('required',true);
        $("input[name=motivo_resiembra]").attr('required',true);
        $("select[name=motivo_resiembra]").attr('required',true);
        validarFechasResiembra();

    } else 
    if ($("#resiembraNo").is(":checked")) {
        $('.resiembra').hide();
        $("input[name=fecha_resiembra]").attr('required',false);
        $("input[name=fecha_resiembra]").val('');
        $("select[name=motivo_resiembra]").attr('required',false);
        $("select[name=motivo_resiembra]").val('');

        $("#msjfechaResiembra").hide();
        $('.buttonTecnologico').attr('disabled',false);
        validarFechasResiembra();


    }
});

$("#msjfechaResiembra").hide();
$("input[name=fecha_resiembra], input[name=fecha_siembra]").change(function (){
    validarFechasResiembra();

});

function validarFechasResiembra(){
    if($("input[name=fecha_resiembra]").val() != '' && $("input[name=fecha_siembra]").val() != ''){
        if($("input[name=fecha_resiembra]").val() <  $("input[name=fecha_siembra]").val()){
            $("#msjfechaResiembra").show();
            $('.buttonTecnologico').attr('disabled',true);
        }else{
            $("#msjfechaResiembra").hide();
            $('.buttonTecnologico').attr('disabled',false);
        }
    }
}

$('.PorqueNoAplicoFertilizante').hide();

$("#SiAplicoElFertilizante, #NoAplicoElFertilizante").change(function () {
    if ($("#SiAplicoElFertilizante").is(":checked")) {
        $('.PorqueNoAplicoFertilizante').hide();

        $("input[name=porque_aplico_ferti]").attr('required',false);
    } else 
    if ($("#NoAplicoElFertilizante").is(":checked")) {
        $('.PorqueNoAplicoFertilizante').show("slow");

        $("input[name=porque_aplico_ferti]").attr('required',true);
    }
});


$('.colFertilizanteCompleto').hide();

$("#fertilizanteCompletoSI, #fertilizanteCompletoNo ,#fertilizanteCompletoNoSabe").change(function () {
    if ($("#fertilizanteCompletoSI").is(":checked")) {
        $('.colFertilizanteCompleto').hide("slow");

        $("input[name=formato_porque]").attr('required',false);
        $("input[name=reporte_hecho]").attr('required',false);

    } else 
    if ($("#fertilizanteCompletoNo").is(":checked")) {
        $('.colFertilizanteCompleto').show();

        $("input[name=formato_porque]").attr('required',true);
        $("input[name=reporte_hecho]").attr('required',true);
    } else 
    if ($("#fertilizanteCompletoNoSabe").is(":checked")) {
        $('.colFertilizanteCompleto').hide();

        $("input[name=formato_porque]").attr('required',false);
        $("input[name=reporte_hecho]").attr('required',false);
    }
});

$('#cualLaborCol').hide();

$("#laborRealizo").change(function (){
    abrirOtroLabor();
});

function abrirOtroLabor(){
    var valorOtroLabor = $('#laborRealizo').val();
    if(valorOtroLabor.includes('5')){
        $('#cualLaborCol').show("slow");
        $('#cualLabor').attr('required', true);
    } else {
        $('#cualLaborCol').hide();
        $('#cualLabor').attr('required', false);
    }
    //
}



$('#cualHerbicidacol').hide();

    $("#siHerbicida, #NoHerbicida").change(function () {
        if ($("#siHerbicida").is(":checked")) {
            $('#cualHerbicidacol').show("slow");
            $('#cualHerbicida').attr('required', true)
        } else 
        if ($("#NoHerbicida").is(":checked")) {
            $('#cualHerbicidacol').hide();
            $('#cualHerbicida').attr('required', false);
        }
    });

$('.col53').hide();

    $("#sirecibiofertilizante, #norecibiofertilizante").change(function () {
        FuncionaCOl57();
        if ($("#sirecibiofertilizante").is(":checked")) {
            $('.col53').show("slow");

            $("input[name=ferti_kg_dap]").attr('required',true);
            $("input[name=ferti_kg_urea]").attr('required',true);
            $("input[name=formato_unico_entrega]").attr('required',true);
            $("input[name=fecha_recibio_fertilizante]").attr('required',true);
            $("input[name=oportuno_siembra]").attr('required',true);
            $("input[name=aplico_fertilizante]").attr('required',true);
        } else 
        if ($("#norecibiofertilizante").is(":checked")) {
            $('.col53').hide();

            $("input[name=ferti_kg_dap]").attr('required',false);
            $("input[name=ferti_kg_urea]").attr('required',false);
            $("input[name=formato_unico_entrega]").attr('required',false);
            $("input[name=fecha_recibio_fertilizante]").attr('required',false);
            $("input[name=oportuno_siembra]").attr('required',false);
            $("input[name=aplico_fertilizante]").attr('required',false);
        }
    });

$('.col57porque').hide();

    $("#siCultivoAplicoFertilizante, #noCultivoAplicoFertilizante").change(function () {
        FuncionaCOl57();
    });

    $('.col57').hide();

    function FuncionaCOl57(){
        if ($("#siCultivoAplicoFertilizante").is(":checked")) {
            $('.col57porque').hide();
            $('.col57').show("slow");

            $("input[name=razon_aplicacion_fertilizante]").attr('required',false);

            $("input[name=hectareas_fertilizante1]").attr('required',true);
            $("input[name=areas_fertilizante1]").attr('required',true);
            $("input[name=centiareas_fertilizante1]").attr('required',true);
            $("input[name=fecha_aplicacion1]").attr('required',true);
            $("input[name=dosis1]").attr('required',true);
            $("input[name=urea_fecha1]").attr('required',true);
            $("input[name=dosis_urea1]").attr('required',true);

            $("input[name=fertilizante_aplicado]").attr('required',true);

            $("input[name=cve_aplicacion_fertilizante]").attr('required',true);
            $("select[name=cve_aplicacion]").attr('required',true);
            $("input[name=fertilizante_complementario_up]").attr('required',true);
            $("input[name=fertilizante_complementario_programa]").attr('required',true);
            $("select[name=cve_quimico]").attr('required',true);
            $("input[name=dosis_quimico]").attr('required',true);


        } else 
        if ($("#noCultivoAplicoFertilizante").is(":checked")) {
            $('.col57porque').show("slow");
            $('.col57').hide();

            $("input[name=razon_aplicacion_fertilizante]").attr('required',true);

            $("input[name=hectareas_fertilizante1]").attr('required',false);
            $("input[name=areas_fertilizante1]").attr('required',false);
            $("input[name=centiareas_fertilizante1]").attr('required',false);
            $("input[name=fecha_aplicacion1]").attr('required',false);
            $("input[name=dosis1]").attr('required',false);
            $("input[name=urea_fecha1]").attr('required',false);
            $("input[name=dosis_urea1]").attr('required',false);

            $("input[name=hectareas_fertilizante2]").attr('required',false);
            $("input[name=areas_fertilizante2]").attr('required',false);
            $("input[name=centiareas_fertilizante2]").attr('required',false);
            $("input[name=fecha_aplicacion2]").attr('required',false);
            $("input[name=dosis2]").attr('required',false);
            $("input[name=urea_fecha2]").attr('required',false);
            $("input[name=dosis_urea2]").attr('required',false);

            $("input[name=fertilizante_aplicado]").attr('required',false);
            $("select[name=cve_aplicacion]").attr('required',false);
            $("input[name=fertilizante_complementario_up]").attr('required',false);
            $("input[name=fertilizante_complementario_programa]").attr('required',false);
            $("select[name=cve_quimico]").attr('required',false);
            $("input[name=dosis_quimico]").attr('required',false);


        }
    }

$('.cualfertilizanteComplemetario').hide();

    $("#TipoFertilizanteComplementario").change(function () {
        if ($("#TipoFertilizanteComplementario").val() == 1) {
            $('.cualfertilizanteComplemetario').show("slow");
        } else {
            $('.cualfertilizanteComplemetario').hide();
        }
    });


$('.CualesPlagas').hide();

    $("#PlagasCultivoSI, #PlagasCultivoNo").change(function () {
        if ($("#PlagasCultivoSI").is(":checked")) {
            $('.CualesPlagas').show("slow");
            $("input[name=cual_plaga]").attr('required',true);
            $("select[name=cve_control_plaga]").attr('required',true);

        } else 
        if ($("#PlagasCultivoNo").is(":checked")) {
            $('.CualesPlagas').hide();
            $("input[name=cual_plaga]").attr('required',false);
            $("select[name=cve_control_plaga]").attr('required',false);



        }
    });

$('.CualesENfermedades').hide();

    $("#EnfermedadesEnCultivoSI, #EnfermedadesEnCultivoNO").change(function () {
        if ($("#EnfermedadesEnCultivoSI").is(":checked")) {
            $('.CualesENfermedades').show("slow");
            $("input[name=enfermedades_cuales]").attr('required',true);
            $("select[name=cve_metodo_enfermedades]").attr('required',true);
        } else 
        if ($("#EnfermedadesEnCultivoNO").is(":checked")) {
            $('.CualesENfermedades').hide();
            $("input[name=enfermedades_cuales]").attr('required',false);
            $("select[name=cve_metodo_enfermedades]").attr('required',false);
        }
    });

$('.PorcentajeAfectoEnfermedadPlaga').hide();

    $("#AfectoPlagaEnfermedadSI, #AfectoPlagaEnfermedadNo").change(function () {
        if ($("#AfectoPlagaEnfermedadSI").is(":checked")) {
            $('.PorcentajeAfectoEnfermedadPlaga').show("slow");
            $("input[name=porcentaje_cultivo]").attr('required',true);
        } else 
        if ($("#AfectoPlagaEnfermedadNo").is(":checked")) {
            $('.PorcentajeAfectoEnfermedadPlaga').hide();
            $("input[name=porcentaje_cultivo]").attr('required',false);
        }
    });


    $('.colSemillaHibrida').hide();

    $("#cve_tipo_semilla").change(function () {
        if ($("#cve_tipo_semilla").val() == 2) {
            $('.colSemillaHibrida').show("slow");
        } else {
            $('.colSemillaHibrida').hide();
        }
    });

    //validar fechas  defertilizante
    $('#mayorDAP').hide();
    $('#mayorUREA').hide();
    $('#mayorDAP2').hide();
    $('#mayorUREA2').hide();

    $("input[name=fecha_recibio_fertilizante]").change(function () {
        validarFechasFertilizantes();
    });
    $("input[name=fecha_aplicacion1]").change(function () {
        validarFechasFertilizantes();
    });
    $("input[name=urea_fecha1]").change(function () {
        validarFechasFertilizantes();
    });

    $("input[name=fecha_aplicacion2]").change(function () {
        validarFechasFertilizantes();
    });
    $("input[name=urea_fecha2]").change(function () {
        validarFechasFertilizantes();
    });


    function validarFechasFertilizantes(){
        if($("input[name=fecha_aplicacion1]").val() != '' && $("input[name=fecha_recibio_fertilizante]").val() != '' && $("input[name=urea_fecha1]").val() != '' ){
            if($("input[name=fecha_aplicacion1]").val() < $("input[name=fecha_recibio_fertilizante]").val() ){
                $('#mayorDAP').show();
                $('.buttonTecnologico').attr('disabled',true);
            }else{
                $('#mayorDAP').hide();
                $('.buttonTecnologico').attr('disabled',false);
            }
            if($("input[name=urea_fecha1]").val() < $("input[name=fecha_recibio_fertilizante]").val() ){
                $('#mayorUREA').show();
                $('.buttonTecnologico').attr('disabled',true);
            }else{
                $('#mayorUREA').hide();
                $('.buttonTecnologico').attr('disabled',false);

            }
        }

        if($("input[name=fecha_aplicacion2]").val() != '' && $("input[name=fecha_recibio_fertilizante]").val() != '' && $("input[name=urea_fecha2]").val() != '' ){
            if($("input[name=fecha_aplicacion2]").val() < $("input[name=fecha_recibio_fertilizante]").val() ){
                $('#mayorDAP2').show();
                $('.buttonTecnologico').attr('disabled',true);
            }else{
                $('#mayorDAP2').hide();
                $('.buttonTecnologico').attr('disabled',false);
            }
            if($("input[name=urea_fecha2]").val() < $("input[name=fecha_recibio_fertilizante]").val() ){
                $('#mayorUREA2').show();
                $('.buttonTecnologico').attr('disabled',true);
            }else{
                $('#mayorUREA2').hide();
                $('.buttonTecnologico').attr('disabled',false);

            }
        }

    }

    $('.fertilizanteComplementario').hide();

    $($("input[name=fertilizante_complementario_programa]")).change(function () {
        if ($("#FertilizanteComplementarioSI").is(":checked")) {
            $('.fertilizanteComplementario').show("slow");
            $("input[name=fertilizante_complementario_programa]").attr('required',true);
            $("select[name=cve_quimico]").attr('required',true);
            $("input[name=dosis_quimico]").attr('required',true);
        } else 
        if ($("#FertilizanteComplementarioNo").is(":checked")) {
            $('.fertilizanteComplementario').hide();
            $("input[name=fertilizante_complementario_programa]").attr('required',false);
            $("select[name=cve_quimico]").attr('required',false);
            $("input[name=dosis_quimico]").attr('required',false);
        }
    });


    $('#service_provider_select').on("change", function() {
        input = $('#service_provider_select').val();
        // console.clear();
        // console.log(input);
        $('#preparadcionTeerrenitor').val( $('#service_provider_select').val())
      });

      $("#msjCantidadesDAP").hide();

      $( $("input[name=ferti_kg_dap]") ).on( "keyup", function() {
        ConfirmarDAP();
    } );

      $( $("input[name=ferti_kg_urea]") ).on( "keyup", function() {
        ConfirmarDAP();
    } );


    function ConfirmarDAP(){
        if($( $("input[name=ferti_kg_dap]") ) != '' &&  $( $("input[name=ferti_kg_urea]") != '' ))
            $("#msjCantidadesDAP").show();
    }

      