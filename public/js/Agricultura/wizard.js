function refrescar() {
    location.reload();
}

$(document).ready(function () {
    $(".noSeleccionoCheck").hide();

    var navListItems = $('div.setup-panel div.padreStepwizard  div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn'),
        allPrevBtn = $('.prevBtn');

            allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();

            $target.find('input:eq(0)').focus();
        }
    });

    allPrevBtn.click(function () {
        var curStep = $(this).closest(".setup-content");
        var curStepBtn = curStep.attr("id");
        var prevStepWizard = $('div.setup-panel div.padreStepwizard div a[href="#' + curStepBtn + '"]').parent().parent().prev().children().children("a");
        prevStepWizard.removeAttr('disabled').trigger('click');
        curStep.find('input:eq(0)').focus();
    });

    allNextBtn.click(function () {
        $('body,html').animate({ scrollTop: '0px' }, 0);
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div.padreStepwizard  div a[href="#' + curStepBtn + '"]').parent().parent().next().children().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='number'],input[type='tel'],input[type='url'],input[type='date'],select,textarea,input[type='radio']"),
            isValid = true;
        // console.log(curInputs);
        curStep.find('input:eq(0)').focus();
        
        $(".form-group").removeClass("has-error");
        alertForm = $(curStep).children().children("form");
        alertaUbi = $(alertForm).children().children(".noSeleccionoCheck");
        $(alertaUbi).hide();
        // $(".noSeleccionoCheck").hide();
        for (var i = 0; i < curInputs.length; i++) {

            // console.log(curInputs[i].validity);
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
                alertForm = $(curStep).children().children("form");
                alertaUbi = $(alertForm).children().children(".noSeleccionoCheck");
                $(alertaUbi).show();
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
});

$(document).on("blur", "input[type='text'],textarea", function () {
    this.value = this.value.trim();
});