var apikey = "ICwUxGpAlG64_cE8_olljHZAHD3FTkLwY1BsSQbTl-A";
$(document).ready(function () {
  $("#panel").hide();
});

// mapa como tal 
var platform = new H.service.Platform({
  apikey: window.apikey
});
var defaultLayers = platform.createDefaultLayers();

var mapContainer = document.getElementById('map');

// Step 2: initialize a map
var map = new H.Map(mapContainer, defaultLayers.raster.satellite.map, {
  // initial center and zoom level of the map
  zoom: 12.5,
  center: {
    lat: 19.74,
    lng: -101.117
  },
  pixelRatio: window.devicePixelRatio || 1
});
// add a resize listener to make sure that the map occupies the whole container
window.addEventListener('resize', () => map.getViewPort().resize());

// Step 3: make the map interactive
// MapEvents enables the event system
// Behavior implements default interactions for pan/zoom (also on mobile touch environments)
var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

// Step 4: Create the default UI
var ui = H.ui.UI.createDefault(map, defaultLayers, 'en-US');
// mapa como tal 

// ############################# CAPTURA MAPA #############################

function capture(resultContainer, map, ui) {
  map.capture(function (canvas) {
    if (canvas) {
      resultContainer.innerHTML = '';
      resultContainer.appendChild(canvas);
      var link = document.createElement('a'); // Crear un enlace de descarga
      link.href = canvas.toDataURL(); // URL de la imagen capturada
      link.download = 'mapa.png'; // Nombre del archivo de descarga
      // Simular el clic en el enlace para iniciar la descarga automáticamente
      link.click();
      // Convertir la imagen capturada a formato base64 y almacenarla en la variable imageData
      imageData = canvas.toDataURL();
      $("#hiddenInputField").val(imageData);

    } else {
      // Por ejemplo, cuando el mapa está en modo Panorama
      console.log('No se pudo realizar captura');
    }
  }, [ui], 20, 20, 300, 300);
  
}

// Función para guardar la imagen en un campo de entrada oculto al enviar el formulario
function saveImage() {
  var inputField = document.getElementById('hiddenInputField'); // Reemplaza 'hiddenInputField' con el ID de tu campo de entrada oculto
  if (inputField) {
    inputField.value = imageData; // Asignar la imagen capturada a la propiedad 'value' del campo de entrada
  } else {
    console.log('Campo de entrada no encontrado');
  }
}


// var logContainer = $('<ul></ul>').addClass('log');
// logContainer.html('<li class="log-entry">Haga un click en el Mapa</li>');
// $(map.getElement()).append(logContainer);

// function logEvent(str) {
//   logContainer.empty();
//   logContainer.html('<li class="log-entry">Haga un click en el Mapa</li>');
//   var entry = $('<li></li>').addClass('log-entry').text(str);
//   logContainer.append(entry);
// }

function ubicacion(map, marker) {
  map.addEventListener('tap', function (evt) {
    var coord = map.screenToGeo(evt.currentPointer.viewportX, evt.currentPointer.viewportY);
    // logEvent('Coordenadas: ' + Math.abs(coord.lat.toFixed(4)) + ((coord.lat > 0) ? 'N' : 'S') + ' ' + Math.abs(coord.lng.toFixed(4)) + ((coord.lng > 0) ? 'E' : 'W'));
    $("#latitud").val(coord.lat.toFixed(4));
    $("#longitud").val(coord.lng.toFixed(4));
    marker.setGeometry({ lat: coord.lat, lng: coord.lng });
  });
}

function addDraggableMarker(map, behavior, marker) {
  // Ensure that the marker can receive drag events
  marker.draggable = true;
  map.addObject(marker);

  // disable the default draggability of the underlying map
  // and calculate the offset between mouse and target's position
  // when starting to drag a marker object:
  map.addEventListener('dragstart', function (ev) {
    var target = ev.target,
      pointer = ev.currentPointer;
    if (target instanceof H.map.Marker) {
      var targetPosition = map.geoToScreen(target.getGeometry());
      target['offset'] = new H.math.Point(pointer.viewportX - targetPosition.x, pointer.viewportY - targetPosition.y);
      behavior.disable();
    }
  }, false);


  // re-enable the default draggability of the underlying map
  // when dragging has completed
  map.addEventListener('dragend', function (ev) {
    var target = ev.target;
    if (target instanceof H.map.Marker) {
      behavior.enable();
    }
  }, false);

  // Listen to the drag event and move the position of the marker
  // as necessary
  map.addEventListener('drag', function (ev) {
    var target = ev.target,
      pointer = ev.currentPointer;
    if (target instanceof H.map.Marker) {
      target.setGeometry(map.screenToGeo(pointer.viewportX - target['offset'].x, pointer.viewportY - target['offset'].y));
      var coord = map.screenToGeo(pointer.viewportX, pointer.viewportY);
      // logEvent('Coordenadas: ' + Math.abs(coord.lat.toFixed(4)) + ((coord.lat > 0) ? 'N' : 'S') + ' ' + Math.abs(coord.lng.toFixed(4)) + ((coord.lng > 0) ? 'E' : 'W'));
      $("#latitud").val(coord.lat.toFixed(4));
      $("#longitud").val(coord.lng.toFixed(4));
    }
  }, false);
}

// Crear el marcador fuera de la función addDraggableMarker
var marker = new H.map.Marker({ lat: 19.74, lng: -101.117 }, {
  // marcar el objeto como volátil para el arrastre suave
  volatility: true
});


ubicacion(map, marker);
addDraggableMarker(map, behavior, marker);

var resultContainer = document.getElementById('panel');

// Create container for the "Capture" button
var containerNode = document.createElement('div');
containerNode.className = 'btn-group';

// Create the "Capture" button
var captureBtn = document.createElement('input');
captureBtn.value = 'Captura';
captureBtn.type = 'button';
captureBtn.className = 'btn btn-sm btn-default';

// Add both button and container to the DOM
containerNode.appendChild(captureBtn);
mapContainer.appendChild(containerNode);

// Step 7: Handle capture button click event
captureBtn.onclick = function () {
  capture(resultContainer, map, ui);
  $("#panel").show("slow");
};