$(document).ready(function () {
    $(".pregunta").hide();//si desea contestar o no
    $(".agricultorOculto").hide();//secciones a contestar
    $('.noporque').hide();//no quiere contestar encuesta
    $(".no_contesta").hide();//si es el beneficiario quien contesta la encuesta
    $(".ejidatario").hide();//si es ejidatario
    $(".ocultar_dialecto").hide();//sino habla dialecto
    $(".beneficio_boton").hide();
    $(".imgGuardada").hide();

    if (miVariable == 2) {
        buscarAgricultor(curp);
    }
});

// traer todos los agricultores por encuestador
$("#cve_municipios").change(function () {
    if($(this).val() != ''){

        var formData = new FormData();

        if (miVariable == 2){
            formData.append("cve_encuestador", miVariable);
        }else{
            formData.append("cve_encuestador", $("#cve_encuestador").val());
        }

        formData.append("cve_municipio_unidad", $("#cve_municipios").val());
        $.ajax({
            url: "obtieneAgricultores",
            type: "POST",
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (datos) {
                localidad = '';
                ejido     = '';

                // $(".cve_encuesta").val(value.cve_encuesta);
                localidades($("#cve_municipios").val(),localidad); 
                obtener_ejidos($("#cve_municipios").val(), ejido);
                //console.log(datos);
                let option = "";
                $.each(datos, function (index, value) {
                    option += '<option value="' + value.curp + '">' + value.nombre + " " + value.paterno + " " + value.materno + '</option>';
                });
                $("#nombreBusqueda").html(option);
                $("#nombreBusqueda").selectpicker('refresh');   
                // $("#nombre").attr("disabled", false);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    }

});

// funcion para buscar agricultor 
function buscarAgricultor(curp){
    curp = curp.replace(/['"]+/g, '');
    // console.log(curp.replace(/['"]+/g, ''));

    var formData = new FormData();
    formData.append("curp", curp);
   
    $.ajax({
        url: "ObtenerInfoAgricultor",
        type: "POST",
        data: formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function (datos) {
            $(".pregunta").show("slow");

            // console.log(datos);
            count = 0;
            $.map(datos, function (value, key) {
                cve_encuesta = datos["RegistroAgricultura"][0]["cve_encuesta"];
                $(".cve_encuesta").val(cve_encuesta);

                if (miVariable == 2) {
                    if (key == 'RegistroAgricultura'){
                        var municipio  = datos["RegistroAgricultura"][0]["cve_municipio_unidad"];
                        var localidad  = datos["RegistroAgricultura"][0]["cve_localidad_unidad"];
                        var ejido      = datos["RegistroAgricultura"][0]["ejido"];
                        var nombre     = datos["RegistroAgricultura"][0]["nombre"] + " " + datos["RegistroAgricultura"][0]["paterno"] + " " +  datos["RegistroAgricultura"][0]["materno"];

                        $('select[name="cve_municipios"] option[value="' + municipio + '"]').attr("selected", true);
                        $('select[name="cve_municipios"]').attr("disabled",true);

                            localidades(municipio,localidad);
                            obtener_ejidos(municipio,ejido);

                      
                            $('#nomagri').hide();

                            htmlNomAgri = " <div class='form-group'>"+
                                                "<label class='control-label'> Nombre:</label >"+
                                                "<input type='text' readonly id='nombreagri' name='nombreagri' class='form-control' value='"+ nombre+"'>"+
                                            "</div>"

                            $('#nomAgriConsulta').html(htmlNomAgri);

                            if (datos["RegistroAgricultura"][0]["cve_estatus"] == 2){
                                $("input[name=cve_estatus][value='1']").attr("checked", true).trigger("change");
                            }else{
                                $("input[name=cve_estatus]").attr("checked", false);
                            }
                    }
                } 
                else {
                    $('#nomAgriConsulta').html("");
                    $('#nomagri').show();
                    $('select[name="cve_municipios"]').attr("disabled", false);
                }

                form = $("form#" + key + "");
                info = datos[key][0] || 0;
                $(form).resetForm();

                if (info != 0) {
                    // desabilitar el ultimo.
                    wizars = $("." + key + "").parent().parent().next().children().children("a");
                    // wizars.removeAttr('disabled').trigger('click').addClass("btn-primary");
                    count++;
                    if (key != "fotos") {
                        $("." + key + "").attr("disabled", false);
                    }
                }
                retornoInformacionBD(form, info);
            });

            if (count != Object.keys(datos).length) {
                $(".fotos").attr("disabled", true).addClass("btn-danger");
            } else {
                $(".fotos").attr("disabled", false);
                // este es para el boton de la parte del toño que no dejen guardar hasta que terminen de llenar todo
                $(".noSepuedeGuardar").attr("disabled", false);
            }


        },
    });
}

// seleccione agricultor y trae la informacion que tenemos de el
$("select[name='nombreBusqueda']").on("change", function () {
    var curp = $(this).val();
    buscarAgricultor(curp);
});

// si desean continuar con la encuesta o no 
$("input[name='cve_estatus']").on("change", function () {
    var valor = $(this).val();
    //console.log(valor);
    noContestarEncuesta(valor);

    if (valor == 1) {
        $('.agricultorOculto').show("slow");
        $('.noporque').hide("slow");
        $("#porque").attr("required", false);
    } else {
        $('.agricultorOculto').hide();
        $('.noporque').show("slow");
        $("#porque").attr("required",true);
    }

});

// si es el beneficiario quien contesta la encuesta
$("select[name='productor_contesta']").on("change", function () {
    var valor = $(this).val();
    if (valor == 1){
        $(".no_contesta").hide("slow");
        $("#cve_quien_contesta").attr("required", false);
        $("#nombre_quien_contesta").attr("required", false);
    }else{
        $(".no_contesta").show("slow");
        $("#cve_quien_contesta").attr("required",true);
        $("#nombre_quien_contesta").attr("required", true);
    }
});

// si es de un ejido
$("select[name='es_ejidatario']").on("change", function () {
    var valor = $(this).val();
    if (valor == 1) {
        $(".ejidatario").show("slow");
        $("#ejido").attr("required", true);
        // $("#ejido").val("");

    } else {
        $(".ejidatario").hide("slow");
        $("#ejido").attr("required", false);
        $("#ejido").val("");
        // $("#ejido").selectpicker('refresh'); 
    }
});

// localidades
function localidades(municipio,localidad){
    if (municipio != "") {
        var formData = new FormData();
        formData.append("cve_municipio_unidad", municipio);
        $.ajax({
            url: "getLocalidades",
            type: "POST",
            data: formData,
            dataType: "json",
            processData: false,
            contentType: false,
            success: function (datos) {
                let option = "";
                $.each(datos, function (index, value) {
                 
                    option += '<option value="' + value.cve_localidad + '">' + value.localidad + '</option>';

                });
                $("#cve_localidad_unidad").html(option);
                $("#cve_localidad_unidad").selectpicker('refresh');

                if (miVariable == 2) {
                    if (localidad != '' && localidad != null){
                       var retorlocalidad;
                        retorlocalidad = localidad.padStart(4, '0');
                        // setTimeout(function () {
                            // $('[name="cve_localidad_unidad"] option[value="'+retorlocalidad + '"]').attr("selected", true).trigger("change");
                            $("#cve_localidad_unidad").selectpicker('val', retorlocalidad).trigger("change");
                        // }, 2000);

                    }
           
                }
                $("#cve_localidad_unidad").attr("disabled", false);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    }
}

function obtener_ejidos(cve_municipio,ejido) {
    //console.log(cve_municipio);
    if (cve_municipio != "") {
        var formData = new FormData();
        formData.append("cve_municipio", cve_municipio);
        $.ajax({
            url: "getEjidos",
            type: "POST",
            data: formData,
            dataType: "json",
            processData: false,
            contentType: false,
            success: function (datos) {
                //console.log(datos);
                let option = '<option value="">Seleccione un ejido</option>';
                $.each(datos, function (index, value) {
                    option += '<option value="' + value.cve_ejido + '">' + value.ejido + '</option>';
                });
                $("#ejido").html(option);
                // $("#ejido").selectpicker('refresh');
                $("#ejido").attr("disabled", false);

                if (ejido != '' && miVariable == 2) {
                    $('[name="ejido"] option[value="' + ejido + '"]').attr("selected", true).trigger("change");

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
    }
}

// dialecto de la persona
$("select[name='dialecto']").on("change", function () {
    var valor = $(this).val();
    if (valor == 1) {
        $(".ocultar_dialecto").show("slow");
        $("#dialecto_hablado").attr("required", true);
    } else {
        $(".ocultar_dialecto").hide("slow");
        $("#dialecto_hablado").attr("required", false);
    }
});

// si al final dice que no quiere contestar la encuesta deja de requerir todo
function noContestarEncuesta(valor) {
    if (valor == 1) {
        inputsNo = $('#RegistroAgricultura').find("input[type='text'],input[type = 'tel'], input[type='url'],input[type='date'],select,textarea,input[type='radio']");
        for (var i = 0; i < inputsNo.length; i++) {
            $(inputsNo[i]).removeAttr('required');
        }
    } else {
        inputsSi = $('#RegistroAgricultura').find("input[name='cve_estatus'],select[name='productor_contesta'], select[name='cve_localidad_unidad'], select[name='es_ejidatario'], input[name='latitud'], input[name='longitud'],input[name='celular'],input[name='anio_productor'], select[name='dialecto'],select[name='leer_escribir'], input[name='anio_estudio'],select[name='beneficiario_bienestar'] ");
        for (var i = 0; i < inputsSi.length; i++) {
            $(inputsSi[i]).attr('required', true);
        }
    }
}

function solonumeros(e) {
    key = e.keyCode || e.which;
    teclado = String.fromCharCode(key);
    numeros = "0123456789";
    especiales = "8-37-38-46";
    tecladoEspecial = false;
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecladoEspecial = true;
        }
    }
    if (numeros.indexOf(teclado) == -1 && !tecladoEspecial) {
        return false;
    }

}

// ############################# RETORNO VALORES SECCIONES ###############################

function retornoInformacionBD(form, datos) {
    if (datos != 0) {

        // console.log(form);
        // console.log(datos);
        calcularIngresosComercializacion();
        obtener_rendimiento();
        $.map(datos, function (value, key) {
            var campoName = key;
            var encontrarCampos = form.find('[name="' + campoName + '"],[id="' + campoName + '"] ');
            // console.log(encontrarCampos);
            // console.log(value);
            // console.log(campoName);
            

            // saber si es beneficiario o no
            if (campoName == 'beneficiario' && value == 1 ){
                satisfacionNobeneficiario(value);
                $(".beneficio_boton").show();
                $("#beneficiario_sagarpa").text("ES BENEFICIARIO");
            } else if (campoName == 'beneficiario' && value == 0){
                satisfacionNobeneficiario(value);


                $(".beneficio_boton").show();
                $("#beneficiario_sagarpa").text("NO ES BENEFICIARIO");
            }

            // si es select
            if ($(encontrarCampos).find('img')) {
                imagenes = $('[id="' + campoName + '"]').attr({ "src": value });
                // console.log(imagenes);

                $(".imgGuardada").show("slow");
            }

            // si es select
            if ($(encontrarCampos).is('select')) {
                // console.log(key);


                if (campoName == 'cve_localidad_unidad') {

                    if (value != null) {

                        if (value.length == 1){
                        value = value.padStart(4, '0');

                        } else if (value.length == 2){
                        value = value.padStart(3, '0');

                        } else if (value.length == 3) {
                        value = value.padStart(2, '0');
                        }
                    }
                    $('[name="' + campoName + '"] option[value="' + value + '"]').attr("selected", true).trigger("change");

                }else{
                    // console.log($('[name="' + campoName + '"]'));

                    $('[name="' + campoName + '"] option[value="' + value + '"]').attr("selected", true).trigger("change");
                }
            }
            // si es select

            // si es selectpicker
            if ($(encontrarCampos).hasClass('selectpicker')) {

                var isMultiple = $(encontrarCampos).prop('multiple');
                // si es opcion multiple
                if (isMultiple) {
                    var clavesEncontradas = buscarValoresSimilares(datos, campoName);
                    // if (clavesEncontradas !== null && clavesEncontradas !== undefined) {
                        var arr = clavesEncontradas.toString().split(',');
                        // console.log(arr);
                    // }
                    // console.log(key, value);

                    if (campoName == 'cve_preparacion') {
                        $('.pickerTerreno').selectpicker('val', arr);
                        setTimeout(function () {
                            $('.pickerTerreno').val(arr);
                        }, 2000);
                    }

                    if (campoName == 'cve_preparacion') {
                        $('.pickerTerreno').selectpicker('val', arr);
                        setTimeout(function () {
                            $('.pickerTerreno').val(arr);
                        }, 2000);
                    }
                    
                    if (campoName == 'laborRealizo') {
                        $('.laboresRealizo').selectpicker('val', arr);
                        setTimeout(function () {
                            $('.laboresRealizo').val(arr).trigger('change');
                        }, 4000);

                    }
                    if (campoName == 'cve_tema') {
                        $('.cve_tema').selectpicker('val', arr);
                        setTimeout(function () {
                            $('.cve_tema').val(arr).trigger('change');
                        }, 6000);
                    }

                    if(campoName = 'servVivienda'){
                        var array = (value ?? '').split(',');

                        // Asegurarse de que haya al menos 5 elementos en el array
                        while (array.length < 5) {
                            array.push('');
                        }
                      
                        setTimeout(function () {
                            $("#servVivienda").selectpicker('val', array).trigger('change');
                        }, 7000);
                    }

                }
                else {// sino es opcion multiple
                    $(encontrarCampos).selectpicker('val', value);
                }
            }
            // si es selectpicker

            // si es radio
            if ($(encontrarCampos).is('input[type="radio"]')) {
                // console.log(key, value);
                // $(encontrarCampos).filter('[value="1"]').attr('checked', true).trigger("change");
                $(encontrarCampos).filter('[value="' + value + '"]').attr('checked', true).trigger("change");
                // console.log($(encontrarCampos).filter('[value="' + value + '"]'));
                // if(value = 2){
                //     $(encontrarCampos).filter('[value="1"]').attr('checked', true).trigger("change");
                // }
            }
            // si es radio

            else {
                $(encontrarCampos).val(value);
            }

        });


        // buscar los valores del selectpicker
        function buscarValoresSimilares(datos, campoName) {
            var valoresSimilares = [];
            Object.keys(datos).forEach(function (key) {

                var longitudMinima = Math.min(key.length, campoName.length);
                if (key.substring(0, longitudMinima) === campoName.substring(0, longitudMinima)) {
                    // valoresSimilares[key] = datos[key];
                    valoresSimilares.push(datos[key]);
                }
            });

            return valoresSimilares;
        }

    }

}