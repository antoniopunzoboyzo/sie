// new DataTable('#datatable1');

var table = $('#datatable1').DataTable({
    language: {
        "decimal": "",
        "emptyTable": "No hay informaci&oacute;n",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ resultados",
        "infoEmpty": "Mostrando 0 a 0 de 0 resultados",
        "infoFiltered": "(Filtrado de _MAX_ total resultados)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Resultados",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "&Uacute;ltimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }
    },
    dom: 'Bfrtip',
    buttons: [
        { extend: 'excel', title: 'Resumen Ejecutivo' },
        { extend: 'pdfHtml5', download: 'open'}
    ],
    "searching": true
});
