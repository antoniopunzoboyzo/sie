$(document).on('click', '._btnModCambiaEstatus', function () { //modalCambiaEstatus. Es el span class que llama el modal
  $("#modalCambiaEstatusBody").html('<h3><i class="fas fa-spinner fa-pulse mr-2 fa-sm"></i>Procesando ...</h3>');
  let cve_encuesta = this.dataset.cve_encuesta;
  var formData = new FormData();
  formData.append("cve_encuesta", cve_encuesta);
  console.log(cve_encuesta);
  if (cve_encuesta != '' && cve_encuesta != null) {
    $.ajax({
      url: "CambiaEncuesta",
      type: "POST",
      data: formData,
      dataType: "json",
      processData: false,
      contentType: false,
      success: function (data) {
        $("#spanModCveEstatus").html(cve_encuesta);
        console.log(data);
        $("#modalCambiaEstatusBody").html(data);
        //$(".selectpicker").selectpicker();
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR);
        console.log(textStatus);
        console.log(errorThrown);
      }
    });
  } else {
    console.log("cve_encuesta " + cve_encuesta);
  }
});

$(document).on('click', '._btnModSubirFotos', function () { //modalCambiaEstatus. Es el span class que llama el modal
  $("#modalSubeFotosBody").html('<h3><i class="fas fa-spinner fa-pulse mr-2 fa-sm"></i>Procesando ...</h3>');
  let cve_encuesta = this.dataset.cve_encuesta;
  var formData = new FormData();
  formData.append("cve_encuesta", cve_encuesta);
  if (cve_encuesta != '' && cve_encuesta != null) {
    $.ajax({
      url: "../Agricultura/adjuntaFotos",
      type: "POST",
      data: formData,
      dataType: "json",
      success: function (data) {
        $("#spanModCveEstatus").html(cve_encuesta);
        //console.log(data);
        $("#modalSubeFotosBody").html(data);
        $(".selectpicker").selectpicker();
      },
      error: function (jqXHR, textStatus, errorThrown) {
        console.log(jqXHR);
        console.log(textStatus);
        console.log(errorThrown);
      }
    });
  } else {
    console.log("cve_encuesta " + cve_encuesta);
  }
});