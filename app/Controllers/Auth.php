<?php 
namespace App\Controllers;  
use CodeIgniter\Controller;
use App\Models\modeloUsuarios;
  
class Auth extends Controller
{
    public function index()
    {
        helper(['form']);
        echo view('login');
    } 
  
    public function loginAuth()
    {
        $session = session();
        $modeloUsuario = new modeloUsuarios();
        //print_r($this->request);
        $usuario = $this->request->getVar('login-form-username');
        $contrasenia = $this->request->getVar('login-form-password');
        $data = $modeloUsuario->buscaUsuario($usuario,$contrasenia);
        if($data){
                $ses_data = [
                    'cve_encuestador'  => $data[0]['cve_encuestador'],
                    'nombre'     => $data[0]['nombre'],
                    'cve_municipio'    => $data[0]['cve_municipio'],
                    'ddr'    => $data[0]['ddr'],
                    'tipo'    => $data[0]['tipo'],
                    'logged_in'  => TRUE
                ];

                $datos['encuestador']=$data[0]['cve_encuestador'];
                $session->set($ses_data);
                if($data[0]['tipo']==2){
                    header('location:Listados');
                }else{

                    header('location:Padron');
                }
                exit();
        }else{
            $session->setFlashdata('msg', 'Verifique sus Credenciales');
            $msg["msg"]='Verifique sus Credenciales';
            return view('login',$msg);
        }
    }

    public function Logout()
    {
        $session = session();
        $session->destroy();
        header('location:Padron');
    }
}