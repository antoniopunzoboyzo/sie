<?php

namespace App\Controllers\Padrones;
use App\Controllers\BaseController;
use App\Models\Padrones\CondicionModelo;
use setasign\Fpdi\Tcpdf\Fpdi;
use App\Helpers\PDF;

class Condicion extends BaseController
{
	 function guardar(){
      $session = session();
        $modeloCondicion = new CondicionModelo();
        
        //echo $this->request->getVar("remesas")=='';
        $sqldel="DELETE FROM condicion_socioeconomica where cve_encuesta='".$session->get('cve_encuesta')."'";
        $sql='INSERT INTO `agricultura`.`condicion_socioeconomica` (`cve_encuesta`,`ingreso`,`ingreso_adicional`,`remesas`,`apoyoFamiliares`,`negocioPropio`,`empleoFueraUP`,`pension`,`otros`,`apoyos_integrantes_hogar`,`monto_total_menusal`,`observaciones_encuestador`,numeroIntegrantes,pisoVivienda,paredVivienda,techoVivienda,espaciosVivienda,servMedico,servVivienda) VALUES
("'.$session->get('cve_encuesta').'",
'.(($this->request->getVar("ingreso")!== "")?$this->request->getVar("ingreso"):"0").',
'.$this->request->getVar("flexRadioDefault").',
'.(($this->request->getVar("remesas")!== "")?$this->request->getVar("remesas"):"0").',
'.(($this->request->getVar("apoyoFamiliares")!== "")?$this->request->getVar("apoyoFamiliares"):"0").',
'.(($this->request->getVar("negocioPropio")!== "")?$this->request->getVar("negocioPropio"):"0").',
'.(($this->request->getVar("empleoFueraUP")!== "")?$this->request->getVar("empleoFueraUP"):"0").',
'.(($this->request->getVar("pension")!== "")?$this->request->getVar("pension"):"0").',
'.(($this->request->getVar("otros")!== "")?$this->request->getVar("otros"):"0").',
'.$this->request->getVar("produccion").',
'.(($this->request->getVar("monto_total_menusal")!== "")?$this->request->getVar("monto_total_menusal"):"0").',
"'.$this->request->getVar("observaciones_encuestador").'","'.$this->request->getVar("numeroIntegrantes").'","'.$this->request->getVar("pisoVivienda").'",
"'.$this->request->getVar("paredVivienda").'","'.$this->request->getVar("techoVivienda").'","'.$this->request->getVar("espaciosVivienda").'","'.$this->request->getVar("servMedico").'","'.implode(',', $this->request->getVar("servVivienda[]")).'")';
        $valorGuardado=$modeloCondicion->guardar($sql,$sqldel);
        if ($valorGuardado == 0) {
          $response = array(
              'alert' => 'error', // o 'error' dependiendo de si la operación tuvo éxito o no
              'message' => '¡La operación no se pudo realizar con éxito, intentelo nuevamente!' // un mensaje que describa el resultado de la operación
          );
      } else {
          $response = array(
              'alert' => 'success', // o 'error' dependiendo de si la operación tuvo éxito o no
              'message' => '¡La operación se realizó con éxito!' // un mensaje que describa el resultado de la operación
          );
      }
        echo json_encode($response);
    }

    function guardarimg(){
      $session = session();
      $modeloCondicion = new CondicionModelo();
        $imgineb64=$this->request->getVar('ineimg');
        $imgpadronb64=$this->request->getVar('productorimg'); 

        $b64 = str_replace('data:image/png;base64,', '', $imgineb64);
        $bin = base64_decode($b64);
        $im = imageCreateFromString($bin);
        if (!$im) {
          die('Base64 value is not a valid image');
        }
        $img_file1 = 'imagenesEncuesta/'.$session->get('cve_encuesta').'_ine.png';
        imagepng($im, $img_file1, 0);

        $b64 = str_replace('data:image/png;base64,', '', $imgpadronb64);
        $bin = base64_decode($b64);
        $im = imageCreateFromString($bin);
        if (!$im) {
          die('Base64 value is not a valid image');
        }
        $img_file2 = 'imagenesEncuesta/'.$session->get('cve_encuesta').'_productor.png';
        imagepng($im, $img_file2, 0);
        $validaImg='select * from evidencia_fotografica where cve_encuesta="'.$session->get('cve_encuesta').'"';
        if(count($modeloCondicion->checafoto($validaImg))>0)
          echo $insIma='update evidencia_fotografica set foto1="'.$img_file1.'", foto2="'.$img_file2.'" where cve_encuesta="'.$session->get('cve_encuesta').'"';
        else
          echo $insIma='insert into evidencia_fotografica values("'.$session->get('cve_encuesta').'","'.$img_file1.'","'.$img_file2.'","" )';
        
        $actEstatus='update agricultor set cve_estatus=2 where cve_encuesta="'.$session->get('cve_encuesta').'"';
        $modeloCondicion->guardarimg($insIma,$actEstatus);
    }

    function mostrarReporte($folio=''){
      $modeloCondicion = new CondicionModelo();
        $resultado=$modeloCondicion->pdfConsulta($folio);
        if(count($resultado)>0){
          // Horario local
          date_default_timezone_set('Etc/GMT-6');
          setlocale(LC_TIME, 'es_MX.UTF-8'); 
          $timestamp = strtotime($resultado[0]['fecha_encuesta']);
          $dia=date ("d",$timestamp);
          $mes=date ("M",$timestamp);
          $h=date ("h:i:s",$timestamp);
          $pdf=new PDF();

          $pdf->setSourceFile("../plantillaspfd/cuestionarioagr.pdf");
          $pdf->AddPage();
          
          $size = $pdf->getTemplateSize($pdf->importPage(1));
          $pdf->SetMargins(22, 20, 22, true);
          $pdf->useTemplate($pdf->importPage(1), 5, 5, $size["width"]-15, $size["height"]);
          $pdf->SetFont ('times', 'B', '8');
          $pdf->setxy(169,33.2);
          $pdf->Cell(10, 0, $resultado[0]['ddr_nombre']."\n", 0, 0, 'L', 0, '', 0);
          $pdf->setxy(169,36.7);
          $pdf->Cell(10, 0, $resultado[0]['cve_encuesta']."\n", 0, 0, 'L', 0, '', 0);
          $pdf->SetFont ('times', '', '10');
          $pdf->sety(62);
          
          $txt="En la localidad de ".$resultado[0]['localidad'].", municipio de ".$resultado[0]['municipio'].", Estado de Michoacán, reunidos el día ".$dia." de ".$mes." del 2023, siendo las  horas ".$h.", inicia el  seguimiento a beneficiarios del Programa  de Fertilizantes 2023."."\n"."Intervienen el C. ".$resultado[0]['jefe_ddr']." , Jefe del DDR 0".$resultado[0]['ddr_nombre']." y el beneficiario C. ".$resultado[0]['nombrebene']." , todos ellos debidamente identificados.";
          $pdf->MultiCell(0, 5,$txt."\n", 1, 'J', 1, 2, '' ,'', true);
          ///datos del beneficiario
          $pdf->SetFont ('times', '', '8');
          $pdf->setxy(80,128);
          $pdf->Cell(120, 0, $resultado[0]['nombrebene']."\n", 0, 0, 'L', 0, '', 0);
          $pdf->setxy(80,134);
          $pdf->Cell(120, 0, $resultado[0]['curp']."\n", 0, 0, 'L', 0, '', 0);
          $pdf->setxy(80,140);
          $pdf->Cell(120, 0, $resultado[0]['folio_estatal']."\n", 0, 0, 'L', 0, '', 0);
          ///

          $pdf->SetFont ('times', '', '8');
          $pdf->setxy(80,153);
          $pdf->Cell(120, 0, $resultado[0]['municipio']."\n", 0, 0, 'L', 0, '', 0);
          $pdf->setxy(80,160);
          $pdf->Cell(120, 0, $resultado[0]['localidad']."\n", 0, 0, 'L', 0, '', 0);
          $pdf->setxy(80,167);
          $pdf->Cell(120, 0, $resultado[0]['ejido']."\n", 0, 0, 'L', 0, '', 0);
          $pdf->setxy(80,172);
          $pdf->Cell(120, 0, $resultado[0]['latitud'].", ".$resultado[0]['longitud']."\n", 0, 0, 'L', 0, '', 0);

          
          $pdf->setxy(80,186);
          $pdf->Cell(120, 0, $resultado[0]['cedas']."\n", 0, 0, 'L', 0, '', 0);
          $pdf->setxy(80,192);
          $pdf->Cell(120, 0, $resultado[0]['cedalugar']."\n", 0, 0, 'L', 0, '', 0);
          

          //entrega y calidad

          $pdf->SetFont ('times', '', '8');
          switch ($resultado[0]['cve_recibio_fertilizante']) {
            case 1:
              $pdf->setxy(23,213);
              $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
              break;
            case 2:
              $pdf->setxy(45,213);
              $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
              break;
            break;
            case 3:
              $pdf->setxy(66,213);
              $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
              break;
            break;
            case 4:
              $pdf->setxy(84,213);
              $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
              break;
            break;
            case 5:
              $pdf->setxy(97,213);
              $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
              break;
          }

          $pdf->SetFont ('times', '', '8');
          switch ($resultado[0]['formato_unico_entrega']) {
            case 1:
              $pdf->setxy(23,231);
              $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
              break;
            case 2:
              $pdf->setxy(33,231);
              $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
              break;
            break;
          
          }
          
          $pdf->setxy(35,237);
          $pdf->Cell(40, 0, $resultado[0]['formato_porque']."\n", 0, 0, 'L', 0, '', 0);
          

          $pdf->SetFont ('times', '', '8');
          switch ($resultado[0]['cve_satisfecho']) {
            case 1:
              $pdf->setxy(23,248);
              $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
              break;
            case 2:
              $pdf->setxy(51,248);
              $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
              break;
            break;
            case 3:
              $pdf->setxy(75,248);
              $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
              break;
            break;
            case 4:
              $pdf->setxy(84,248);
              $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
              break;
          }

          
          

  
          $pdf->AddPage();
          $tplidx = $pdf->ImportPage(2);
          $pdf->useTemplate($tplidx);

          $pdf->SetFont ('times', 'B', '8');
          $pdf->setxy(176,28);
          $pdf->Cell(10, 0, $resultado[0]['ddr_nombre']."\n", 0, 0, 'L', 0, '', 0);
          $pdf->setxy(176,31.5);
          $pdf->Cell(10, 0, $resultado[0]['cve_encuesta']."\n", 0, 0, 'L', 0, '', 0);
          

          $pdf->SetFont ('times', '', '8');
          $pdf->setxy(38,45);
          $pdf->Cell(120, 0, $resultado[0]['sugerencias']."\n", 0, 0, 'L', 0, '', 0);

          switch($resultado[0]['aplico_fertilizante']){
            case 1:
              $pdf->setxy(22,86);
              $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
            break;
            case 0:
              $pdf->setxy(40,86);
              $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
              $pdf->setxy(60,86);
              $pdf->Cell(3, 0, $resultado[0]['porqueaplicofert']."\n", 0, 0, 'L', 0, '', 0);
            break;
          }

          $pdf->setxy(38,68);
          $pdf->Cell(120, 0, $resultado[0]['cultivo'].", ".$resultado[0]['cultivo1']."\n", 0, 0, 'L', 0, '', 0);
          
          $pdf->setxy(125,103);
          $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
          
          switch($resultado[0]['mejora_produccion']){
            case 1:
              $pdf->setxy(20,126.5);
              $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
              switch($resultado[0]['cve_porcentaje']){
                case 1:
                  $pdf->setxy(20,138);
                  $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                  break;
                case 3:
                  $pdf->setxy(45,138);
                  $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                  break;
                case 2:
                  $pdf->setxy(68,138);
                  $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                  break;
              }
            break;
            case 0:
              $pdf->setxy(43,126.5);
              $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
              $pdf->setxy(65,126.5);
              $pdf->Cell(3, 0, $resultado[0]['porque']."\n", 0, 0, 'L', 0, '', 0);
            break;
          }
          $pdf->setxy(21,156);
          $pdf->MultiCell(0, 5,$resultado[0]['observaciones_encuestador']."\n", 1, 'J', 1, 2, '' ,'', true);
                   
          
          $pdf->AddPage();
          $tplidx = $pdf->ImportPage(3);
          $pdf->useTemplate($tplidx);

          $pdf->SetFont ('times', 'B', '8');
          $pdf->setxy(178,28);
          $pdf->Cell(10, 0, $resultado[0]['ddr_nombre']."\n", 0, 0, 'L', 0, '', 0);
          $pdf->setxy(176,31.5);
          $pdf->Cell(10, 0, $resultado[0]['cve_encuesta']."\n", 0, 0, 'L', 0, '', 0);
          $pdf->Image($resultado[0]['foto1'],30,55,75,75);
          $pdf->Image($resultado[0]['foto2'],110,55,75,75);
          
          $pdf->Image($resultado[0]['mapa'],30,138,75,75);      
        $pdf->output('pantilla.pdf', 'I');
        exit();
      }else
        echo "sin informacion para generar el pdf";
    }
}