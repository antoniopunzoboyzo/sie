<?php

namespace App\Controllers\Padrones;
use App\Controllers\BaseController;
use App\Models\Padrones\PadronModelo;
use App\Models\Padrones\catAgriculturaModel;
use setasign\Fpdi\Tcpdf\Fpdi;
use App\Helpers\PDF;


class Padron extends BaseController
{
    public function index(){
        $session = session();
        if($session->get("logged_in")==1){

            $curpConsulta =  $this->request->getGet('mi_parametro');

            if($session->get("tipo")==0 ||$session->get("tipo") == 2 ){
                // var_dump($cve_encuesta);
                $modeloPadron       = new PadronModelo();
                $modeloAgricultor   = new catAgriculturaModel();
                $cve_encuestador    = $session->get("cve_encuestador");
                $encuestador        = $modeloAgricultor->encuestadores($cve_encuestador)[0];

                // traer los datos para visualizacion
                if ($curpConsulta !== null) {
                    $rol = 2;
                    $agricultoresMpios = $modeloAgricultor->agricultoresMpio($cve_encuestador, $rol);
                    $info["encuestaConsulta"]         = $curpConsulta;

                }else{
                    $rol = 0;
                    $agricultoresMpios = $modeloAgricultor->agricultoresMpio($cve_encuestador, $rol);
                }

                // var_dump($cve_encuesta);

                //$modeloAgricultura
                $quien_contesta    = $modeloAgricultor->catQuienContesta();
    

                $catCapacitacion =  $modeloPadron->catCapacitacion();
                $cattemas        =  $modeloPadron->catTemas();
                $catCedas        =  $modeloPadron->catCedas();
                $catrecursos     =  $modeloPadron->catRecursos();
                $catEstadoFerti  =  $modeloPadron->catEstadoFerti();
                $catAtencion     =  $modeloPadron->catAtencion();
                $catPorcentajes  =  $modeloPadron->catPorcentajes();

                $info["encuestador"]              = $encuestador;
                $info["agricultoresMpios"]        = $agricultoresMpios;
                $info["quien_contesta"]           = $quien_contesta;
                $info["catCapacitaciones"]        = $catCapacitacion;
                $info["cattemas"]                 = $cattemas;

                $info["catCedas"]                 = $catCedas;
                $info["catrecursos"]              = $catrecursos;
                $info["estadoFertili"]            = $catEstadoFerti;
                $info["catAtencion"]              = $catAtencion;
                $info["porcentajes"]              = $catPorcentajes;

                //datos productoivos

                $DatosProductivos = $modeloPadron->ConsultarSiYaExisteFolio('ARTYESC2023');
                if(isset($DatosProductivos[0]['folio'])){
                    $session->setFlashdata('hectarea_sup_up',   $DatosProductivos[0]['hectarea_sup_up']);
                    $session->setFlashdata('area_sup_up',       $DatosProductivos[0]['area_sup_up']);
                    $session->setFlashdata('centiarea_sup_up',  $DatosProductivos[0]['centiarea_sup_up']);
                }
                
                    $info["catFertilizantes"]   = $modeloPadron->catFertilizantes($where = '');
                    $info["catTenencia"]   = $modeloPadron->catTenencia($where = '');
                    $info["catCultivos"]   = $modeloPadron->catCultivos($where = '');
                    $info["catCiclo"]      = $modeloPadron->catCiclo($where = '');
                    $info["catRegimen"]      = $modeloPadron->catRegimen($where = '');
                //datos productivos

                //datos tecnologicos
                    $info["catSistemaProduccion"]   = $modeloPadron->catSistemaProduccion();
                    $info["catAdquisisionSemilla"]   = $modeloPadron->catAdquisisionSemilla();
                    $info["catTiempoSemilla"]   = $modeloPadron->catTiempoSemilla();
                    $info["catResiembra"]   = $modeloPadron->catResiembra();
                    $info["catTempLluvias"]   = $modeloPadron->catTempLluvias();

                    $info["catPreparacionTerreno"]   = $modeloPadron->catPreparacionTerreno();
                    $info["catSemilla"]   = $modeloPadron->catSemilla();
                    $info["catTipoSiembra"]   = $modeloPadron->catTipoSiembra();
                    $info["catLabores"]   = $modeloPadron->catLabores();
                    $info["catAplicaciones"]   = $modeloPadron->catFormaAplicarFertilizante();
                    $info["catTIpoFertilizante"]   = $modeloPadron->catTIpoFertilizante();
                    $info["catControlPlagas"]   = $modeloPadron->catControlPlagas();
                    $info["catEnfermedades"]   = $modeloPadron->catEnfermedades();
                //ends datos tecnologicos

                // return redirect()->to('Agricultura/listadoAgricultores'.$info);
                return view('Agricultura/AgriculturaRegistro',$info);
            }else if($session->get("tipo")==1){
                $session = session();
                $cve_encuestador = $session->get("cve_encuestador");
    
                $modeloPadron = new PadronModelo();
                $where = "cve_supervisor='".$cve_encuestador."'";
                $agricultores = $modeloPadron->agricultores($where);
                $info["agricultores"] = $agricultores;
                return view('Agricultura/listadoAgricultores', $info);

            }
        }else
            return view('login');
    }


    public function superAdmin(){
        $session = session();
        if($session->get("tipo") == 2){
            // todos los terminados para superusuario
            $cve_encuestador = $session->get("cve_encuestador");
            $modeloPadron = new PadronModelo();
            $where = "";
            // $where = "cve_estatus=2";
            $agricultores = $modeloPadron->agricultores($where);
            $info["agricultores"] = $agricultores;
            $session->setFlashdata('SuperAdmin', 1);
            return view('Agricultura/listadoAgricultores', $info);
        }
    }

    // FORMULARIO PARA AGREGAR UN NUEVO AGRICULTOR
    function RegistroAgricultura(){
        $session = session();
        $modeloPadron = new PadronModelo();

        $cve_estatus = $this->request->getPost('cve_estatus');

        if($cve_estatus == 1){
            //step 2
            setlocale(LC_ALL, 'es_MX', 'Spanish_Spain', 'Spanish');

            // datos a guardar
                $datosEncuenta2 = array(
                    "cve_estatus" => $this->request->getPost('cve_estatus'),
                    "productor_contesta" => $this->request->getPost('productor_contesta'),
                    "cve_quien_contesta" => $this->request->getPost('cve_quien_contesta'),
                    "nombre_quien_contesta" => $this->request->getPost('nombre_quien_contesta'),
                    "cve_localidad_unidad" => $this->request->getPost('cve_localidad_unidad'),
                    "es_ejidatario" => $this->request->getPost('es_ejidatario'),
                    "ejido" => $this->request->getPost('ejido'),
                    "latitud" => $this->request->getPost('latitud'),
                    "longitud" => $this->request->getPost('longitud'),
                    "telefono" => $this->request->getPost('telefono'),
                    "anio_productor" => $this->request->getPost('anio_productor'),
                    "dialecto" => $this->request->getPost('dialecto'),
                    "dialecto_hablado" => $this->request->getPost('dialecto_hablado'),
                    "leer_escribir" => $this->request->getPost('leer_escribir'),
                    "anio_estudio" => $this->request->getPost('anio_estudio'),
                    "beneficiario_bienestar" => $this->request->getPost('beneficiario_bienestar'),
                    "fecha_encuesta" => date("Y-m-d h:m:s")
                );
            // datos a guardar
            
            $imgineb64= $this->request->getVar('mapImage');
            if($imgineb64 !=  "" ){//tengo que validar si tambien hay datos dentro de $datosEncuenta2
                // imagen
                    $b64 = str_replace('data:image/png;base64,', '', $imgineb64);
                    $bin = base64_decode($b64);
                    $im = imageCreateFromString($bin);
                    if (!$im) {
                        die('Base64 value is not a valid image');
                    }
                    $img_file1 = '../imagenesEncuesta/'.$session->get('cve_encuesta').'_mapa.png';
                    // imagepng($im, $img_file1, 0);
                    $datosMapa = array(
                        "cve_encuesta" => $session->get('cve_encuesta'),  
                        "mapa" => $img_file1
                    );
                // imagen

            }else{
                $datosMapa = array();
                $response = array(
                    'alert' => 'error', // o 'error' dependiendo de si la operación tuvo éxito o no
                    'message' => '¡La operación no se pudo realizar con éxito, la imagen del mapa no esta incluida!' // un mensaje que describa el resultado de la operación
                ); 
            }

        }else if( $cve_estatus == 0){
             
            $datosEncuenta2 = array(
                "cve_estatus" => $this->request->getPost('cve_estatus'),
                "porque"    => $this->request->getPost('porque'),

            );
            $datosMapa=array();
        }
   
            if($cve_estatus == 1 && $datosEncuenta2 != [] && $datosMapa != [] ){
                $guardarInfoAgri =  $modeloPadron->guardarAgricultor($datosEncuenta2, $this->request->getPost('curp'), $datosMapa);
               
                if ($guardarInfoAgri == 1) {
                    imagepng($im, $img_file1, 0);

                    $response = array(
                        'alert' => 'success', // o 'error' dependiendo de si la operación tuvo éxito o no
                        'message' => '¡La operación se realizó con éxito!' // un mensaje que describa el resultado de la operación
                    );
                } else {
                    $response = array(
                        'alert' => 'error', // o 'error' dependiendo de si la operación tuvo éxito o no
                        'message' => '¡La operación no se pudo realizar con éxito, intentelo nuevamente!' // un mensaje que describa el resultado de la operación
                    );
                }

            }elseif($cve_estatus == 0 && $datosEncuenta2 != [] && $datosMapa == [] ){
                $guardarInfoAgri =  $modeloPadron->guardarAgricultor($datosEncuenta2, $this->request->getPost('curp'), $datosMapa);

                if ($guardarInfoAgri == 1) {
                    $response = array(
                        'alert' => 'success', // o 'error' dependiendo de si la operación tuvo éxito o no
                        'message' => '¡La operación se realizó con éxito!' // un mensaje que describa el resultado de la operación
                    );
                } else {
                    $response = array(
                        'alert' => 'error', // o 'error' dependiendo de si la operación tuvo éxito o no
                        'message' => '¡La operación no se pudo realizar con éxito, intentelo nuevamente!' // un mensaje que describa el resultado de la operación
                    );
                }
            }else{
                $response = array(
                    'alert' => 'error', // o 'error' dependiendo de si la operación tuvo éxito o no
                    'message' => '¡La operación no se pudo realizar con éxito, falta información!' // un mensaje que describa el resultado de la operación
                ); 
            }
        echo json_encode($response);

    }
    
    public function GuardarDatosProductivos(){
        $session = session();
        if($session->get('cve_encuesta') != ''){
            $DatosProductivos = array( 
                'folio' => $session->get('cve_encuesta'),
                'hectarea_sup_up'   => str_pad(($this->request->getPost("hectarea_sup_up")), 3, "0", STR_PAD_LEFT),
                'area_sup_up'       => str_pad(($this->request->getPost("area_sup_up")), 3, "0", STR_PAD_LEFT),
                'centiarea_sup_up'  => str_pad(($this->request->getPost('centiarea_sup_up')), 3, "0", STR_PAD_LEFT),
                'cve_tenencia'      => $this->request->getPost('cve_tenencia'),
            
                'sembro_ciclo_anterior' => $this->request->getPost('sembro_ciclo_anterior'),
                'motivo_no_sembro'      => $this->request->getPost('sembro_ciclo_anterior') == 0 ? $this->request->getPost('motivo_no_sembro') : '',
            
                //linera base
                    'ciclo_anterior_cultivo_1'      => $this->request->getPost('sembro_ciclo_anterior') == 1 ? $this->request->getPost('ciclo_anterior_cultivo_1') : '',
                    'hectarea_ciclo_anterior_1'     => $this->request->getPost('sembro_ciclo_anterior') == 1 ? str_pad(($this->request->getPost("hectarea_ciclo_anterior_1")), 3, "0", STR_PAD_LEFT) : '',
                    'area_ciclo_anterior_1'         => $this->request->getPost('sembro_ciclo_anterior') == 1 ? str_pad(($this->request->getPost("area_ciclo_anterior_1")), 3, "0", STR_PAD_LEFT) : '',
                    'centiarea_ciclo_anterior_1'    => $this->request->getPost('sembro_ciclo_anterior') == 1 ? str_pad(($this->request->getPost('centiarea_ciclo_anterior_1')), 3, "0", STR_PAD_LEFT) : '',
                    'ciclo_anterior_1'              => $this->request->getPost('sembro_ciclo_anterior') == 1 ? $this->request->getPost('ciclo_anterior_1') : '',
                    'regimen_hidrico_anterior_1'    => $this->request->getPost('sembro_ciclo_anterior') == 1 ? $this->request->getPost('regimen_hidrico_anterior_1') : '',
            
                    'ciclo_anterior_cultivo_2'      => $this->request->getPost('sembro_ciclo_anterior') == 1 ? $this->request->getPost('ciclo_anterior_cultivo_2') : '',
                    'hectarea_ciclo_anterior_2'     => $this->request->getPost('sembro_ciclo_anterior') == 1 ? str_pad(($this->request->getPost("hectarea_ciclo_anterior_2")), 3, "0", STR_PAD_LEFT) : '',
                    'area_ciclo_anterior_2'         => $this->request->getPost('sembro_ciclo_anterior') == 1 ? str_pad(($this->request->getPost("area_ciclo_anterior_2")), 3, "0", STR_PAD_LEFT) : '',
                    'centiarea_ciclo_anterior_2'    => $this->request->getPost('sembro_ciclo_anterior') == 1 ? str_pad(($this->request->getPost('centiarea_ciclo_anterior_2')), 3, "0", STR_PAD_LEFT): '',
                    'ciclo_anterior_2'              => $this->request->getPost('sembro_ciclo_anterior') == 1 ? $this->request->getPost('ciclo_anterior_2') : '',
                    'regimen_hidrico_anterior_2'    => $this->request->getPost('sembro_ciclo_anterior') == 1 ? $this->request->getPost('regimen_hidrico_anterior_2') : '',
            
                    'aplico_fertilizante'           => $this->request->getPost('sembro_ciclo_anterior') == 1 ? $this->request->getPost('aplico_fertilizante') : '',
                    'tipo_fertilizante_uso'         => $this->request->getPost('sembro_ciclo_anterior') == 1 && $this->request->getPost('aplico_fertilizante') == 1 ? $this->request->getPost('tipo_fertilizante_uso') : '',
                    'cual_fertilizante_uso'         => $this->request->getPost('sembro_ciclo_anterior') == 1 && $this->request->getPost('aplico_fertilizante') == 1 && $this->request->getPost('tipo_fertilizante_uso') == 1 ?  $this->request->getPost('cual_fertilizante_uso') : '',
                    'kg_fertilizante'               => $this->request->getPost('sembro_ciclo_anterior') == 1 && $this->request->getPost('aplico_fertilizante') == 1 ? $this->request->getPost('kg_fertilizante') : '',
            
                    'hectarea_cosecho_anterior_1'     => $this->request->getPost('sembro_ciclo_anterior') == 1 ? str_pad(($this->request->getPost("hectarea_cosecho_anterior_1")), 3, "0", STR_PAD_LEFT) : '',
                    'area_cosecho_anterior_1'         => $this->request->getPost('sembro_ciclo_anterior') == 1 ? str_pad(($this->request->getPost("area_cosecho_anterior_1")), 3, "0", STR_PAD_LEFT) : '',
                    'centiarea_cosecho_anterior_1'    => $this->request->getPost('sembro_ciclo_anterior') == 1 ? str_pad(($this->request->getPost('centiarea_cosecho_anterior_1')), 3, "0", STR_PAD_LEFT) : '',
                    'toneladas_cosecho_anterior_1'              => $this->request->getPost('sembro_ciclo_anterior') == 1 ? $this->request->getPost('toneladas_cosecho_anterior_1') : '',
                    // 'rendimiento_tonelada_cosecho_anterior_1'   => $this->request->getPost('sembro_ciclo_anterior') == 1 ? $this->request->getPost('rendimiento_tonelada_cosecho_anterior_1') : '',
            
                    'hectarea_cosecho_anterior_2'     => $this->request->getPost('sembro_ciclo_anterior') == 1 ? str_pad(($this->request->getPost("hectarea_cosecho_anterior_2")), 3, "0", STR_PAD_LEFT) : '',
                    'area_cosecho_anterior_2'         => $this->request->getPost('sembro_ciclo_anterior') == 1 ? str_pad(($this->request->getPost("area_cosecho_anterior_2")), 3, "0", STR_PAD_LEFT) : '',
                    'centiarea_cosecho_anterior_2'    => $this->request->getPost('sembro_ciclo_anterior') == 1 ? str_pad(($this->request->getPost('centiarea_cosecho_anterior_2')), 3, "0", STR_PAD_LEFT) : '',
                    'toneladas_cosecho_anterior_2'              => $this->request->getPost('sembro_ciclo_anterior') == 1 ? $this->request->getPost('toneladas_cosecho_anterior_2') : '',
                    // 'rendimiento_tonelada_cosecho_anterior_2'   => $this->request->getPost('sembro_ciclo_anterior') == 1 ? $this->request->getPost('rendimiento_tonelada_cosecho_anterior_2') : '',
            
                    'toneladas_autoconsumo_familiar_cult_1'     => $this->request->getPost('sembro_ciclo_anterior') == 1 ? $this->request->getPost('toneladas_autoconsumo_familiar_cult_1') : '',
                    'toneladas_autoconsumo_productivo_cult_1'   => $this->request->getPost('sembro_ciclo_anterior') == 1 ? $this->request->getPost('toneladas_autoconsumo_productivo_cult_1') : '',
                    'toneladas_comercializadora_cult_1'         => $this->request->getPost('sembro_ciclo_anterior') == 1 ? $this->request->getPost('toneladas_comercializadora_cult_1') : '',
                    'precio_venta_destino_prod_cult_1'         => $this->request->getPost('sembro_ciclo_anterior') == 1 ? $this->request->getPost('precio_venta_destino_prod_cult_1') : '',
                    // 'ingresos_comercializacion_destino_prod_cult_1'   => $this->request->getPost('sembro_ciclo_anterior') == 1 ? $this->request->getPost('ingresos_comercializacion_destino_prod_cult_1') : '',
            
                    'toneladas_autoconsumo_familiar_cult_2'     => $this->request->getPost('sembro_ciclo_anterior') == 1 ? $this->request->getPost('toneladas_autoconsumo_familiar_cult_2') : '',
                    'toneladas_autoconsumo_productivo_cult_2'   => $this->request->getPost('sembro_ciclo_anterior') == 1 ? $this->request->getPost('toneladas_autoconsumo_productivo_cult_2') : '',
                    'toneladas_comercializadora_cult_2'         => $this->request->getPost('sembro_ciclo_anterior') == 1 ? $this->request->getPost('toneladas_comercializadora_cult_2') : '',
                    'precio_venta_destino_prod_cult_2'         => $this->request->getPost('sembro_ciclo_anterior') == 1 ? $this->request->getPost('precio_venta_destino_prod_cult_2') : '',
                    // 'ingresos_comercializacion_destino_prod_cult_2'   => $this->request->getPost('sembro_ciclo_anterior') == 1 ? $this->request->getPost('ingresos_comercializacion_destino_prod_cult_2') : '',
            
                //ciclo actual
                    'sembro_este_ciclo'                         => $this->request->getPost('sembro_este_ciclo'),
                    'porque_no_sembro_ciclo_actual'             => $this->request->getPost('sembro_este_ciclo') == 0 ? $this->request->getPost('porque_no_sembro_ciclo_actual') : '',
            
                    'cultivo_actual_1'                          => $this->request->getPost('sembro_este_ciclo') == 1 ? $this->request->getPost('cultivo_actual_1') : '',
                    'hectarea_actual_cultivo_1'                 => $this->request->getPost('sembro_este_ciclo') == 1 ? $this->request->getPost('hectarea_actual_cultivo_1') : '',
                    'area_actual_cultivo_1'                     => $this->request->getPost('sembro_este_ciclo') == 1 ? $this->request->getPost('area_actual_cultivo_1') : '',
                    'centiarea_actual_cultivo_1'                => $this->request->getPost('sembro_este_ciclo') == 1 ? $this->request->getPost('centiarea_actual_cultivo_1') : '',
                    'ciclo_actual_cultivo_1'                    => $this->request->getPost('sembro_este_ciclo') == 1 ? $this->request->getPost('ciclo_actual_cultivo_1') : '',
                    'regimen_hidrico_actual_cultivo_1'          => $this->request->getPost('sembro_este_ciclo') == 1 ? $this->request->getPost('regimen_hidrico_actual_cultivo_1') : '',
            
                    'cultivo_actual_2'                          => $this->request->getPost('sembro_este_ciclo') == 1 ? $this->request->getPost('cultivo_actual_2') : '',
                    'hectarea_actual_cultivo_2'                 => $this->request->getPost('sembro_este_ciclo') == 1 ? $this->request->getPost('hectarea_actual_cultivo_2') : '',
                    'area_actual_cultivo_2'                     => $this->request->getPost('sembro_este_ciclo') == 1 ? $this->request->getPost('area_actual_cultivo_2') : '',
                    'centiarea_actual_cultivo_2'                => $this->request->getPost('sembro_este_ciclo') == 1 ? $this->request->getPost('centiarea_actual_cultivo_2') : '',
                    'ciclo_actual_cultivo_2'                    => $this->request->getPost('sembro_este_ciclo') == 1 ? $this->request->getPost('ciclo_actual_cultivo_2') : '',
                    'regimen_hidrico_actual_cultivo_2'          => $this->request->getPost('sembro_este_ciclo') == 1 ? $this->request->getPost('regimen_hidrico_actual_cultivo_2') : '',
            
                    'hectarea_cosecho_actual_1'                 => $this->request->getPost('sembro_este_ciclo') == 1 ? str_pad(($this->request->getPost("hectarea_cosecho_actual_1")), 3, "0", STR_PAD_LEFT) : '',
                    'area_cosecho_actual_1'                     => $this->request->getPost('sembro_este_ciclo') == 1 ? str_pad(($this->request->getPost("area_cosecho_actual_1")), 3, "0", STR_PAD_LEFT) : '',
                    'centiarea_cosecho_actual_1'                => $this->request->getPost('sembro_este_ciclo') == 1 ? str_pad(($this->request->getPost('centiarea_cosecho_actual_1')), 3, "0", STR_PAD_LEFT) : '',
                    'toneladas_cosecho_actual_1'                => $this->request->getPost('sembro_este_ciclo') == 1 ? $this->request->getPost('toneladas_cosecho_actual_1') : '',
                    // 'rendimiento_tonelada_cosecho_actual_1'     => $this->request->getPost('sembro_este_ciclo') == 1 ? $this->request->getPost('rendimiento_tonelada_cosecho_actual_1') : '',
            
                    'hectarea_cosecho_actual_2'                 => $this->request->getPost('sembro_este_ciclo') == 1 ? str_pad(($this->request->getPost("hectarea_cosecho_actual_2")), 3, "0", STR_PAD_LEFT) : '',
                    'area_cosecho_actual_2'                     => $this->request->getPost('sembro_este_ciclo') == 1 ? str_pad(($this->request->getPost("area_cosecho_actual_2")), 3, "0", STR_PAD_LEFT) : '',
                    'centiarea_cosecho_actual_2'                => $this->request->getPost('sembro_este_ciclo') == 1 ? str_pad(($this->request->getPost('centiarea_cosecho_actual_2')), 3, "0", STR_PAD_LEFT) : '',
                    'toneladas_cosecho_actual_2'                => $this->request->getPost('sembro_este_ciclo') == 1 ? $this->request->getPost('toneladas_cosecho_actual_2') : '',
                    // 'rendimiento_tonelada_cosecho_actual_2'     => $this->request->getPost('sembro_este_ciclo') == 1 ? $this->request->getPost('rendimiento_tonelada_cosecho_actual_2') : '',
            
                    'toneladas_autoconsumo_familiar_actual_cult_1'    => $this->request->getPost('sembro_este_ciclo') == 1 ? str_pad(($this->request->getPost("toneladas_autoconsumo_familiar_actual_cult_1")), 3, "0", STR_PAD_LEFT) : '',
                    'toneladas_autoconsumo_productivo_actual_cult_1'  => $this->request->getPost('sembro_este_ciclo') == 1 ? str_pad(($this->request->getPost("toneladas_autoconsumo_productivo_actual_cult_1")), 3, "0", STR_PAD_LEFT) : '',
                    'toneladas_comercializadora_actual_cult_1'        => $this->request->getPost('sembro_este_ciclo') == 1 ? str_pad(($this->request->getPost('toneladas_comercializadora_actual_cult_1')), 3, "0", STR_PAD_LEFT) : '',
                    'precio_venta_destino_prod_actual_cult_1'         => $this->request->getPost('sembro_este_ciclo') == 1 ? $this->request->getPost('precio_venta_destino_prod_actual_cult_1') : '',
                    // 'ingresos_comercializacion_destino_prod_actual_cult_1'  => $this->request->getPost('sembro_este_ciclo') == 1 ? $this->request->getPost('ingresos_comercializacion_destino_prod_actual_cult_1') : '',
            
                    'toneladas_autoconsumo_familiar_actual_cult_2'    => $this->request->getPost('sembro_este_ciclo') == 1 ? str_pad(($this->request->getPost("toneladas_autoconsumo_familiar_actual_cult_2")), 3, "0", STR_PAD_LEFT) : '',
                    'toneladas_autoconsumo_productivo_actual_cult_2'  => $this->request->getPost('sembro_este_ciclo') == 1 ? str_pad(($this->request->getPost("toneladas_autoconsumo_productivo_actual_cult_2")), 3, "0", STR_PAD_LEFT) : '',
                    'toneladas_comercializadora_actual_cult_2'        => $this->request->getPost('sembro_este_ciclo') == 1 ? str_pad(($this->request->getPost('toneladas_comercializadora_actual_cult_2')), 3, "0", STR_PAD_LEFT) : '',
                    'precio_venta_destino_prod_actual_cult_2'         => $this->request->getPost('sembro_este_ciclo') == 1 ? $this->request->getPost('precio_venta_destino_prod_actual_cult_2') : '',
                    // 'ingresos_comercializacion_destino_prod_actual_cult_2' => $this->request->getPost('sembro_este_ciclo') == 1 ? $this->request->getPost('ingresos_comercializacion_destino_prod_actual_cult_2') : '',
            
            );
            
            $modeloPadron = new PadronModelo();
            if($modeloPadron->GuardarDatosProductivos($DatosProductivos)){
                $response = array(
                    'alert' => 'success', // o 'error' dependiendo de si la operación tuvo éxito o no
                    'message' => '¡La operación se realizó con éxito!' // un mensaje que describa el resultado de la operación
                );
            }else{
                $response = array(
                    'alert' => 'error', // o 'error' dependiendo de si la operación tuvo éxito o no
                    'message' => '¡La operación no se pudo realizar con éxito, intentelo nuevamente!' // un mensaje que describa el resultado de la operación
                ); 
            }
        }else{
            $response = array(
                'alert' => 'error', // o 'error' dependiendo de si la operación tuvo éxito o no
                'message' => '¡La operación no se pudo realizar con éxito, intentelo nuevamente!' // un mensaje que describa el resultado de la operación
            ); 
        }
        
        // Devuelve la respuesta en formato JSON
        echo json_encode($response);
        
    }
        
    public function Tecnologicos(){
        $session = session();
        // var_dump($session->get('cve_encuesta'));

        // die();
        if($session->get('cve_encuesta') != ''){
            $DatosTecnologicos = array( 
                'cve_encuesta' => $session->get('cve_encuesta'),
                'cve_sistema_produccion'    => $this->request->getPost("cve_sistema_produccion"),
                'cve_tipo_semilla'          => $this->request->getPost("cve_tipo_semilla"),

                'hibrido_variedad'          => $this->request->getPost("cve_tipo_semilla") == 2 ? $this->request->getPost("hibrido_variedad") : '',

                'cve_adquisicion_semilla'   => $this->request->getPost("cve_adquisicion_semilla"),
                'cve_tiempo_semilla'        => $this->request->getPost("cve_tiempo_semilla"),
                'cve_tipo_siembra'          => $this->request->getPost("cve_tipo_siembra"),
                'semilla_gramos'            => $this->request->getPost("semilla_gramos"),
                'fecha_siembra'             => $this->request->getPost("fecha_siembra"),
                
                'resiembra'                 => $this->request->getPost("resiembra") == 1 ? 1 : 0 ,
                    'fecha_resiembra'       => $this->request->getPost("resiembra") == 1 ? $this->request->getPost("fecha_resiembra") : null,
                    'motivo_resiembra'      =>$this->request->getPost("resiembra") == 1 ? $this->request->getPost("motivo_resiembra"): null,

                'cuales_mas_labores'        => in_array("5", $this->request->getPost('laboresRealizo')) ? $this->request->getPost("cuales_mas_labores") : null,
                'utilizo_herbicidas'        => $this->request->getPost("utilizo_herbicidas") == 1 ? 1 : 0,
                'cuales_herbicidas'         => $this->request->getPost("cuales_herbicidas"),

                'fertilizante_bienestar'    => $this->request->getPost("fertilizante_bienestar"),
                    'ferti_kg_dap'                  => $this->request->getPost("fertilizante_bienestar") == 1 ? $this->request->getPost("ferti_kg_dap") : null,
                    'ferti_kg_urea'                 => $this->request->getPost("fertilizante_bienestar") == 1 ? $this->request->getPost("ferti_kg_urea") : null,

                'formato_unico_entrega'        => $this->request->getPost("formato_unico_entrega"),
                    'formato_porque'               => $this->request->getPost("formato_unico_entrega") == 0 ? $this->request->getPost("formato_porque") : null,
                    'reporte_hecho'                => $this->request->getPost("formato_unico_entrega") == 0 ? $this->request->getPost("reporte_hecho") : null,

                'fecha_recibio_fertilizante'        => $this->request->getPost("fecha_recibio_fertilizante"),
                'oportuno_siembra'                  => $this->request->getPost("oportuno_siembra"),
                'aplico_fertilizante'               => $this->request->getPost("aplico_fertilizante") ,
                'razon_aplicacion_fertilizante'     => $this->request->getPost("aplico_fertilizante") == 0 ? $this->request->getPost("razon_aplicacion_fertilizante") : null,
                
                    'hectareas_fertilizante1'  =>$this->request->getPost("aplico_fertilizante") == 1 ? str_pad(($this->request->getPost("hectareas_fertilizante1")), 3, "0", STR_PAD_LEFT): null,
                    'areas_fertilizante1'      =>$this->request->getPost("aplico_fertilizante") == 1 ?str_pad(($this->request->getPost("areas_fertilizante1")), 3, "0", STR_PAD_LEFT): null, 
                    'centiareas_fertilizante1' =>$this->request->getPost("aplico_fertilizante") == 1 ?str_pad(($this->request->getPost("centiareas_fertilizante1")), 3, "0", STR_PAD_LEFT): null,
                    'fecha_aplicacion1'=>$this->request->getPost("aplico_fertilizante") == 1 ? $this->request->getPost("fecha_aplicacion1"): null,
                    'dosis1'          =>$this->request->getPost("aplico_fertilizante") == 1 ? $this->request->getPost("dosis1"): null,
                    'urea_fecha1'       =>$this->request->getPost("aplico_fertilizante") == 1 ? $this->request->getPost("urea_fecha1"): null ,
                    'dosis_urea1'       =>$this->request->getPost("aplico_fertilizante") == 1 ? $this->request->getPost("dosis_urea1"): null ,
                    
                    'hectareas_fertilizante2'  =>$this->request->getPost("aplico_fertilizante") == 1 ? str_pad(($this->request->getPost("hectareas_fertilizante2")), 3, "0", STR_PAD_LEFT): null,
                    'areas_fertilizante2'      =>$this->request->getPost("aplico_fertilizante") == 1 ?str_pad(($this->request->getPost("areas_fertilizante2")), 3, "0", STR_PAD_LEFT): null, 
                    'centiareas_fertilizante2' =>$this->request->getPost("aplico_fertilizante") == 1 ?str_pad(($this->request->getPost("centiareas_fertilizante2")), 3, "0", STR_PAD_LEFT): null,
                    'fecha_aplicacion2'=>$this->request->getPost("aplico_fertilizante") == 1 ? $this->request->getPost("fecha_aplicacion2"): null,
                    'dosis2'          =>$this->request->getPost("aplico_fertilizante") == 1 ? $this->request->getPost("dosis2"): null,
                    'urea_fecha2'       =>$this->request->getPost("aplico_fertilizante") == 1 ? $this->request->getPost("urea_fecha2"): null ,
                    'dosis_urea2'       =>$this->request->getPost("aplico_fertilizante") == 1 ? $this->request->getPost("dosis_urea2"): null ,

                    'fertilizante_aplicado' =>$this->request->getPost("aplico_fertilizante") == 1 ? $this->request->getPost("fertilizante_aplicado"): null,
                    'porque_aplico_ferti'   =>$this->request->getPost("aplico_fertilizante") == 1 && $this->request->getPost("fertilizante_aplicado") ==  0 ? $this->request->getPost("porque_aplico_ferti"): null,
                    'cve_aplicacion_fertilizante' =>$this->request->getPost("aplico_fertilizante") == 1 ? $this->request->getPost("cve_aplicacion_fertilizante"): null ,
                    'cve_aplicacion'        =>$this->request->getPost("aplico_fertilizante") == 1 ? $this->request->getPost("cve_aplicacion"): null ,


                'fertilizante_complementario_up' =>$this->request->getPost("fertilizante_complementario_up"),
                
                'fertilizante_complementario_programa' => $this->request->getPost("fertilizante_complementario_programa"),
                'cve_quimico' =>$this->request->getPost("cve_quimico"),
                'cual_quimico' => $this->request->getPost("cve_quimico") == 1 ? $this->request->getPost("cual_quimico") : null,
                'dosis_quimico' =>$this->request->getPost("dosis_quimico"),
                'observaciones' =>$this->request->getPost("observaciones"),
                
                'riegos' => $this->request->getPost("riego"),

                'cve_temporal' => $this->request->getPost("cve_temporal"),
                'plagas' => $this->request->getPost("plagas"),
                'cual_plaga' => $this->request->getPost("plagas") == 1 ? $this->request->getPost("cual_plaga") : null,
                'cve_control_plaga' => $this->request->getPost("plagas") == 1 ? $this->request->getPost("cve_control_plaga") : null,

                'cve_enfermedad' => $this->request->getPost("cve_enfermedad"),
                'enfermedades_cuales' => $this->request->getPost("cve_enfermedad") == 1 ? $this->request->getPost("enfermedades_cuales") : null,
                'cve_metodo_enfermedades' => $this->request->getPost("cve_enfermedad") == 1 ? $this->request->getPost("cve_metodo_enfermedades") : null,

                'rendimiento_cultivo' => $this->request->getPost("rendimiento_cultivo"),
                'porcentaje_cultivo' => $this->request->getPost("rendimiento_cultivo") == 1 ? $this->request->getPost("porcentaje_cultivo") : null,
            );

            $cve_labor = $this->request->getPost('preparacion_terreno[]');
            $countLabor = count($cve_labor);
    
            $datos = array();
            for ($i = 0; $i < $countLabor; $i++) {
                if($i == 0){
                    $variable = "cve_preparacion";
                    $datos[$variable] = $cve_labor[$i];
                }else{
                    $variable = "cve_preparacion".($i);
                    $datos[$variable] = $cve_labor[$i];
                }
            }
            
            $preparacionTerreno = [
                'cve_encuesta' => $session->get('cve_encuesta')
            ];
            $totalDatos = $preparacionTerreno+$datos;

            $cve_laboress = $this->request->getPost('laboresRealizo[]');
            $countLab = count($cve_laboress);
            $datos = array();

            for ($i = 0; $i < $countLab; $i++) {
                if($i == 0){
                    $variable = "laborRealizo";
                    $datos[$variable] = $cve_laboress[$i];
                }else{
                    $variable = "laborRealizo".($i);
                    $datos[$variable] = $cve_laboress[$i];
                }

            }
    
            $labores = [
                'cve_encuesta' => $session->get('cve_encuesta')
            ];
            $totalabores = $labores+$datos;

            $modeloPadron = new PadronModelo();
            if($modeloPadron->TransaccionTecnologica($DatosTecnologicos,$totalDatos,$totalabores)){
                $response = array(
                    'alert' => 'success', // o 'error' dependiendo de si la operación tuvo éxito o no
                    'message' => '¡La operación se realizó con éxito!' // un mensaje que describa el resultado de la operación
                );
            }else{
                $response = array(
                    'alert' => 'error', // o 'error' dependiendo de si la operación tuvo éxito o no
                    'message' => '¡La operación no se pudo realizar con éxito, intentelo nuevamente!' // un mensaje que describa el resultado de la operación
                ); 
            }
            
        }
            // Devuelve la respuesta en formato JSON
        echo json_encode($response);
        
    }

    // guardado del step 5
    public function Capacitacion()
    {
        $modeloPadron = new PadronModelo();

        //step 5 
        $cve_encuesta             = $this->request->getPost('cve_encuesta');
        $temacapacitacion         = $this->request->getPost('cve_tema');
  
        if ($cve_encuesta != '') {

            $datosEncuenta = array(
                //aun falta definir los nombres de los campos para mandar a guardar 
                'cve_encuesta'             =>  $cve_encuesta,
                'cve_capacitacion'         =>  $this->request->getPost('cve_capacitacion'),
                'de_quien'                 =>  $this->request->getPost('de_quien'),
                'conoce_programa'          =>  $this->request->getPost('conoce_programa'),
                'capacitacion_programa'    =>  $this->request->getPost('capacitacion_programa'),
                'elaborar_biofertilizante' =>  $this->request->getPost('elaborar_biofertilizante'),
            );


            if ($temacapacitacion != '') {
                $temaCapPrep = array();
                $countTemas = count($temacapacitacion);

                for ($i = 0; $i < $countTemas; $i++) {
                    if($i == 0){
                        $variable = "cve_tema";
                        $temaCapPrep[$variable] = $temacapacitacion[$i];
                    }else{
                        $variable = "cve_tema".($i);
                        $temaCapPrep[$variable] = $temacapacitacion[$i];
                    }
                }

                $temaCapPrep['cve_encuesta'] = $cve_encuesta;
                // var_dump($temaCapPrep);

            } else {
                $temaCapPrep = array();
            }

            // guardar seccion 5
            $valorGuardado = $modeloPadron->Capacitacion($datosEncuenta, $temaCapPrep);

            if ($valorGuardado == 0) {
                $response = array(
                    'alert' => 'error', // o 'error' dependiendo de si la operación tuvo éxito o no
                    'message' => '¡La operación no se pudo realizar con éxito, intentelo nuevamente!' // un mensaje que describa el resultado de la operación
                );
            } else {
                $response = array(
                    'alert' => 'success', // o 'error' dependiendo de si la operación tuvo éxito o no
                    'message' => '¡La operación se realizó con éxito!' // un mensaje que describa el resultado de la operación
                );
            }
        } else {
            $response = array(
                'alert' => 'error', // o 'error' dependiendo de si la operación tuvo éxito o no
                'message' => '¡La operación no se pudo realizar con éxito, no existe una encuesta asignada en el proceso!' // un mensaje que describa el resultado de la operación
            );
        }

        // Devuelve la respuesta en formato JSON
        echo json_encode($response);
    }
    // guardado del step 5



    // guardar step 6
    public function Satisfaccion(){
        $modeloPadron = new PadronModelo();

        //step 6
        $cve_encuesta             = $this->request->getPost('cve_encuesta');
        //step 6

        if($cve_encuesta != ''){
            $datosEncuenta = array(
                //aun falta definir los nombres de los campos para mandar a guardar 
                'cve_encuesta'              => $cve_encuesta,
                'cedas_nombre'              => $this->request->getPost('cedas_nombre'),
                'conoce_recursos'           => $this->request->getPost('conoce_recursos'),
                'cve_recurso'               => $this->request->getPost('cve_recurso'),
                'quien'                     => $this->request->getPost('quien'),
                'convocado'                 => $this->request->getPost('convocado'),
                'tiempo_espera'             => $this->request->getPost('tiempo_espera'),
                'entrega_formal'            => $this->request->getPost('entrega_formal'),
                'explicacion_aplicarlo'     => $this->request->getPost('explicacion_aplicarlo'),
                'entrega_triptico'          => $this->request->getPost('entrega_triptico'),
                'claro_triptico'            => $this->request->getPost('claro_triptico'),
                'cve_recibio_fertilizante'  => $this->request->getPost('cve_recibio_fertilizante'),
                'cve_satisfecho'            => $this->request->getPost('cve_satisfecho'),
                'porque'                    => $this->request->getPost('porque'),
                'recibio_suficiente'        => $this->request->getPost('recibio_suficiente'),
                'mejora_produccion'         => $this->request->getPost('mejora_produccion'),
                'cve_porcentaje'            => $this->request->getPost('cve_porcentaje'),
                'porque_satisfaccion'       => $this->request->getPost('porque_satisfaccion'),
                'sugerencias'               => $this->request->getPost('sugerencia'),
            );

            // var_dump($datosEncuenta6);
            $valorGuardado = $modeloPadron->Satisfaccion($datosEncuenta);
            if ($valorGuardado == 0) {
                $response = array(
                    'alert' => 'error', // o 'error' dependiendo de si la operación tuvo éxito o no
                    'message' => '¡La operación no se pudo realizar con éxito, intentelo nuevamente!' // un mensaje que describa el resultado de la operación
                );
            } else {
                $response = array(
                    'alert' => 'success', // o 'error' dependiendo de si la operación tuvo éxito o no
                    'message' => '¡La operación se realizó con éxito!' // un mensaje que describa el resultado de la operación
                );
            }
        }else{
            $response = array(
                'alert' => 'error', // o 'error' dependiendo de si la operación tuvo éxito o no
                'message' => '¡La operación no se pudo realizar con éxito, no existe una encuesta asignada en el proceso!' // un mensaje que describa el resultado de la operación
            );
        }
        // Devuelve la respuesta en formato JSON
        echo json_encode($response);

    }

    public function obtieneEncuestadores(){
        $modeloAgricultura = new catAgriculturaModel();
        $resultados = $modeloAgricultura->encuestadores( $this->request->getPost('cve_encuestador'));
        echo json_encode($resultados);

    }

    // public function obtieneAgricultor(){
    //     $modeloAgricultura = new catAgriculturaModel();
    //     $session = session();
    //     //var_dump($this->request->getPost('cve_municipio_unidad'));
    //     $resultados = $modeloAgricultura->agricultor($this->request->getPost('curp'), $this->request->getPost('cve_municipio_unidad'));
    //     //$ses_data = ['cve_encuesta'  => $resultados[0]['cve_encuesta']];
    //     $session->set("cve_encuesta", $resultados[0]['cve_encuesta']);
    //     echo json_encode($resultados);
    // }

    // obtiene todos los agricultores correspondientes al encuestador
    public function obtieneAgricultores(){
        $modeloAgricultura = new catAgriculturaModel();
        // var_dump($this->request->getPost('cve_municipio_unidad'));
        // var_dump($this->request->getPost('cve_encuestador'));
        $session = session();
        $rol = $session->get("tipo");
        // var_dump($rol);
        $resultados = $modeloAgricultura->agricultores($this->request->getPost('cve_encuestador'), $this->request->getPost('cve_municipio_unidad'));
        echo json_encode($resultados);

    }

    public function obtieneMunicipios(){
        $modeloAgricultura = new catAgriculturaModel();
        //var_dump($this->request->getPost('cve_municipio'));
        $resultados = $modeloAgricultura->CatMunicipios($this->request->getPost('cve_municipio'));
        echo json_encode($resultados);

    }

    public function consultarCultivo(){
        $cultivo = new PadronModelo(); 
        $where = $this->request->getPost('ciclo') !=  "" ? " cve_cultivo=".$this->request->getPost('ciclo') : "";
        $resultado = $cultivo->catCultivos($where);
        echo json_encode($resultado);

    }

    public function obtieneLocalidades(){
        $modeloAgricultura = new catAgriculturaModel();
        //var_dump($this->request->getPost('cve_municipio'));
        $where = " cve_entidad = 16 and cve_mpioINEGI=".$this->request->getPost('cve_municipio_unidad');
        //var_dump($where);
        $resultados = $modeloAgricultura->CatLocalidades($where);
        echo json_encode($resultados);
        //echo json_encode("Hola");
    }

    public function obtieneEjidos(){
        $modeloAgricultura = new catAgriculturaModel();
        $resultados = $modeloAgricultura->CatEjidos($this->request->getPost('cve_municipio'));
        echo json_encode($resultados);
        //echo json_encode("Hola");
    }

    public function obtieneQuienContesta(){
        $modeloAgricultura = new catAgriculturaModel();
        $resultados = $modeloAgricultura->catQuienContesta();
        echo json_encode($resultados);
        //echo json_encode("Hola");
    }


    public function ObtenerInfoAgricultor(){
        $modeloPadron = new PadronModelo();
        $session = session();
        $resultados = $modeloPadron->buscarDatosAgricultor($this->request->getPost('curp'));

        // var_dump($resultados["RegistroAgricultura"][0]["cve_encuesta"]);
        $session->set("cve_encuesta", $resultados["RegistroAgricultura"][0]["cve_encuesta"]);
        echo json_encode($resultados);
    }

    public function modificaCve_estatus(){
        //$cve_encuesta = $this->request->getPost("cve_encuesta") != null ? $this->request->getPost("cve_encuesta") : '';
        $cve_encuesta  = array(
            "cve_encuesta" => $this->request->getPost("cve_encuesta"));
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode( view('Agricultura/estatusEncuesta', $cve_encuesta) );
    }


    public function cambiaStatusEncuesta()
    {
        $modeloAgricultor = new catAgriculturaModel();
        $resultado = $modeloAgricultor->cambiaEstatusEncuesta("1", $this->request->getPost("cve_encuesta"));
        if ($resultado == 0) {
            $response = array(
                'alert' => 'error', // o 'error' dependiendo de si la operación tuvo éxito o no
                'message' => '¡La operación no se pudo realizar con éxito, intentelo nuevamente!' // un mensaje que describa el resultado de la operación
            );
        } else {
            $response = array(
                'alert' => 'success', // o 'error' dependiendo de si la operación tuvo éxito o no
                'message' => '¡La operación se realizó con éxito!' // un mensaje que describa el resultado de la operación
            );
        }
        return ($this->index());
    }

    // ############################## PDFS 

    /**PDF NO*/
    public function mostrarReportePFB_NO($cve_encuesta)
    {
        $modeloAgricultor = new catAgriculturaModel();
        $resultado = $modeloAgricultor->pdfConsultaPFBNO($cve_encuesta);
        var_dump($resultado);
        if (count($resultado) > 0) {
            $pdf = new PDF();

            $pdf->setSourceFile("../plantillaspfd/PFB_FORMATO_NO.pdf");
            $pdf->AddPage();


            $size = $pdf->getTemplateSize($pdf->importPage(1));
            $pdf->SetMargins(22, 20, 22, true);
            $pdf->useTemplate($pdf->importPage(1), 5, 5, $size["width"] - 15, $size["height"]);
            $pdf->SetFont('times', 'B', '8');
            $pdf->setxy(145, 30);
            $pdf->Cell(10, 0, "XX" . $resultado[0]['cve_encuesta'] . "\n", 0, 0, 'L', 0, '', 0);
            $pdf->setxy(50, 36.5);
            $pdf->Cell(10, 0, $resultado[0]['cve_encuestador'] . "\n", 0, 0, 'L', 0, '', 0);
            $pdf->setxy(50, 42.5);
            $pdf->Cell(10, 0, $resultado[0]['nombre'] . "\n", 0, 0, 'L', 0, '', 0);
            $pdf->setxy(57, 48.5);
            $pdf->Cell(10, 0, $resultado[0]['fecha_encuesta'] . "\n", 0, 0, 'L', 0, '', 0);

            if ($resultado[0]['cve_estatus'] == 0) {
                $pdf->setxy(127, 66);
                $pdf->Cell(120, 0, "x\n", 0, 0, 'L', 0, '', 0);
                $pdf->setxy(60, 72.5);
                $pdf->Cell(125, 0, "x" . $resultado[0]['porque'] . "\n", 0, 0, 'L', 0, '', 0);
            } else if ($resultado[0]['cve_estatus'] == 2) {
                $pdf->setxy(83, 66);
                $pdf->Cell(120, 0, "x\n", 0, 0, 'L', 0, '', 0);
            }

            $pdf->SetFont('times', '', '8');
            $pdf->sety(62);

            //datos del beneficiario
            $pdf->SetFont('times', '', '8');
            $pdf->setxy(40, 86.5);
            $pdf->Cell(120, 0, $resultado[0]['municipio'] . "\n", 0, 0, 'L', 0, '', 0);
            $pdf->setxy(130, 86.5);
            $pdf->Cell(120, 0, $resultado[0]['nombrebene'] . "\n", 0, 0, 'L', 0, '', 0);
            $pdf->setxy(40, 92.5);
            $pdf->Cell(120, 0, $resultado[0]['curp'] . "\n", 0, 0, 'L', 0, '', 0);
            $pdf->setxy(130, 92.5);
            $pdf->Cell(120, 0, "" . $resultado[0]['folio_estatal'] . "\n", 0, 0, 'L', 0, '', 0);
            $pdf->setxy(25, 104.5);
            $pdf->Cell(100, 0, $resultado[0]['sexo'] . "\n", 0, 0, 'L', 0, '', 0);

            $pdf->setxy(83, 104.5);
            $pdf->Cell(100, 0, $resultado[0]['nacio'] . "\n", 0, 0, 'L', 0, '', 0);

            $pdf->output($cve_encuesta . '_NO' . '.pdf', 'I');
            exit();
        } else {
            echo "sin informacion para generar el pdf";
        }
    }

    /**PDF GRANDE */
    function mostrarReportePFB($cve_encuesta, $cve_estatus){
        $modeloAgricultor = new catAgriculturaModel();
        $resultado=$modeloAgricultor->pdfConsulta_PFB($cve_encuesta, $cve_estatus);
        //var_dump($resultado);
        if(count($resultado)>0){
            $pdf=new PDF();
        
            if($resultado[0]['cve_estatus'] == 0){
                $pdf=new PDF();

                $pdf->setSourceFile("../plantillaspfd/PFB_FORMATO_NO.pdf");
                $pdf->AddPage();
        
                
                $size = $pdf->getTemplateSize($pdf->importPage(1));
                $pdf->SetMargins(22, 20, 22, true);
                $pdf->useTemplate($pdf->importPage(1), 5, 5, $size["width"]-15, $size["height"]);
                $pdf->SetFont ('times', 'B', '8');
                $pdf->setxy(145,30);
                $pdf->Cell(10, 0, $resultado[0]['cve_encuesta']."\n", 0, 0, 'L', 0, '', 0);
                $pdf->setxy(50,36.5);
                $pdf->Cell(10, 0, $resultado[0]['cve_encuestador']."\n", 0, 0, 'L', 0, '', 0);
                $pdf->setxy(50,42.5);
                $pdf->Cell(10, 0, $resultado[0]['nombre']."\n", 0, 0, 'L', 0, '', 0);
                $pdf->setxy(57,48.5);
                $pdf->Cell(10, 0, $resultado[0]['fecha_encuesta']."\n", 0, 0, 'L', 0, '', 0);
        
                if($resultado[0]['cve_estatus'] == 0){
                    $pdf->setxy(127,66);
                    $pdf->Cell(120, 0, "x\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(60,72.5);
                    $pdf->Cell(125, 0, "x".$resultado[0]['porque']."\n", 0, 0, 'L', 0, '', 0);
                }else if($resultado[0]['cve_estatus'] == 2){
                    $pdf->setxy(83,66);
                    $pdf->Cell(120, 0, "x\n", 0, 0, 'L', 0, '', 0);
                }
                
                $pdf->SetFont ('times', '', '8');
                $pdf->sety(62);
                
                //datos del beneficiario
                $pdf->SetFont ('times', '', '8');
                $pdf->setxy(40,86.5);
                $pdf->Cell(120, 0, $resultado[0]['municipio']."\n", 0, 0, 'L', 0, '', 0);
                $pdf->setxy(130,86.5);
                $pdf->Cell(120, 0, $resultado[0]['nombrebene']."\n", 0, 0, 'L', 0, '', 0);
                $pdf->setxy(40,92.5);
                $pdf->Cell(120, 0, $resultado[0]['curp']."\n", 0, 0, 'L', 0, '', 0);
                $pdf->setxy(130,92.5);
                $pdf->Cell(120, 0, "".$resultado[0]['folio_estatal']."\n", 0, 0, 'L', 0, '', 0);
                $pdf->setxy(25,104.5);
                $pdf->Cell(100, 0, $resultado[0]['sexo']."\n", 0, 0, 'L', 0, '', 0);
        
                $pdf->setxy(83,104.5);
                $pdf->Cell(100, 0, "x1".$resultado[0]['nacio']."\n", 0, 0, 'L', 0, '', 0);
                
                $pdf->output($cve_encuesta.'_NO'.'.pdf', 'I');
                exit();
            }else if($resultado[0]['cve_estatus'] == 2){
            
                $pdf->setSourceFile("../plantillaspfd/PFB_FORMATO.pdf");
                $pdf->AddPage();
                
                $size = $pdf->getTemplateSize($pdf->importPage(1));
                $pdf->SetMargins(22, 20, 22, true);
                $pdf->useTemplate($pdf->importPage(1), 5, 5, $size["width"]-15, $size["height"]);
                $pdf->SetFont ('times', 'B', '8');
                $pdf->setxy(160,31.7);
                $pdf->Cell(10, 0, $resultado[0]['cve_encuesta']."\n", 0, 0, 'L', 0, '', 0);
                $pdf->setxy(55,37.7);
                $pdf->Cell(10, 0, $resultado[0]['cve_encuestador']."\n", 0, 0, 'L', 0, '', 0);
                $pdf->setxy(52,44.5);
                $pdf->Cell(10, 0, $resultado[0]['nombre']."\n", 0, 0, 'L', 0, '', 0);
                $pdf->setxy(60,50.5);
                $pdf->Cell(10, 0, $resultado[0]['fecha_encuesta']."\n", 0, 0, 'L', 0, '', 0);
        
                if($resultado[0]['cve_estatus'] == 0){
                    $pdf->setxy(130,69);
                    $pdf->Cell(120, 0, "x\n", 0, 0, 'L', 0, '', 0);
                }else if($resultado[0]['cve_estatus'] == 2){
                    $pdf->setxy(80,69);
                $pdf->Cell(120, 0, "x\n", 0, 0, 'L', 0, '', 0);
                }
                
                $pdf->SetFont ('times', '', '8');
                $pdf->sety(62);
                
                //datos del beneficiario
                $pdf->SetFont ('times', '', '8');
                $pdf->setxy(50,83);
                $pdf->Cell(120, 0, $resultado[0]['municipio']."\n", 0, 0, 'L', 0, '', 0);
                $pdf->setxy(130,83);
                $pdf->Cell(120, 0, $resultado[0]['nombrebene']."\n", 0, 0, 'L', 0, '', 0);
                $pdf->setxy(40,89);
                $pdf->Cell(120, 0, $resultado[0]['curp']."\n", 0, 0, 'L', 0, '', 0);
                $pdf->setxy(130,89);
                $pdf->Cell(120, 0, $resultado[0]['folio_estatal']."\n", 0, 0, 'L', 0, '', 0);
                ///
                $pdf->setxy(63,95);
                $pdf->Cell(120, 0, $resultado[0]['productor_contesta']."\n", 0, 0, 'L', 0, '', 0);
                //$pdf->setxy(93,95);
                //$pdf->Cell(120, 0, "X".$resultado[0]['quien_contesta']."\n", 0, 0, 'L', 0, '', 0);
                switch($resultado[0]['quien_contesta']){
                    case 1:
                        $pdf->setxy(90,95);
                        $pdf->Cell(3, 0, "HIJO(A)\n", 0, 0, 'L', 0, '', 0);
                        break;
                    case 2:
                        $pdf->setxy(90,95);
                        $pdf->Cell(3, 0, "ESPOSO (A)\n", 0, 0, 'L', 0, '', 0);
                        break;
                    case 3:
                        $pdf->setxy(90,95);
                        $pdf->Cell(3, 0, "NIETO(A)\n", 0, 0, 'L', 0, '', 0);
                        break;
                    case 4:
                        $pdf->setxy(90,95);
                        $pdf->Cell(3, 0, "SOBRINO(A)\n", 0, 0, 'L', 0, '', 0);
                        break;
                    case 5:
                        $pdf->setxy(90,95);
                        $pdf->Cell(3, 0, "TIO(A)\n", 0, 0, 'L', 0, '', 0);
                        break;
                    case 6:
                        $pdf->setxy(90,95);
                        $pdf->Cell(3, 0, "ABUELO(A)\n", 0, 0, 'L', 0, '', 0);
                        break;
                    case 7:
                        $pdf->setxy(90,95);
                        $pdf->Cell(3, 0, "PRIMO (A)\n", 0, 0, 'L', 0, '', 0);
                        break;
                    case 8:
                    //default:
                        $pdf->setxy(90,95);
                        $pdf->Cell(3, 0, "OTRO\n", 0, 0, 'L', 0, '', 0);
                        break;
                break;
                }
                $pdf->setxy(122,95);
                $pdf->Cell(120, 0,$resultado[0]['nombre_quien_contesta']."\n", 0, 0, 'L', 0, '', 0);
                /** */
                $pdf->SetFont ('times', '', '8');
                $pdf->setxy(40,101.5);
                $pdf->Cell(120, 0, $resultado[0]['localidad']."\n", 0, 0, 'L', 0, '', 0);
                $pdf->setxy(135,101.5);
                $pdf->Cell(120, 0, $resultado[0]['ejido']."\n", 0, 0, 'L', 0, '', 0);
                $pdf->setxy(30,113.5);
                $pdf->Cell(120, 0, $resultado[0]['latitud']."\n", 0, 0, 'L', 0, '', 0);
                $pdf->setxy(93,113.5);
                $pdf->Cell(100, 0, $resultado[0]['longitud']."\n", 0, 0, 'L', 0, '', 0);
        
                $pdf->setxy(25,125.5);
                $pdf->Cell(100, 0, $resultado[0]['sexo']."\n", 0, 0, 'L', 0, '', 0);
        
                $pdf->setxy(83,125.5);
                $pdf->Cell(100, 0, $resultado[0]['nacio']."\n", 0, 0, 'L', 0, '', 0);
        
                if(intval($resultado[0]['edad'])>=1900 and intval($resultado[0]['edad'])<2000)
                    $edad= 1900-intval($resultado[0]['edad']);
                elseif (intval($resultado[0]['edad'])>=2000)
                    $edad= 2000-intval($resultado[0]['edad']);
                else
                    $edad= intval($resultado[0]['edad']);
                $pdf->setxy(118,125.5);
                $pdf->Cell(100, 0, $edad."\n", 0, 0, 'L', 0, '', 0);
        
                $pdf->setxy(178,125.5);
                $pdf->Cell(100, 0, $resultado[0]['anio_productor']."\n", 0, 0, 'L', 0, '', 0);

                $pdf->setxy(35,132.5);
                $tel = isset($resultado[0]['telefono']) ? $resultado[0]['telefono'] : "";
                $pdf->Cell(100, 0, $tel."\n", 0, 0, 'L', 0, '', 0);

                if($resultado[0]['dialecto'] == '0'){
                    $pdf->setxy(81,138);
                    $pdf->Cell(100, 0, "X"."\n", 0, 0, 'L', 0, '', 0);
                }else{
                    $pdf->setxy(95,138);
                    $pdf->Cell(100, 0, "X"."\n", 0, 0, 'L', 0, '', 0);    
                }
                $pdf->setxy(118,138);
                $pdf->Cell(100, 0, $resultado[0]['dialecto_hablado']."\n", 0, 0, 'L', 0, '', 0);
        
                if($resultado[0]['leer_escribir'] == 0){
                    $pdf->setxy(72,144);
                    $pdf->Cell(100, 0, "X"."\n", 0, 0, 'L', 0, '', 0);
                }else{
                    $pdf->setxy(80,144);
                    $pdf->Cell(100, 0, "X"."\n", 0, 0, 'L', 0, '', 0);    
                }
                $pdf->setxy(130,144);
                $pdf->Cell(100, 0, $resultado[0]['anio_estudio']."\n", 0, 0, 'L', 0, '', 0);
        
                if($resultado[0]['beneficiario_bienestar'] == 0){
                    $pdf->setxy(128,150);
                    $pdf->Cell(100, 0, "X"."\n", 0, 0, 'L', 0, '', 0);
                }else{
                    $pdf->setxy(112,150.5);
                    $pdf->Cell(100, 0, "X"."\n", 0, 0, 'L', 0, '', 0);    
                }
            /**TErmina datos generales */
            /**Inicia datos PRoductor */
                    $pdf->setxy(70,162.5);
                    $pdf->Cell(120, 0, $resultado[0]['hectarea_sup_up']."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(85,162.5);
                    $pdf->Cell(120, 0, $resultado[0]['area_sup_up']."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(100,162.5);
                    $pdf->Cell(120, 0, $resultado[0]['centiarea_sup_up']."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(150,162.5);
                    $pdf->Cell(120, 0, strtoupper($resultado[0]['tipo_tenencia'])."\n", 0, 0, 'L', 0, '', 0);
            
                    /**LINEA BASE */
                    
                    if($resultado[0]['sembro_ciclo_anterior'] == 1){
                        $pdf->setxy(91,173.5);
                        $pdf->Cell(100, 0, "X"."\n", 0, 0, 'L', 0, '', 0);
                    }else{
                        $pdf->setxy(115,173.5);
                        $pdf->Cell(100, 0, "X"."\n", 0, 0, 'L', 0, '', 0);    
                    }
                    $pdf->setxy(150,173.5);
                    $pdf->Cell(100, 0, $resultado[0]['motivo_no_sembro']."\n", 0, 0, 'L', 0, '', 0);  
                    if($resultado[0]['ciclo_anterior_cultivo_1'] !=null or $resultado[0]['ciclo_anterior_cultivo_1'] != ''){
                        $pdf->setxy(31,185.5);
                        $pdf->Cell(100, 0, $resultado[0]['ciclo_anterior_cultivo1']."\n", 0, 0, 'L', 0, '', 0);
                        $pdf->setxy(68,185.5);
                        $pdf->Cell(100, 0, $resultado[0]['hectarea_ciclo_anterior_1']."\n", 0, 0, 'L', 0, '', 0);
                        $pdf->setxy(84,185.5);
                        $pdf->Cell(100, 0, $resultado[0]['area_ciclo_anterior_1']."\n", 0, 0, 'L', 0, '', 0);
                        $pdf->setxy(98,185.5);
                        $pdf->Cell(115, 0, $resultado[0]['centiarea_ciclo_anterior_1']."\n", 0, 0, 'L', 0, '', 0);
                        $pdf->SetFont ('times', '', '4');
                        if($resultado[0]['ciclo_anterior_1']==1){
                            $pdf->setxy(120,187.5);
                            $pdf->Cell(12, 0, "PRIMAVERA-VERANO\n", 0, 0, 'L', 0, '', 0);
                        }elseif($resultado[0]['ciclo_anterior_1']==2){
                            $pdf->setxy(120,187.5);
                            $pdf->Cell(12, 0, "OTOÑO-INVIERNO\n", 0, 0, 'L', 0, '', 0);    
                        }
                        $pdf->SetFont ('times', '', '7');
                        // $pdf->setxy(170,191.5);
                        // $pdf->Cell(100, 0, $resultado[0]['regimen_hidrico_anterior_1']."\n", 0, 0, 'L', 0, '', 0);
                        switch($resultado[0]['regimen_hidrico_anterior_1']){
                            case 1:
                                $pdf->setxy(170,186);
                                $pdf->Cell(3, 0, "TEMPORAL\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 2:
                                $pdf->setxy(170,186);
                                $pdf->Cell(3, 0, "HUMEDAD\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 3:
                                $pdf->setxy(170,186);
                                $pdf->Cell(3, 0, "PUNTA DE RIEGO\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 4:
                                $pdf->setxy(170,186);
                                $pdf->Cell(3, 0, "RIEGO\n", 0, 0, 'L', 0, '', 0);
                                break;
                        break;
                        }
                    }
                    /** */
                    $pdf->SetFont ('times', '', '8');
                    if($resultado[0]['ciclo_anterior_cultivo_2'] !=null or $resultado[0]['ciclo_anterior_cultivo_2'] != ''){
                        $pdf->setxy(31,191.5);
                        $pdf->Cell(100, 0, $resultado[0]['ciclo_anterior_cultivo2']."\n", 0, 0, 'L', 0, '', 0);
                        $pdf->setxy(68,191.5);
                        $pdf->Cell(100, 0, $resultado[0]['hectarea_ciclo_anterior_2']."\n", 0, 0, 'L', 0, '', 0);
                        $pdf->setxy(84,191.5);
                        $pdf->Cell(100, 0, $resultado[0]['area_ciclo_anterior_2']."\n", 0, 0, 'L', 0, '', 0);
                        $pdf->setxy(98,191.5);
                        $pdf->Cell(115, 0, $resultado[0]['centiarea_ciclo_anterior_2']."\n", 0, 0, 'L', 0, '', 0);
                        $pdf->SetFont ('times', '', '4');
                        if($resultado[0]['ciclo_anterior_2']==1){
                            $pdf->setxy(121,193.5);
                            $pdf->Cell(123, 0, "PRIMAVERA-VERANO\n", 0, 0, 'L', 0, '', 0);
                        }else if($resultado[0]['ciclo_anterior_2']==2){
                            $pdf->setxy(121,193.5);
                            $pdf->Cell(123, 0, "OTOÑO-INVIERNO\n", 0, 0, 'L', 0, '', 0);    
                        }
                        // $pdf->setxy(170,197.5);
                        // $pdf->Cell(100, 0, $resultado[0]['regimen_hidrico_anterior_2']."\n", 0, 0, 'L', 0, '', 0);
                        $pdf->SetFont ('times', '', '7');
                        switch($resultado[0]['regimen_hidrico_anterior_2']){
                            case 1:
                                $pdf->setxy(170,192.5);
                                $pdf->Cell(3, 0, "TEMPORAL\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 2:
                                $pdf->setxy(170,192.5);
                                $pdf->Cell(3, 0, "HUMEDAD\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 3:
                                $pdf->setxy(170,192.5);
                                $pdf->Cell(3, 0, "PUNTA DE RIEGO\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 4:
                                $pdf->setxy(170,192.5);
                                $pdf->Cell(3, 0, "RIEGO\n", 0, 0, 'L', 0, '', 0);
                                break;
                            break;
                            }
                    }
                        $pdf->SetFont ('times', '', '8');
                        if($resultado[0]['aplico_fertilizante'] == '0'){
                               $pdf->setxy(91,199);
                                $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            }else{
                                $pdf->setxy(70,199);
                                $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                                $pdf->setxy(174,205);
                                $pdf->Cell(3, 0, " ".$resultado[0]['kg_fertilizante']."\n", 0, 0, 'L', 0, '', 0);
                            }
                        $pdf->SetFont ('times', '', '5');

                        switch($resultado[0]['tipo_fertilizante_uso']){
                            case 1:
                                $pdf->setxy(87,206);
                                $pdf->Cell(3, 0,"QUÍMICO\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 2:
                                $pdf->setxy(87,206);
                                $pdf->Cell(3, 0, "ABONO / COMPOSTA\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 3:
                                $pdf->setxy(87,206);
                                $pdf->Cell(3, 0, "BIOFERTILIZANTE / ORGANICO\n", 0, 0, 'L', 0, '', 0);
                                break;
                        break;
                        }
                
                        switch($resultado[0]['cual_fertilizante_uso']){
                            case 1:
                                $pdf->setxy(128,206);
                                $pdf->Cell(3, 0,"UREA (46-0-0)\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 2:
                                $pdf->setxy(128,206);
                                $pdf->Cell(3, 0, "DAP (18-46-0)\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 3:
                                $pdf->setxy(128,206);
                                $pdf->Cell(3, 0, "MAP (12-61-0)n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 4:
                                $pdf->setxy(128,206);
                                $pdf->Cell(3, 0, "SULFATO DE AMONIO (21-0-0)\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 5:
                                $pdf->setxy(128,206);
                                $pdf->Cell(3, 0, "APP (10-34-0)\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 6:
                                $pdf->setxy(128,206);
                                $pdf->Cell(3, 0, "OTROS\n", 0, 0, 'L', 0, '', 0);
                                break;
                        break;
                        }
                        $pdf->SetFont ('times', '', '8');
                        /**QUÉ SUPERFICIE COSECHO? */
                        if($resultado[0]['ciclo_anterior_cultivo_1'] !=null or $resultado[0]['ciclo_anterior_cultivo_1'] != ''){
                            $pdf->setxy(31,217.5);
                            $pdf->Cell(100, 0, $resultado[0]['ciclo_anterior_cultivo1']."\n", 0, 0, 'L', 0, '', 0);
                            $pdf->setxy(68,217.5);
                            $pdf->Cell(100, 0, $resultado[0]['hectarea_cosecho_anterior_1']."\n", 0, 0, 'L', 0, '', 0);
                            $pdf->setxy(84,217.5);
                            $pdf->Cell(100, 0, $resultado[0]['area_cosecho_anterior_1']."\n", 0, 0, 'L', 0, '', 0);
                            $pdf->setxy(98,217.5);
                            $pdf->Cell(115, 0, $resultado[0]['centiarea_cosecho_anterior_1']."\n", 0, 0, 'L', 0, '', 0);
                            $pdf->setxy(143,217.5);
                            $pdf->Cell(123, 0, number_format($resultado[0]['toneladas_cosecho_anterior_1'], 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                            $pdf->setxy(181,217.5);
                            $hectareas = floatval($resultado[0]['hectarea_cosecho_anterior_1'].".".intval($resultado[0]['area_cosecho_anterior_1']).intval($resultado[0]['centiarea_cosecho_anterior_1']));
                            $division = (floatval($resultado[0]['toneladas_cosecho_anterior_1']) == 0 or intval($resultado[0]['hectarea_cosecho_anterior_1']) == 0) ? 0 : (floatval($resultado[0]['toneladas_cosecho_anterior_1']) / floatval($resultado[0]['hectarea_cosecho_anterior_1'].".".intval($resultado[0]['area_cosecho_anterior_1']).intval($resultado[0]['centiarea_cosecho_anterior_1'])));
                            $pdf->Cell(100, 0, number_format($division, 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                        }
                        if($resultado[0]['ciclo_anterior_cultivo_2'] != null or $resultado[0]['ciclo_anterior_cultivo_2'] !=''){
                        $pdf->setxy(31,223.5);
                        $pdf->Cell(100, 0, $resultado[0]['ciclo_anterior_cultivo2']."\n", 0, 0, 'L', 0, '', 0);
                        $pdf->setxy(68,223.5);
                        $pdf->Cell(100, 0, $resultado[0]['hectarea_cosecho_anterior_2']."\n", 0, 0, 'L', 0, '', 0);
                        $pdf->setxy(84,223.5);
                        $pdf->Cell(100, 0, $resultado[0]['area_cosecho_anterior_2']."\n", 0, 0, 'L', 0, '', 0);
                        $pdf->setxy(98,223.5);
                        $pdf->Cell(115, 0, $resultado[0]['centiarea_cosecho_anterior_2']."\n", 0, 0, 'L', 0, '', 0);
                        $pdf->setxy(143,223.5);
                        $pdf->Cell(123, 0, number_format($resultado[0]['toneladas_cosecho_anterior_2'], 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                        $pdf->setxy(181,223.5);
                        $division = (intval($resultado[0]['toneladas_cosecho_anterior_2']) == 0 or intval($resultado[0]['hectarea_cosecho_anterior_2']) == 0) ? 0 : (floatval($resultado[0]['toneladas_cosecho_anterior_2']) / floatval($resultado[0]['hectarea_cosecho_anterior_2'].".".intval($resultado[0]['area_cosecho_anterior_2']).intval($resultado[0]['centiarea_cosecho_anterior_2'])));
                        $pdf->Cell(100, 0, number_format($division, 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                        }
                        /** ¿CUÁL FUE EL DESTINO DE LA PRODUCCIÓN OBTENIDA*/
                        if($resultado[0]['ciclo_anterior_cultivo_1'] !=null or $resultado[0]['ciclo_anterior_cultivo_1'] != ''){
                            $pdf->setxy(31,242.5);
                            $pdf->Cell(100, 0, $resultado[0]['ciclo_anterior_cultivo1']."\n", 0, 0, 'L', 0, '', 0);
                            $pdf->setxy(70,242.5);
                            $pdf->Cell(110, 0, number_format(floatval($resultado[0]['toneladas_autoconsumo_familiar_cult_1']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                            $pdf->setxy(100,242.5);
                            $pdf->Cell(115, 0, number_format(floatval($resultado[0]['toneladas_autoconsumo_productivo_cult_1']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                            $pdf->setxy(126,242.5);
                            $pdf->Cell(115, 0, number_format(floatval($resultado[0]['toneladas_comercializadora_cult_1']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                            $pdf->setxy(150,242.5);
                            $pdf->Cell(115, 0, number_format(floatval($resultado[0]['precio_venta_destino_prod_cult_1']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                            $pdf->setxy(173,242.5);
                            $pdf->Cell(100, 0, number_format((floatval($resultado[0]['toneladas_comercializadora_cult_1'])*floatval($resultado[0]['precio_venta_destino_prod_cult_1'])), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                        }
                        if($resultado[0]['ciclo_anterior_cultivo_2'] != null or $resultado[0]['ciclo_anterior_cultivo_2'] != ''){
                        $pdf->setxy(31,248.5);
                        $pdf->Cell(100, 0, $resultado[0]['ciclo_anterior_cultivo2']."\n", 0, 0, 'L', 0, '', 0);
                        $pdf->setxy(70,248.5);
                        $pdf->Cell(110, 0, number_format(floatval($resultado[0]['toneladas_autoconsumo_familiar_cult_2']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                        $pdf->setxy(100,248.5);
                        $pdf->Cell(100, 0, number_format(floatval($resultado[0]['toneladas_autoconsumo_productivo_cult_2']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                        $pdf->setxy(126,248.5);
                        $pdf->Cell(115, 0, number_format(floatval($resultado[0]['toneladas_comercializadora_cult_2']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                        $pdf->setxy(150,248.5);
                        $pdf->Cell(118, 0, number_format(floatval($resultado[0]['precio_venta_destino_prod_cult_2']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                        $pdf->setxy(173,248.5);
                        $pdf->Cell(100, 0,number_format(floatval($resultado[0]['toneladas_comercializadora_cult_2'])*floatval($resultado[0]['precio_venta_destino_prod_cult_2']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                        // //entrega y calidad
                        }
                        $suma=(floatval($resultado[0]['toneladas_comercializadora_cult_1'])*floatval($resultado[0]['precio_venta_destino_prod_cult_1']))+(floatval($resultado[0]['toneladas_comercializadora_cult_2'])*floatval($resultado[0]['precio_venta_destino_prod_cult_2']));
                        $pdf->setxy(173,254.5);
                        $pdf->Cell(100, 0, number_format((float)$suma, 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                
                        /** */
                        $pdf->AddPage();
                        $tplidx = $pdf->ImportPage(2);
                        $pdf->useTemplate($tplidx);
                
                        $pdf->SetFont ('times', '', '8');
                        $pdf->sety(62);
                        if($resultado[0]['sembro_este_ciclo'] == 1){
                            $pdf->setxy(93,28.5);
                            $pdf->Cell(10, 0, "X"."\n", 0, 0, 'L', 0, '', 0);
                        }else{
                            $pdf->setxy(119,28.5);
                            $pdf->Cell(100, 0, "X"."\n", 0, 0, 'L', 0, '', 0);    
                        }
                        $pdf->setxy(161,28.5);
                        $pdf->Cell(100, 0, $resultado[0]['porque_no_sembro_ciclo_actual']."\n", 0, 0, 'L', 0, '', 0);  
                
                        /** CUAL(ES) CULTIVOS SEMBRÓ EN ESTE CICLO?:*/
                        $pdf->setxy(31,40.5);
                        $pdf->Cell(100, 0, $resultado[0]['cultivo_actual1']."\n", 0, 0, 'L', 0, '', 0);
                        $pdf->setxy(68,40.5);
                        $pdf->Cell(100, 0, $resultado[0]['hectarea_actual_cultivo_1']."\n", 0, 0, 'L', 0, '', 0);
                        $pdf->setxy(84,40.5);
                        $pdf->Cell(100, 0, $resultado[0]['area_actual_cultivo_1']."\n", 0, 0, 'L', 0, '', 0);
                        $pdf->setxy(101,40.5);
                        $pdf->Cell(115, 0, $resultado[0]['centiarea_actual_cultivo_1']."\n", 0, 0, 'L', 0, '', 0);
                        // $pdf->setxy(127,42.5);
                        // $pdf->Cell(123, 0, $resultado[0]['ciclo_actual_cultivo_1']."\n", 0, 0, 'L', 0, '', 0);
                        // $pdf->setxy(176,42.5);
                        // $pdf->Cell(100, 0, $resultado[0]['regimen_hidrico_actual_cultivo_1']."\n", 0, 0, 'L', 0, '', 0);
                
                        $pdf->SetFont ('times', '', '4');
                        if($resultado[0]['ciclo_actual_cultivo_1']=='1'){
                            $pdf->setxy(125,41.5);
                            $pdf->Cell(123, 0, "PRIMAVERA-VERANO\n", 0, 0, 'L', 0, '', 0);
                        }elseif($resultado[0]['ciclo_actual_cultivo_1']=='2'){
                            $pdf->setxy(125,41.5);
                            $pdf->Cell(123, 0, "OTOÑO-INVIERNO\n", 0, 0, 'L', 0, '', 0);    
                        }
                        $pdf->SetFont ('times', '', '7');
                        switch($resultado[0]['regimen_hidrico_actual_cultivo_1']){
                            case 1:
                                $pdf->setxy(176,40.5);
                                $pdf->Cell(3, 0, "TEMPORAL\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 2:
                                $pdf->setxy(176,40.5);
                                $pdf->Cell(3, 0, "HUMEDAD\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 3:
                                $pdf->setxy(176,40.5);
                                $pdf->Cell(3, 0, "PUNTA DE RIEGO\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 4:
                                $pdf->setxy(176,40.5);
                                $pdf->Cell(3, 0, "RIEGO\n", 0, 0, 'L', 0, '', 0);
                                break;
                        break;
                        }
                    /** */
                    $pdf->SetFont ('times', '', '8');
            
                    /** */
                    $pdf->setxy(31,46.5);
                    $pdf->Cell(100, 0, $resultado[0]['cultivo_actual2']."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(68,46.5);
                    $pdf->Cell(100, 0, $resultado[0]['hectarea_actual_cultivo_2']."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(84,46.5);
                    $pdf->Cell(100, 0, $resultado[0]['area_actual_cultivo_2']."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(101,46.5);
                    $pdf->Cell(115, 0, $resultado[0]['centiarea_actual_cultivo_2']."\n", 0, 0, 'L', 0, '', 0);
                    // $pdf->setxy(127,48.5);
                    // $pdf->Cell(123, 0, $resultado[0]['ciclo_actual_cultivo_2']."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(176,46.5);
                    $pdf->Cell(100, 0, $resultado[0]['regimen_hidrico_actual_cultivo_2']."\n", 0, 0, 'L', 0, '', 0);
            
                    $pdf->SetFont ('times', '', '4');
                    if($resultado[0]['ciclo_actual_cultivo_2']==1){
                        $pdf->setxy(125,47.5);
                        $pdf->Cell(123, 0, "PRIMAVERA-VERANO\n", 0, 0, 'L', 0, '', 0);
                    }elseif($resultado[0]['ciclo_actual_cultivo_2']==2){
                        $pdf->setxy(125,47.5);
                        $pdf->Cell(123, 0, "OTOÑO-INVIERNO\n", 0, 0, 'L', 0, '', 0);    
                    }
                    $pdf->SetFont ('times', '', '7');
                    switch($resultado[0]['regimen_hidrico_actual_cultivo_2']){
                        case 1:
                            $pdf->setxy(176,46.5);
                            $pdf->Cell(3, 0, "TEMPORAL\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 2:
                            $pdf->setxy(176,46.5);
                            $pdf->Cell(3, 0, "HUMEDAD\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 3:
                            $pdf->setxy(176,46.5);
                            $pdf->Cell(3, 0, "PUNTA DE RIEGO\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 4:
                            $pdf->setxy(176,46.5);
                            $pdf->Cell(3, 0, "RIEGO\n", 0, 0, 'L', 0, '', 0);
                            break;
                    break;
                    }
            
                    /** ¿QUÉ SUPERFICIE COSECHO?:*/
                    $pdf->setxy(31,60);
                    $pdf->Cell(100, 0, $resultado[0]['cultivo_actual1']."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(68,60);
                    $pdf->Cell(100, 0, $resultado[0]['hectarea_cosecho_actual_1']."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(84,60);
                    $pdf->Cell(100, 0, $resultado[0]['area_cosecho_actual_1']."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(100,60);
                    $pdf->Cell(115, 0, $resultado[0]['centiarea_cosecho_actual_1']."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(146,60);
                    $pdf->Cell(123, 0, number_format($resultado[0]['toneladas_cosecho_actual_1'], 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(188,60);
                    $division = (floatval($resultado[0]['toneladas_cosecho_actual_1']) == 0 or intval($resultado[0]['hectarea_cosecho_actual_1']) == 0) ? 0 : (floatval($resultado[0]['toneladas_cosecho_actual_1']) / floatval(intval($resultado[0]['hectarea_cosecho_actual_1']).".".intval($resultado[0]['area_cosecho_actual_1']).intval($resultado[0]['centiarea_cosecho_actual_1'])));
                    $pdf->Cell(100, 0, number_format($division, 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
            
                    if($resultado[0]['cultivo_actual2'] != null or $resultado[0]['cultivo_actual2'] != ''){
                    $pdf->setxy(31,66);
                    $pdf->Cell(100, 0, $resultado[0]['cultivo_actual2']."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(68,66);
                    $pdf->Cell(100, 0, $resultado[0]['hectarea_cosecho_actual_2']."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(84,66);
                    $pdf->Cell(100, 0, $resultado[0]['area_cosecho_actual_2']."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(100,66);
                    $pdf->Cell(115, 0, $resultado[0]['centiarea_cosecho_actual_2']."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(146,66);
                    $pdf->Cell(123, 0, number_format(floatval($resultado[0]['toneladas_cosecho_actual_2']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(188,66);
                    $division = (floatval($resultado[0]['toneladas_cosecho_actual_2']) == 0 or intval($resultado[0]['hectarea_cosecho_actual_2']) == 0) ? 0 : (floatval($resultado[0]['toneladas_cosecho_actual_2']) / floatval(intval($resultado[0]['hectarea_cosecho_actual_2']).".".intval($resultado[0]['area_cosecho_actual_2']).intval($resultado[0]['centiarea_cosecho_actual_2'])));
                    $pdf->Cell(100, 0, number_format($division, 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                    }
                    /** CUÁL FUE EL DESTINO DE LA PRODUCCIÓN OBTENIDA?*/
                    $pdf->setxy(31,85.5);
                    $pdf->Cell(100, 0, $resultado[0]['cultivo_actual1']."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(72,85.5);
                    $pdf->Cell(110, 0, number_format(floatval($resultado[0]['toneladas_autoconsumo_familiar_actual_cult_1']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(103,85.5);
                    $pdf->Cell(118, 0, number_format(floatval($resultado[0]['toneladas_autoconsumo_productivo_actual_cult_1']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(132,85.5);
                    $pdf->Cell(115, 0, number_format(floatval($resultado[0]['toneladas_comercializadora_actual_cult_1']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(158,85.5);
                    $pdf->Cell(123, 0, number_format(floatval($resultado[0]['precio_venta_destino_prod_actual_cult_1']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(182,85.5);
                    $pdf->Cell(100, 0, number_format(floatval($resultado[0]['toneladas_comercializadora_actual_cult_1'])*intval($resultado[0]['precio_venta_destino_prod_actual_cult_1']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
            
                    if($resultado[0]['cultivo_actual2'] != null or $resultado[0]['cultivo_actual2'] != ''){
                    $pdf->setxy(31,92);
                    $pdf->Cell(100, 0, $resultado[0]['cultivo_actual2']."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(72,92);
                    $pdf->Cell(110, 0, number_format(floatval($resultado[0]['toneladas_autoconsumo_familiar_actual_cult_2']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(103,92);
                    $pdf->Cell(118, 0, number_format(floatval($resultado[0]['toneladas_autoconsumo_productivo_actual_cult_2']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(132,92);
                    $pdf->Cell(115, 0, number_format(floatval($resultado[0]['toneladas_comercializadora_actual_cult_2']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(158,92);
                    $pdf->Cell(123, 0, number_format(floatval($resultado[0]['precio_venta_destino_prod_actual_cult_2']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                    $pdf->setxy(182,92);

                    $pdf->Cell(100, 0, number_format(floatval($resultado[0]['toneladas_comercializadora_actual_cult_2'])*floatval($resultado[0]['precio_venta_destino_prod_actual_cult_2']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                    
                    }
                    $suma=(floatval($resultado[0]['toneladas_comercializadora_actual_cult_1'])*floatval($resultado[0]['precio_venta_destino_prod_actual_cult_1']))+(floatval($resultado[0]['toneladas_comercializadora_actual_cult_2'])*floatval($resultado[0]['precio_venta_destino_prod_actual_cult_2']));
                    $pdf->setxy(182,97.5);
                    $pdf->Cell(100, 0, number_format($suma, 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
            
                /** termina datos productivos*/
                $pdf->SetFont ('times', '', '8');
                $pdf->setxy(77,113);
                $pdf->Cell(100, 0, $resultado[0]['sistema']."\n", 0, 0, 'L', 0, '', 0);
        
                /**Preparacion del terreno */
                $aux = explode(",", $resultado[0]['temas']);
                for($i = 0; $i < count($aux); $i++ ){
                    switch ($aux[$i]) {
                        case 1:
                            $pdf->setxy(27,125);
                            $pdf->Cell(100, 0, "x\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 2:
                            $pdf->setxy(55,125);
                            $pdf->Cell(100, 0, "x\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 3:
                            $pdf->setxy(83,125);
                            $pdf->Cell(100, 0, "x\n", 0, 0, 'L', 0, '', 0);
                        break;
                        case 4:
                            $pdf->setxy(105,125);
                            $pdf->Cell(100, 0, "x\n", 0, 0, 'L', 0, '', 0);
                        break;
                        case 5:
                            $pdf->setxy(127,125);
                            $pdf->Cell(100, 0, "x\n", 0, 0, 'L', 0, '', 0);
                        break;
                        case 6:
                            $pdf->setxy(152,125);
                            $pdf->Cell(100, 0, "x\n", 0, 0, 'L', 0, '', 0);
                        break;
                        case 7:
                            $pdf->setxy(173,125);
                            $pdf->Cell(100, 0, "x\n", 0, 0, 'L', 0, '', 0);
                        break;
                        case 8:
                            $pdf->setxy(194,125);
                            $pdf->Cell(100, 0, "x\n", 0, 0, 'L', 0, '', 0);
                        break;
                        }
                }
        
                $pdf->SetFont ('times', '', '8');
                switch ($resultado[0]['cve_tipo_semilla']) {
                    case 1:
                    $pdf->setxy(97,137);
                    $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                    break;
                    case 2:
                    $pdf->setxy(124,137);
                    $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                    break;
                    }
                
                $pdf->setxy(173,137);
                $pdf->Cell(100, 0, $resultado[0]['hibrido_variedad']."\n", 0, 0, 'L', 0, '', 0);
        
                $pdf->setxy(176,143.5);
                //$pdf->Cell(100, 0, $resultado[0]['cve_adquisicion_semilla']."\n", 0, 0, 'L', 0, '', 0);
                switch ($resultado[0]['cve_adquisicion_semilla']) {
                    case 1:
                        $pdf->setxy(54,143.5);
                    $pdf->Cell(15, 0, "DISTRIBUIDOR LOCAL\n", 0, 0, 'L', 0, '', 0);
                    break;
                    case 2:
                        $pdf->setxy(54,143.5);
                    $pdf->Cell(15, 0, "EMPRESA PRODUCTORA\n", 0, 0, 'L', 0, '', 0);
                    break;
                    case 3:
                        $pdf->setxy(54,143.5);
                        $pdf->Cell(15, 0, "PROGRAMA DE GOBIERNO DEL ESTADO\n", 0, 0, 'L', 0, '', 0);
                        break;
                    case 4:
                        $pdf->setxy(54,143.5);
                        $pdf->Cell(15, 0, "PROGRAMA DE GOBIERNO FEDERAL\n", 0, 0, 'L', 0, '', 0);
                        break;
                    case 5:
                        $pdf->setxy(54,143.5);
                        $pdf->Cell(15, 0, "PROGRAMA DE GOBIERNO MUNICIPAL\n", 0, 0, 'L', 0, '', 0);
                        break;
                    case 6:
                        $pdf->setxy(54,143.5);
                        $pdf->Cell(15, 0, "SEMILLA PROPIA\n", 0, 0, 'L', 0, '', 0);
                        break;
                    case 7:
                        $pdf->setxy(54,143.5);
                        $pdf->Cell(15, 0, "OTRAS\n", 0, 0, 'L', 0, '', 0);
                        break;
                    }
                switch ($resultado[0]['cve_tiempo_semilla']) {
                    case 1:
                        $pdf->setxy(175,143.5);
                        $pdf->Cell(15, 0, "PRIMERA VEZ \n", 0, 0, 'L', 0, '', 0);
                        break;
                    case 2:
                        $pdf->setxy(175,143.5);
                        $pdf->Cell(15, 0, "1 AÑO\n", 0, 0, 'L', 0, '', 0);
                        break;
                    case 3:
                        $pdf->setxy(175,143.5);
                        $pdf->Cell(15, 0, "2 AÑO\n", 0, 0, 'L', 0, '', 0);
                        break;
                    case 4:
                    $pdf->setxy(173,143.5);
                        $pdf->Cell(15, 0, "MAS DE 2 AÑOS \n", 0, 0, 'L', 0, '', 0);
                        break;
                    }
                    /**tipo de siembra realizo */
                    switch ($resultado[0]['cve_tipo_siembra']) {
                        case 1:
                            $pdf->setxy(54,150.5);
                            $pdf->Cell(15, 0, "NO MECANIZADA\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 2:
                            $pdf->setxy(54,150.5);
                            $pdf->Cell(15, 0, "MECANIZADA\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 3:
                            $pdf->setxy(54,150.5);
                            $pdf->Cell(15, 0, "SIEMBRA DE PRECISIÓN\n", 0, 0, 'L', 0, '', 0);
                            break;
                        }
                        $pdf->setxy(163,150.5);
                        $pdf->Cell(15, 0, $resultado[0]['semilla_gramos']."\n", 0, 0, 'L', 0, '', 0);
        
                        $pdf->setxy(50,159);
                        $pdf->Cell(15, 0, date('d-m-Y', strtotime($resultado[0]['fecha_siembra']))."\n", 0, 0, 'L', 0, '', 0);
                        switch ($resultado[0]['resiembra']) {
                            case 1:
                                
                                $pdf->setxy(136,159);
                                $pdf->Cell(15, 0, "X\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 0:
                                $pdf->setxy(119,159);
                                $pdf->Cell(15, 0, "X\n", 0, 0, 'L', 0, '', 0);
                                break;
                            }
                            $pdf->setxy(176,159);
                            if($resultado[0]['fecha_resiembra'] != NULL or $resultado[0]['fecha_resiembra'] != ''){
                                $fecha=date('d-m-Y', strtotime($resultado[0]['fecha_resiembra']));
                            }else{ $fecha="";}
                            $pdf->Cell(15, 0, $fecha."\n", 0, 0, 'L', 0, '', 0);
                            $pdf->setxy(83,174);
                            $pdf->Cell(15, 0, $resultado[0]['motivo_resiembra']."\n", 0, 0, 'L', 0, '', 0);
                            // switch ($resultado[0]['motivo_resiembra']) {
                            //     case 1:
                            //         $pdf->setxy(83,166);
                            //         $pdf->Cell(15, 0, "CALIDAD DE LA SEMILLA\n", 0, 0, 'L', 0, '', 0);
                            //         break;
                            //     case 2:
                            //         $pdf->setxy(83,166);
                            //         $pdf->Cell(15, 0, "CONDICIONES CLIMATOLÓGICAS\n", 0, 0, 'L', 0, '', 0);
                            //         break;
                            //     //case 3:
                            //     default:
                            //         $pdf->setxy(83,166);
                            //         $pdf->Cell(15, 0, "CONDICIONES SANITARIAS\n", 0, 0, 'L', 0, '', 0);
                            //         break;
                            //     }
                    
                    $aux = explode(",", $resultado[0]['labores']);
                    for($i = 0; $i < count($aux); $i++ ){
                    switch ($aux[$i]) {                
                        case 1:
                            $pdf->setxy(27,184);
                            $pdf->Cell(100, 0, "x\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 2:
                            $pdf->setxy(60,184);
                            $pdf->Cell(100, 0, "x\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 3:
                            $pdf->setxy(87,184);
                            $pdf->Cell(100, 0, "x\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 4:
                            $pdf->setxy(109,184);
                            $pdf->Cell(100, 0, "x\n", 0, 0, 'L', 0, '', 0);
                            break;
                        }
                    }
                    $pdf->setxy(139,184);
                    $pdf->Cell(100, 0, $resultado[0]['cuales_mas_labores']."\n", 0, 0, 'L', 0, '', 0);
            
                    switch ($resultado[0]['utilizo_herbicidas']) {
                        case 0:
                            $pdf->setxy(56,189.5);
                            $pdf->Cell(15, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 1:
                            $pdf->setxy(70,189.5);
                            $pdf->Cell(15, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                    }
                    $pdf->setxy(95,189.5);
                    $pdf->Cell(100, 0, $resultado[0]['cuales_herbicidas']."\n", 0, 0, 'L', 0, '', 0);
            
                    switch ($resultado[0]['fertilizante_bienestar']) {
                        case 0:
                            $pdf->setxy(140,202);
                            $pdf->Cell(15, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            $pdf->setxy(67,226.5);
                            $pdf->Cell(100, 0, "\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 1:
                            $pdf->setxy(125,202);
                            $pdf->Cell(15, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            $pdf->setxy(67,226.5);
                            $pdf->Cell(100, 0, date('d-m-Y', strtotime($resultado[0]['fecha_recibio_fertilizante']))."\n", 0, 0, 'L', 0, '', 0);
                            break;
                    }
            
                    $pdf->setxy(164,208.5);
                    $pdf->Cell(100, 0, $resultado[0]['ferti_kg_dap']."\n", 0, 0, 'L', 0, '', 0);
            
                    $pdf->setxy(191,208.5);
                    $pdf->Cell(100, 0, $resultado[0]['ferti_kg_urea']."\n", 0, 0, 'L', 0, '', 0);
            
                    // $pdf->setxy(166,218.5);
                    // $pdf->Cell(100, 0, $resultado[0]['formato_unico_entrega']."\n", 0, 0, 'L', 0, '', 0);
            
                    switch ($resultado[0]['formato_unico_entrega']) {
                        case 0:
                            $pdf->setxy(193,214.5);
                            $pdf->Cell(15, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            $pdf->setxy(38,220.5);
                            $pdf->Cell(100, 0, $resultado[0]['formato_porque']."\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 1:
                            $pdf->setxy(178,214.5);
                            $pdf->Cell(15, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        default:
                            $pdf->setxy(163,214.5);
                            $pdf->Cell(15, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        
                    }
                    // $pdf->setxy(38,220.5);
                    // $pdf->Cell(100, 0, $resultado[0]['formato_porque']."\n", 0, 0, 'L', 0, '', 0);
                    if($resultado[0]['formato_unico_entrega']==0){
                        switch ($resultado[0]['reporte_hecho']) {
                            case 0:
                                $pdf->setxy(193,220.5);
                                $pdf->Cell(15, 0, "X\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 1:
                                $pdf->setxy(178,220.5);
                                $pdf->Cell(15, 0, "X\n", 0, 0, 'L', 0, '', 0);
                                break;
                        }    
                    }
            
                          
                    
                    switch ($resultado[0]['oportuno_siembra']) {
                        case 1:
                            $pdf->setxy(178,226.5);
                            $pdf->Cell(15, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 0:
                            $pdf->setxy(193,226.5);
                            $pdf->Cell(15, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        
                    }
            
            //         /**fin datos tecnologicos */
            
                    $pdf->AddPage();
                    $tplidx = $pdf->ImportPage(3);
                    $pdf->useTemplate($tplidx);
                    if($resultado[0]['aplico_fertilizante'] == 0){
                        $pdf->setxy(135,27.5);
                        $pdf->Cell(100, 0, "X\n", 0, 0, 'L', 0, '', 0);
                    }else{
            
                            $pdf->setxy(168,27.5);
                            $pdf->Cell(100, 0, $resultado[0]['razon_aplicacion_fertilizante']."\n", 0, 0, 'L', 0, '', 0);
                    
                            $pdf->setxy(31,36.5);
                            $pdf->Cell(100, 0, $resultado[0]['cultivo_actual1']."\n", 0, 0, 'L', 0, '', 0);
                            $pdf->setxy(68,36.5);
                            $pdf->Cell(100, 0, $resultado[0]['hectareas_fertilizante1']."\n", 0, 0, 'L', 0, '', 0);
                            $pdf->setxy(84,36.5);
                            $pdf->Cell(100, 0, $resultado[0]['areas_fertilizante1']."\n", 0, 0, 'L', 0, '', 0);
                            $pdf->setxy(101,36.5);
                            $pdf->Cell(115, 0, $resultado[0]['centiareas_fertilizante1']."\n", 0, 0, 'L', 0, '', 0);
                            $pdf->setxy(120,36.5);
                            $pdf->Cell(123, 0, date('d-m-Y', strtotime($resultado[0]['fecha_aplicacion1']))."\n", 0, 0, 'L', 0, '', 0);
                            $pdf->setxy(146,36.5);
                            $pdf->Cell(100, 0, $resultado[0]['dosis1']."\n", 0, 0, 'L', 0, '', 0);
                            $pdf->setxy(166,36.5);
                            $pdf->Cell(100, 0, date('d-m-Y', strtotime($resultado[0]['urea_fecha1']))."\n", 0, 0, 'L', 0, '', 0);
                            $pdf->setxy(193,36.5);
                            $pdf->Cell(100, 0, $resultado[0]['dosis_urea1']."\n", 0, 0, 'L', 0, '', 0);
                    
                            /** */
                            if($resultado[0]['cultivo_actual2'] != ""){
                                $pdf->setxy(31,42.5);
                                $pdf->Cell(100, 0, $resultado[0]['cultivo_actual2']."\n", 0, 0, 'L', 0, '', 0);
                                $pdf->setxy(68,42.5);
                                $pdf->Cell(100, 0, $resultado[0]['hectareas_fertilizante2']."\n", 0, 0, 'L', 0, '', 0);
                                $pdf->setxy(84,42.5);
                                $pdf->Cell(100, 0, $resultado[0]['areas_fertilizante2']."\n", 0, 0, 'L', 0, '', 0);
                                $pdf->setxy(101,42.5);
                                $pdf->Cell(115, 0, $resultado[0]['centiareas_fertilizante2']."\n", 0, 0, 'L', 0, '', 0);
                                $pdf->setxy(120,42.5);
                                $pdf->Cell(123, 0, date('d-m-Y', strtotime($resultado[0]['fecha_aplicacion2']))."\n", 0, 0, 'L', 0, '', 0);
                                $pdf->setxy(146,42.5);
                                $pdf->Cell(100, 0, $resultado[0]['dosis2']."\n", 0, 0, 'L', 0, '', 0);
                                $pdf->setxy(166,42.5);
                                $pdf->Cell(100, 0, date('d-m-Y', strtotime($resultado[0]['urea_fecha2']))."\n", 0, 0, 'L', 0, '', 0);
                                $pdf->setxy(193,42.5);
                                $pdf->Cell(100, 0, $resultado[0]['dosis_urea2']."\n", 0, 0, 'L', 0, '', 0);
                            }
                            
                            if($resultado[0]['fertilizante_aplicado'] == 1){
                                $pdf->setxy(126,52.5);
                                $pdf->Cell(100, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            }else{
                                $pdf->setxy(146,52.5);
                                $pdf->Cell(100, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            }
                    
                            $pdf->setxy(176,52.5);
                            $pdf->Cell(100, 0, $resultado[0]['porque_aplico_ferti']."\n", 0, 0, 'L', 0, '', 0);
                    
                            // $pdf->setxy(51,59.5);
                            // $pdf->Cell(100, 0, "x".$resultado[0]['cve_aplicacion_fertilizante']."\n", 0, 0, 'L', 0, '', 0);
                            switch ($resultado[0]['cve_aplicacion_fertilizante']) {
                                case 0:
                                    $pdf->setxy(54,57.5);
                                    $pdf->Cell(3, 0, "MANUAL", 0, 0, 'L', 0, '', 0);
                                    break;
                                    case 1:
                                        $pdf->setxy(54,57.5);
                                    $pdf->Cell(3, 0, "MECANICA", 0, 0, 'L', 0, '', 0);
                                    break;
                                // case 1:
                                //     $pdf->setxy(54,57.5);
                                // $pdf->Cell(3, 0, "UREA (46-0-0)\n", 0, 0, 'L', 0, '', 0);
                                // break;
                                // case 2:
                                //     $pdf->setxy(54,57.5);
                                // $pdf->Cell(3, 0, "DAP (18-46-0)\n", 0, 0, 'L', 0, '', 0);
                                // break;
                                // case 3:
                                //     $pdf->setxy(54,57.5);
                                // $pdf->Cell(3, 0, "MAP (12-61-0)\n", 0, 0, 'L', 0, '', 0);
                                // break;
                                // case 4:
                                //     $pdf->setxy(54,57.5);
                                // $pdf->Cell(3, 0, "SULFATO DE AMONIO (21-0-0)\n", 0, 0, 'L', 0, '', 0);
                                // break;
                                // case 5:
                                //     $pdf->setxy(54,57.5);
                                // $pdf->Cell(3, 0, "APP (10-34-0)\n", 0, 0, 'L', 0, '', 0);
                                // break;
                                // case 6:
                                //     $pdf->setxy(54,57.5);
                                //     $pdf->Cell(3, 0, "OTROS\n", 0, 0, 'L', 0, '', 0);
                                // break;
                                // default:
                                //     $pdf->setxy(54,57.5);
                                //     $pdf->Cell(3, 0, "xx\n", 0, 0, 'L', 0, '', 0);
                                // break;
                                break;
                            }
                    
                            //   $pdf->setxy(150,60.5);
                            //   $pdf->Cell(3, 0, $resultado[0]['cve_aplicacion']."\n", 0, 0, 'L', 0, '', 0);
                            switch ($resultado[0]['cve_aplicacion']) {
                                case 1:
                                    $pdf->setxy(150,58.5);
                                $pdf->Cell(3, 0, "AL VOLEO\n", 0, 0, 'L', 0, '', 0);
                                break;
                                case 2:
                                    $pdf->setxy(150,58.5);
                                $pdf->Cell(3, 0, "MATEADO\n", 0, 0, 'L', 0, '', 0);
                                break;
                                case 3:
                                    $pdf->setxy(150,58.5);
                                $pdf->Cell(3, 0, "EN BANDA\n", 0, 0, 'L', 0, '', 0);
                                break;
                                case 4:
                                    $pdf->setxy(150,58.5);
                                $pdf->Cell(3, 0, "OTRA\n", 0, 0, 'L', 0, '', 0);
                                break;
                                }
                    
                            switch ($resultado[0]['fertilizante_complementario_up']) {
                            case 0:
                                $pdf->setxy(194,64.5);
                                $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 1:
                                $pdf->setxy(172,64.5);
                                $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                                break;
                            }  
                            
                            switch ($resultado[0]['fertilizante_complementario_programa']) {
                                case 0:
                                    $pdf->setxy(194,70.5);
                                    $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                                    break;
                                case 1:
                                    $pdf->setxy(172,70.5);
                                    $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                                    break;
                                }
                                $pdf->SetFont ('times', '', '5');
                            switch ($resultado[0]['cve_quimico']) {
                                case 1:
                                    $pdf->setxy(102,77.5);
                                    $pdf->Cell(3, 0, "QUÍMICO"."\n", 0, 0, 'L', 0, '', 0);
                                    break;
                                case 2:
                                    $pdf->setxy(102,77.5);
                                    $pdf->Cell(3, 0, "ABONO / COMPOSTA"."\n", 0, 0, 'L', 0, '', 0);
                                    break;
                                case 3:
                                    $pdf->setxy(102,77.5);
                                    $pdf->Cell(3, 0, "BIOFERTILIZANTE / ORGANICO"."\n", 0, 0, 'L', 0, '', 0);
                                    break;
                            }
                            $pdf->SetFont ('times', '', '8');
                            $pdf->setxy(148,76.5);
                            $pdf->Cell(3, 0, $resultado[0]['cual_quimico']."\n", 0, 0, 'L', 0, '', 0);
                    
                            $pdf->setxy(189,76.5);
                            $pdf->Cell(3, 0, $resultado[0]['dosis_quimico']."\n", 0, 0, 'L', 0, '', 0);
                    
                            $pdf->setxy(15,86.5);
                            $pdf->Cell(3, 0, $resultado[0]['observaciones']."\n", 0, 0, 'L', 0, '', 0);
                    
                            $pdf->setxy(56,112.5);
                            $pdf->Cell(3, 0, $resultado[0]['riegos']."\n", 0, 0, 'L', 0, '', 0);
                    }
                    switch ($resultado[0]['cve_temporal']) {
                    case 1:
                        $pdf->setxy(103,125.5);
                        $pdf->Cell(3, 0, "IGUAL AL AÑO PASADO\n", 0, 0, 'L', 0, '', 0);
                        break;
                    case 2:
                        $pdf->setxy(103,125.5);
                        $pdf->Cell(3, 0, "MENOR AL AÑO PASADO\n", 0, 0, 'L', 0, '', 0);
                        break;
                    case 3:
                        $pdf->setxy(103,125.5);
                        $pdf->Cell(3, 0, "MAYOR AL AÑO PASADO\n", 0, 0, 'L', 0, '', 0);
                        break;
                    }
        
                    switch ($resultado[0]['plagas']) {
                        case 0:
                            $pdf->setxy(109,137.5);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 1:
                            $pdf->setxy(125,137.5);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        }
        
                    $pdf->setxy(147,137.5);
                    $pdf->Cell(3, 0, $resultado[0]['cual_plaga']."\n", 0, 0, 'L', 0, '', 0);
                    
                    switch ($resultado[0]['cve_control_plaga']) {
                    case 1:
                        $pdf->setxy(125,143.5);
                        $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                        break;
                    case 2:
                        $pdf->setxy(146,143.5);
                        $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                        break;
                    case 3:
                        $pdf->setxy(167,143.5);
                        $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                        break;
                    case 4:
                        $pdf->setxy(194,143.5);
                        $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                        break;
                }
                switch ($resultado[0]['cve_enfermedad']) {
                    case 0:
                        $pdf->setxy(125,149.5);
                        $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                        break;
                    case 1:
                        $pdf->setxy(136,149.5);
                        $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                        break;
                }
                $pdf->setxy(156,149.5);
                $pdf->Cell(3, 0, $resultado[0]['enfermedades_cuales']."\n", 0, 0, 'L', 0, '', 0);     
                
                switch ($resultado[0]['cve_metodo_enfermedades']) {
                    case 1:
                        $pdf->setxy(125,156);
                        $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                        break;
                    case 2:
                        $pdf->setxy(146,156);
                        $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                        break;
                    case 3:
                        $pdf->setxy(172,156);
                        $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                        break;
                    }
                
                switch ($resultado[0]['rendimiento_cultivo']) {
                    case 0:
                        $pdf->setxy(142,161.5);
                        $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                        break;
                    case 1:
                        $pdf->setxy(157,161.5);
                        $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                        break;
                }
                $pdf->setxy(189,161.5);
                $pdf->Cell(3, 0, $resultado[0]['porcentaje_cultivo']."\n", 0, 0, 'L', 0, '', 0);
        
                $pdf->SetFont ('times', '', '8');
        
                switch ($resultado[0]['cve_capacitacion']) {
                case 1:
                    $pdf->setxy(92,174);
                    $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                    break;
                case 2:
                    $pdf->setxy(113,174);
                    $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                    break;
                case 3:
                    $pdf->setxy(137,174);
                    $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                    break;
                break;
                }
        
                $pdf->setxy(156,174);
                $pdf->Cell(3, 0, $resultado[0]['de_quien']."\n", 0, 0, 'L', 0, '', 0);
        
                if($resultado[0]['conoce_programa'] == 0){
                    $pdf->setxy(194,192.5);
                    $pdf->Cell(100, 0, "X"."\n", 0, 0, 'L', 0, '', 0);
                }else{
                    $pdf->setxy(178,192.5);
                    $pdf->Cell(100, 0, "X"."\n", 0, 0, 'L', 0, '', 0);    
                }
        
                if($resultado[0]['capacitacion_programa'] == 0){
                    $pdf->setxy(193,198.5);
                    $pdf->Cell(100, 0, "X"."\n", 0, 0, 'L', 0, '', 0);
                }else{
                    $pdf->setxy(177,198.5);
                    $pdf->Cell(100, 0, "X"."\n", 0, 0, 'L', 0, '', 0);    
                }
        
                if($resultado[0]['elaborar_biofertilizante'] == 0){
                    $pdf->setxy(193,204);
                    $pdf->Cell(100, 0, "X"."\n", 0, 0, 'L', 0, '', 0);
                }else{
                    $pdf->setxy(177,204);
                    $pdf->Cell(100, 0, "X"."\n", 0, 0, 'L', 0, '', 0);    
                }
                $aux = explode(",", $resultado[0]['temas']);
                        for($i = 0; $i < count($aux); $i++ ){
                        switch ($aux[$i]) {
                            case 1:
                                $pdf->setxy(45,186);
                                $pdf->Cell(100, 0, "X"."\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 2:
                                $pdf->setxy(60,186);
                                $pdf->Cell(100, 0, "X"."\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 3:
                                $pdf->setxy(87,186);
                                $pdf->Cell(100, 0, "X"."\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 4:
                            $pdf->setxy(103,186);
                                $pdf->Cell(100, 0, "X"."\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 5:
                                $pdf->setxy(125,186);
                                $pdf->Cell(100, 0, "X"."\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 6: 
                                $pdf->setxy(162,186);
                                $pdf->Cell(100, 0, "X"."\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 7:    
                                $pdf->setxy(194,186);
                                $pdf->Cell(100, 0, "X"."\n", 0, 0, 'L', 0, '', 0);
                                break;
                            break;
                        }
                        }
        
                $pdf->AddPage();
                $tplidx = $pdf->ImportPage(4);
                $pdf->useTemplate($tplidx);
        
                
                $pdf->SetFont ('times', '', '8');
                $pdf->sety(62);
                
                if($resultado[0]['fertilizante_bienestar']==1){
                        $pdf->setxy(83,31.5);

                        $pdf->Cell(100, 0, $resultado[0]['cedas']."\n", 0, 0, 'L', 0, '', 0);
                
                        $pdf->SetFont ('times', '', '8');
                        switch ($resultado[0]['conoce_recursos']) {
                        case 0:
                            $pdf->setxy(194,37.5);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 1:
                            $pdf->setxy(172,37.5);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        break;
                        }
                
                        $pdf->SetFont ('times', '', '8');
                        switch ($resultado[0]['cve_recurso']) {
                            case 1:
                                $pdf->setxy(38,44);
                                $pdf->Cell(100, 0, "X"."\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 2:
                                $pdf->setxy(75,44);
                                $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 3:
                                $pdf->setxy(114,44);
                                $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 4:
                                $pdf->setxy(135,44);
                                $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        break;
                        }
                        
                        $pdf->setxy(157,44);
                        $pdf->Cell(40, 0, $resultado[0]['quien']."\n", 0, 0, 'L', 0, '', 0);
                        
                        switch ($resultado[0]['convocado']) {
                            case 1:
                            $pdf->setxy(178,50);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                            case 0:
                            $pdf->setxy(195,50);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                            break;
                        }
                
                        $pdf->setxy(139,56);
                        //$pdf->Cell(40, 0, $resultado[0]['tiempo_espera']." horas.\n", 0, 0, 'L', 0, '', 0);
                        switch ($resultado[0]['tiempo_espera']) {
                            case 1:
                                $pdf->setxy(139,56);
                                $pdf->Cell(3, 0, "MENOS DE 1 HORA\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 2:
                                $pdf->setxy(139,56);
                                $pdf->Cell(3, 0, "DE 1 HORA A 2 HORAS\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 3:
                                $pdf->setxy(139,56);
                                $pdf->Cell(3, 0, "DE 2 HORA A 3 HORAS\n", 0, 0, 'L', 0, '', 0);
                                break;
                            case 4:
                                $pdf->setxy(139,56);
                                $pdf->Cell(3, 0, "MÁS DE 3 HORAS\n", 0, 0, 'L', 0, '', 0);
                                break;
                            break;
                        }
                
                        switch ($resultado[0]['entrega_formal']) {
                            case 1:
                            $pdf->setxy(178,62);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                            case 0:
                            $pdf->setxy(193, 62);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                            break;
                        }
                
                        switch ($resultado[0]['explicacion_aplicarlo']) {
                            case 1:
                            $pdf->setxy(178,68);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 0:
                            $pdf->setxy(193, 68);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        break;
                        }
                
                    switch ($resultado[0]['entrega_triptico']) {
                            case 1:
                            $pdf->setxy(178,74);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 0:
                            $pdf->setxy(193, 74);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        break;
                        }
                        
                    switch ($resultado[0]['claro_triptico']) {
                            case 1:
                            $pdf->setxy(178,81);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 0:
                            $pdf->setxy(193, 81);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        break;
                        }
                
                        $pdf->SetFont ('times', '', '8');
                        switch ($resultado[0]['cve_recibio_fertilizante']) {
                        case 1:
                            $pdf->setxy(98,86.5);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 2:
                            $pdf->setxy(126,86.5);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 3:
                            $pdf->setxy(148,86.5);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 4:
                            $pdf->setxy(168,86.5);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 5:
                            $pdf->setxy(193,86.5);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        }
                
                        $pdf->SetFont ('times', '', '8');
                        switch ($resultado[0]['cve_satisfecho']) {
                        case 1:
                            $pdf->setxy(39,98.5);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 2:
                            $pdf->setxy(70,98.5);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 3:
                            $pdf->setxy(108,98.5);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 4:
                            $pdf->setxy(146,98.5);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        }
                
                        $pdf->setxy(39,105);
                        $pdf->Cell(45, 0, $resultado[0]['porque']."\n", 0, 0, 'L', 0, '', 0);
                
                        switch ($resultado[0]['recibio_suficiente']) {
                            case 1:
                            $pdf->setxy(178,111);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 0:
                            $pdf->setxy(194, 111);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        break;
                        }
                
                        switch ($resultado[0]['mejora_produccion']) {
                            case 1:
                            $pdf->setxy(136,117);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 0:
                            $pdf->setxy(151, 117);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        break;
                        }
                        $pdf->setxy(172,117);
                        $pdf->Cell(45, 0, $resultado[0]['porque_satisfaccion']."\n", 0, 0, 'L', 0, '', 0);
                        
                        switch ($resultado[0]['cve_porcentaje']) {
                            case 1:
                            $pdf->setxy(109,123.5);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 2:
                            $pdf->setxy(140, 123.5);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                            case 3:
                            $pdf->setxy(168, 123.5);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        break;
                        }
                    }
                $pdf->SetFont ('times', '', '8');
                $pdf->setxy(13,135);
                $pdf->Cell(120, 0, $resultado[0]['sugerencias']."\n", 0, 0, 'L', 0, '', 0);
                //$pdf->MultiCell(0, 5,$resultado[0]['sugerencias']."\n", 1, 'J', 1, 2, '' ,'', true);
        
                switch ($resultado[0]['pisoVivienda']) {
                    case 1:
                        $pdf->setxy(163, 163.5);
                    $pdf->Cell(3, 0, "TIERRA\n", 0, 0, 'L', 0, '', 0);
                    break;
                case 2:
                    $pdf->setxy(163, 163.5);
                    $pdf->Cell(3, 0, "CEMENT0\n", 0, 0, 'L', 0, '', 0);
                    break;
                case 3:
                    $pdf->setxy(163, 163.5);
                    $pdf->Cell(3, 0, "MADERA-MOSAICO\n", 0, 0, 'L', 0, '', 0);
                    break;
                break;
                }
                switch ($resultado[0]['paredVivienda']) {
                    case 1:
                        $pdf->setxy(147, 169.5);
                    $pdf->Cell(3, 0, "TABIQUE / BLOCK / PIEDRA\n", 0, 0, 'L', 0, '', 0);
                    break;
                case 2:
                    $pdf->setxy(147, 169.5);
                    $pdf->Cell(3, 0, "ADOBE\n", 0, 0, 'L', 0, '', 0);
                    break;
                case 3:
                    $pdf->setxy(147, 169.5);
                    $pdf->Cell(3, 0, "MADERA\n", 0, 0, 'L', 0, '', 0);
                    break;
                case 4:
                    $pdf->setxy(147, 169.5);
                    $pdf->Cell(3, 0, "MATERIALES BLANDOS (CARRIZO, LAMINA, BAMBU, ETC)\n", 0, 0, 'L', 0, '', 0);
                        break;
                break;
                }
                switch ($resultado[0]['techoVivienda']) {
                    case 1:
                        $pdf->setxy(147, 176.5);
                    $pdf->Cell(3, 0, "LOSA CONCRETO\n", 0, 0, 'L', 0, '', 0);
                    break;
                case 2:
                    $pdf->setxy(147, 176.5);
                    $pdf->Cell(3, 0, "LAMINA\n", 0, 0, 'L', 0, '', 0);
                    break;
                case 3:
                    $pdf->setxy(147, 176.5);
                    $pdf->Cell(3, 0, "MADERA / PALMA\n", 0, 0, 'L', 0, '', 0);
                    break;
                break;
                }
                // 
                        $aux = explode(",", $resultado[0]['servVivienda']);
                        for($i = 0; $i < count($aux); $i++ ){
                        switch ($aux[$i]) {
                            case 1:
                            $pdf->setxy(103,188.5);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 2:
                            $pdf->setxy(130, 188.5);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 3:
                            $pdf->setxy(152, 188.5);
                            $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                            break;
                        case 4:
                                $pdf->setxy(189, 188.5);
                                $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                                break;
                        break;
                        }
                }
                $pdf->setxy(183,194);
                $pdf->Cell(120, 0, $resultado[0]['espaciosVivienda']."\n", 0, 0, 'L', 0, '', 0);
        
                switch ( $resultado[0]['servMedico']) {
                    case 1:
                    $pdf->setxy(95,206.5);
                    $pdf->Cell(3, 0, "IMSS\n", 0, 0, 'L', 0, '', 0);
                    break;
                case 2:
                    $pdf->setxy(95, 206.5);
                    $pdf->Cell(3, 0, "ISSSTE\n", 0, 0, 'L', 0, '', 0);
                    break;
                case 3:
                    $pdf->setxy(95, 206.5);
                    $pdf->Cell(3, 0, "FUERZAS ARMADAS\n", 0, 0, 'L', 0, '', 0);
                    break;
                case 4:
                        $pdf->setxy(95, 206.5);
                        $pdf->Cell(3, 0, "CENTRO DE SALUD\n", 0, 0, 'L', 0, '', 0);
                        break;
                case 5:
                    $pdf->setxy(95, 206.5);
                    $pdf->Cell(3, 0, "CENTRO DE SALUD\n", 0, 0, 'L', 0, '', 0);
                    break;
                break;
                }
            
                $pdf->setxy(176,212.5);
                $pdf->Cell(120, 0, number_format(floatval($resultado[0]['ingreso']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
        
                switch ( $resultado[0]['ingreso_adicional']) {
                    case 0:
                    $pdf->setxy(178,219);
                    $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                    break;
                case 1:
                    $pdf->setxy(194, 219);
                    $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                    break;
                }
                $suma=0;
                $pdf->setxy(18,233);
                $pdf->Cell(120, 0, number_format(floatval($resultado[0]['remesas']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                $suma+=intval($resultado[0]['remesas']);
                $pdf->setxy(48,233);
                $pdf->Cell(120, 0, number_format(floatval($resultado[0]['apoyoFamiliares']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                $suma+=intval($resultado[0]['apoyoFamiliares']);
                $pdf->setxy(75,233);
                $pdf->Cell(120, 0, number_format(floatval($resultado[0]['negocioPropio']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                $suma+=intval($resultado[0]['negocioPropio']);
                $pdf->setxy(101,233);
                $pdf->Cell(120, 0, number_format(floatval($resultado[0]['empleoFueraUP']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                $suma+=intval($resultado[0]['empleoFueraUP']);
                $pdf->setxy(128,233);
                $pdf->Cell(120, 0, number_format(floatval($resultado[0]['pension']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                $suma+=intval($resultado[0]['pension']);
                $pdf->setxy(158,233);
                $pdf->Cell(125, 0, number_format(floatval($resultado[0]['otros']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                $suma+=intval($resultado[0]['otros']);
                $pdf->setxy(181,233);
                $pdf->Cell(120, 0, "$ ".number_format(floatval($suma), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                //$suma+=intval($resultado[0]['otros']);
                
                $pdf->setxy(176,240.5);
                $pdf->Cell(120, 0, $resultado[0]['numeroIntegrantes']."\n", 0, 0, 'L', 0, '', 0);
                $pdf->setxy(176,246.5);
                $pdf->Cell(120, 0, number_format($resultado[0]['monto_total_menusal'], 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);
                switch ( $resultado[0]['apoyos_integrantes_hogar']) {
                    case 1:
                    $pdf->setxy(125,247);
                    $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                    break;
                case 0:
                    $pdf->setxy(103, 247);
                    $pdf->Cell(3, 0, "X\n", 0, 0, 'L', 0, '', 0);
                    break;
                }
                
                $suma+=floatval(($resultado[0]['ingreso'])/12)+floatval($resultado[0]['monto_total_menusal']);
                //$suma+=floatval($resultado[0]['monto_total_menusal']);
                $pdf->setxy(44,252.5);
                $pdf->Cell(10, 0, "$ ".$suma."\n", 0, 0, 'L', 0, '', 0);
                $pdf->setxy(173,252.5);
                //$suma+=intval($resultado[0]['ingreso'])+intval($resultado[0]['monto_total_menusal']);
                $pdf->Cell(10, 0, "$ ".number_format($suma/intval($resultado[0]['numeroIntegrantes']), 2, '.', ',')."\n", 0, 0, 'L', 0, '', 0);   
            
                $pdf->AddPage();
                $tplidx = $pdf->ImportPage(5);
                $pdf->useTemplate($tplidx);
        
                $pdf->SetFont ('times', 'B', '8');
                $pdf->setxy(15,26);
                //$pdf->Cell(10, 0, $resultado[0]['observaciones_encuestador']."\n", 0, 0, 'L', 0, '', 0);
                $pdf->MultiCell(185, 1,$resultado[0]['observaciones_encuestador'], 0, 'L', 0, 0, '', '', true);
               
        
                $pdf->Image($resultado[0]['foto2'],20,60,65,45);
                $pdf->Image($resultado[0]['foto1'],110,60,65,45); 
                $pdf->Image($resultado[0]['mapa'],20,114,65,45);      
                $pdf->output($cve_encuesta.'.pdf', 'I');
                exit();
            }
        }else{
            echo "sin informacion para generar el pdf";
            }
    }

    public function DescargarReporte(){
        $padronmodel = new PadronModelo(); 
        $info["reporte"] = $padronmodel->descargarReporte();
        foreach ($info["reporte"] as $row) {
            $key = array_keys($row);
        }
        $info["keys"] = $key;

        header("Content-type: text/xls");
        header("Content-Disposition: attachment; filename=Plantilla_VacantesAsignables.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        return view('Agricultura/ReporteExcel', $info);
    }

}
