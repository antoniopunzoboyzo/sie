<?= $this->extend('Layout') ?>
<?= $this->section('CSS') ?>
<!-- seccion wizard -->
<style>
    /* wizard */
    .has-error .form-control {
        border-color: #a94442;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075)
    }

    .has-error .form-control:focus {
        border-color: #843534;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px #ce8483;
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 6px #ce8483
    }


    input.invalid {
        background-color: #ffdddd;
    }

    select.invalid {
        background-color: #ffdddd;
    }

    textarea.invalid {
        background-color: #ffdddd;
    }

    /* input[type="radio"]:invalid {
        outline: 2px solid red;
        color: red;
    } */

    .stepwizard {
        /* display: table; */
        width: 100%;
        position: relative;
        margin-top: 50px;
        padding-top: 15px;
    }

    .stepwizard-step p {
        margin-top: 10px;
        font-size: 90.5%;

    }

    .stepwizard-step a {
        margin-top: 10px;
        font-size: 70.5%;

    }

    .setup-content label,
    p {
        font-size: 100%;
    }

    .stepwizard-row {
        /* display: table-row; */
        display: flex;
        flex-wrap: wrap;
        flex-direction: row;
        justify-content: space-around
    }

    .stepwizard-step button[disabled] {
        opacity: 1 !important;
        filter: alpha(opacity=100) !important;
    }

    .stepwizard-row:before {
        top: 14px;
        bottom: 0;
        position: absolute;
        content: " ";
        width: 95%;
        height: 1px;
        background-color: #ccc;
        z-order: 0;
    }

    .padreStepwizard {
        flex: 1;
        max-width: 200px;
        display: flex;
        justify-content: center;
        align-items: flex-start;
    }

    .stepwizard-step {
        display: table-cell;
        text-align: center;
        position: relative;
    }

    .btn-circle {
        width: 30px;
        height: 30px;
        text-align: center;
        padding: 6px 0;
        font-size: 12px;
        line-height: 1.428571429;
        border-radius: 15px;
    }

    section::before {
        content: "";
        position: absolute;
        background-size: cover;
        background-attachment: fixed;
        background-position: center;
        background-repeat: no-repeat;
        width: 100%;
        height: 100%;
        opacity: .2;
        background-image: url('imagenes/agricultura.jpeg');
    }

    /* wizard */
    /* mapa css */
    .log {
        position: absolute;
        top: 5px;
        left: 5px;
        height: 150px;
        width: 250px;
        overflow: scroll;
        background: white;
        margin: 0;
        padding: 0;
        list-style: none;
        font-size: 12px;
    }

    .log-entry {
        padding: 5px;
        border-bottom: 1px solid #d0d9e9;
    }

    .log-entry:nth-child(odd) {
        background-color: #e1e7f1;
    }

    .directions li span.arrow {
        display: inline-block;
        min-width: 28px;
        min-height: 28px;
        background-position: 0px;
        background-image: url("https://heremaps.github.io/maps-api-for-javascript-examples/map-with-route-from-a-to-b/img/arrows.png");
        position: relative;
        top: 8px;
    }

    .directions li span.depart {
        background-position: -28px;
    }

    .directions li span.rightturn {
        background-position: -224px;
    }

    .directions li span.leftturn {
        background-position: -252px;
    }

    .directions li span.arrive {
        background-position: -1288px;
    }

    /**demo.css */
    #map {
        width: 95%;
        height: 450px;
        background: grey;
    }

    #panel {
        width: 100%;
        height: 400px;
    }

    .imgMapa {
        width:30%;
        height: 40%;
    }

    /* mapa css */
</style>
<?= $this->endSection() ?>
<?= $this->section('css_linkvistas') ?>
<!--para testeo es el del link ya que los deja navegar entre pestañas -->
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">-->
<link rel="stylesheet" type="text/css" href="css/wizard.min.css" />
<link rel="stylesheet" type="text/css" href="css/mapa_css.css" />

<script>
    window.ENV_VARIABLE = 'developer.here.com'
</script>
<script src='https://js-examples.developer.here.com/pr/iframeheight.js'></script>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<?php
if ($encuestador["tipo"] == 2) :
    $miVariableJson = json_encode(intval($encuestador["tipo"]));
    $agricultor = json_encode($encuestaConsulta);
else :
    $miVariableJson = null;
endif;
?>
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="row">
                <div class="col-lg-12">
                    <button class="btn btn-primary pull-right mt-2" id="beneficiario" type="button" onclick="refrescar();">Nuevo Cuestionario<i class="icon-reload"></i> </button>
                </div>

                <div class="col-lg-12">

                    <!-- titulo -->
                    <div class="row">
                        <div class="col-md-12 mt-4">
                            <div class="container mt-5 text-center">
                                <h4 style="text-transform:uppercase;">SECRETARIA DE AGRICULTURA Y DESARROLLO RURAL MICHOACÁN</h4>
                                <p>Cuestionario de Evaluación del Programa de Fertilizantes para el Bienestar en el Estado de Michoacán
                                    Productores Apoyados</p>
                            </div>
                        </div>
                    </div>
                    <!-- titulo -->

                    <div class="row">
                        <div class="col-lg-12 beneficio_boton">
                            <!-- <h5 class="pull-right">Recibio el apoyo? aqui va</h5> -->
                            <!-- <a href="#" class="button button-small button-circle button-blue pull-right"><i class="icon-people-carry"></i>Recibio el apoyo? aqui va</a> -->
                            <a href="#" id="beneficiario_sagarpa" class="button button-border button-border-thin  button-dark pull-right"><i class="icon-people-carry"></i></a>

                        </div>
                    </div>

                    <!-- cabeza wizard -->
                    <div class="stepwizard">
                        <div class="stepwizard-row setup-panel">
                            <div class="padreStepwizard">
                                <div class="stepwizard-step">
                                    <a href="#step-1" type="button" class="btn  btn-primary btn-circle RegistroAgricultura">1</a>
                                    <p>INICIO CUESTIONARIO</p>
                                </div>
                            </div>
                            <div class="padreStepwizard">
                                <div class="stepwizard-step">
                                    <a href="#step-2" type="button" class="btn btn-default btn-circle RegistroAgricultura" disabled="disabled">2</a>
                                    <p>DATOS GENERALES</p>
                                </div>
                            </div>
                            <div class="padreStepwizard agricultor">
                                <div class="stepwizard-step ">
                                    <a href="#step-3" type="button" class="btn btn-default btn-circle RegistroProductivo" disabled="disabled">3</a>
                                    <p>DATOS PRODUCTIVOS</p>
                                </div>
                            </div>
                            <div class="padreStepwizard agricultor">
                                <div class="stepwizard-step ">
                                    <a href="#step-4" type="button" class="btn btn-default btn-circle Tecnologias" disabled="disabled">4</a>
                                    <p>DATOS TECNOLOGICOS</p>
                                </div>
                            </div>
                            <div class="padreStepwizard agricultor">
                                <div class="stepwizard-step ">
                                    <a href="#step-5" type="button" class="btn  btn-default   btn-circle Capacitacion" disabled="disabled">5</a>
                                    <p>CAPACITACION Y ASISTENCIA TECNICA</p>
                                </div>
                            </div>
                            <div class="padreStepwizard agricultor">
                                <div class="stepwizard-step ">
                                    <a href="#step-6" type="button" class="btn  btn-default  btn-circle Satisfaccion" disabled="disabled">6</a>
                                    <p>SATISFACCION DEL PRODUCTOR</p>
                                </div>
                            </div>
                            <div class="padreStepwizard agricultor">
                                <div class="stepwizard-step ">
                                    <a href="#step-7" type="button" class="btn btn-default btn-circle Condicion" disabled="disabled">7</a>
                                    <p>CONDICIÓN SOCIOECONÓMICA</p>
                                </div>
                            </div>
                            <div class="padreStepwizard agricultor">
                                <div class="stepwizard-step ">
                                    <a href="#step-8" type="button" class="btn btn-default  btn-circle fotos" disabled="disabled">8</a>
                                    <p>EVIDENCIA FOTOGRÁFICA</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--fin cabeza wizard -->

                    <!-- ############ formularios ############## -->

                    <!-- step 1 -->
                    <div class="row setup-content" id="step-1">
                        <div class="form-widget" data-alert-type="inline">
                            <div class="form-result"></div>
                            <form role="form" id="RegistroAgricultura" action="RegistroAgricultura" method="post" accept-charset="utf-8" autocomplete="off" enctype="multipart/form-data">
                                <div class="form-process"></div>
                                <div class="col-md-12">
                                    <h3> INICIO CUESTIONARIO</h3>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">CLAVE DEL ENCUESTADOR:</label>
                                                <input maxlength="8" type="text" disabled id="cve_encuestador" name="cve_encuestador" required="required" class="form-control" placeholder="Clave encuestador" value="<?= $encuestador["cve_encuestador"] ?>">
                                                <input type="hidden" id="cve_mpio_encuestador" name="cve_mpio_encuestador" value="<?= $encuestador["cve_municipio"] ?>" class="form-control">
                                            </div>
                                        </div>
                                        <div id="div_alert_encuestador" class="col-md-12 alert alert-danger d-none" role="alert">
                                            La clave ingresada del encuestador es incorrecta.
                                        </div>
                                    </div>

                                    <div class="row ">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">NOMBRE DEL ENCUESTADOR:</label>
                                                <input maxlength="100" disabled type="text" required="required" class="form-control" id="nombre_encuestador" name="nombre_encuestador" value="<?= $encuestador["nombre"] ?>" placeholder="Enter Last Name">
                                            </div>
                                            <div class="invalid-feedback">
                                                Debe ingresar el número del encuestador, de favor.
                                            </div>
                                        </div>
                                    </div>

                                    <?php if ($encuestador["tipo"] != 2) : ?>
                                        <div class="card p-2">
                                            <p> HOLA, BUENOS DÍAS/TARDES, SOY (NOMBRE DEL ENCUESTADOR), TRABAJO EN LA SECRETARÍA DE AGRICULTURA Y DESARROLLO RURAL (ME IDENTIFICO CON MI CREDENCIAL INSTITUCIONAL), EN EL DISTRITO DE DESARROLLO RURAL (NÚMERO Y NOMBRE), Y CENTRO DE APOYO AL DESARROLLO RURAL DEL MUNICIPIO DE (NOMBRE DEL CADER), LO ESTOY ABORDANDO PARA SOLICITARLE DE FAVOR QUE ME PERMITA HACERLE UNAS PREGUNTAS SOBRE EL PROGRAMA DE FERTILIZANTES PARA EL BIENESTAR, ESPERO NO QUITARLE MUCHO TIEMPO, SON PREGUNTAS SENCILLAS SOBRE LA FORMA EN LA QUE SEMBRÓ Y COSECHÓ SU CULTIVO CON LA APLICACIÓN DE LOS FERTILIZANTES. MUCHAS GRACIAS POR ATENDERME.
                                                LOS DATOS PERSONALES QUE AQUI SE REGISTRAN TIENEN EL CARACTER DE CONFIDENCIAL Y QUEDAN RESGUARDADOS EN LA SECRETARÍA DE AGRICULTURA.
                                            </p>
                                        </div>
                                    <?php endif; ?>

                                    <button class="btn btn-primary nextBtn gotoTop btn-lg pull-right" type="button">Siguiente</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- fin step 1 -->

                    <!-- step 2 -->
                    <div class="row setup-content" id="step-2">
                        <div class="form-widget">
                            <div class="form-result"></div>
                            <?php echo view("Agricultura/datosGenerales"); ?>
                        </div>
                    </div>

                    <!-- step 2 -->

                    <!-- step 3 -->
                    <div class="row setup-content" id="step-3">
                        <div class="form-widget" id="Productivo">
                            <div class="form-result"></div>
                            <div class="col-md-12">
                                <h3> DATOS PRODUCTIVOS</h3>
                                <form action="guardarDatosProductivos" method="post" role="form" id="RegistroProductivo" accept-charset="utf-8" autocomplete="off" enctype="multipart/form-data">
                                    <input class="cve_encuesta" name="cve_encuesta_productivo" type="text" hidden>
                                    <!-- superficie up -->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="control-label">¿CUÁL ES LA SUPERFICIE TOTAL DE SU UP?</label>
                                            <div class="form-group">
                                                <div class="col-md-4">
                                                    <label class="control-label">HECTÁREAS</label>
                                                    <input type="text" name="hectarea_sup_up" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="HECTÁREAS" required>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">ÁREAS</label>
                                                    <input type="text" name="area_sup_up" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="ÁREAS" required>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">Centiareas</label>
                                                    <input type="text" name="centiarea_sup_up" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="CENTIAREAS" required>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">TIPO DE TENENCIA</label>
                                                <select name="cve_tenencia" id="cve_tenencia" class="form-control" required>
                                                    <option value="">SELECCIONE UNA OPCIÓN</option>
                                                    <?php foreach ($catTenencia as $tenencia) : ?>
                                                        <option value="<?= $tenencia["cve_tenencia"] ?>"><?= $tenencia["tipo_tenencia"] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- linea base -->
                                    <div class="row my-4">
                                        <div class="card">
                                            <div class="row">
                                                <div class="card-header">
                                                    <h4>LINEA BASE</h4>
                                                </div>
                                                <div class="col-md-6 my-3">
                                                    <div class="form-group">
                                                        <label class="control-label">¿Sembró en el ciclo inmediato anterior?</label>
                                                        <div class="form-check">
                                                            <input class="form-check-input" name="sembro_ciclo_anterior" type="radio" name="flexRadioDefault" id="sembro" value="1" required>
                                                            <label class="form-check-label" for="sembro">
                                                                Sembro
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" name="sembro_ciclo_anterior" type="radio" name="flexRadioDefault" id="nosembro" value="0" required>
                                                            <label class="form-check-label" for="nosembro">
                                                                No sembro
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 nosembroMotivo my-3">
                                                    <div class="form-group">
                                                        <label class="control-label">¿POR QUÉ MOTIVO? </label>
                                                        <input maxlength="100" type="text" name="motivo_no_sembro" class="form-control" placeholder="Ingresa el motivo de no sembrar">
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- cultivo 1 CULTIVOS SEMBRO EL CICLO ANTERIOR?-->
                                            <div class="row col24">
                                                <label class="control-label">¿CUAL(ES) CULTIVOS SEMBRO EL CICLO ANTERIOR?: <div id="SembrarmasdelaUPLineaBAse" style="color:red">No puedes sembrar más de la superficie total de la UP</div></label>

                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label class="control-label">CULTIVO 1</label>
                                                        <select name="ciclo_anterior_cultivo_1" id="ciclo_anterior_cultivo_1" class="form-control">
                                                            <option value="">SELECCIONE UNA OPCIÓN</option>
                                                            <?php foreach ($catCultivos as $cultivo) : ?>
                                                                <option value="<?= $cultivo["cve_cultivo"] ?>"><?= $cultivo["cultivo"] ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <div class="col-md-4">
                                                            <label class="control-label">HECTÁREAS</label>
                                                            <input type="text" name="hectarea_ciclo_anterior_1" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="HECTÁREAS">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label class="control-label">ÁREAS</label>
                                                            <input type="text" name="area_ciclo_anterior_1" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="ÁREAS">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label class="control-label">Centiareas</label>
                                                            <input type="text" name="centiarea_ciclo_anterior_1" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="CENTIAREAS">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label class="control-label">CICLO:</label>
                                                        <select name="ciclo_anterior_1" id="ciclo_anterior_1" class="form-control">
                                                            <option value="">SELECCIONE UNA OPCIÓN</option>
                                                            <?php foreach ($catCiclo as $ciclo) : ?>
                                                                <option value="<?= $ciclo["cve_ciclo"] ?>"><?= $ciclo["ciclo"] ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label"> REGIMEN HIDRICO:</label>
                                                        <select name="regimen_hidrico_anterior_1" id="regimen_hidrico_anterior_1" class="form-control">
                                                            <option value="">SELECCIONE UNA OPCIÓN</option>
                                                            <?php foreach ($catRegimen as $registro) : ?>
                                                                <option value="<?= $registro["cve_regimen"] ?>"><?= $registro["regimen"] ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- cultivo 2 CULTIVOS SEMBRO EL CICLO ANTERIOR?-->
                                            <div class="row col24">
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label class="control-label">CULTIVO 2</label>
                                                        <select name="ciclo_anterior_cultivo_2" id="ciclo_anterior_cultivo_2" class="form-control">
                                                            <option value="">SELECCIONE UNA OPCIÓN</option>
                                                            <?php foreach ($catCultivos as $cultivo) : ?>
                                                                <option value="<?= $cultivo["cve_cultivo"] ?>"><?= $cultivo["cultivo"] ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-5 NoOcupamosSegundoCultivo">
                                                    <div class="form-group">
                                                        <div class="col-md-4">
                                                            <label class="control-label">HECTÁREAS</label>
                                                            <input type="text" name="hectarea_ciclo_anterior_2" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="HECTÁREAS">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label class="control-label">ÁREAS</label>
                                                            <input type="text" name="area_ciclo_anterior_2" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="ÁREAS">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label class="control-label">Centiareas</label>
                                                            <input type="text" name="centiarea_ciclo_anterior_2" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="CENTIAREAS">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 NoOcupamosSegundoCultivo">
                                                    <div class="form-group">
                                                        <label class="control-label">CICLO:</label>
                                                        <select name="ciclo_anterior_2" id="ciclo_anterior_2" class="form-control">
                                                            <option value="">SELECCIONE UNA OPCIÓN</option>
                                                            <?php foreach ($catCiclo as $ciclo) : ?>
                                                                <option value="<?= $ciclo["cve_ciclo"] ?>"><?= $ciclo["ciclo"] ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 NoOcupamosSegundoCultivo">
                                                    <div class="form-group">
                                                        <label class="control-label"> REGIMEN HIDRICO:</label>
                                                        <select name="regimen_hidrico_anterior_2" id="regimen_hidrico_anterior_2" class="form-control">
                                                            <option value="">SELECCIONE UNA OPCIÓN</option>
                                                            <?php foreach ($catRegimen as $registro) : ?>
                                                                <option value="<?= $registro["cve_regimen"] ?>"><?= $registro["regimen"] ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>

                                            <!-- aplico fertilizante -->
                                            <div class="row col24">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label">¿APLICO FERTILIZANTE EN SU UP?</label>
                                                        <div class="form-check">
                                                            <input class="form-check-input" name="aplico_fertilizante" value="1" type="radio" id="SiaplicoFertilizanteUP">
                                                            <label class="form-check-label" for="SiaplicoFertilizanteUP">
                                                                Si
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" name="aplico_fertilizante" value="0" type="radio" id="noaplicoFertilizanteUP">
                                                            <label class="form-check-label" for="noaplicoFertilizanteUP">
                                                                No
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-3 aplicoFertilizante">
                                                    <div class="form-group">
                                                        <label class="control-label">¿QUÉ TIPO DE FERTILIZANTE UTILIZÓ Y EN QUE DOSIS?:</label>
                                                        <select name="tipo_fertilizante_uso" id="tipoFertilizanteuso" class="form-control">
                                                            <option value="">SELECCIONE UNA OPCIÓN</option>
                                                            <?php foreach ($catTIpoFertilizante as $tipoFertilizante) : ?>
                                                                <option value="<?= $tipoFertilizante["cve_tipo"] ?>"><?= $tipoFertilizante["tipo_fertilizante"] ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-3 cualFertilizante">
                                                    <div class="form-group">
                                                        <label class="control-label">CUAL:</label>
                                                        <select name="cual_fertilizante_uso" id="cual_fertilizante_uso" class="form-control">
                                                            <option value="">SELECCIONE UNA OPCIÓN</option>
                                                            <?php foreach ($catFertilizantes as $fertilizante) : ?>
                                                                <option value="<?= $fertilizante["cve_fertilizante"] ?>"><?= $fertilizante["fertilizante"] ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-3 aplicoFertilizante">
                                                    <div class="form-group">
                                                        <label class="control-label">KG/HA o UM/HA:</label>
                                                        <input type="text" name="kg_fertilizante" maxlength="5" onkeypress="return soloNumeros(event);" class="form-control" placeholder="Ingresa KG/HA o UM/HA">
                                                    </div>
                                                </div>
                                            </div>


                                            <!-- ¿QUÉ SUPERFICIE COSECHO? ¿QUÉ PRODUCCIÓN OBTUVO? cultivo 1 -->
                                            <div class="row col24">
                                                <label class="control-label">¿QUÉ SUPERFICIE COSECHO? ¿QUÉ PRODUCCIÓN OBTUVO?</label>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label class="control-label">CULTIVO 1</label><br>
                                                        <span class="font-weight-bold text-gray-700 text-uppercase border-bottom" id="cultivocosecho1">
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <div id="superficie_cosecho" style="color:red">No puedes cosechar más de lo que sembraste</div>
                                                        <div class="col-md-4">
                                                            <label class="control-label">HECTÁREAS </label>
                                                            <input type="text" name="hectarea_cosecho_anterior_1" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="HECTÁREAS">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label class="control-label">ÁREAS</label>
                                                            <input type="text" name="area_cosecho_anterior_1" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="ÁREAS">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label class="control-label">Centiareas</label>
                                                            <input type="text" name="centiarea_cosecho_anterior_1" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="Centiareas">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label class="control-label">TONELADAS TOTALES</label>
                                                        <input type="text" name="toneladas_cosecho_anterior_1" maxlength="5" class="form-control decimal" placeholder="Ingresa las toneladas totales">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label">RENDIMIENTO TON/HA</label>
                                                        <div id="rendimiento_tonelada_cosecho_anterior_1"></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row NoOcupamosSegundoCultivo">
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label class="control-label">CULTIVO 2</label>
                                                        <span class="font-weight-bold text-gray-700 text-uppercase border-bottom" id="cultivocosecho2">
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <div id="superficie_cosecho2" style="color:red">No puedes cosechar más de lo que sembraste</div>
                                                        <div class="col-md-4">
                                                            <label class="control-label">HECTÁREAS</label>
                                                            <input type="text" name="hectarea_cosecho_anterior_2" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="HECTÁREAS">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label class="control-label">ÁREAS</label>
                                                            <input type="text" name="area_cosecho_anterior_2" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="ÁREAS">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <label class="control-label">Centiareas</label>
                                                            <input type="text" name="centiarea_cosecho_anterior_2" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="Centiareas">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label class="control-label">TONELADAS TOTALES</label>
                                                        <input type="text" name="toneladas_cosecho_anterior_2" maxlength="5" class="form-control decimal" placeholder="Ingresa las toneladas totales">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label">RENDIMIENTO TON/HA</label>
                                                        <div id="rendimiento_tonelada_cosecho_anterior_2"></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row col24">
                                                <label class="control-label">¿CUÁL FUE EL DESTINO DE LA PRODUCCIÓN OBTENIDA?<div id="msjToneladasMasLineaBase" style="color:red"> No puedes exceder las toneladas totales</div></label>

                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label class="control-label">CULTIVO 1</label>
                                                        <span class="font-weight-bold text-gray-700 text-uppercase border-bottom" id="cultivoProduccionObtenida1">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label class="control-label">AUTO CONSUMO FAMILIAR (TON)</label>
                                                        <input type="text" name="toneladas_autoconsumo_familiar_cult_1" maxlength="5" class="form-control decimal" placeholder="Ingresa las toneladas de auto consumo familiar">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label class="control-label">AUTO CONSUMO PRODUCTIVO (TON)</label>
                                                        <input type="text" name="toneladas_autoconsumo_productivo_cult_1" maxlength="5" class="form-control decimal" placeholder="Ingresa las tonelada de auto consumo productivo">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label class="control-label">COMERCIALIZADA (TON)</label>
                                                        <input type="text" name="toneladas_comercializadora_cult_1" maxlength="5" class="form-control decimal" placeholder="Ingresa las tonelada de la comercializadora">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label class="control-label">PRECIO DE VENTA ($/TON)</label>
                                                        <input type="text" name="precio_venta_destino_prod_cult_1" maxlength="5" class="form-control decimal" placeholder="Ingresa el precio de venta">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label class="control-label">INGRESOS POR COMERCIALIZACIÓN (PESOS)</label>
                                                        <div id="ingresos_comercializacion_destino_prod_cult_1"></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row NoOcupamosSegundoCultivo">
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label class="control-label">CULTIVO 2</label>
                                                        <span class="font-weight-bold text-gray-700 text-uppercase border-bottom" id="cultivoProduccionObtenida2">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label class="control-label">AUTO CONSUMO FAMILIAR (TON)</label>
                                                        <input type="text" name="toneladas_autoconsumo_familiar_cult_2" maxlength="5" class="form-control decimal" placeholder="Ingresa las toneladas de auto consumo familiar">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label class="control-label">AUTO CONSUMO PRODUCTIVO (TON)</label>
                                                        <input type="text" name="toneladas_autoconsumo_productivo_cult_2" maxlength="5" class="form-control decimal" placeholder="Ingresa las tonelada de auto consumo productivo">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label class="control-label">COMERCIALIZADA (TON)</label>
                                                        <input type="text" name="toneladas_comercializadora_cult_2" maxlength="5" class="form-control decimal" placeholder="Ingresa las tonelada de la comercializadora">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label class="control-label">PRECIO DE VENTA ($/TON)</label>
                                                        <input type="text" name="precio_venta_destino_prod_cult_2" maxlength="5" class="form-control decimal" placeholder="Ingresa el precio de venta">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label class="control-label">INGRESOS POR COMERCIALIZACIÓN (PESOS)</label>
                                                        <div id="ingresos_comercializacion_destino_prod_cult_2"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row my-4">
                                        <h3> DATOS PRODUCTIVOS DEL CICLO ACTUAL</h3>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">¿SEMBRÓ EN ESTE CICLO?</label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="sembro_este_ciclo" id="sembroEsteCiclo" value="1" required>
                                                    <label class="form-check-label" for="sembroEsteCiclo">
                                                        Sembro
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="sembro_este_ciclo" id="nosembroEsteCiclo" value="0" required>
                                                    <label class="form-check-label" for="nosembroEsteCiclo">
                                                        No sembro
                                                    </label>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-6 nosembroMotivoEsteCiclo">
                                            <div class="form-group">
                                                <label class="control-label">¿POR QUÉ MOTIVO? </label>
                                                <input maxlength="100" name="porque_no_sembro_ciclo_actual" type="text" class="form-control" placeholder="Ingresa por qué no sembró en el ciclo actual">
                                            </div>
                                        </div>
                                    </div>

                                    <label class="control-label col63">¿CUAL(ES) CULTIVOS SEMBRÓ EN ESTE CICLO? <div id="SembrarmasdelaUP" style="color:red">No puedes sembrar más de la superficie total de la UP</div></label>

                                    <div class="row col63">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">CULTIVO 1</label>
                                                <select name="cultivo_actual_1" id="CultivoActual1" class="form-control">
                                                    <option value="">SELECCIONE UNA OPCIÓN</option>
                                                    <?php foreach ($catCultivos as $cultivo) : ?>
                                                        <option value="<?= $cultivo["cve_cultivo"] ?>"><?= $cultivo["cultivo"] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">

                                                <div class="col-md-4">
                                                    <label class="control-label">HECTÁREAS</label>
                                                    <input type="text" name="hectarea_actual_cultivo_1" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="HECTÁREAS">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">ÁREAS</label>
                                                    <input type="text" name="area_actual_cultivo_1" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="ÁREAS">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">Centiareas</label>
                                                    <input type="text" name="centiarea_actual_cultivo_1" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="Centiareas">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">CICLO:</label>
                                                <select name="ciclo_actual_cultivo_1" id="ciclo" class="form-control">
                                                    <option value="">SELECCIONE UNA OPCIÓN</option>
                                                    <?php foreach ($catCiclo as $ciclo) : ?>
                                                        <option value="<?= $ciclo["cve_ciclo"] ?>"><?= $ciclo["ciclo"] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label"> REGIMEN HIDRICO:</label>
                                                <select name="regimen_hidrico_actual_cultivo_1" id="regimenHidricoActualCultivo1" class="form-control">
                                                    <option value="">SELECCIONE UNA OPCIÓN</option>
                                                    <?php foreach ($catRegimen as $registro) : ?>
                                                        <option value="<?= $registro["cve_regimen"] ?>"><?= $registro["regimen"] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row col63">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">CULTIVO 2</label>
                                                <select name="cultivo_actual_2" id="CultivoActual2" class="form-control">
                                                    <option value="">SELECCIONE UNA OPCIÓN</option>
                                                    <?php foreach ($catCultivos as $cultivo) : ?>
                                                        <option value="<?= $cultivo["cve_cultivo"] ?>"><?= $cultivo["cultivo"] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-5 NoOcupamosSegundoCultivoActual">
                                            <div class="form-group">
                                                <div class="col-md-4">
                                                    <label class="control-label">HECTÁREAS</label>
                                                    <input type="text" name="hectarea_actual_cultivo_2" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="HECTÁREAS">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">ÁREAS</label>
                                                    <input type="text" name="area_actual_cultivo_2" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="ÁREAS">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">Centiareas</label>
                                                    <input type="text" name="centiarea_actual_cultivo_2" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="Centiareas">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 NoOcupamosSegundoCultivoActual">
                                            <div class="form-group">
                                                <label class="control-label">CICLO:</label>
                                                <select name="ciclo_actual_cultivo_2" id="ciclo" class="form-control">
                                                    <option value="">SELECCIONE UNA OPCIÓN</option>
                                                    <?php foreach ($catCiclo as $ciclo) : ?>
                                                        <option value="<?= $ciclo["cve_ciclo"] ?>"><?= $ciclo["ciclo"] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3 NoOcupamosSegundoCultivoActual">
                                            <div class="form-group">
                                                <label class="control-label"> REGIMEN HIDRICO:</label>
                                                <select name="regimen_hidrico_actual_cultivo_2" id="regimenHidricoActualCultivo2" class="form-control">
                                                    <option value="">SELECCIONE UNA OPCIÓN</option>
                                                    <?php foreach ($catRegimen as $registro) : ?>
                                                        <option value="<?= $registro["cve_regimen"] ?>"><?= $registro["regimen"] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row col63">
                                        <label class="control-label">¿QUÉ SUPERFICIE COSECHO?</label>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">CULTIVO 1</label>
                                                <span class="font-weight-bold text-gray-700 text-uppercase border-bottom" id="cultivoActualCosecho1">
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <div id="superficie_cosecho_actual_1" style="color:red">No puedes cosechar más de lo que sembraste</div>
                                                <div class="col-md-4">
                                                    <label class="control-label">HECTÁREAS</label>
                                                    <input type="text" name="hectarea_cosecho_actual_1" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="HECTÁREAS">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">ÁREAS</label>
                                                    <input type="text" name="area_cosecho_actual_1" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="ÁREAS">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">Centiareas</label>
                                                    <input type="text" name="centiarea_cosecho_actual_1" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="Centiareas">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">TONELADAS TOTALES</label>
                                                <input type="text" name="toneladas_cosecho_actual_1" maxlength="5" class="form-control decimal" placeholder="Ingresa las toneladas totales">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">RENDIMIENTO TON/HA</label>
                                                <div id="rendimiento_tonelada_cosecho_actual_1"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row NoOcupamosSegundoCultivoActual">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">CULTIVO 2</label>
                                                <span class="font-weight-bold text-gray-700 text-uppercase border-bottom" id="cultivoActualCosecho2">
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <div id="superficie_cosecho_actual_2" style="color:red">No puedes cosechar más de lo que sembraste</div>
                                                <div class="col-md-4">
                                                    <label class="control-label">HECTÁREAS</label>
                                                    <input type="text" name="hectarea_cosecho_actual_2" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="HECTÁREAS">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">ÁREAS</label>
                                                    <input type="text" name="area_cosecho_actual_2" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="ÁREAS">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">Centiareas</label>
                                                    <input type="text" name="centiarea_cosecho_actual_2" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="Centiareas">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 ">
                                            <div class="form-group">
                                                <label class="control-label">TONELADAS TOTALES</label>
                                                <input type="text" name="toneladas_cosecho_actual_2" maxlength="5" class="form-control decimal" placeholder="Ingresa las toneladas totales">
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">RENDIMIENTO TON/HA</label>
                                                <div id="rendimiento_tonelada_cosecho_actual_2"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row col63">
                                        <label class="control-label">¿CUÁL FUE EL DESTINO DE LA PRODUCCIÓN OBTENIDA? <div id="msjToneladasMas" style="color:red"> No puedes exceder las toneladas totales</div></label>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">CULTIVO 1</label>
                                                <span class="font-weight-bold text-gray-700 text-uppercase border-bottom" id="cultivoActualDestino1">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">AUTO CONSUMO FAMILIAR (TON)</label>
                                                <input type="text" name="toneladas_autoconsumo_familiar_actual_cult_1" maxlength="5" class="form-control decimal" placeholder="Ingresa las toneladas de auto consumo familiar">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">AUTO CONSUMO PRODUCTIVO (TON)</label>
                                                <input type="text" name="toneladas_autoconsumo_productivo_actual_cult_1" maxlength="5" class="form-control decimal" placeholder="Ingresa las tonelada de auto consumo productivo">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">COMERCIALIZADA (TON)</label>
                                                <input type="text" name="toneladas_comercializadora_actual_cult_1" maxlength="5" class="form-control decimal" placeholder="Ingresa las tonelada de la comercializadora">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">PRECIO DE VENTA ($/TON)</label>
                                                <input type="text" name="precio_venta_destino_prod_actual_cult_1" maxlength="5" onkeypress="return soloNumeros(event);" class="form-control" placeholder="Ingresa el precio de venta">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">INGRESOS POR COMERCIALIZACIÓN (PESOS)</label>
                                                <div id="ingresos_comercializacion_destino_prod_actual_cult_1"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row NoOcupamosSegundoCultivoActual">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">CULTIVO 2</label>
                                                <span class="font-weight-bold text-gray-700 text-uppercase border-bottom" id="cultivoActualDestino2">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">AUTO CONSUMO FAMILIAR (TON)</label>
                                                <input type="text" name="toneladas_autoconsumo_familiar_actual_cult_2" maxlength="5" class="form-control decimal" placeholder="Ingresa las toneladas de auto consumo familiar">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">AUTO CONSUMO PRODUCTIVO (TON)</label>
                                                <input type="text" name="toneladas_autoconsumo_productivo_actual_cult_2" maxlength="5" class="form-control decimal" placeholder="Ingresa las tonelada de auto consumo productivo">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">COMERCIALIZADA (TON)</label>
                                                <input type="text" name="toneladas_comercializadora_actual_cult_2" maxlength="5" class="form-control decimal" placeholder="Ingresa las tonelada de la comercializadora">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">PRECIO DE VENTA ($/TON)</label>
                                                <input type="text" name="precio_venta_destino_prod_actual_cult_2" maxlength="5" class="form-control decimal" placeholder="Ingresa el precio de venta">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">INGRESOS POR COMERCIALIZACIÓN (PESOS)</label>
                                                <div id="ingresos_comercializacion_destino_prod_actual_cult_2"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="alert alert-danger noSeleccionoCheck">
                                        <div class="counter counter-inherit">
                                            <strong>Atencion:</strong>
                                            Usted tiene que seleccionar una opción <strong>en los campos</strong>.
                                        </div>
                                    </div>
                                    <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Regresar</button>
                                    <!-- <button class="btn btn-primary nextBtn gotoTop btn-lg pull-right" type="button">Siguiente</button> -->
                                    <button class="btn btn-primary btn-lg  nextBtn gotoTop center-block pull-right button_jairo" type="submit">Guardar y continuar</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- step 3 -->

                    <!-- step 4 -->
                    <div class="row setup-content" id="step-4">
                        <div class="form-widget" id="Tecnologias">
                            <div class="form-result"></div>
                            <form role="form" id="Tecnologias" action="Tecnologicos" method="post" accept-charset="utf-8" autocomplete="off" enctype="multipart/form-data">
                                <input class="cve_encuesta" name="cve_encuesta_Tecnologicos" type="text" hidden>
                                <div class="col-md-12">
                                    <h3>DATOS TECNOLOGICOS (PAQUETE TECNOLÓGICO UTILIZADO)</h3><br>
                                    <h4 class="my-3">PREPARACIÓN DEL TERRENO</h4>

                                    <div class="row ">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">¿QUÉ SISTEMA DE PRODUCCIÓN UTILIZA?</label>
                                                <select name="cve_sistema_produccion" id="cve_sistema_produccion" class="form-control" required>
                                                    <option value="">SELECCIONE UNA OPCIÓN</option>
                                                    <?php foreach ($catSistemaProduccion as $sistemaProduccion) : ?>
                                                        <option value="<?= $sistemaProduccion["cve_sistema"] ?>"><?= $sistemaProduccion["sistema"] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label class="control-label">¿QUÉ LABORES DE PREPARACIÓN DE TERRENO REALIZÓ?</label>
                                                    <select class="selectpicker form-control customjs pickerTerreno" name="preparacion_terreno[]" id="cve_preparacion" required="required" title="SELECCIONE UNA OPCIÓN" data-size="7" data-live-search="true" multiple data-live-search="true" style="width:100%;">
                                                        <?php foreach ($catPreparacionTerreno as $preparacionTerreno) : ?>
                                                            <option value="<?= $preparacionTerreno["cve_preparacion"] ?>"><?= $preparacionTerreno["preparacion_terreno"] ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <hr>
                                    <h4>SIEMBRA</h4><br>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">¿QUÉ TIPO DE SEMILLA UTILIZÓ? (MATERIAL GENETICO)</label>
                                                <select name="cve_tipo_semilla" id="cve_tipo_semilla" class="form-control" required>
                                                    <option value="">SELECCIONE UNA OPCIÓN</option>
                                                    <?php foreach ($catSemilla as $semilla) { ?>
                                                        <option value="<?= $semilla["cve_tipo_semilla"] ?>"><?= $semilla["tipo_semilla"] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4 colSemillaHibrida">
                                            <div class="form-group">
                                                <label class="control-label">Híbrido o variedad utilizado</label>
                                                <input maxlength="100" name="hibrido_variedad" type="text" class="form-control" placeholder="Híbrido o variedad utilizado">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">¿DÓNDE ADQUIRIO LA SEMILLA?</label>
                                                <select name="cve_adquisicion_semilla" id="cve_tipo_semilla" class="form-control" required>
                                                    <option value="">SELECCIONE UNA OPCIÓN</option>
                                                    <?php foreach ($catAdquisisionSemilla as $adqSemilla) { ?>
                                                        <option value="<?= $adqSemilla["cve_adquisicion_semilla"] ?>"><?= $adqSemilla["adquisicion_semilla"] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">¿CUANTÓ TIEMPO TIENE UTILIZANDO ESTA SEMILLA?</label>
                                                <select name="cve_tiempo_semilla" id="cve_tipo_semilla" class="form-control" required>
                                                    <option value="">SELECCIONE UNA OPCIÓN</option>
                                                    <?php foreach ($catTiempoSemilla as $tiempoSemilla) { ?>
                                                        <option value="<?= $tiempoSemilla["cve_tiempo_semilla"] ?>"><?= $tiempoSemilla["tiempo_semilla"] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">¿QUÉ TIPO DE SIEMBRA REALIZÓ?</label>
                                                <select name="cve_tipo_siembra" id="cve_tipo_siembra" class="form-control" required>
                                                    <option value="">SELECCIONE UNA OPCIÓN</option>
                                                    <?php foreach ($catTipoSiembra as $tipoSiembra) : ?>
                                                        <option value="<?= $tipoSiembra["cvet_tipo_siembra"] ?>"><?= $tipoSiembra["tipo_siembra"] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">¿CUANTOS KILOGRAMOS DE SEMILLA UTILIZÓ POR HA?</label>
                                                <input maxlength="5" name="semilla_gramos" type="text" class="form-control" placeholder="KG/HA" onkeypress="return soloNumeros(event);" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">FECHA DE SIEMBRA</label>
                                                <input type="date" name="fecha_siembra" min="2023-01-01" max="<?= date('Y-m-d'); ?>" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">¿REALIZÓ RESIEMBRA?</label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="resiembra" id="resiembraSI" value="1" required>
                                                    <label class="form-check-label" for="resiembraSI">
                                                        Si
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="resiembra" id="resiembraNo" value="0" required>
                                                    <label class="form-check-label" for="resiembraNo">
                                                        No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 resiembra">
                                            <div class="form-group">
                                                <label class="control-label">FECHA DE RESIEMBRA <div id="msjfechaResiembra" style="color:red"> No puedes resembrar antes de sembrar</div></label>
                                                <input type="date" name="fecha_resiembra" class="form-control" min="2023-01-01" max="<?= date('Y-m-d'); ?>">
                                            </div>
                                        </div>

                                        <div class="col-md-4 resiembra">
                                            <div class="form-group">
                                                <label class="control-label">¿CUÁL FUE EL MOTIVO PARA REALIZAR LA RESIEMRA?</label>
                                                <select name="motivo_resiembra" id="motivo_resiembra" class="form-control">
                                                    <option value="">SELECCIONE UNA OPCIÓN</option>
                                                    <?php foreach ($catResiembra as $resiembra) : ?>
                                                        <option value="<?= $resiembra["cve_resiembra"] ?>"><?= $resiembra["resiembra"] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <hr>
                                    <h4>LABORES CULTURALES QUE REALIZÓ</h4><br>

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">¿CUÁL DE LAS SIGUIENTES LABORES REALIZÓ?</label>
                                                <select class="selectpicker form-control customjs laboresRealizo" name="laboresRealizo[]" required="required" id="laborRealizo" title="SELECCIONE UNA OPCIÓN" data-size="7" data-live-search="true" multiple data-live-search="true" style="width:100%;">
                                                    <?php foreach ($catLabores as $labores) : ?>
                                                        <option value="<?= $labores["cve_labor"] ?>"><?= $labores["labor"] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-3" id="cualLaborCol">
                                            <div class="form-group">
                                                <label class="control-label">CUALES</label>
                                                <input name="cuales_mas_labores" id="cualLabor" maxlength="100" type="text" class="form-control" placeholder="Ingresa cuales labores realizó">
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="control-label">¿UTILIZÓ HERBICIDAS?</label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="utilizo_herbicidas" value="1" id="siHerbicida" required>
                                                    <label class="form-check-label" for="siHerbicida">
                                                        Si
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="utilizo_herbicidas" value="0" id="NoHerbicida" required>
                                                    <label class="form-check-label" for="NoHerbicida">
                                                        No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3" id="cualHerbicidacol">
                                            <div class="form-group">
                                                <label class="control-label">CUAL herbicida</label>
                                                <input name="cuales_herbicidas" id="cualHerbicida" maxlength="100" type="text" class="form-control" placeholder="Ingresa cuales labores realizó">
                                            </div>
                                        </div>
                                    </div>

                                    <hr>
                                    <h4>FERTILIZACIÓN</h4><br>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">¿RECIBIO FERTILIZANTE DEL PROGRAMA DE FERTILIZANTES PARA EL BIENESTAR?</label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="fertilizante_bienestar" id="sirecibiofertilizante" value="1" required>
                                                    <label class="form-check-label" for="sirecibiofertilizante">
                                                        Si
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="fertilizante_bienestar" id="norecibiofertilizante" value="0" required>
                                                    <label class="form-check-label" for="norecibiofertilizante">
                                                        No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col53 ">
                                            <label class="control-label">¿CUANTO KG DE FERTILIZANTE RECIBIO DEL PROGRAMA FERTILIZANTES PARA EL BIENESTAR? <div id="msjCantidadesDAP" style="color:red">Confirme la cantidad</div></label>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">DAP O FORMULA</label>
                                                    <input maxlength="4" name="ferti_kg_dap" type="text" class="form-control" placeholder="Kg" onkeypress="return soloNumeros(event);">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col53">
                                                <div class="form-group">
                                                    <label class="control-label">UREA</label>
                                                    <input maxlength="4" name="ferti_kg_urea" type="text" class="form-control" placeholder="Kg" onkeypress="return soloNumeros(event);">
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row col53">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">¿RECIBIO EL FERTILIZANTE COMPLETO INDICADO EN EL CUADERNILLO (FORMATO UNICO DE ENTREGA)?</label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="formato_unico_entrega" id="fertilizanteCompletoSI" value="1">
                                                    <label class="form-check-label" for="fertilizanteCompletoSI">
                                                        Si
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="formato_unico_entrega" id="fertilizanteCompletoNo" value="0">
                                                    <label class="form-check-label" for="fertilizanteCompletoNO">
                                                        No
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="formato_unico_entrega" id="fertilizanteCompletoNoSabe" value="2">
                                                    <label class="form-check-label" for="fertilizanteCompletoNoSabe">
                                                        No sabe
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 colFertilizanteCompleto">
                                            <div class="form-group">
                                                <label class="control-label">¿POR QUÉ?</label>
                                                <input maxlength="100" name="formato_porque" type="text" class="form-control" placeholder="Ingresa el porque no recibio completo el fertilizante">
                                            </div>
                                        </div>

                                        <div class="col-md-4 colFertilizanteCompleto">
                                            <div class="form-group">
                                                <label class="control-label">¿REPORTO EL HECHO?</label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="reporte_hecho" id="flexRadioDefault1" value="1">
                                                    <label class="form-check-label" for="flexRadioDefault1">
                                                        Si
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="reporte_hecho" id="flexRadioDefault2" value="0">
                                                    <label class="form-check-label" for="flexRadioDefault2">
                                                        No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row col53">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">¿EN QUE FECHA RECIBIÓ EL FERTILIZANTE?</label>
                                                <input type="date" name="fecha_recibio_fertilizante" min="2023-01-01" max="<?= date('Y-m-d'); ?>" class="form-control" placeholder="Ingresa el porque no recibio completo el fertilizante">
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">¿FUE OPORTUNO PARA SU SIEMBRA?</label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="oportuno_siembra" id="flexRadioDefault1" value="1">
                                                    <label class="form-check-label" for="flexRadioDefault1">
                                                        Si
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="oportuno_siembra" id="flexRadioDefault2" value="0">
                                                    <label class="form-check-label" for="flexRadioDefault2">
                                                        No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row col53">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">¿APLICO EL FERTILIZANTE QUE RECIBIO DEL PROGRAMA?</label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="aplico_fertilizante" id="siCultivoAplicoFertilizante" value="1">
                                                    <label class="form-check-label" for="siCultivoAplicoFertilizante">
                                                        Si lo aplico
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="aplico_fertilizante" id="noCultivoAplicoFertilizante" value="0">
                                                    <label class="form-check-label" for="noCultivoAplicoFertilizante">
                                                        No lo aplico
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col57porque">
                                            <div class="form-group">
                                                <label class="control-label">¿POR QUÉ RAZÓN?</label>
                                                <input maxlength="100" type="text" name="razon_aplicacion_fertilizante" id="RazonNoAplicoFertilizante" class="form-control" placeholder="Ingresa porque no aplico el fertilizante">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row col57">
                                        <label class="control-label">¿EN QUE CULTIVO APLICO EL FERTILIZANTE QUE RECIBIO DEL PROGRAMA?</label>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label class="control-label">CULTIVO 1</label>
                                                <span class="font-weight-bold text-gray-700 text-uppercase border-bottom" id="cultivo1AplicoFertilizante">
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <div class="col-md-4">
                                                    <label class="control-label">HECTÁREAS</label>
                                                    <input type="text" name="hectareas_fertilizante1" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="HECTÁREAS">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">ÁREAS</label>
                                                    <input type="text" name="areas_fertilizante1" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="ÁREAS">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">Centiareas</label>
                                                    <input type="text" name="centiareas_fertilizante1" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="CENTIAREAS">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">DAP FECHA DE APLICACIÓN<div id="mayorDAP" style="color:red">no puede ser menor a la fecha de recepción</div></label>
                                                <input maxlength="100" name="fecha_aplicacion1" type="date" min="2023-01-01" max="<?= date('Y-m-d'); ?>" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label class="control-label">DOSIS</label>
                                                <input maxlength="100" name="dosis1" type="text" onkeypress="return soloNumeros(event);" class="form-control" placeholder="KG/HA">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">UREA FECHA DE APLICACIÓN<div id="mayorUREA" style="color:red">no puede ser menor a la fecha de recepción</div></label>
                                                <input maxlength="100" name="urea_fecha1" type="date" class="form-control" min="2023-01-01" max="<?= date('Y-m-d'); ?>" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label class="control-label">DOSIS</label>
                                                <input maxlength="100" name="dosis_urea1" type="text" onkeypress="return soloNumeros(event);" class="form-control" placeholder="KG/HA">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row NoOcupamosSegundoCultivoActual">
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label class="control-label">CULTIVO 2</label>
                                                <span class="font-weight-bold text-gray-700 text-uppercase border-bottom" id="cultivo2AplicoFertilizante">
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <div class="col-md-4">
                                                    <label class="control-label">HECTÁREAS</label>
                                                    <input type="text" name="hectareas_fertilizante2" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="HECTÁREAS">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">ÁREAS</label>
                                                    <input type="text" name="areas_fertilizante2" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="HECTÁREAS">
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="control-label">Centiareas</label>
                                                    <input type="text" name="centiareas_fertilizante2" maxlength="3" onkeypress="return soloNumeros(event);" class="form-control" placeholder="CENTIÁREAS">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">DAP FECHA DE APLICACIÓN<div id="mayorDAP2" style="color:red">no puede ser menor a la fecha de recepción</div></label>
                                                <input maxlength="100" type="date" name="fecha_aplicacion2" min="2023-01-01" max="<?= date('Y-m-d'); ?>" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label class="control-label">DOSIS</label>
                                                <input maxlength="100" type="text" name="dosis2" class="form-control" onkeypress="return soloNumeros(event);" placeholder="KG/HA">
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label class="control-label">UREA FECHA DE APLICACIÓN<div id="mayorUREA2" style="color:red">no puede ser menor a la fecha de recepción</div></label>
                                                <input maxlength="100" type="date" name="urea_fecha2" min="2023-01-01" max="<?= date('Y-m-d'); ?>" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label class="control-label">DOSIS</label>
                                                <input maxlength="100" type="text" name="dosis_urea2" class="form-control" onkeypress="return soloNumeros(event);" placeholder="KG/HA">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row col57">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">¿APLICÓ EL FERTILIZANTE PARA EL CULTIVO Y PREDIO PARA EL QUE FUE AUTORIZADO?</label>
                                                <div class="form-check">
                                                    <input class="form-check-input" name="fertilizante_aplicado" type="radio" id="SiAplicoElFertilizante" value="1">
                                                    <label class="form-check-label" for="SiAplicoElFertilizante">
                                                        SI
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" name="fertilizante_aplicado" type="radio" id="NoAplicoElFertilizante" value="0">
                                                    <label class="form-check-label" for="NoAplicoElFertilizante">
                                                        NO
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 PorqueNoAplicoFertilizante">
                                            <div class="form-group">
                                                <label class="control-label">¿POR QUÉ?</label>
                                                <input maxlength="100" type="text" name="porque_aplico_ferti" class="form-control" placeholder="Ingresa el por qué no utlizó el fertilizante">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row col57">
                                        <div class="col-md-4 col53">
                                            <div class="form-group">
                                                <label class="control-label">¿COMO APLICO EL FERTILIZANTE?</label>
                                                <div class="form-check">
                                                    <input class="form-check-input" name="cve_aplicacion_fertilizante" type="radio" id="flexRadioDefault2" value="1">
                                                    <label class="form-check-label" for="flexRadioDefault2">
                                                        MECANICA
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" name="cve_aplicacion_fertilizante" type="radio" id="flexRadioDefault2" value="0">
                                                    <label class="form-check-label" for="flexRadioDefault2">
                                                        MANUAL
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col53">
                                            <div class="form-group">
                                                <label class="control-label">¿DE QUÉ FORMA LO APLICO?</label>
                                                <select name="cve_aplicacion" id="cve_aplicacion" class="form-control">
                                                    <option value="">SELECCIONE UNA OPCIÓN</option>
                                                    <?php foreach ($catAplicaciones as $aplicacion) : ?>
                                                        <option value="<?= $aplicacion["cve_aplicacion"] ?>"><?= $aplicacion["aplicacion"] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label"> ¿APLICO FERTILIZANTE COMPLEMENTARIO AL SUMINISTRADO POR EL PROGRAMA EN SU UP?</label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="fertilizante_complementario_up" id="flexRadioDefault2" value="1">
                                                    <label class="form-check-label" for="flexRadioDefault2">
                                                        Si
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="fertilizante_complementario_up" id="flexRadioDefault2" value="0">
                                                    <label class="form-check-label" for="flexRadioDefault2">
                                                        No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row col57 ">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">¿APLICO FERTILIZANTE Y/O ABONO COMPLEMENTARIO A LA MISMA SUPERFICIE BENEFICIADA POR EL PROGRAMA?</label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="fertilizante_complementario_programa" id="FertilizanteComplementarioSI" value="1">
                                                    <label class="form-check-label" for="flexRadioDefault2">
                                                        Si
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="fertilizante_complementario_programa" id="FertilizanteComplementarioNo" value="0">
                                                    <label class="form-check-label" for="flexRadioDefault2">
                                                        No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 fertilizanteComplementario">
                                            <div class="form-group">
                                                <label class="control-label">¿DEL FERTILIZANTE COMPLETARIO QUÉ TIPO DE FERTILIZANTE UTILIZÓ?:</label>
                                                <select name="cve_quimico" id="TipoFertilizanteComplementario" class="form-control">
                                                    <option value="">SELECCIONE UNA OPCIÓN</option>
                                                    <?php foreach ($catTIpoFertilizante as $tipoFertilizante) : ?>
                                                        <option value="<?= $tipoFertilizante["cve_tipo"] ?>"><?= $tipoFertilizante["tipo_fertilizante"] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 cualfertilizanteComplemetario">
                                            <div class="form-group">
                                                <label class="control-label">¿Cual?</label>
                                                <input maxlength="45" name="cual_quimico" type="text" class="form-control" placeholder="Ingresar el motivo de no aplicar el fertilizante complementario">
                                            </div>
                                        </div>
                                        <div class="col-md-2 fertilizanteComplementario">
                                            <div class="form-group">
                                                <label class="control-label">Dosis</label>
                                                <input maxlength="4" name="dosis_quimico" type="text" class="form-control" onkeypress="return soloNumeros(event);" placeholder="KG/HA">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row col57">
                                        <div class="col-md-12">
                                            <label class="control-label">Observaciones sobre la fertilización</label>
                                            <textarea maxlength="50" name="observaciones" type="text" class="form-control" placeholder=""></textarea>
                                        </div>
                                    </div>

                                    <hr class=" colSoloPuntaRiego">
                                    <h4 class=" colSoloPuntaRiego">RIEGO</h4>

                                    <div class="row colSoloPuntaRiego">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">¿CUANTOS RIEGOS APLICO?</label>
                                                <input maxlength="3" name="riegos" type="text" class="form-control" placeholder="Ingresa el número de riegos" onkeypress="return soloNumeros(event);">
                                            </div>
                                        </div>
                                    </div>


                                    <hr>
                                    <h4>COMPORTAMIENTO DEL TEMPORAL DE LLUVIAS</h4>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">¿CÓMO CONSIDERA EL COMPORTAMIENTO DEL TEMPORAL DE LLUVIAS?</label>
                                                <select name="cve_temporal" id="comportamientoLLuvias" class="form-control" required>
                                                    <option value="">SELECCIONE UNA OPCIÓN</option>
                                                    <?php foreach ($catTempLluvias as $lluvias) : ?>
                                                        <option value="<?= $lluvias["cve_temporal"] ?>"><?= $lluvias["temporal_lluvia"] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <hr>
                                    <h4>PLAGAS Y ENFERMEDADES PRESENTADAS</h4>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">¿SE PRESENTARON PLAGAS DURANTE EL DESARROLLO DEL CULTIVO?</label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="plagas" id="PlagasCultivoSI" value="1" required>
                                                    <label class="form-check-label" for="PlagasCultivoSI">
                                                        Si
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="plagas" id="PlagasCultivoNo" value="0" required>
                                                    <label class="form-check-label" for="PlagasCultivoNo">
                                                        No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 CualesPlagas">
                                            <div class="form-group">
                                                <label class="control-label">¿Cuales?</label>
                                                <input maxlength="50" name="cual_plaga" type="text" class="form-control" placeholder="Ingresa cual plaga">
                                            </div>
                                        </div>
                                        <div class="col-md-4 CualesPlagas">
                                            <div class="form-group">
                                                <label class="control-label">¿CÓMO CONTROLO LAS PLAGAS? (METODO)</label>
                                                <select name="cve_control_plaga" id="cve_control_plaga" class="form-control">
                                                    <option value="">SELECCIONE UNA OPCIÓN</option>
                                                    <?php foreach ($catControlPlagas as $controlPlagas) : ?>
                                                        <option value="<?= $controlPlagas["cve_control_plaga"] ?>"><?= $controlPlagas["control_plaga"] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">¿SE PRESENTARON ENFERMEDADES DURANTE EL DESARROLLO DEL CULTIVO?</label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="cve_enfermedad" id="EnfermedadesEnCultivoSI" value="1" required>
                                                    <label class="form-check-label" for="EnfermedadesEnCultivoSI">
                                                        Si
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="cve_enfermedad" id="EnfermedadesEnCultivoNO" value="0" required>
                                                    <label class="form-check-label" for="EnfermedadesEnCultivoNO">
                                                        No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 CualesENfermedades">
                                            <div class="form-group">
                                                <label class="control-label">¿Cuales?</label>
                                                <input maxlength="50" name="enfermedades_cuales" type="text" class="form-control" placeholder="Ingresa cuales enfermedades">
                                            </div>
                                        </div>
                                        <div class="col-md-4 CualesENfermedades">
                                            <div class="form-group">
                                                <label class="control-label">¿CÓMO CONTROLO LAS ENFERMEDADES? (METODO)</label>
                                                <select name="cve_metodo_enfermedades" id="cve_control_plaga" class="form-control">
                                                    <option value="">SELECCIONE UNA OPCIÓN</option>
                                                    <?php foreach ($catEnfermedades as $enfermedades) : ?>
                                                        <option value="<?= $enfermedades["cve_enfermedad"] ?>"><?= $enfermedades["control_enfermedad"] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="control-label">¿LA PRESENCIA DE PLAGAS Y/O ENFERMEDADES AFECTÓ EL RENDIMIENTO DE SUS CULTIVOS?</label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="rendimiento_cultivo" id="AfectoPlagaEnfermedadSI" value="1" required>
                                                    <label class="form-check-label" for="AfectoPlagaEnfermedadSI">
                                                        Si
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="rendimiento_cultivo" id="AfectoPlagaEnfermedadNo" value="0" required>
                                                    <label class="form-check-label" for="AfectoPlagaEnfermedadNo">
                                                        No
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 PorcentajeAfectoEnfermedadPlaga">
                                            <div class="form-group">
                                                <label class="control-label">PORCENTAJE</label>
                                                <input maxlength="3" name="porcentaje_cultivo" type="text" onkeypress="return soloNumeros(event);" class="form-control" placeholder="Ingresa el porcentaje">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="alert alert-danger noSeleccionoCheck">
                                        <div class="counter counter-inherit">
                                            <strong>Atencion:</strong>
                                            Usted tiene que seleccionar una opción <strong>en los campos</strong>.
                                        </div>
                                    </div>
                                    <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Regresar</button>
                                    <!-- <button class="btn btn-primary nextBtn gotoTop btn-lg pull-right" type="button">Siguiente</button> -->
                                    <button class="btn btn-primary btn-lg  nextBtn gotoTop center-block pull-right buttonTecnologico" type="submit">Guardar y continuar</button>

                                </div>
                            </form>
                        </div>
                        <div id="nodatosTecnologicos">
                            <div class="text-center">
                                <h3> NO HUBO SIEMBRA POR LO CUAL NO ES NECESARIO LLENAR ESTA SECCION</h3>
                            </div>
                            <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Regresar</button>
                            <button class="btn btn-primary nextBtn gotoTop btn-lg pull-right" type="button">Siguiente</button>
                        </div>
                    </div>
                    <!-- step 4 -->

                    <!-- step 5 -->
                    <div class="row setup-content" id="step-5">
                        <div class="form-widget" id="CapacitacionForm">
                            <div class="form-result"></div>
                            <form role="form" id="Capacitacion" action="Capacitacion" method="post" accept-charset="utf-8" autocomplete="off" enctype="multipart/form-data">
                                <div class="form-process"></div>
                                <input type="hidden" name="cve_encuesta" class="cve_encuesta">
                                <div class="col-md-12">
                                    <h3> CAPACITACION Y ASISTENCIA TECNICA</h3>
                                    <input type="hidden" name="paso" value="step5">

                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="control-label">¿RECIBE CAPACITACION Y ASISTENCIA TECNICA?</label>
                                                <select name="cve_capacitacion" id="cve_capacitacion" class="form-control" required="required">
                                                    <option value="">SELECCIONE UNA OPCIÓN</option>
                                                    <?php foreach ($catCapacitaciones as $capacitacion) : ?>
                                                        <option value="<?= $capacitacion["cve_capacitacion"] ?>"><?= $capacitacion["capacitacion"] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col quienCapacito">
                                            <div class="form-group">
                                                <label class="control-label">¿DE QUIEN?</label>
                                                <input maxlength="100" type="text" id="de_quien" name="de_quien" class="form-control" placeholder="Nombre de quien lo asistió">
                                            </div>
                                        </div>
                                        <div class="col temaCapacitacion">
                                            <div class="form-group">
                                                <label class="control-label">¿EN QUE TEMAS RECIBE LA CAPACITACIÓN?</label>
                                                <select class="selectpicker form-control customjs cve_tema" name="cve_tema[]" id="cve_tema" title="SELECCIONE UNA OPCIÓN" data-size="7" data-live-search="true" multiple data-live-search="true" style="width:100%;">
                                                    <?php foreach ($cattemas as $cattema) : ?>
                                                        <option value="<?= $cattema["cve_tema"] ?>"><?= $cattema["tema"] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label"> ¿HA ESCUCHADO, LE HAN COMENTADO, O SABE Y TIENE CONOCIMIENTO DEL PROGRAMA AGROSANO?</label>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="radio" name="conoce_programa" id="escaagrosanoSI" placeholder="Yes" value="1" required="required">
                                                        <label for="escaagrosanoSI">SI</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <input type="radio" name="conoce_programa" id="escaagrosanoNO" placeholder="NO" value="0" required="required">
                                                        <label for="escaagrosanoNO">NO</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">¿RECIBIO CAPACITACIÓN O ACOMPAÑAMIENTO TÉCNICO DEL PROGRAMA AGROSANO?</label>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="radio" name="capacitacion_programa" id="reciagrosanoSI" placeholder="Yes" value="1" required="required">
                                                        <label for="reciagrosanoSI">SI</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <input type="radio" name="capacitacion_programa" id="reciagrosanoNO" placeholder="NO" value="0" required="required">
                                                        <label for="reciagrosanoNO">NO</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label"> ¿HA PARTICIPADO O LO HAN INVITADO PARA APRENDER O ELABORAR BIOFERTILIZANTES?</label>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="radio" name="elaborar_biofertilizante" id="aprebiofertilizantesSI" placeholder="Yes" value="1" required="required">
                                                        <label for="aprebiofertilizantesSI">SI</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <input type="radio" name="elaborar_biofertilizante" id="aprebiofertilizantesNO" placeholder="NO" value="0" required="required">
                                                        <label for="aprebiofertilizantesNO">NO</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="alert alert-danger noSeleccionoCheck">
                                        <div class="counter counter-inherit">
                                            <strong>Atencion:</strong>
                                            Usted tiene que seleccionar una opción <strong>en los campos</strong>.
                                        </div>
                                    </div>
                                    <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Regresar</button>
                                    <!-- <button class="btn btn-primary nextBtn gotoTop btn-lg pull-right" type="button">Siguiente</button> -->
                                    <button class="btn btn-primary btn-lg  nextBtn gotoTop center-block pull-right" type="submit">Guardar y continuar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- step 5 -->

                    <!-- step 6 -->
                    <div class="row setup-content col-md-12" id="step-6">
                        <div class="form-widget">
                            <div class="form-result"></div>
                            <form role="form" id="Satisfaccion" action="Satisfaccion" method="post" accept-charset="utf-8" autocomplete="off" enctype="multipart/form-data">
                                <div class="form-process"></div>
                                <input type="hidden" name="cve_encuesta" class="cve_encuesta">

                                <div class="col-md-12">
                                    <h3> SATISFACCION DEL PRODUCTOR</h3>
                                    <div class="row">
                                        <div class="col-md-4 col-sm-12">
                                            <div class="form-group">
                                                <label class="control-label"> NOMBRE DEL CEDAS EN EL QUE RECIBIO SU APOYO:</label>
                                                <select class="selectpicker form-control customjs" name="cedas_nombre" required="required" id="cedas_nombre" title="SELECCIONE UNA OPCIÓN" data-size="7" data-live-search="true" data-live-search="true" style="width:100%;">
                                                    <option value="">SELECCIONE UNA OPCIÓN</option>
                                                    <?php foreach ($catCedas as $ceda) : ?>
                                                        <option value="<?= $ceda["cve_ceda"] ?>"><?= $ceda["cedas"] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-12">

                                            <div class="form-group">
                                                <label class="control-label"> ¿CONOCE QUIEN APORTA LOS RECURSOS DEL PROGRAMA DE FERTILIZANTES PARA EL BIENESTAR? </label>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="radio" name="conoce_recursos" id="conocejefeSI" placeholder="Yes" value="1" required="required">
                                                        <label for="conocejefeSI">SI</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <input type="radio" name="conoce_recursos" id="conocejefeNO" placeholder="NO" value="0" required="required">
                                                        <label for="conocejefeNO">NO</label>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-12 nivelJefe">
                                            <div class="form-group">
                                                <label class="control-label">¿A QUIEN?</label>
                                                <select name="cve_recurso" id="cve_recurso" class="form-control">
                                                    <option value="">SELECCIONE UNA OPCIÓN</option>
                                                    <?php foreach ($catrecursos as $catrecursos) : ?>
                                                        <option value="<?= $catrecursos["cve_recurso"] ?>"><?= $catrecursos["recurso"] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row espeQuien">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">¿QUIEN?</label>
                                                <input maxlength="100" name="quien" id="quien" type="text" class="form-control" placeholder="ESPECIFICACIÓN DE A QUIEN CONOCE">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label"> ¿USTED FUE CONVOCADO CON ANTICIPACIÓN A RECIBIR EL FERTILIZANTE?</label>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="radio" name="convocado" id="convocacionfertiSI" placeholder="Yes" value="1" required="required">
                                                        <label for="convocacionfertiSI">SI</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <input type="radio" name="convocado" id="convocacionfertiNO" placeholder="NO" value="0" required="required">
                                                        <label for="convocacionfertiNO">NO</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label">¿CUANTO TIEMPO ESPERÓ EN EL CEDAS DESDE QUE LLEGO HASTA QUE CARGO SU FERTILIZANTE?</label>
                                                <!-- <input maxlength="100" name="tiempoEspera" id="tiempoEspera" type="text" required class="form-control" placeholder="¿CUANTO TIEMPO ESPERÓ EN EL CEDAS DESDE QUE LLEGO HASTA QUE CARGO SU FERTILIZANTE?"> -->
                                                <select name="tiempo_espera" id="tiempo_espera" class="form-control" required>
                                                    <option value="">SELECCIONE UNA OPCIÓN</option>
                                                    <option value="1">Menos de 1 hora</option>
                                                    <option value="2">De 1 hora a 2 horas</option>
                                                    <option value="3">De 2 hora a 3 horas</option>
                                                    <option value="4">Más de 3 horas</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label"> ¿LE ENTREGARON EL FERTILIZANTE EL DIA QUE FUE CITADO AL (CEDAS)?</label>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="radio" name="entrega_formal" id="entregaendiaSI" placeholder="Yes" value="1" required="required">
                                                        <label for="entregaendiaSI">SI</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <input type="radio" name="entrega_formal" id="entregaendiaNO" placeholder="NO" value="0" required="required">
                                                        <label for="entregaendiaNO">NO</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label class="control-label"> ¿EN EL LUGAR DONDE RECIBIO EL FERTILIZANTES, ¿LE EXPLICARON COMO APLICARLO?</label>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="radio" name="explicacion_aplicarlo" id="expliAplicacionSI" placeholder="Yes" value="1" required="required">
                                                        <label for="expliAplicacionSI">SI</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <input type="radio" name="explicacion_aplicarlo" id="expliAplicacionNO" placeholder="NO" value="0" required="required">
                                                        <label for="expliAplicacionNO">NO</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">

                                                <label class="control-label"> ¿LE ENTREGARON UN TRIPTICO SOBRE EL USO DEL FERTILIZANTE?</label>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="radio" name="entrega_triptico" id="tripticoSI" placeholder="Yes" value="1" required="required">
                                                        <label for="tripticoSI">SI</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <input type="radio" name="entrega_triptico" id="tripticoNO" placeholder="NO" value="0" required="required">
                                                        <label for="tripticoNO">NO</label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-4 siDieronTriptico">
                                            <div class="form-group">
                                                <label class="control-label">¿ES CLARO Y SUFICIENTE EL CONTENIDO DEL TRÍPTICO?</label>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="radio" name="claro_triptico" id="claroTripticoSI" placeholder="Yes" value="1">
                                                        <label for="claroTripticoSI">SI</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <input type="radio" name="claro_triptico" id="claroTripticoNO" placeholder="NO" value="0">
                                                        <label for="claroTripticoNO">NO</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label"> ¿EN QUÉ CONDICIONES RECIBIO SU FERTILIZANTE?</label>
                                                <select name="cve_recibio_fertilizante" id="cve_recibio_fertilizante" class="form-control" required>
                                                    <option value="">SELECCIONE UNA OPCIÓN</option>
                                                    <?php foreach ($estadoFertili as $estado) : ?>
                                                        <option value="<?= $estado["cve_recibio_fcertilizante"] ?>"><?= $estado["recibio_fertilizante"] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label"> ¿QUÉ TAN SATISFECHO ESTA CON LA ATENCIÓN QUE RECIBIO EN EL CENTRO DE DISTRIBUCIÓN (CEDAS)?</label>
                                                <select name="cve_satisfecho" id="cve_satisfecho" class="form-control" required>
                                                    <option value="">SELECCIONE UNA OPCIÓN</option>
                                                    <?php foreach ($catAtencion as $atencion) : ?>
                                                        <option value="<?= $atencion["cve_atencion"] ?>"><?= $atencion["atencion"] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">¿POR QUÉ? </label>
                                                <textarea name="porque" id="porque" minlength="5" maxlength="200" rows="3" class="form-control" required></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label class="control-label"> ¿CONSIDERA QUE EL FERTILIZANTE RECIBIDO ES SUFICIENTE?</label>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="radio" name="recibio_suficiente" id="suficienciaferSI" placeholder="Yes" value="1" required="required">
                                                        <label for="suficienciaferSI">SI</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <input type="radio" name="recibio_suficiente" id="suficienciaferNO" placeholder="NO" value="0" required="required">
                                                        <label for="suficienciaferNO">NO</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label"> ¿CONSIDERA QUE LA APLICACIÓN DEL FERTILIZANTE LE AYUDO A MEJORAR SU PRODUCCIÓN?</label>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <input type="radio" name="mejora_produccion" id="mejoraproducSI" placeholder="Yes" value="1" required="required">
                                                        <label for="mejoraproducSI">SI</label>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <input type="radio" name="mejora_produccion" id="mejoraproducNO" placeholder="NO" value="0" required="required">
                                                        <label for="mejoraproducNO">NO</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 porcentaje">
                                            <div class="form-group">
                                                <label class="control-label"> ¿EN CASO DE SER AFIRMATIVO, ¿EN QUÉ PORCENTAJE?</label>
                                                <select name="cve_porcentaje" id="cve_porcentaje" class="form-control">
                                                    <option value="">SELECCIONE UNA OPCIÓN</option>
                                                    <?php foreach ($porcentajes as $porcentaje) : ?>
                                                        <option value="<?= $porcentaje["cve_porcentaje"] ?>"><?= $porcentaje["porcentaje"] ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-12 porquenoayudo">
                                            <div class="form-group">
                                                <label class="control-label">¿POR QUÉ? </label>
                                                <textarea name="porque_satisfaccion" id="porque_satisfaccion" minlength="5" maxlength="200" rows="5" class="form-control" required></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">¿TIENE ALGUNA SUGERENCIA PARA MEJORAR EL PROGRAMA DE FERTILIZANTES PARA EL BIENESTAR?</label>
                                                <textarea name="sugerencias" id="sugerencias" rows="5" maxlength="200" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="alert alert-danger noSeleccionoCheck">
                                        <div class="counter counter-inherit">
                                            <strong>Atencion:</strong>
                                            Usted tiene que seleccionar una opción <strong>en los campos</strong>.
                                        </div>
                                    </div>

                                    <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Regresar</button>
                                    <!-- <button class="btn btn-primary nextBtn gotoTop btn-lg pull-right" type="button">Siguiente</button>
                                    <button class="btn btn-success nextBtn gotoTop btn-lg center-block " type="submit">Registrar</button> -->
                                    <button class="btn btn-primary btn-lg  nextBtn gotoTop center-block pull-right" type="submit">Guardar y continuar</button>


                                </div>
                            </form>

                        </div>
                        <div id="noBeneficiarioSatis">
                            <div class="text-center">
                                <h3> LA SATISFACCION DEL PRODUCTOR NO PUEDE SER LLENADA POR EL, YA QUE NO ES BENEFICIARIO</h3>
                            </div>
                            <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Regresar</button>
                            <button class="btn btn-primary nextBtn gotoTop btn-lg pull-right" type="button">Siguiente</button>
                        </div>
                    </div>
                    <!-- step 6 -->



                    <!-- step 7 -->
                    <!-- te agrego el form para que mandes a tu funcion en especifico ;) de nada jefazo -->
                    <div class="row setup-content" id="step-7">
                        <div class="form-widget">
                            <div class="form-result"></div>
                            <form role="form" id="Condicion" action="condicion" method="post" accept-charset="utf-8" autocomplete="off">
                                <?php echo view("Agricultura/socioeconomicos"); ?>
                            </form>
                        </div>
                        <!-- step 7 -->
                        <!-- step 8 -->
                        <div class="row setup-content" id="step-8">
                            <div class="form-widget">
                                <div class="form-result"></div>
                                <form role="form" id="fotos" accept-charset="utf-8" autocomplete="off">
                                    <?php echo view("Agricultura/fotografias"); ?>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--fin seccion 8-->
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->endSection() ?>

<?= $this->section('js_linkvistas') ?>
<script>
    var miVariable = <?php echo isset($miVariableJson) && !empty($miVariableJson) ? json_encode($miVariableJson) : "null"; ?>;
    var curp = <?php echo isset($agricultor) && !empty($agricultor) ? json_encode($agricultor) : "null"; ?>;
</script>
<!-- mapa js  -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/1.3.4/html2canvas.min.js"></script>
<script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-core.js"></script>
<script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-service.js"></script>
<script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-ui.js"></script>
<script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-mapevents.js"></script>
<script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-data.js"></script>

<!-- mapa js -->
<script src="js/Agricultura/wizard.js?v=1.0.2"></script>
<script src="js/camara.js?v=1.0.0"></script>
<!-- <script src="js/Agricultura/datosGenerales.js?v=1.0.0"></script> -->
<script src="js/Agricultura/datosAgricultor.js?v=1.0.3"></script>
<script src="js/Agricultura/ubicacionUP.js?v=1.0.2"></script>
<script src="js/Agricultura/DatosTecnologicos.js?v=1.0.1"></script>
<script src="js/Agricultura/DatosProductivos.js?v=1.1.1"></script>
<script src="js/Agricultura/funcionalidadSelects56.js?v=1.0.2"></script>
<script src="js/Agricultura/ingresos_imagen.js?v=1.0.2"></script>
<script src="js/functions.js?v=1.0.0"></script>

<?= $this->endSection() ?>