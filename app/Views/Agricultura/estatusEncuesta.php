<form action="guardaCveEstatus" class="form-documentos" method="POST" enctype="multipart/form-data">
    <div class="form-group col-md-12 mb-12">
        <label class="text-center">¿Desea re-activar la encuesta?</label>
    </div>
    <input class="d-none" name="cve_encuesta" id="cve_encuesta" value="<?= $cve_encuesta ?>">
    <input class="d-none" name="cve_estatus" id="cve_estatus" value="1">
    <div class="col-md-12 text-center">
        <button class="btn btn-primary btn-lg  nextBtn center-block pull-center" type="submit">Activar</button>
    </div>
    </div>
</form>