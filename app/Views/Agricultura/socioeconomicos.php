<div class="form-process"></div>
<div class="col-md-12">
    <h3> CONDICIÓN SOCIOECONÓMICA</h3>
    <div class="row">
        <div class="col-md-3 form-group">
            <label class="control-label">¿DE QUE MATERIAL ES EL PISO DE SU VIVIENDA?</label>
            <select name="pisoVivienda" id="pisoVivienda" class="form-control">
                <option value="">SELECCIONE UNA OPCIÓN</option>
                <option value="1">TIERRA</option>
                <option value="2">CEMENTO</option>
                <option value="3">MADERA / MOSAICO </option>
            </select>
        </div>

        <div class="col-md-3 form-group">
            <label class="control-label">¿DE QUE MATERIAL SON LAS PAREDES DE LA VIVIENDA?</label>
            <select name="paredVivienda" id="paredVivienda" class="form-control">
                <option value="">SELECCIONE UNA OPCIÓN</option>
                <option value="1">TABIQUE / BLOCK / PIEDDRA</option>
                <option value="2">ADOBE</option>
                <option value="3">MADERA</option>
                <option value="4">MATERIALES BLANDOS (CARRIZO, LAMINA, BAMBU, ETC)</option>
            </select>
        </div>
        <div class="col-md-3 form-group">
            <label class="control-label">¿DE QUÉ MATERIAL ES EL TECHO DE SU VIVIENDA?</label>
            <select name="techoVivienda" id="techoVivienda" class="form-control">
                <option value="">SELECCIONE UNA OPCIÓN</option>
                <option value="1">LOSA CONCRETO</option>
                <option value="2">LAMINA</option>
                <option value="3">MADERA / PALMA</option>
            </select>
        </div>

        <div class="col-md-3 form-group">
            <label class="control-label">¿CON QUÉ SERVICIOS CUENTA EN LA VIVIENDA?</label>
            <select class="selectpicker form-control customjs" name="servVivienda[]" id="servVivienda" required="required" id="serviciosVivienda" title="SELECCIONE UNA OPCIÓN" data-size="7" data-live-search="true" multiple data-live-search="true" style="width:100%;">
                <option value="1">ENERGIA ELECTRICA</option>
                <option value="2">AGUA POTABLE</option>
                <option value="3">DRENAJE</option>
                <option value="4">TELEFONIA / INTERNET</option>
            </select>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 form-group">
            <label class="control-label">SIN CONTAR CON LA SALA O ESPACIOS COMUNES Y EL BAÑO ¿CUÁNTOS CUARTOS TIENE EN SU VIVIENDA?</label>
            <input maxlength="100" type="number" name="espaciosVivienda" id="espaciosVivienda" required="required" class="form-control" placeholder="número">
        </div>
        <div class="col-md-6 form-group">
            <label class="control-label">EL SERVICIO MEDICO QUE RECIBE LA FAMILIA ES DE:</label>
            <select name="servMedico" id="servMedico" class="form-control">
                <option value="">SELECCIONE UNA OPCIÓN</option>
                <option value="1">IMSS</option>
                <option value="2">ISSSTE</option>
                <option value="3">FUERZAS ARMADAS</option>
                <option value="4">CENTRO DE SALUD</option>
                <option value="5">NO TIENE SERVICIO MEDICO PUBLICO</option>
            </select>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3 form-group">
            <label class="control-label">¿CUÁL FUE SU INGRESO TOTAL DENTRO DE SU UNIDAD DE PRODUCCIÓN?</label>
            <input maxlength="100" type="number" name="ingreso" id="ingreso" required="required" class="form-control" onblur="validaProduccion();" placeholder="Cantidad en pesos">
        </div>
    </div>
    <div class="col-md-3 form-group">
        <label class="control-label">¿TIENE INGRESOS FUERA DE SU PRODUCCIÓN?</label>
        <div class="form-check form-check-inline">
            <input class="form-check-input" checked="checked" type="radio" value="1" name="flexRadioDefault" id="flexRadioDefault1">
            <label class="form-check-label" for="flexRadioDefault1">
                Si
            </label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input" type="radio" value="0" name="flexRadioDefault" id="flexRadioDefault2">
            <label class="form-check-label" for="flexRadioDefault2">
                No
            </label>
        </div>
    </div>
</div>
<div class="row ">
    <div class="row ">
        <div class="col-md-12 ingresos">
            <div class="form-group form-group">
                <label class="control-label">¿CUÁLES SON SUS FUENTES DE INGRESO FUERA DE SU UNIDAD DE PRODUCCIÓN Y EN QUE CANTIDAD EN PESOS?:</label>
                <div class="row">
                    <div class="col-md-2 col-sm-12">
                        <label class="control-label">remesas</label>
                        <input maxlength="12" class="ingresosCampos entero form-control" onblur="validaEnteros(); totali()" type="number" name="remesas" id="remesas" required="required" placeholder="Remesas">
                    </div>
                    <div class="col-md-2 col-sm-12">
                        <label class="control-label">Apoyos Familiares en el Pais</label>
                        <input maxlength="12" type="number" class="ingresosCampos entero form-control" onblur="validaEnteros(); totali()" name="apoyoFamiliares" id='apoyoFamiliares' required="required" placeholder="Apoyos Familiares en el Pais">
                    </div>
                    <div class="col-md-2 col-sm-12">
                        <label class="control-label">Negocio Propio</label>
                        <input maxlength="12" type="number" class="ingresosCampos entero form-control" onblur="validaEnteros(); totali()" name="negocioPropio" id='negocioPropio' required="required" placeholder="Negocio Propio">
                    </div>
                    <div class="col-md-2 col-sm-12">
                        <label class="control-label">Empleo fuera de la UP</label>
                        <input maxlength="12" type="number" class="ingresosCampos entero form-control" onblur="validaEnteros(); totali()" name="empleoFueraUP" id='empleoFueraUP' required="required" placeholder="Empleo fuera de la UP">
                    </div>
                    <div class="col-md-2 col-sm-12">
                        <label class="control-label">Pension / jubilacion</label>
                        <input maxlength="12" type="number" class="ingresosCampos entero form-control" onblur="validaEnteros(); totali()" name="pension" id='pension' required="required" placeholder="Pension / jubilacion">
                    </div>
                    <div class="col-md-2 col-sm-12">
                        <label class="control-label">Otros</label>
                        <input maxlength="12" type="number" class="ingresosCampos entero form-control" onblur="validaEnteros(); totali()" name="otros" id='otros' required="required" placeholder="Otros">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 col-sm-12 form-group">
                        <input maxlength="12" type="number" readonly class=" form-control" value=0 name="total" id='total' placeholder="Total Ingresos fuera UP">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 ">
            <label class="control-label">¿CUANTAS PERSONAS INTEGRAN SU FAMILIA?</label>
        </div>
        <div class="col-md-4  form-group">
            <input maxlength="10" type="number" required="required" id="numeroIntegrantes" name="numeroIntegrantes" onblur="validaEnteros()" class="form-control entero" placeholder="Personas">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <label class="control-label">¿ALGUNOS INTEGRANTES DEL HOGAR RECIBEN APOYOS DE LOS PROGRAMAS SOCIALES DE GOBIERNO?</label>
        </div>
        <div class="col-md-4">
            <div class="form-group ">
                <div class="form-check form-check-inline">
                    <input class="form-check-input " checked="checked" type="radio" value="1" name="produccion" id="produccion1">
                    <label class="form-check-label" for="produccion1">
                        Si
                    </label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" value="0" name="produccion" id="produccion2">
                    <label class="form-check-label" for="produccion2">
                        No
                    </label>
                </div>
            </div>
        </div>
        <div class="col-md-4 integrantes form-group">
            <input maxlength="10" type="number" required="required" id="monto_total_menusal" name="monto_total_menusal" onblur="validaEnteros()" class="form-control entero apoyos" placeholder="Cantidad en Pesos Mensuales">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <label class="control-label">COMENTARIOS DEL ENCUESTADOR</label>
    </div>

    <div class="col-md-12">
        <textarea class="form-control" name='observaciones_encuestador'></textarea>
    </div>
</div>
<br>
<button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Regresar</button>
<button class="btn btn-primary btn-lg  nextBtn center-block pull-right" type="submit">Guardar y continuar</button> <!-- "Enviar" este boton es en lo que se implementa la funcion de enviar cuestionario y siguiente al mismo tiempo, 
            mientras tanto ya se va a mandar esta parte del fomulario con ajax sin necesidad de que coloquen uno-->
</div>