                                <div class="col-xs-10 col-md-offset-1">
                                    <div class="col-md-12">
                                        <h3> EVIDENCIA FOTOGRAFICA</h3>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <video autoplay id="video" height="400" width="400"></video>
                                                </div>
                                                <div class="row">

                                                    <button class="col-md-3 button is-success" type="button" id="btnScreenshot">
                                                        <span class="icon is-small">
                                                            <i class="fas fa-camera">Productor</i>
                                                        </span>
                                                    </button>
                                                    <button class="col-md-4 button is-success" type="button" id="btnScreenshot2">
                                                        <span class="icon is-small">
                                                            <i class="fas fa-camera">INE</i>
                                                        </span>
                                                    </button>
                                                    <button class="col-md-4 button" id="btnChangeCamera">
                                                        <span class="icon">
                                                            <i class="fas fa-sync-alt">Cambia Camara</i>
                                                        </span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="img1"></div>
                                            <div id="img1txt" hidden></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="img2"></div>
                                        </div>
                                    </div>
                                    <canvas hidden id="canvas"></canvas>
                                    <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Regresar</button>
                                    <button class="btn btn-primary btn-lg  nextBtn center-block pull-right" onclick='guardarImagenes()' type="button">Guardar y terminar</button>
                                </div>