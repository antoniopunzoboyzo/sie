<!-- esta seccion debe estar fuera del form -->
<h3> INFORMACION DEL PRODUCTOR</h3>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">MUNICIPIO</label>
            <select name="cve_municipios" id="cve_municipios" class="form-control" required="required">
                <option value="">Seleccione una opción</option>
                <?php foreach ($agricultoresMpios as $mpios) : ?>
                    <option value="<?= $mpios["cve_mpioINEGI"] ?>"><?= $mpios["municipio"] ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
    <div class="col-md-6 nomagri" id="nomagri">
        <div class="form-group">
            <label class="control-label">NOMBRE</label>
            <select class="form-control selectpicker  customjs" required="required" name="nombreBusqueda" id="nombreBusqueda" title="SELECCIONE UNA OPCIÓN" data-size="7" data-live-search="true" data-live-search="true" style="width:100%;">
            </select>
        </div>
    </div>
    <div class="col-md-6 nomAgriConsulta" id="nomAgriConsulta">
    </div>
</div>

<!-- esta seccion debe estar fuera del form -->

<form role="form" id="RegistroAgricultura" action="RegistroAgricultura" method="post" accept-charset="utf-8" autocomplete="off">
    <div class="form-process"></div>
    <div class="col-md-12">

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label">CURP:</label>
                    <input maxlength="100" readonly id="curp" name="curp" type="text" required="required" class="form-control" placeholder="Ingrese la CURP del productor">
                    <input class="cve_encuesta" type="hidden">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label"> FOLIO DEL ACUSE ESTATAL:</label>
                    <input maxlength="100" readonly type="text" id="folio_estatal" name="folio_estatal" required="required" class="form-control" placeholder="Folio Estatal">
                </div>
            </div>
            <div id="div_alert_curp" class="col-md-12 alert alert-danger d-none" role="alert">
                La CURP del agricultor ingresada es incorrecto.
            </div>
        </div>
        <!--Row datos seguir encuesta-->
        <div class="row pregunta">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label">¿ME PERMITE CONTINUAR CON LA ENTREVISTA?</label>
                </div>
            </div>
            <div class="col-6">
                <input type="radio" id="si" name="cve_estatus" value="1">
                <label for="si">Si</label><br>
            </div>
            <div class="col-6">
                <input type="radio" id="no" name="cve_estatus" value="0">
                <label for="no">No, agradecer y terminar</label><br>
            </div>
            <div class="col-md-6 noporque">
                <div class="form-group">
                    <label class="control-label">No, ¿Por qué?</label>
                    <input maxlength="100" type="text" id="porque" name="porque" class="form-control" placeholder="¿Por qué no quizó contestar?">
                </div>
            </div>
        </div>
        <p></p>
        <div class="agricultorOculto">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">¿ES USTED EL PROPIETARIO DE LA UP?</label>
                        <select name="productor_contesta" id="productor_contesta" class="form-control" required="required">
                            <option value="">SELECCIONE UNA OPCIÓN</option>
                            <option value="0">NO</option>
                            <option value="1">SI</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 no_contesta">
                    <div class="form-group">
                        <label class="control-label">¿QUIÉN CONTESTA?</label>
                        <select class="form-control selectpicker  customjs" name="cve_quien_contesta" id="cve_quien_contesta" title="SELECCIONE UNA OPCIÓN" data-size="7" data-live-search="true" style="width:100%;">
                            <option value="">SELECCIONE UNA OPCIÓN</option>
                            <?php foreach ($quien_contesta as $contesta) : ?>
                                <option value="<?= $contesta["cve_quien_contesta"] ?>"><?= $contesta["quien_contesta"] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 no_contesta">
                    <div class="form-group">
                        <label class="control-label">NOMBRE DE QUIÉN CONTESTA</label>
                        <input id="nombre_quien_contesta" name="nombre_quien_contesta" type="text" class="form-control" placeholder="Nombre de quién contesta">
                    </div>
                </div>
            </div>
            <p>UBICACIÓN DE LA UNIDAD DE PRODUCCIÓN</p>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">LOCALIDAD</label>
                        <select class="form-control selectpicker  customjs" required name="cve_localidad_unidad" id="cve_localidad_unidad" title="SELECCIONE UNA OPCIÓN" data-size="7" data-live-search="true" style="width:100%;">
                            <!-- <select name="cve_localidad_unidad" id="cve_localidad_unidad" class="form-control" required="required">        -->
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">¿PERTENECE A UN EJIDO?</label>
                        <select name="es_ejidatario" id="es_ejidatario" class="form-control" required>
                            <option value="">SELECCIONE UNA OPCIÓN</option>
                            <option value="0">NO</option>
                            <option value="1">SI</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 ejidatario">
                    <div class="form-group">
                        <label class="control-label">EJIDO / PEQUEÑA PROPIEDAD</label>
                        <!-- <select class="form-control selectpicker customjs" name="ejido" id="ejido" title="SELECCIONE UNA OPCIÓN" data-size="7" data-live-search="true" style="width:100%;"> -->
                        <select name="ejido" id="ejido" class="form-control" required>
                            <option value="">SELECCIONE UNA OPCIÓN</option>
                        </select>
                    </div>
                </div>
            </div>

            <hr>
            <p>GEOREFERENCIA DE LA UNIDAD DE PRODUCCIÓN</p>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">LATITUD</label>
                        <input id="latitud" name="latitud" readonly type="text" required="required" class="form-control" placeholder="Latitud">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">LONGITUD</label>
                        <input id="longitud" name="longitud" readonly type="text" required="required" class="form-control" placeholder="Longitud">
                    </div>
                </div>
                <div class="mb-5" id="map"></div>
                <div id="panel">
                </div>
            </div>

            <div class="row imgGuardada">
                <img src="" name="mapa" id="mapa" alt="" class="imgMapa" srcset="" required>
            </div>
            <hr>

            <input type="hidden" id="hiddenInputField" name="mapImage" value="">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">TELÉFONO - CELULAR</label>
                        <input maxlength="10" minlength="10" inputmode="tel" id="celular" name="telefono" type="tel" required onkeypress="return solonumeros(event)" pattern="[0-9]{10}" class="form-control" placeholder="443XXXXXXX">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">¿DESDE QUE AÑO ES PRODUCTOR? </label>
                        <input maxlength="4" minlength="4" id="anio_productor" required name="anio_productor" type="text" required="required" onkeypress="return solonumeros(event)" pattern="[0-9]{4}" class="form-control" placeholder="Año de productor">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">¿HABLA ALGUN DIALECTO? (LENGUA INDIGENA)</label>
                        <select name="dialecto" id="dialecto" class="form-control" required>
                            <option value="">SELECCIONE UNA OPCIÓN</option>
                            <option value="0">NO</option>
                            <option value="1">SI</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row ocultar_dialecto">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">¿CUAL?</label>
                        <input maxlength="15" id="dialecto_hablado" name="dialecto_hablado" type="text" class="form-control text-uppercase" placeholder="¿Cuál?">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">¿SABE LEER Y ESCRIBIR UN RECADO?</label>
                        <select name="leer_escribir" id="leer_escribir" class="form-control" required>
                            <option value="">SELECCIONE UNA OPCIÓN</option>
                            <option value="0">NO</option>
                            <option value="1">SI</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">¿AÑOS EN SISTEMA ESCOLAR?</label>
                        <input maxlength="2" minlength="1" id=" anio_estudio" name="anio_estudio" type="text" required onkeypress="return solonumeros(event)" pattern="[0-9]{2}" class="form-control text-uppercase" placeholder="Inserte los años en el sistema escolar">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label"> ¿ES BENEFICIARIO DEL PROGRAMA DE FERTILIZANTES PARA EL BIENESTAR?</label>
                        <select name="beneficiario_bienestar" id="beneficiario_bienestar" class="form-control" required>
                            <option value="">SELECCIONE UNA OPCIÓN</option>
                            <option value="0">NO</option>
                            <option value="1">SI</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <button class="btn btn-primary prevBtn btn-lg pull-left" type="button">Regresar</button>
        <button class="btn btn-primary btn-lg  nextBtn center-block pull-right" type="submit">Guardar y continuar</button>
    </div>
</form>