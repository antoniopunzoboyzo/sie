<?= $this->extend('Layout') ?>
<?= $this->section('CSS') ?>

<?= $this->endSection() ?>
<?= $this->section('css_linkvistas') ?>
<!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.13.7/css/jquery.dataTables.min.css"> -->
<link rel="stylesheet" href="js/datatables/dataTables.bootstrap4.min.css">
<!-- <link rel="stylesheet" href="js/datatables/dataTables.bootstrap4.min.css"> -->

<?= $this->endSection() ?>

<?= $this->section('content') ?>
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="form-widget">
                <div class="row">
                    <div class="col-lg-12">

                        <!-- titulo -->
                        <div class="row">
                            <div class="col-md-10 mt-4">
                                <div class="container mt-5 text-center">
                                    <h4 style="text-transform:uppercase;">SECRETARIA DE AGRICULTURA Y DESARROLLO RURAL MICHOACÁN</h4>
                                    <p>Listado de Evaluación del Programa de Fertilizantes para el Bienestar en el Estado de Michoacán
                                        Productores</p>
                                </div>
                            </div>
                            <div class="col-md-2 mt-4">
                                <div class="container mt-5 text-center">
                                    <form action="DescargarReporte" method="post" target="_blank" id="reporte">
                                        <button type="submit" class="btn btn-success" id="descargarReporte"><i class="fas fa-camera"></i> Reporte</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- titulo -->

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped" cellspacing="0" width="100%" id="datatable1" data-ordering="false">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>FOLIO</th>
                                        <th>CURP</th>
                                        <th>Nombre</th>
                                        <th>DDR</th>
                                        <th>Municipio</th>
                                        <th>Folio estatal</th>
                                        <th>Encuestador</th>
                                        <th>Estado</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php $count = 1;
                                    foreach ($agricultores as $agricultor) :
                                        // var_dump($agricultor["beneficiario"]);
                                    ?>
                                        <tr>
                                            <td>
                                                <?= $count; ?>
                                            </td>
                                            <td>
                                                <?= $agricultor["cve_encuesta"] ?>
                                            </td>
                                            <td>
                                                <?= $agricultor["curp"] ?>
                                            </td>
                                            <td>
                                                <?= $agricultor["paterno"] . " " . $agricultor["materno"] . " " . $agricultor["nombre"] ?>
                                            </td>
                                            <td>
                                                <?= $agricultor["ddr_nombre"] ?>
                                            </td>
                                            <td>
                                                <?= $agricultor["municipio"] ?>
                                            </td>
                                            <td>
                                                <?= $agricultor["folio_estatal"] ?>
                                            </td>
                                            <td>
                                                <?= $agricultor["nombreEncuestador"] ?>
                                            </td>
                                            <td>
                                                <?php
                                                switch ($agricultor["cve_estatus"]) {
                                                    case 0:
                                                        print_r("NO CONTESTO");
                                                        break;
                                                    case 1:
                                                        print_r("EN PROCESO DE ENCUESTA..");
                                                        break;
                                                    case 2:
                                                        print_r("FINALIZADA");
                                                        break;
                                                    default:
                                                        print_r("SIN ENCUESTAR..");
                                                        break;
                                                }
                                                ?>
                                            </td>

                                            <td>
                                                <div class="btn-group dropup">
                                                    <button type="button" class="btn btn-secondary dropdown-toggle button-teal" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Acciones
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <?php switch ($agricultor["cve_estatus"]) {
                                                            case 0: ?>
                                                                <a class="dropdown-item" href="generaPFB_PDF/<?= $agricultor["cve_encuesta"] ?>/0" title="Imprimir PDF No contestó." target="_blank"><i class="icon-file-pdf btn btn-danger btn-circle btn-sm"></i> Imprimir PDF </a>
                                                                <div class="dropdown-divider"></div>
                                                                <?php break; ?>
                                                            <?php
                                                            case 2: ?>
                                                                <a class="dropdown-item _btnModCambiaEstatus" data-cve_encuesta="<?= $agricultor["cve_encuesta"] ?>" data-toggle="modal" data-target="#modalCambiaEstatus" href="#"> <i class="icon-line-unlock btn btn-warning btn-circle btn-sm"></i> Activar encuesta</a>
                                                                <div class="dropdown-divider"></div>
                                                                <?php if ($agricultor["beneficiario"] == 1) : ?>
                                                                    <a class="dropdown-item" href="generaPDF/<?= $agricultor["cve_encuesta"] ?>" target="_blank"><i class="icon-file-pdf btn btn-danger btn-circle btn-sm"></i> PDF sencillo</a>
                                                                    <div class="dropdown-divider"></div>
                                                                    <a class="dropdown-item" href="generaPFB_PDF/<?= $agricultor["cve_encuesta"] ?>/2" target="_blank"><i class="icon-file-pdf btn btn-danger btn-circle btn-sm"></i> PDF completo</a>
                                                                    <div class="dropdown-divider"></div>
                                                                <?php endif; ?>
                                                                <?php if (session()->getFlashdata('SuperAdmin') == 1) { ?>
                                                                    <a class="dropdown-item" href="Padron?mi_parametro=<?= esc($agricultor["curp"]) ?>" target="_blank"><i class="icon-line-eye btn  btn-primary btn-circle btn-sm"></i> Ver encuesta</a>
                                                                <?php } ?>
                                                                <?php break; ?>
                                                        <?php
                                                            default:
                                                                break;
                                                        }

                                                        ?>
                                                    </div>

                                                </div>
                                            </td>
                                        </tr>

                                    <?php $count++;
                                    endforeach; ?>

                                </tbody>
                            </table>

                        </div>



                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade " id="modalCambiaEstatus">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Cambiar el estatus de la encuesta</h4>
                        <h5 class="modal-title" id="exampleModalLabel">
                            <span id="spanModCveEstatus" class="text-primary font-weight-bold">0000-000000-0000</span>
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div id="modalCambiaEstatusBody" class="modal-body">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?= $this->endSection() ?>
<?= $this->section('js_linkvistas') ?>
<!-- <script src="js/Agricultura/wizard.js?v=1.0.2"></script> -->
<!-- <script src="js/components/bs-datatable.js"></script> -->
<script src="js/components/bs-datatable.js"></script>
<script src="js/dataTables.bootstrap4.min.js"></script>
<script src="js/datatables/dataTables.buttons.min.js"></script>
<script src="js/datatables/pdfmake.min.js"></script>
<script src="js/datatables/vfs_fonts.js"></script>
<script src="js/datatables/buttons.html5.min.js"></script>
<script src="js/datatables/buttons.bootstrap4.min.js"></script>
<script src="js/datatables/jszip.min.js"></script>
<script src="js/listadosAgricultores/AgricultoresListado.js?v=1.1.0"></script>
<script src="js/listadosAgricultores/listadoSupervisor.js?v=1.1.0"></script>
<script src="js/Agricultura/ingresos_imagen.js?v=1.0.0"></script>
<script src="js/functionSinReporte.js?v=1.0.0"></script>

<?= $this->endSection() ?>