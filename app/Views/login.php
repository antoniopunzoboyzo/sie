   <!DOCTYPE html>
   <html lang="en">

   <head>
       <meta charset="UTF-8">
       <meta http-equiv="X-UA-Compatible" content="IE=edge">
       <meta name="viewport" content="width=}, initial-scale=1.0">
       <title>Registro</title>
   </head>
   <!-- Stylesheets
	============================================= -->
   <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />
   <link rel="stylesheet" href="bootstrap/css/bootstrap.css" type="text/css" />
   <link rel="stylesheet" href="style.css" type="text/css" />
   <link rel="stylesheet" href="css/dark.css" type="text/css" />
   <link rel="stylesheet" href="css/font-icons.css" type="text/css" />
   <link rel="stylesheet" href="css/animate.css" type="text/css" />
   <link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />
   <link rel="stylesheet" href="css/components/bs-filestyle.css" type="text/css" />
   <link rel="stylesheet" href="css/components/select-boxes.css" type="text/css" />
   <link rel="stylesheet" href="css/components/bs-select.css" type="text/css" />
   <link rel="stylesheet" href="css/components/datepicker.css" type="text/css" />
   <link rel="stylesheet" href="css/components/timepicker.css" type="text/css" />
   <link rel="stylesheet" href="css/components/ion.rangeslider.css" type="text/css" />
   <link rel="stylesheet" href="css/components/bs-rating.css" type="text/css" />
   <link rel="stylesheet" href="css/components/bs-switches.css" type="text/css" />
   <link rel="stylesheet" href="css/responsive.css" type="text/css" />
   <meta name="viewport" content="width=device-width, initial-scale=1" />

   <body class="stretched">
       <div id="wrapper" class="clearfix">
           <section id="content">
               <div class="content-wrap nopadding">

                   <div class="section nopadding nomargin" style="width: 100%; height: 100%; position: absolute; left: 0; top: 0; background: url('imagenes/siembra.webp') center center no-repeat; background-size: cover;"></div>

                   <div class="section nobg full-screen nopadding nomargin">
                       <div class="container-fluid vertical-middle divcenter clearfix">
                           <div class="card divcenter noradius noborder" style="max-width: 400px; background-color: rgba(255,255,255,0.93);">
                               <div class="card-body" style="padding: 40px;">
                                   <form id="login-form" name="login-form" class="nobottommargin" action="acceso" method="post">
                                       <h3>Iniciar Sesion</h3>
                                       <div class="col_full">
                                           <label for="login-form-username">correo:</label>
                                           <input type="text" id="login-form-username" name="login-form-username"  value="" class="form-control not-dark" />
                                       </div>
                                       <div class="col_full">
                                           <label for="login-form-password">constraseña:</label>
                                           <input type="password" id="login-form-password" name="login-form-password"  value="" class="form-control not-dark" />
                                       </div>
                                       <div class="col_full nobottommargin">
                                           <button class="button button-3d button-black nomargin" id="login-form-submit" name="login-form-submit" value="login">Login</button>
                                       </div>
                                   </form>
                                   <div class="line line-sm"> <?= isset($msg)?$msg:""; ?></div>
                               </div>
                           </div>
                       </div>
                   </div>

               </div>
           </section>
       </div>
       <!-- js -->
       <div id="gotoTop" class="icon-angle-up"></div>
       <script src="js/jquery.js"></script>
       <script src="js/plugins.js"></script>
       <script src="js/components/bs-filestyle.js"></script>
       <script src="js/components/bs-select.js"></script>
       <script src="js/components/datepicker.js"></script>
       <script src="js/components/daterangepicker.js"></script>
       <script src="js/components/moment.js"></script>
       <script src="js/components/rangeslider.min.js"></script>
       <script src="js/components/select-boxes.js"></script>
       <script src="js/components/selectsplitter.js"></script>
       <script src="js/components/star-rating.js"></script>
       <script src="js/components/timepicker.js"></script>
       <script src="js/components/tinymce/tinymce.min.js"></script>
       <script src="js/functions.js?v=1.0.0"></script>
   </body>

   </html>