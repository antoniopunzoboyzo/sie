<!-- Stylesheets
	============================================= -->
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="bootstrap/css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="style.css" type="text/css" />
<link rel="stylesheet" href="css/dark.css" type="text/css" />
<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
<link rel="stylesheet" href="css/animate.css" type="text/css" />
<link rel="stylesheet" href="css/magnific-popup.css" type="text/css" />
<link rel="stylesheet" href="css/components/bs-filestyle.css" type="text/css" />
<link rel="stylesheet" href="css/components/select-boxes.css" type="text/css" />
<link rel="stylesheet" href="css/components/bs-select.css" type="text/css" />
<link rel="stylesheet" href="css/components/datepicker.css" type="text/css" />
<link rel="stylesheet" href="css/components/timepicker.css" type="text/css" />
<link rel="stylesheet" href="css/components/ion.rangeslider.css" type="text/css" />
<link rel="stylesheet" href="css/components/bs-rating.css" type="text/css" />
<link rel="stylesheet" href="css/components/bs-switches.css" type="text/css" />
<link rel="stylesheet" href="css/responsive.css" type="text/css" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<?= $this->renderSection('css_linkvistas') ?>
<title>Registro agricultura</title>
<?= $this->renderSection('CSS') ?>

<body class="stretched">

    <!-- Document Wrapper
	============================================= -->
    <div id="wrapper" class="clearfix">

        <!-- Header
		============================================= -->
        <header id="header" class="full-header no-sticky clearfix">

            <div id="header-wrap">

                <div class="container clearfix">

                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                    <!-- Logo
					============================================= -->
                    <div id="logo">
                        <!-- <a href="#" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src="imagenes/sader.svg" alt="Logo sader"></a>
                        <a href="#" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src="imagenes/sader.svg" alt="Logo sader"></a> -->
                    </div><!-- #logo end -->

                    <!-- Primary Navigation
					============================================= -->
                    <nav id="primary-menu">
                        <ul>
                            <li>
                                <a href="#">
                                    <div onclick='cerrarsesion()'>Cerrar Sesion </div>
                                </a>

                            </li>

                        </ul>
                        <!-- <ul>
                            <li><a href="#">
                                    <div>Home</div>
                                </a>
                                <ul>
                                    <li><a href="#">
                                            <div>Home - Corporate</div>
                                        </a>
                                        <ul>
                                            <li><a href="#">
                                                    <div>Corporate - Layout 1</div>
                                                </a></li>
                                        </ul>
                                    </li>

                                </ul>
                            </li>

                        </ul> -->

                        <!-- <div id="top-search">
                            <a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
                            <form action="search.html" method="get">
                                <input type="text" name="q" class="form-control" value="" placeholder="Busqueda" autocomplete="off">
                            </form>
                        </div> -->
                        <!-- #top-search end -->

                    </nav><!-- #primary-menu end -->

                </div>

            </div>

        </header><!-- #header end -->
        <!-- Content
		============================================= -->
        <?= $this->renderSection('content') ?>
        <!-- #content end -->

        <!-- Footer
		============================================= -->
        <footer id="footer" class="dark">

            <!-- Copyrights
			============================================= -->
            <div id="copyrights">

                <div class="container clearfix">

                    <div class="col_half">
                        Copyrights &copy; 2023 All Rights Reserved by MAJP Inc.<br>
                        <div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div>
                    </div>

                    <div class="col_half col_last tright">
                        <div class="fright clearfix">
                            <a href="#" class="social-icon si-small si-borderless si-facebook">
                                <i class="icon-facebook"></i>
                                <i class="icon-facebook"></i>
                            </a>

                            <a href="#" class="social-icon si-small si-borderless si-twitter">
                                <i class="icon-twitter"></i>
                                <i class="icon-twitter"></i>
                            </a>


                            <a href="#" class="social-icon si-small si-borderless si-github">
                                <i class="icon-github"></i>
                                <i class="icon-github"></i>
                            </a>



                            <a href="#" class="social-icon si-small si-borderless si-linkedin">
                                <i class="icon-linkedin"></i>
                                <i class="icon-linkedin"></i>
                            </a>
                        </div>

                        <div class="clear"></div>

                        <i class="icon-envelope2"></i> info@pruebas.com <span class="middot">&middot;</span> <i class="icon-headphones"></i> +44-33-223-234 <span class="middot">&middot;</span>
                    </div>

                </div>

            </div><!-- #copyrights end -->

        </footer><!-- #footer end -->

    </div><!-- #wrapper end -->

    <!-- js -->
    <div id="gotoTop" class="icon-angle-up"></div>
    <script src="js/jquery.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/components/bs-filestyle.js"></script>
    <script src="js/components/bs-select.js"></script>
    <script src="js/components/datepicker.js"></script>
    <script src="js/components/daterangepicker.js"></script>
    <script src="js/components/moment.js"></script>
    <script src="js/components/rangeslider.min.js"></script>
    <script src="js/components/select-boxes.js"></script>
    <script src="js/components/selectsplitter.js"></script>
    <script src="js/components/star-rating.js"></script>
    <script src="js/components/timepicker.js"></script>
    <script src="js/components/tinymce/tinymce.min.js"></script>
    <!-- js -->
    <?= $this->renderSection('js_linkvistas') ?>

    <script>
        jQuery(document).ready(function() {

            jQuery(".range_01").ionRangeSlider({
                grid: true,
                min: 18,
                max: 70,
                from: 30,
                prefix: "Age ",
                max_postfix: "+"
            });

            jQuery(".select-tags").select2({
                tags: true,
                placeholder: "Add Values and Press Enter"
            });

            jQuery('.datetimepicker1').datetimepicker();

            jQuery('.selectsplitter').selectsplitter();

            tinymce.init({
                selector: '.textarea-message',
                menubar: false,
                setup: function(editor) {
                    editor.on('change', function(e) {
                        editor.save();
                    });
                }
            });

        });

        // recargar pagina alerta
        var formHasChanged = false;
        var submitted = false;
        $("input[type='text'],input[type='url'],input[type='date'],select,textarea,input[type='radio']").on('change', function(e) {
            formHasChanged = true;
        });

        $(document).ready(function() {
            window.onbeforeunload = function(e) {
                if (formHasChanged && !submitted) {
                    var message = "You have not saved your changes.",
                        e = e || window.event;
                    if (e) {
                        e.returnValue = message;
                    }
                    return message;
                }
            }
            $("form").submit(function() {
                submitted = true;
            });
        });
        
    </script>
</body>