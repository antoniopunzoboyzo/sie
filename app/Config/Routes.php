<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/',                             'Auth::index');
$routes->get('/iniciarsesion',                'Auth::index');
$routes->post('acceso', 'Auth::loginAuth');
$routes->post('cerrarsesion',                'Auth::Logout');




// $routes->get('/', 'Home::index');
// $routes->get('Padron/(:any)', 'Padrones\Padron::index/$1');
$routes->add('Padron',           'Padrones\Padron::index');
$routes->add('Listados', 'Padrones\Padron::superAdmin');
$routes->post('DescargarReporte', 'Padrones\Padron::DescargarReporte');
$routes->match(['get', 'post'],   'Agricultores',          'Padrones\Padron::Agricultores');
$routes->match(['get', 'post'], 'RegistroAgricultura',  'Padrones\Padron::RegistroAgricultura');
// funcion para guardar step 4
    $routes->post('guardarDatosProductivos',  'Padrones\Padron::guardarDatosProductivos');
// funcion para guardar step 5
$routes->match(['get', 'post'], 'Capacitacion',  'Padrones\Padron::Capacitacion');
// funcion para guardar step 6
$routes->match(['get', 'post'], 'Satisfaccion',  'Padrones\Padron::Satisfaccion');

$routes->match(['get', 'post'],   'ObtenerInfoAgricultor',          'Padrones\Padron::ObtenerInfoAgricultor');





$routes->post( 'Tecnologicos',  'Padrones\Padron::Tecnologicos');


/**Encuestador */
$routes->match(['get', 'post'],   'obtieneAgricultor',          'Padrones\Padron::obtieneAgricultor');
$routes->match(['get', 'post'],   'obtieneEncuestadores',       'Padrones\Padron::obtieneEncuestadores');
$routes->match(['get', 'post'],   'getMunicipios',              'Padrones\Padron::obtieneMunicipios');
$routes->match(['get', 'post'],   'getLocalidades',             'Padrones\Padron::obtieneLocalidades');
$routes->match(['get', 'post'],   'getEjidos',                  'Padrones\Padron::obtieneEjidos');
$routes->match(['get', 'post'],   'getQuienContesta',           'Padrones\Padron::obtieneQuienContesta');
$routes->match(['get', 'post'],   'obtieneAgricultores',        'Padrones\Padron::obtieneAgricultores');
$routes->match(['get', 'post'],   'guardaCveEstatus',           'Padrones\Padron::cambiaStatusEncuesta');
$routes->match(['get', 'post'],   'CambiaEncuesta',             'Padrones\Padron::modificaCve_estatus');
$routes->match(['get', 'post'],   'condicion',                  'Padrones\Condicion::guardar');
$routes->match(['get', 'post'],   'guardarimagen',              'Padrones\Condicion::guardarimg');
$routes->match(['get', 'post'],   'generaPDF/(:any)',           'Padrones\Condicion::mostrarReporte/$1');
$routes->match(['get', 'post'],   'generaPFB_PDF/(:any)/(:any)',        'Padrones\Padron::mostrarReportePFB/$1/$2');




//asincronos
$routes->post('consultarCultivo',  'Padrones\Padron::consultarCultivo');

