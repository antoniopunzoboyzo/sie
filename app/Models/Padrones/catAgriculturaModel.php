<?php

namespace App\Models\Padrones;

use CodeIgniter\Model;

class catAgriculturaModel extends Model
{

    protected $DBGroup          = 'default';
    protected $table            = 'encuestadores';

    public function encuestadores($cve_encuestador)
    {
        $query = $this->db->table("encuestadores");
        $query->select("*");
        $query->where("cve_encuestador", $cve_encuestador);
        $resultado = $query->get()->getResultArray();
        return $resultado;
    }

    public function agricultores($cve_encuestador, $cve_municipio_unidad){
        if($cve_encuestador == 2){
            $where = "cve_municipio_unidad=" . $cve_municipio_unidad . " and cve_estatus=2";
        }else{
            $where = "cve_encuestador='".$cve_encuestador."' and cve_municipio_unidad=".$cve_municipio_unidad." and (cve_estatus is NULL or cve_estatus=1)";
        }
        $query = $this->db->table("agricultor");
        $query->select("curp, nombre, paterno, materno");
        $query->where($where);
        $resultado = $query->get()->getResultArray();
        return $resultado;
    }

    public function agricultoresMpio($cve_encuestador, $rol){
        $query = $this->db->table("agricultor as a");
        $query->select("m.cve_mpioINEGI, m.municipio");
        $query->join("cat_municipios m", " m.cve_mpioINEGI=a.cve_municipio_unidad");
        if($rol == 0){
            $query->where("a.cve_encuestador", $cve_encuestador);
        }
        $query->groupBy(" a.cve_municipio_unidad ");
        $resultado = $query->get()->getResultArray();
        return $resultado;
    }

    // public function agricultor($curp, $cve_mpio_encuestador){
    //     $query = $this->db->table("agricultor");
    //     $query->select("*");
    //     $query->where("curp", $curp);
    //    // $query->where("cve_municipio", $cve_municipio_up);
    //     $resultado = $query->get()->getResultArray();
    //     return $resultado;
    // }

    public function CatMunicipios($cve_mpio_encuestador){
        $table = $this->db->table("cat_municipios");
        $table->select("*");
        $table->where("cve_entidad", 16);
        $table->where("cve_mpioINEGI",$cve_mpio_encuestador);
        $resultados = $table->get()->getResultArray();
        return $resultados;
    }

    public function CatLocalidades($where){
        $table = $this->db->table("cat_localidades");
        $table->select("*");
        $table->where($where);
        //$table->where("cve_mpioINEGI", $cve_mpio_encuestador);
        $resultados = $table->get()->getResultArray();
        return $resultados;
    }

    public function CatEjidos($cve_municipio){
        $table = $this->db->table("cat_ejidos");
        $table->select("*");
        $table->where("cve_municipio", $cve_municipio);
        $resultados = $table->get()->getResultArray();
        return $resultados;
    }

    public function catQuienContesta(){
        $table = $this->db->table("cat_quien_contesta");
        $table->select("*");
        $resultados = $table->get()->getResultArray();
        return $resultados;
    }

    public function cambiaEstatusEncuesta($cve_estatus, $cve_encuesta){
        $this->db->transStart();
        $table = $this->db->table("agricultor");
        $table->set("cve_estatus", $cve_estatus);
        $table->where('cve_encuesta', $cve_encuesta);
        $table->update();
        $this->db->transComplete();
        if ($this->db->transStatus() === false) {
            return 0;
        } else {
            return 1;
        }
    }


    // ######################## PDF ##############################
    public function pdfConsulta_PFB($cve_encuesta="", $cve_estatus=""){
        if($cve_estatus==2){
            $encuesta="select a.cve_encuesta as cve_encuesta, a.cve_encuestador, a.telefono, concat(a.nombre, ' ', a.paterno, ' ', a.materno) as nombrebene, cve_estatus, 
            a.edad as edad, DATE_FORMAT(a.fecha_nacimiento, '%d-%m-%Y') as nacio, 
            DATE_FORMAT(a.fecha_encuesta, '%d               %m                %Y') as fecha_encuesta, case when substring(curp,11,1)='H' THEN 'MASCULINO' ELSE 'FEMENINO' END AS sexo, 
            a.curp, a.folio_estatal, if(a.productor_contesta = 1, 'SI', 'NO') AS productor_contesta, a.cve_quien_contesta, e.nombre, 
            cm.municipio, UPPER(cl.localidad) AS localidad, cqc.quien_contesta, a.nombre_quien_contesta, ce.ejido, a.latitud, a.longitud, a.anio_productor, a.anio_estudio, 
            a.dialecto, a.dialecto_hablado, a.leer_escribir, a.beneficiario_bienestar, ef.foto1, ef.foto2, ef.mapa,  
            g.*, ct.tipo_tenencia,  
            cc1.cultivo as ciclo_anterior_cultivo1, cc2.cultivo as ciclo_anterior_cultivo2, h.cultivo as cultivo_actual1, i.cultivo as cultivo_actual2, dt.*, csp.sistema, 
            ac.*, atc.*, asp.*, k.*, 
            concat(
                if((case when atc.cve_tema  is null then '' else atc.cve_tema  end) != '', concat((case when atc.cve_tema  is null then '' else atc.cve_tema  end), ','), ''), 
                if((case when atc.cve_tema1 is null then '' else atc.cve_tema1 end) != '', concat((case when atc.cve_tema1 is null then '' else atc.cve_tema1 end), ','), ''),
                if((case when atc.cve_tema2 is null then '' else atc.cve_tema2 end) != '', concat((case when atc.cve_tema2 is null then '' else atc.cve_tema2 end), ','), ''),
                if((case when atc.cve_tema3 is null then '' else atc.cve_tema3 end) != '', concat((case when atc.cve_tema3 is null then '' else atc.cve_tema3 end), ','), ''),
                if((case when atc.cve_tema4 is null then '' else atc.cve_tema4 end) != '', concat((case when atc.cve_tema4 is null then '' else atc.cve_tema4 end), ','), ''),
                if((case when atc.cve_tema5 is null then '' else atc.cve_tema5 end) != '', concat((case when atc.cve_tema5 is null then '' else atc.cve_tema5 end), ','), ''),
                if((case when atc.cve_tema6 is null then '' else atc.cve_tema6 end) != '', concat((case when atc.cve_tema6 is null then '' else atc.cve_tema6 end), ','), '')
                ) as temas,
                concat(
                    if((case when apt.cve_preparacion  is null then '' else apt.cve_preparacion  end) != '', concat((case when apt.cve_preparacion  is null then '' else apt.cve_preparacion  end), ','), ''), 
                    if((case when apt.cve_preparacion1 is null then '' else apt.cve_preparacion1 end) != '', concat((case when apt.cve_preparacion1 is null then '' else apt.cve_preparacion1 end), ','), ''),
                    if((case when apt.cve_preparacion2 is null then '' else apt.cve_preparacion2 end) != '', concat((case when apt.cve_preparacion2 is null then '' else apt.cve_preparacion2 end), ','), ''),
                    if((case when apt.cve_preparacion3 is null then '' else apt.cve_preparacion3 end) != '', concat((case when apt.cve_preparacion3 is null then '' else apt.cve_preparacion3 end), ','), ''),
                    if((case when apt.cve_preparacion4 is null then '' else apt.cve_preparacion4 end) != '', concat((case when apt.cve_preparacion4 is null then '' else apt.cve_preparacion4 end), ','), ''),
                    if((case when apt.cve_preparacion5 is null then '' else apt.cve_preparacion5 end) != '', concat((case when apt.cve_preparacion5 is null then '' else apt.cve_preparacion5 end), ','), ''),
                    if((case when apt.cve_preparacion6 is null then '' else apt.cve_preparacion6 end) != '', concat((case when apt.cve_preparacion6 is null then '' else apt.cve_preparacion6 end), ','), ''),
                    if((case when apt.cve_preparacion7 is null then '' else apt.cve_preparacion7 end) != '', concat((case when apt.cve_preparacion7 is null then '' else apt.cve_preparacion7 end), ','), '')
                    ) as preparacion, 
                concat(
                    if((case when alr.laborRealizo  is null then '' else alr.laborRealizo   end) != '', concat((case when alr.laborRealizo   is null then '' else alr.laborRealizo   end), ','), ''), 
                    if((case when alr.laborRealizo1 is null then '' else alr.laborRealizo1 end) != '', concat((case when alr.laborRealizo1 is null then '' else alr.laborRealizo1 end), ','), ''),
                    if((case when alr.laborRealizo2 is null then '' else alr.laborRealizo2 end) != '', concat((case when alr.laborRealizo2 is null then '' else alr.laborRealizo2 end), ','), ''),
                    if((case when alr.laborRealizo3 is null then '' else alr.laborRealizo3 end) != '', concat((case when alr.laborRealizo3 is null then '' else alr.laborRealizo3 end), ','), ''),
                    if((case when alr.laborRealizo4 is null then '' else alr.laborRealizo4 end) != '', concat((case when alr.laborRealizo4 is null then '' else alr.laborRealizo4 end), ','), '')
                    ) as labores, acc.cedas
            from agricultura.agricultor a 
            left join encuestadores e on e.cve_encuestador=a.cve_encuestador 
            left join cat_municipios cm ON cm.cve_mpioINEGI=a.cve_municipio_unidad 
            left join cat_localidades cl ON cl.cve_mpioINEGI=a.cve_municipio_unidad and cl.cve_localidad=a.cve_localidad_unidad 
            left join cat_quien_contesta cqc ON cqc.cve_quien_contesta=a.cve_quien_contesta 
            left join cat_ejidos ce on ce.cve_ejido=a.ejido 
            left join evidencia_fotografica ef on ef.cve_encuesta=a.cve_encuesta 
            left join datos_productivos g on g.folio=a.cve_encuesta 
            left join datos_tecnologicos dt on dt.cve_encuesta=a.cve_encuesta 
            left join cat_sistema_produccion csp On dt.cve_sistema_produccion=csp.cve_sistema 
            left join cat_tenencias ct on ct.cve_tenencia=g.cve_tenencia 
            left join cat_cultivos cc1 on cc1.cve_cultivo=g.ciclo_anterior_cultivo_1 
            left join cat_cultivos cc2 on cc2.cve_cultivo=g.ciclo_anterior_cultivo_2 
            left join cat_cultivos h on h.cve_cultivo=g.cultivo_actual_1 
            left join cat_cultivos i on i.cve_cultivo=g.cultivo_actual_2 
            left join condicion_socioeconomica k on k.cve_encuesta=a.cve_encuesta 
            left join capacitacion ac on ac.cve_encuesta=a.cve_encuesta 
            LEFT join temas_capacitacion atc on atc.cve_encuesta=a.cve_encuesta     
            left join satisfaccion_producto asp on asp.cve_encuesta=a.cve_encuesta 
            left join cat_cedas acc on acc.cve_ceda=asp.cedas_nombre 
            left join preparacion_terreno apt on apt.cve_encuesta=a.cve_encuesta 
            left join agricultura.labores_realizo alr on alr.cve_encuesta=a.cve_encuesta 
             where a.cve_encuesta='".$cve_encuesta."';";
        }else if($cve_estatus == 0){
            $encuesta="select a.cve_encuesta as cve_encuesta, a.cve_encuestador, concat(a.nombre, ' ', a.paterno, ' ', a.materno) as nombrebene, cve_estatus, 
            e.nombre, DATE_FORMAT(a.fecha_encuesta, '%d         %m        %Y') as fecha_encuesta, 
            case when substring(curp,11,1)='H' THEN 'MASCULINO' ELSE 'FEMENINO' END AS sexo, 
            a.curp, a.folio_estatal, case when year(cast(substring(curp,5,9) as date))>2023  
            then date_sub(cast(substring(curp,5,9) as date),interval 100 YEAR) else cast(substring(curp,5,9) as date) end as nacio, a.porque, cm.municipio  
            from agricultura.agricultor as a  
            left join agricultura.encuestadores e ON e.cve_encuestador=a.cve_encuestador 
            left join agricultura.cat_municipios cm ON cm.cve_mpioINEGI=a.cve_municipio_unidad  
            where a.cve_encuesta='".$cve_encuesta."';";
       //var_dump($encuesta);
        }
       return $this->db->query($encuesta)->getResultArray();
    }


    public function pdfConsulta_PFB_NO($cve_encuesta = ""){
        $encuesta = "select a.cve_encuesta as cve_encuesta, a.cve_encuestador, concat(a.nombre, ' ', a.paterno, ' ', a.materno) as nombrebene, cve_estatus, 
        DATE_FORMAT(a.fecha_encuesta, '%d         %m        %Y') as fecha_encuesta, 
        case when substring(curp,11,1)='H' THEN 'HOMBRE' ELSE 'MUJER' END AS sexo, 
        a.curp, a.folio_estatal, 
        case when year(cast(substring(curp,5,9) as date))>2023  
        then date_sub(cast(substring(curp,5,9) as date),interval 100 YEAR) 
        else cast(substring(curp,5,9) as date) end as nacio, a.porque, 
        cm.municipio, e.nombre, 
        from agricultura.agricultor as a  
        left join agricultura.encuestadores e on e.cve_encuestador=a.cve_encuestador 
        left join agricultura.cat_municipios cm ON cm.cve_mpioINEGI=a.cve_municipio_unidad 
        where a.cve_encuesta='" . $cve_encuesta . "';";
        //var_dump($encuesta);
        return $this->db->query($encuesta)->getResultArray();
    }
    
    
}
