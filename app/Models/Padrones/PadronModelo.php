<?php

namespace App\Models\Padrones;

use CodeIgniter\Model;

class PadronModelo extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'pruebasJairo';
    /*protected $primaryKey       = 'id';
    protected $useAutoIncrement = true;
    protected $returnType       = 'array';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = [];

    // Dates
    protected $useTimestamps = false;
    protected $dateFormat    = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];*/

    // listado de los agricultores falta de agregar funcionalidad de vinculo entre el encuestador y el agricultor
    public function agricultores($where){
        $query = $this->db->table("agricultor a");
        $query->select("a.cve_encuesta,a.curp,a.paterno,a.materno,a.nombre,m.municipio,a.folio_estatal, 
        a.cve_encuestador,en.nombre as nombreEncuestador ,en.nombre as nombreEn,a.beneficiario,cat_ddr.ddr_nombre,
          a.cve_estatus");
        $query->join("encuestadores en", " a.cve_encuestador=en.cve_encuestador");
        $query->join("cat_municipios m", " m.cve_mpioINEGI=a.cve_municipio_unidad");
        $query->join("cat_ddr", " a.cve_ddr = cat_ddr.cve_ddr");
        if($where != ''){
            $query->where($where);

        }
        $query->orderBy("cve_estatus", "desc");
        $query->orderBy("m.municipio", "asc");
        $res = $query->get()->getResultArray();
        return $res;
    }

    public function catCapacitacion()
    {
        $query = $this->db->table("cat_capacitacion");
        $query->select("*");
        $res = $query->get()->getResultArray();
        return $res;
    }

    public function catTemas()
    {
        $query = $this->db->table("cat_temas");
        $query->select("*");
        $res = $query->get()->getResultArray();
        return $res;
    }

    // guardado y actualizado step 5
    public function Capacitacion($datosEncuenta, $temas)
    {
        $this->db->transStart();
        $DatosProductivo =  $this->db->table("capacitacion");
        $DatosProductivo->select("cve_encuesta");
        $DatosProductivo->where('cve_encuesta', $datosEncuenta['cve_encuesta']);
        $existeEncuesta = $DatosProductivo->countAllResults();

        // buscar temas y cambiar 
        if ($temas != []) {

            $temaCapacitacion =  $this->db->table("temas_capacitacion");
            $temaCapacitacion->select("cve_encuesta");
            $temaCapacitacion->where('cve_encuesta', $datosEncuenta['cve_encuesta']);
            $existenTemas = $temaCapacitacion->countAllResults();

            // guardar temas capacitacion
            if ($existenTemas > 0) {
                $temaCapacitacion->where('cve_encuesta', $datosEncuenta['cve_encuesta']);
                $temaCapacitacion->delete();

                    $temaCapacitacion->insert($temas);
            } else {
                    $temaCapacitacion->insert($temas);
            }
        }

        if ($existeEncuesta > 0) {
            $DatosProductivo->set($datosEncuenta);
            $DatosProductivo->where('cve_encuesta', $datosEncuenta['cve_encuesta']);
            $DatosProductivo->update();
        } else {
            $DatosCapacitacion = $this->db->table("capacitacion");
            $DatosCapacitacion->insert($datosEncuenta);
        }

        $this->db->transComplete();
        if ($this->db->transStatus() === false) {
            return 0;
        } else {
            return 1;
        }
    }

    // SATISFACCION DEL PRODUCTOR
    // catalogos

        public function catCedas()
        {
            $query = $this->db->table("cat_cedas");
            $query->select("*");
            $res = $query->get()->getResultArray();
            return $res;
        }

        public function catRecursos()
        {
            $query = $this->db->table("cat_recursos");
            $query->select("*");
            $res = $query->get()->getResultArray();
            return $res;
        }

        public function catEstadoFerti()
        {
            $query = $this->db->table("cat_recibio_fertilizantes");
            $query->select("*");
            $res = $query->get()->getResultArray();
            return $res;
        }

        public function catAtencion()
        {
            $query = $this->db->table("cat_atencion");
            $query->select("*");
            $res = $query->get()->getResultArray();
            return $res;
        }

        public function catPorcentajes()
        {
            $query = $this->db->table("cat_porcentajes");
            $query->select("*");
            $res = $query->get()->getResultArray();
            return $res;
        }
    // catalogos

    // guardado step 6
    public function Satisfaccion($datosEncuenta)
    {
        $this->db->transStart();
        $DatosSatisfaccion =  $this->db->table("satisfaccion_producto");
        $DatosSatisfaccion->select("cve_encuesta");
        $DatosSatisfaccion->where('cve_encuesta', $datosEncuenta['cve_encuesta']);
        $existeEncuesta = $DatosSatisfaccion->countAllResults();
        if($existeEncuesta > 0){
            $DatosSatisfaccion = $this->db->table("satisfaccion_producto");
            $DatosSatisfaccion->where('cve_encuesta', $datosEncuenta['cve_encuesta']);
            $DatosSatisfaccion->update($datosEncuenta);
        }else{
            $DatosSatisfaccion->insert($datosEncuenta);
        }
        $this->db->transComplete();
        if ($this->db->transStatus() === false) {
            return 0;
        } else {
            return 1;
        }
    }


    public function guardarEncuenta($datosEncuenta, $datosTecnologicos){
        $this->db->transStart();
            $Datosegresados = $this->db->table("encuesta");
            $Datosegresados->insert($datosEncuenta);
            $this->GuardarTecnologicos($datosTecnologicos);
            // si va a haber otra tabla pues se pone aqui
        $this->db->transComplete();
        if($this->db->transStatus() === false ){
            return 0;
        }
        else{
            return 1;
        }
    }

    public function guardarAgricultor($datosAgricultor, $curp, $mapa){
        $this->db->transStart();
          
        $DatosAgricultores = $this->db->table("agricultor");
        $DatosAgricultores->set($datosAgricultor);
        $DatosAgricultores->where("curp", $curp);
        $DatosAgricultores->update();
       

            // si va a haber otra tabla pues se pone aqui
            if($mapa != []){
                // se tiene que condicionar el mapa
                $evidenciaFoto =  $this->db->table("evidencia_fotografica");
                $evidenciaFoto->select("cve_encuesta");
                $evidenciaFoto->where('cve_encuesta', $mapa['cve_encuesta']);
                $existeFoto = $evidenciaFoto->countAllResults(); 
                if( $existeFoto > 0){
                    $evidenciaFoto->set($mapa);
                    $evidenciaFoto->where('cve_encuesta', $mapa['cve_encuesta']);
                    $evidenciaFoto->update();
                    // var_dump($image);
                }else{ 
                    $evidenciaFoto->insert($mapa);
                }
            }

        $this->db->transComplete();
        if($this->db->transStatus() === false ){
            return 0;
        }else{
            return 1;
        }
    }

    //JAIRO DATOS PRODUCTIVOS
        public function catFertilizantes($where)
        {
            $query = $this->db->table("cat_fertilizantes");
            $query->select("*");
            if( $where != '' ){
                $query->where($where);
            }
            $res = $query->get()->getResultArray();
            return $res;
        }
        public function catTenencia($where)
        {
            $query = $this->db->table("cat_tenencias");
            $query->select("*");
            if( $where != '' ){
                $query->where($where);
            }
            $res = $query->get()->getResultArray();
            return $res;
        }

        public function catCultivos($where)
        {
            $query = $this->db->table("cat_cultivos");
            $query->select("*");
            if( $where != '' ){
                $query->where($where);
            }
            $res = $query->get()->getResultArray();
            return $res;
        }

        public function catCiclo($where)
        {
            $query = $this->db->table("cat_ciclos");
            $query->select("*");
            if( $where != '' ){
                $query->where($where);
            }
            $res = $query->get()->getResultArray();
            return $res;
        }

        public function catRegimen($where)
        {
            $query = $this->db->table("cat_regimen");
            $query->select("*");
            if( $where != '' ){
                $query->where($where);
            }
            $res = $query->get()->getResultArray();
            return $res;
        }

        public function GuardarDatosProductivos($DatosProductivos){
            // $local =  \Config\Database::connect('tests', false);
            $DatosProductivo =  $this->db->table("datos_productivos");

            $DatosProductivo->select("folio");
            $DatosProductivo->where('datos_productivos.folio', $DatosProductivos['folio']);
            $estatus = $DatosProductivo->get()->getResultArray();

            //consultamos si el folio ya existe
            if(isset($estatus[0]['folio'])){
                if($estatus[0]['folio'] == $DatosProductivos['folio']){ 
                    unset($DatosProductivos['folio']);
                    $DatosProductivo->set($DatosProductivos);
                    $DatosProductivo->where('folio', $estatus[0]['folio'] );
                   return $DatosProductivo->update();
                }
            }else{
                return $DatosProductivo->insert($DatosProductivos);
            }
        }

        public function ConsultarSiYaExisteFolio($folio){
            $DatosProductivo =  $this->db->table("datos_productivos");
            $DatosProductivo->select("*");
            $DatosProductivo->where('folio', $folio);
            return $DatosProductivo->get()->getResultArray();

        }
    //JAIRO DATOS PRODUCTIVOS

    //JAIRO DATOS PRODUCTIVOS

    //jairo datos tecnologicos
        public function catAdquisisionSemilla()
        {
            $query = $this->db->table("cat_adquisicion_semillas");
            $query->select("*");
            $res = $query->get()->getResultArray();
            return $res;
        }
        public function catTiempoSemilla()
        {
            $query = $this->db->table("cat_tiempo_semilla");
            $query->select("*");
            $res = $query->get()->getResultArray();
            return $res;
        }
        public function catResiembra()
        {
            $query = $this->db->table("cat_resiembra");
            $query->select("*");
            $res = $query->get()->getResultArray();
            return $res;
        }
        public function catSistemaProduccion()
        {
            $query = $this->db->table("cat_sistema_produccion");
            $query->select("*");
            $res = $query->get()->getResultArray();
            return $res;
        }

        public function catPreparacionTerreno()
        {
            $query = $this->db->table("cat_preparacion_terreno");
            $query->select("*");
            $res = $query->get()->getResultArray();
            return $res;
        }

        public function catSemilla()
        {
            $query = $this->db->table("cat_tipo_semillas");
            $query->select("*");
            $res = $query->get()->getResultArray();
            return $res;
        }

        public function catTipoSiembra()
        {
            $query = $this->db->table("cat_tipo_siembra");
            $query->select("*");
            $res = $query->get()->getResultArray();
            return $res;
        }

        public function catTempLluvias()
        {
            $query = $this->db->table("cat_temporal_lluvias");
            $query->select("*");
            $res = $query->get()->getResultArray();
            return $res;
        }

        public function catLabores()
        {
            $query = $this->db->table("cat_labores");
            $query->select("*");
            $res = $query->get()->getResultArray();
            return $res;
        }

        public function catFormaAplicarFertilizante()
        {
            $query = $this->db->table("cat_aplicaciones");
            $query->select("*");
            $res = $query->get()->getResultArray();
            return $res;
        }

        public function catTIpoFertilizante()
        {
            $query = $this->db->table("cat_tipo_fertilizantes");
            $query->select("*");
            $res = $query->get()->getResultArray();
            return $res;
        }

        public function catControlPlagas()
        {
            $query = $this->db->table("cat_control_plagas");
            $query->select("*");
            $res = $query->get()->getResultArray();
            return $res;
        }

        public function catEnfermedades()
        {
            $query = $this->db->table("cat_enfermedades");
            $query->select("*");
            $res = $query->get()->getResultArray();
            return $res;
        }

        public function TransaccionTecnologica($DatosTecnologicos, $preparacionTerreno,$laboresLimpia){
                // public function TransaccionTecnologica($GuardarDatosTecnologicos,$laboresLimpia){
                    $this->db->transStart();
                    $this->GuardarDatosTecnologicos($DatosTecnologicos);
                    $this->GuardarpreparacionTerreno($preparacionTerreno);
                    $this->GuardarlaboresLimpia($laboresLimpia);
                    $this->db->transComplete();
                    if ($this->db->transStatus() === false) {
                        return 0;
                    } else {
                        return 1;
                    }
        }
        
        public function GuardarDatosTecnologicos($GuardarDatosTecnologicos){
            $DatosProductivo =  $this->db->table("datos_tecnologicos");
        
            $DatosProductivo->select("cve_encuesta");
            $DatosProductivo->where('datos_tecnologicos.cve_encuesta', $GuardarDatosTecnologicos['cve_encuesta']);
            $estatus = $DatosProductivo->get()->getResultArray();
        
                //consultamos si el folio ya existe
            if(isset($estatus[0]['cve_encuesta'])){
                if($estatus[0]['cve_encuesta'] == $GuardarDatosTecnologicos['cve_encuesta']){ 
                    unset($GuardarDatosTecnologicos['cve_encuesta']);
                    $DatosProductivo->set($GuardarDatosTecnologicos);
                    $DatosProductivo->where('cve_encuesta', $estatus[0]['cve_encuesta'] );
                    return $DatosProductivo->update();
                }
            }else{
                return $DatosProductivo->insert($GuardarDatosTecnologicos);
            }
        }
        
        public function GuardarpreparacionTerreno($preparacionTerreno){
            
            $labores_realizo =  $this->db->table("preparacion_terreno");
            $labores_realizo->select("cve_encuesta");
            $labores_realizo->where('preparacion_terreno.cve_encuesta', $preparacionTerreno['cve_encuesta']);
            $estatus = $labores_realizo->get()->getResultArray();
        
            // var_dump($preparacionTerreno);//pendiente revisar porque no actualiza
                // consultamos si el folio ya existe
            if(isset($estatus[0]['cve_encuesta'])){
                if($estatus[0]['cve_encuesta'] == $preparacionTerreno['cve_encuesta']){ 
        
                    $labores_realizo->set($preparacionTerreno);
                    $labores_realizo->where('cve_encuesta', $estatus[0]['cve_encuesta']);
                    $labores_realizo->delete();
                    $labores_realizo->insert($preparacionTerreno);
                }
            }else{
                return $labores_realizo->insert($preparacionTerreno);
            }
        }
        
        public function GuardarlaboresLimpia($laboresLimpia){
            
            $labores_realizo =  $this->db->table("labores_realizo");
            $labores_realizo->select("cve_encuesta");
            $labores_realizo->where('labores_realizo.cve_encuesta', $laboresLimpia['cve_encuesta']);
            $estatus = $labores_realizo->get()->getResultArray();
            
            if(isset($estatus[0]['cve_encuesta'])){
                if($estatus[0]['cve_encuesta'] == $laboresLimpia['cve_encuesta']){ 
        
                    // unset($laboresLimpia['cve_encuesta']);
                    $labores_realizo->set($laboresLimpia);
                    $labores_realizo->where('cve_encuesta', $estatus[0]['cve_encuesta']);
                    $labores_realizo->delete();
                    $labores_realizo->insert($laboresLimpia);
                }
            }else{
                return $labores_realizo->insert($laboresLimpia);
            }
        }


        public function buscarDatosAgricultor($curp){
            $agricultor =  $this->db->table("agricultor agri");
            $agricultor->select("cve_encuesta, cve_estatus");
            $agricultor->where("curp", $curp);
            $cve_encuesta = $agricultor->get()->getResultArray();
            $encuestaInfo = $cve_encuesta[0]["cve_encuesta"];

            // buscar temas y cambiar 
            if ($encuestaInfo != "") {
                $DatosProductivos =  $this->db->table("agricultor agri");
                $DatosProductivos->select("*");
                $DatosProductivos->where("cve_encuesta", $encuestaInfo);
                $Agri = $DatosProductivos->get()->getResultArray();
            
                $DatosProductivos =  $this->db->table("datos_productivos Prod");
                $DatosProductivos->select("*");
                $DatosProductivos->where("Prod.folio", $encuestaInfo);
                $Prod = $DatosProductivos->get()->getResultArray();

                $datosTecnologicos =  $this->db->table("datos_tecnologicos tec");
                $datosTecnologicos->select("*");
                $datosTecnologicos->join('labores_realizo lab', "tec.cve_encuesta = lab.cve_encuesta","left");
                $datosTecnologicos->join('preparacion_terreno prep', "tec.cve_encuesta = prep.cve_encuesta", "left");
                $datosTecnologicos->where("tec.cve_encuesta", $encuestaInfo);
                $Tec = $datosTecnologicos->get()->getResultArray();

                $DatosCapacitacion =  $this->db->table("capacitacion cap");
                $DatosCapacitacion->select("cap.cve_encuesta, cap.cve_capacitacion, cap.de_quien, cap.conoce_programa, cap.capacitacion_programa, cap.elaborar_biofertilizante,
                 tem.cve_tema,tem.cve_tema1, tem.cve_tema2, tem.cve_tema3, tem.cve_tema4, tem.cve_tema5, tem.cve_tema6");
                $DatosCapacitacion->join('temas_capacitacion tem', "cap.cve_encuesta = tem.cve_encuesta", "left");
                $DatosCapacitacion->where("cap.cve_encuesta", $encuestaInfo);
                $Cap = $DatosCapacitacion->get()->getResultArray();

                $DatosSatisfaccion =  $this->db->table("satisfaccion_producto satis");
                $DatosSatisfaccion->select("*");
                $DatosSatisfaccion->where("satis.cve_encuesta", $encuestaInfo);
                $Satis = $DatosSatisfaccion->get()->getResultArray();

                $DatosSocioeconomicos =  $this->db->table("condicion_socioeconomica condi");
                $DatosSocioeconomicos->select("*");
                $DatosSocioeconomicos->where("condi.cve_encuesta", $encuestaInfo);
                $Condi = $DatosSocioeconomicos->get()->getResultArray();

                $DatosEvidencia =  $this->db->table("evidencia_fotografica evi");
                $DatosEvidencia->select("*");
                $DatosEvidencia->where("evi.cve_encuesta", $encuestaInfo);
                $Evi = $DatosEvidencia->get()->getResultArray();

                $resultados_totales = array(
                                            'RegistroAgricultura' => $Agri,
                                            'RegistroProductivo' => $Prod,
                                            'Tecnologias' => $Tec,
                                            'Capacitacion' => $Cap, 
                                            'Satisfaccion' => $Satis, 
                                            'Condicion' => $Condi,
                                            'fotos' => $Evi
                                        );
                return $resultados_totales;
      
            }


        }

        // traer al encuestado para consulta en superUsuario
        // public function EncuestadoVer($cve_encuesta){
        //     $query = $this->db->table("agricultor");
        //     $query->select("*");
        //     $query->join('cat_municipios', 'usuario.id=agricultor.id');
        //     $query->where('cve_encuesta', $cve_encuesta);
        //     $res = $query->get()->getResultArray();
        //     return $res;
        // }
 
        public function descargarReporte(){
            
        ini_set('memory_limit','2048M');
        set_time_limit(0);
        $sinllenar = "SELECT 
        agricultor.cve_encuesta,
        case when beneficiario=0 then 'NO' WHEN beneficiario=1 then 'SI' END as 'ES BENEFICIARIO', 
        case when agricultor.cve_estatus = 2 then 'FINALIZADA' when agricultor.cve_estatus = 1 then 'INCONCLUSA' when agricultor.cve_estatus = 0 then 'NO CONTESTO' when agricultor.cve_estatus is null then 'SIN INICIAR' END as 'estatus',
        agricultor.folio_estatal, 
        agricultor.curp,concat(nombre,' ',paterno,' ', materno) as nombre,
        datediff(now(),case when cast(substring(curp, 5,11) as date)>now() then DATE_SUB(cast(substring(curp, 5,11) as date), INTERVAL 100 YEAR)  else cast(substring(curp, 5,11) as date)  end  )/365  edad,
        agricultor.telefono, 
        mpios.cve_municipio as idmunicipio, municipio,
        cat_ddr.ddr_nombre,
        agricultor.cve_ddr, 
        loc.cve_localidad idlocalidad, localidad,
        agricultor.ejido, cat_ejidos.ejido as 'nombre ejido',
        #cve_ejido as idejido, ejido.ejido,
        
        case when es_ejidatario=1 then 'SI' when es_ejidatario=0 then 'NO' END as 'Ejidatario',
        latitud,longitud,
        case when substr(curp,11,1)='H' THEN 'MASCULINO'  WHEN substr(curp,11,1)='M' THEN 'MUJER' END AS SEXO, fecha_nacimiento,
        cast(DATEDIFF(now(), fecha_nacimiento)/365 as unsigned) AS edad, 
        anio_productor as 'desde que año es productor',
        case when dialecto=0 then 'NO' WHEN dialecto=1 then 'SI' end as 'HABLA ALGUN DIALECTO', 
        dialecto_hablado, 
        case when leer_escribir=1 then 'SI' WHEN leer_escribir=0 then 'NO' END AS 'Sabe leer y escribir', 
        anio_estudio,
        case when beneficiario_bienestar=0 then 'NO' WHEN beneficiario_bienestar=1 then 'SI' END as 'ES BENEFICIARIO BIENESTAR', 
        case when productor_contesta=0 then 'NO' WHEN productor_contesta=1 then 'SI' END as 'CONTESTA PRODUCTOR', quien_contesta, nombre_quien_contesta, 
        #datos productivos
        hectarea_sup_up,
        area_sup_up,
        centiarea_sup_up,
        prod.cve_tenencia,
        tipo_tenencia,
        case when prod.sembro_ciclo_anterior=0 then 'NO' ELSE 'SI' END AS 'sembro ciclo anterior',
        prod.motivo_no_sembro,
        prod.ciclo_anterior_cultivo_1, 
        cultivoant1.cultivo,
        prod.hectarea_ciclo_anterior_1,
        prod.area_ciclo_anterior_1,
        prod.centiarea_ciclo_anterior_1,
        case when prod.ciclo_anterior_1=1 then 'PV' when prod.ciclo_anterior_1=2  then 'OI' END 'ciclo anterior',
        case when prod.regimen_hidrico_anterior_1=1 then 'TEMPORAL' when prod.regimen_hidrico_anterior_1=2 then 'HUMEDAD' when prod.regimen_hidrico_anterior_1=3 then  'PUNTA DE RIEGO' WHEN prod.regimen_hidrico_anterior_1=4 then 'PUNTA DE RIEGO' END AS 'regimen hidrico cult ant 1' ,
        prod.ciclo_anterior_cultivo_2,
        cultivoant2.cultivo,
        prod.hectarea_ciclo_anterior_2,
        prod.area_ciclo_anterior_2,
        prod.centiarea_ciclo_anterior_2,
        case when prod.ciclo_anterior_2=1 then 'PV' when prod.ciclo_anterior_2=2 then 'OI' END 'ciclo anterior cultivo 2',
        case when prod.regimen_hidrico_anterior_2=1 then 'TEMPORAL' when prod.regimen_hidrico_anterior_2=2 then 'HUMEDAD' when prod.regimen_hidrico_anterior_2=3 then  'PUNTA DE RIEGO' WHEN prod.regimen_hidrico_anterior_2=4 then 'PUNTA DE RIEGO' END AS 'regimen hidrico cult ant 2', 
        case when prod.aplico_fertilizante=1 then 'SI' when prod.aplico_fertilizante=2 then 'NO' END as aplico_fertilizante ,
        tipo_fertilizante_uso AS idtipo_fertilizante_uso,
        case when prod.tipo_fertilizante_uso=1 then 'QUIMICO' WHEN prod.tipo_fertilizante_uso=2 then 'ABONO/COMPOSTA' WHEN prod.tipo_fertilizante_uso=3 then 'Biofertilizante / Organico' end as 'TIPO FERTILIZANTE',
        prod.kg_fertilizante,
        prod.hectarea_cosecho_anterior_1,
        prod.area_cosecho_anterior_1,
        prod.centiarea_cosecho_anterior_1,
        prod.toneladas_cosecho_anterior_1,
        prod.hectarea_cosecho_anterior_2,
        prod.area_cosecho_anterior_2,
        prod.centiarea_cosecho_anterior_2,
        prod.toneladas_cosecho_anterior_2,
        prod.toneladas_autoconsumo_familiar_cult_1 as 'autoconsumo ciclo ant cult1',
        prod.toneladas_autoconsumo_productivo_cult_1 as 'productivo ciclo ant cult1',
        prod.toneladas_comercializadora_cult_1 as 'comercial ciclo ant cult1',
        prod.precio_venta_destino_prod_cult_1 as 'precio venta ciclo ant cult1',
        prod.toneladas_autoconsumo_familiar_cult_2 as 'autoconsumo ciclo ant cult2',
        prod.toneladas_autoconsumo_productivo_cult_2 as 'productivo ciclo ant cult2',
        prod.toneladas_comercializadora_cult_2 as 'comercial ciclo ant cult2',
        prod.precio_venta_destino_prod_cult_2 as 'precio venta ciclo ant cult2',
        case when prod.sembro_este_ciclo=1 then 'SI' when prod.sembro_este_ciclo=0 THEN 'NO' end as 'sembro 2023? **',
        porque_no_sembro_ciclo_actual,
        prod.ciclo_actual_cultivo_1, 
        cultivo1.cultivo,
        prod.hectarea_actual_cultivo_1,
        prod.area_actual_cultivo_1,
        prod.centiarea_actual_cultivo_1,
        case when prod.ciclo_actual_cultivo_1=1 then 'PV' when prod.ciclo_actual_cultivo_1=2  then 'OI' END 'ciclo actual',
        case when prod.regimen_hidrico_actual_cultivo_1=1 then 'TEMPORAL' when prod.regimen_hidrico_actual_cultivo_1=2 then 'HUMEDAD' when prod.regimen_hidrico_actual_cultivo_1=3 then  'PUNTA DE RIEGO' WHEN prod.regimen_hidrico_actual_cultivo_1=4 then 'PUNTA DE RIEGO' END AS 'regimen hidrico cult act 1' ,
        prod.ciclo_actual_cultivo_2, 
        cultivo2.cultivo,
        prod.hectarea_actual_cultivo_2,
        prod.area_actual_cultivo_2,
        prod.centiarea_actual_cultivo_2,
        case when prod.ciclo_actual_cultivo_2=1 then 'PV' when prod.ciclo_actual_cultivo_2=2  then 'OI' END 'ciclo actual',
        case when prod.regimen_hidrico_actual_cultivo_2=1 then 'TEMPORAL' when prod.regimen_hidrico_actual_cultivo_2=2 then 'HUMEDAD' when prod.regimen_hidrico_actual_cultivo_2=3 then  'PUNTA DE RIEGO' WHEN prod.regimen_hidrico_actual_cultivo_2=4 then 'PUNTA DE RIEGO' END AS 'regimen hidrico cult act 1', 
        prod.hectarea_cosecho_actual_1,
        prod.area_cosecho_actual_1,
        prod.centiarea_cosecho_actual_1,
        prod.toneladas_cosecho_actual_1,
        prod.hectarea_cosecho_actual_2,
        prod.area_cosecho_actual_2,
        prod.centiarea_cosecho_actual_2,
        prod.toneladas_cosecho_actual_2,
        prod.toneladas_autoconsumo_familiar_actual_cult_1 as 'autoconsumo ciclo act cult1',
        prod.toneladas_autoconsumo_productivo_actual_cult_1 as 'productivo ciclo act cult1',
        prod.toneladas_comercializadora_actual_cult_1 as 'comercial ciclo act cult1',
        prod.precio_venta_destino_prod_actual_cult_1 as 'precio venta ciclo act cult1',
        prod.toneladas_autoconsumo_familiar_actual_cult_2 as 'autoconsumo ciclo act cult2',
        prod.toneladas_autoconsumo_productivo_actual_cult_2 as 'productivo ciclo act cult2',
        prod.toneladas_comercializadora_actual_cult_2 as 'comercial ciclo act cult2',
        prod.precio_venta_destino_prod_actual_cult_2 as 'precio venta ciclo act cult2',
        #datos tecnologicos
        case when dt.cve_sistema_produccion=1 THEN 'Espeque' when dt.cve_sistema_produccion=2 THEN 'No mecanizada' when dt.cve_sistema_produccion=3 THEN 'Mecanizada' when dt.cve_sistema_produccion=4 THEN 'Labranza reducida' END AS 'sistema produccion',
        cpt.preparacion_terreno ,
        cpt1.preparacion_terreno,
        cpt2.preparacion_terreno,
        cpt3.preparacion_terreno,
        cpt4.preparacion_terreno,
        cpt5.preparacion_terreno,
        cpt6.preparacion_terreno,
        cpt7.preparacion_terreno,
        case when dt.cve_tipo_semilla=1 then 'CRIOLLA' when dt.cve_tipo_semilla=2 then 'MEJORADA' end as 'tipo semilla',
        dt.hibrido_variedad,
        adsem.adquisicion_semilla,
        ctsem.tiempo_semilla,
        case when dt.cve_tipo_siembra=1 then 'No mecanizada' when dt.cve_tipo_siembra=2 then 'Mecanizada' when dt.cve_tipo_siembra=3 then 'Siembra de precisión'  end as 'tipo de siembra',
        dt.semilla_gramos as 'semilla  hect',
        dt.fecha_siembra,
        case when dt.resiembra=0 then 'NO' when dt.resiembra=1 then 'SI' end  as 'realizo resiembra',
        dt.fecha_resiembra,
        case when dt.motivo_resiembra = 1 then 'Calidad de la Semilla' when dt.motivo_resiembra = 2 then 'Condiciones Climatológicas' when dt.motivo_resiembra = 3 then 'Condiciones Sanitarias' END as 'motivo_resiembra',
        clb.labor,
        clb1.labor,
        clb2.labor,
        clb3.labor,
        clb4.labor,
        case when dt.utilizo_herbicidas=0 then 'NO' when dt.utilizo_herbicidas=1 then 'SI' END AS 'utilizo herbicidas',
        dt.cuales_herbicidas,
        #fertilizacion datos datos_tecnologicos
        case when dt.fertilizante_bienestar = 0 then 'NO' when dt.fertilizante_bienestar = 1 then 'SI' END AS 'fertilizante_bienestar',
        dt.ferti_kg_dap,
        case when dt.formato_unico_entrega = 0 then 'NO' when dt.formato_unico_entrega = 1 then 'SI' when dt.formato_unico_entrega = 2 then 'NO SABE' END AS 'FERTILIZANTE FORMATO UNICO DE ENTREGA',
        dt.formato_porque,
        case when dt.reporte_hecho = 0 then 'NO' when dt.reporte_hecho = 1 then 'SI' END AS 'REPORTO EL HECHO',
        dt.fecha_recibio_fertilizante, 
        case when dt.oportuno_siembra = 0 then 'NO' when dt.oportuno_siembra = 1 then 'SI' END AS 'FUE OPORTUNO PARA SU SIEMBRA',
        case when dt.aplico_fertilizante = 0 then 'NO' when dt.aplico_fertilizante = 1 then 'SI' END as 'APLICO FERTILIZANTE QUE RECIBIO DEL PROGRAMA **',
        dt.razon_aplicacion_fertilizante,
        dt.hectareas_fertilizante1,
        dt.areas_fertilizante1,
        dt.centiareas_fertilizante1,
        dt.fecha_aplicacion1,
        dt.dosis1,
        dt.urea_fecha1 as 'urea fecha aplicacion',
        dt.dosis_urea1,
        dt.hectareas_fertilizante2,
        dt.areas_fertilizante2,
        dt.centiareas_fertilizante2,
        dt.fecha_aplicacion2,
        dt.dosis2,
        dt.urea_fecha2,
        dt.dosis_urea2,
        case when dt.fertilizante_aplicado =0 then 'NO' when dt.fertilizante_aplicado = 1 then 'SI' END as 'APLICÓ EL FERTILIZANTE PARA EL CULTIVO Y PREDIO PARA EL QUE FUE AUTORIZADO',
        dt.porque_aplico_ferti,
        case when dt.cve_aplicacion_fertilizante = 0 then 'MANUAL' when dt.cve_aplicacion_fertilizante = 1 then 'MECANICA' END AS 'aplicacion_fertilizante',
        case when dt.cve_aplicacion = 1 then 'Al voleo' when dt.cve_aplicacion = 2 then 'Mateado' when dt.cve_aplicacion = 3 then 'En banda' when dt.cve_aplicacion = 4 then 'Otra' END 'DE QUÉ FORMA LO APLICO' ,
        cat_apli.aplicacion,
        case when dt.fertilizante_complementario_up = 0 then 'NO' when dt.fertilizante_complementario_up = 1 then 'SI' END as 'fertilizante_complementario_up',
        case when dt.fertilizante_complementario_programa = 0 then 'NO' when dt.fertilizante_complementario_programa = 1 then 'SI' END as 'fertilizante_complementario_programa',
        case when dt.cve_quimico = 1 then 'Químico' when dt.cve_quimico  = 2 then 'Abono / Composta' when dt.cve_quimico = 3 then 'Biofertilizante / Organico' END AS 'QUÉ TIPO DE FERTILIZANTE UTILIZÓ',
        dt.cual_quimico,
        dt.dosis_quimico,
        dt.observaciones,
        dt.riegos,
        case when dt.cve_temporal = 1 then 'IGUAL AL AÑO PASADO' when dt.cve_temporal = 2 then 'MENOR AL AÑO PASADO' when dt.cve_temporal = 3 then 'MAYOR AL AÑO PASADO' END AS 'COMPORTAMIENTO DEL TEMPORAL DE LLUVIAS',
         #PLAGAS Y ENFERMEDADES PRESENTADAS
        case when dt.plagas = 0 then 'NO' when dt.plagas  = 1 then 'SI' END AS 'PLAGAS DURANTE EL DESARROLLO DEL CULTIVO',
        dt.cual_plaga,
        case when dt.cve_control_plaga = 1 then 'Químicos' when dt.cve_control_plaga = 2 then 'Manual' when dt.cve_control_plaga = 3 then 'Biológico' when dt.cve_control_plaga = 4 then 'No controlado' END as 'CONTROLO LAS PLAGAS',
        case when dt.cve_enfermedad = 0 then 'NO' when dt.cve_enfermedad  = 1 then 'SI' END AS 'ENFERMEDADES DURANTE EL DESARROLLO DEL CULTIVO',
        dt.enfermedades_cuales,
        case when dt.cve_metodo_enfermedades = 1 then 'Químicos' when dt.cve_metodo_enfermedades = 2 then 'Biológico' when dt.cve_metodo_enfermedades = 3 then 'No controlado' END AS 'CONTROLO LAS ENFERMEDADES',
        case when dt.rendimiento_cultivo = 0 then 'NO' when dt.rendimiento_cultivo  = 1 then 'SI' END AS 'AFECTÓ EL RENDIMIENTO DE SUS CULTIVOS',
        dt.porcentaje_cultivo,
        #CAPACITACION Y ASISTENCIA TECNICA
        case when cap.cve_capacitacion = 1 then 'NO' when cap.cve_capacitacion = 2 then 'SI, PRIVADA' when cap.cve_capacitacion = 3 then 'SI, PUBLICA' END AS 'CAPACITACION Y ASISTENCIA TECNICA',
        cap.de_quien,
        tema.tema,
        tema1.tema,
        tema2.tema,
        tema3.tema,
        tema4.tema,
        tema5.tema,
        tema6.tema,
        case when cap.conoce_programa = 0 then 'NO' when  cap.conoce_programa = 1 then 'SI' END as 'conoce_programa',
        case when cap.capacitacion_programa = 0 then 'NO' when cap.capacitacion_programa = 1 then 'SI' END as 'capacitacion_programa',
        case when cap.elaborar_biofertilizante = 0 then 'NO' when cap.elaborar_biofertilizante = 1 then 'SI' END as 'elaborar_biofertilizante',
        #satisfaccion_producto
        cat_cedas.cedas,
        case when sat.conoce_recursos = 0 then 'NO' when sat.conoce_recursos = 1 then 'SI' END as 'QUIEN APORTA LOS RECURSOS DEL PROGRAMA',
        recu.recurso,
        sat.quien as 'ESPECIFICACIÓN DE A QUIEN CONOCE',
        case when sat.convocado = 0 then 'NO' when sat.convocado = 1 then 'SI' END as 'FUE CONVOCADO CON ANTICIPACIÓN A RECIBIR EL FERTILIZANTE',
        case when sat.tiempo_espera = 1 then 'Menos de 1 hora' when sat.tiempo_espera = 2 then 'De 1 hora a 2 horas' when sat.tiempo_espera = 3 then 'De 2 hora a 3 horas' when sat.tiempo_espera = 4 then 'Más de 3 horas' END as 'Tiempo espera',
        case when sat.entrega_formal = 0 then 'NO' when sat.entrega_formal = 1 then 'SI' END as 'ENTREGARON EL FERTILIZANTE EL DIA',
        case when sat.explicacion_aplicarlo = 0 then 'NO' when sat.explicacion_aplicarlo = 1 then 'SI' END as 'LE EXPLICARON COMO APLICARLO',
        case when sat.entrega_triptico = 0 then 'NO' when sat.entrega_triptico = 1 then 'SI' END as 'TRIPTICO SOBRE EL USO',
        case when sat.claro_triptico = 0 then 'NO' when sat.claro_triptico = 1 then 'SI' END as 'ES CLARO Y SUFICIENTE EL CONTENIDO TRIPTICO',
        cat_fert.recibio_fertilizante,
        case when sat.cve_satisfecho = 1 then 'Muy satisfecho' when sat.cve_satisfecho = 2 then 'Satisfecho' when sat.cve_satisfecho = 3 then 'Poco satisfecho' when sat.cve_satisfecho = 4 then 'Nada satisfecho' END AS 'SATISFACCION',
        sat.porque,
        case when sat.recibio_suficiente = 0 then 'NO' when sat.recibio_suficiente = 1 then 'SI' END as 'RECIBIDO SUFICIENTE FERTILIZANTE',
        case when sat.mejora_produccion  = 0 then 'NO' when sat.mejora_produccion = 1 then 'SI' END as 'LE AYUDO A MEJORAR SU PRODUCCIÓN',
        case when sat.cve_porcentaje = 1 then 'Del 1 al 10 %' when sat.cve_porcentaje = 2 then 'Del 11 al 20 %' when sat.cve_porcentaje = 3 then 'Mayor al 20 %' END as 'DE SER AFIRMATIVO, ¿EN QUÉ PORCENTAJE?',
        sat.porque_satisfaccion,
        sat.sugerencias,
        # CONDICIÓN SOCIOECONÓMICA
        case when eco.pisoVivienda = 1 then 'TIERRA' when eco.pisoVivienda = 2 then 'CEMENTO' when eco.pisoVivienda = 3 then 'MADERA / MOSAICO' END as 'PISO DE SU VIVIENDA',
        case when eco.paredVivienda = 1 then 'TABIQUE / BLOCK / PIEDDRA' when eco.paredVivienda = 2 then 'ADOBE' when eco.paredVivienda = 3 then 'MADERA' when  eco.paredVivienda = 4 then 'MATERIALES BLANDOS (CARRIZO, LAMINA, BAMBU, ETC)' END as 'LAS PAREDES DE LA VIVIENDA' ,
        case when eco.techoVivienda = 1 then 'LOSA CONCRETO' when eco.techoVivienda = 2 then 'LAMINA' when eco.techoVivienda = 3 then 'MADERA / PALMA' END as 'TECHO DE SU VIVIENDA',
        case when find_in_set('1',servVivienda) then 'ENERGIA ELECTRICA'end as 'ENERGIA', 
        case when find_in_set('2',servVivienda) then 'AGUA POTABLE' end AS 'AGUA POTABLE', 
        case when find_in_set('3',servVivienda) then 'DRENAJE' end as 'DRENAJE', 
        case when find_in_set('4',servVivienda) then 'Telefonia/internet' end as 'tel / internet',
        eco.espaciosVivienda,
        case when eco.servMedico = 1 then 'IMSS' when eco.servMedico = 2 then 'ISSSTE' when eco.servMedico = 3 then 'FUERZAS ARMADAS' when eco.servMedico = 4 then 'CENTRO DE SALUD' when eco.servMedico = 5 then 'NO TIENE SERVICIO MEDICO PUBLICO' END as 'SERVICIO MEDICO',
        eco.ingreso,
        case when eco.ingreso_adicional = 0 then 'NO' when eco.ingreso_adicional = 1 then 'SI' end as 'INGRESOS FUERA DE SU PRODUCCIÓN',
        eco.remesas,
        eco.apoyoFamiliares,
        eco.negocioPropio,
        eco.empleoFueraUP,
        eco.pension,
        eco.otros,
        eco.numeroIntegrantes, 
        case when eco.apoyos_integrantes_hogar = 0 then 'NO' when eco.apoyos_integrantes_hogar = 1 then 'SI' end as 'APOYOS DE LOS PROGRAMAS SOCIALES DE GOBIERNO',
        eco.monto_total_menusal
        from agricultor 
        left join cat_municipios mpios on cve_municipio_unidad=mpios.cve_mpioINEGI and mpios.cve_entidad=16
        left join cat_ddr on agricultor.cve_ddr = cat_ddr.cve_ddr
        left join cat_localidades loc on loc.cve_entidad=16 and loc.cve_mpioINEGI=cve_municipio_unidad and loc.cve_localidad = cve_localidad_unidad
        left join cat_ejidos on agricultor.ejido = cve_ejido
        #on loc.cve_entidad=16 and 
        #and loc.cve_localidad = cat_ejidos.cve_ejido
        left join cat_quien_contesta quien on quien.cve_quien_contesta=agricultor.cve_quien_contesta
        left join datos_productivos prod on prod.folio=cve_encuesta
        left join cat_tenencias tenencia on prod.cve_tenencia=tenencia.cve_tenencia
        left join cat_cultivos cultivoant1 on cultivoant1.cve_cultivo=prod.ciclo_anterior_cultivo_1 
        left join cat_cultivos cultivoant2 on cultivoant2.cve_cultivo=prod.ciclo_anterior_cultivo_2
        left join cat_cultivos cultivo1 on cultivo1.cve_cultivo=prod.ciclo_actual_cultivo_1 
        left join cat_cultivos cultivo2 on cultivo2.cve_cultivo=prod.ciclo_actual_cultivo_2
        left join datos_tecnologicos dt on dt.cve_encuesta=agricultor.cve_encuesta 
        left join preparacion_terreno pt on pt.cve_encuesta=agricultor.cve_encuesta
        left join cat_preparacion_terreno cpt on pt.cve_preparacion=cpt.cve_preparacion 
        left join cat_preparacion_terreno cpt1 on pt.cve_preparacion1=cpt1.cve_preparacion 
        left join cat_preparacion_terreno cpt2 on pt.cve_preparacion2=cpt2.cve_preparacion 
        left join cat_preparacion_terreno cpt3 on pt.cve_preparacion3=cpt3.cve_preparacion 
        left join cat_preparacion_terreno cpt4 on pt.cve_preparacion4=cpt4.cve_preparacion 
        left join cat_preparacion_terreno cpt5 on pt.cve_preparacion5=cpt5.cve_preparacion 
        left join cat_preparacion_terreno cpt6 on pt.cve_preparacion6=cpt6.cve_preparacion 
        left join cat_preparacion_terreno cpt7 on pt.cve_preparacion7=cpt7.cve_preparacion 
        left join cat_adquisicion_semillas adsem on adsem.cve_adquisicion_semilla=dt.cve_adquisicion_semilla
        left join cat_tiempo_semilla ctsem on ctsem.cve_tiempo_semilla=dt.cve_tiempo_semilla
        left join labores_realizo lbr on lbr.cve_encuesta=agricultor.cve_encuesta
        left join cat_labores clb on lbr.laborRealizo=clb.cve_labor
        left join cat_labores clb1 on lbr.laborRealizo1=clb1.cve_labor
        left join cat_labores clb2 on lbr.laborRealizo2=clb2.cve_labor
        left join cat_labores clb3 on lbr.laborRealizo3=clb3.cve_labor
        left join cat_labores clb4 on lbr.laborRealizo4=clb4.cve_labor
        left join cat_aplicaciones cat_apli on dt.cve_aplicacion = cat_apli.cve_aplicacion
        # capacitacion
        left join capacitacion cap on cap.cve_encuesta = agricultor.cve_encuesta
        left join temas_capacitacion tem_cap on agricultor.cve_encuesta = tem_cap.cve_encuesta
        left join cat_temas tema on tem_cap.cve_tema = tema.cve_tema
        left join cat_temas tema1 on tem_cap.cve_tema1 = tema1.cve_tema
        left join cat_temas tema2 on tem_cap.cve_tema2 = tema2.cve_tema
        left join cat_temas tema3 on tem_cap.cve_tema3 = tema3.cve_tema
        left join cat_temas tema4 on tem_cap.cve_tema4 = tema4.cve_tema
        left join cat_temas tema5 on tem_cap.cve_tema5 = tema5.cve_tema
        left join cat_temas tema6 on tem_cap.cve_tema6 = tema6.cve_tema
        #satisfaccion_producto
        left join satisfaccion_producto sat on agricultor.cve_encuesta = sat.cve_encuesta
        LEFT JOIN cat_cedas on sat.cedas_nombre = cat_cedas.cve_ceda
        LEFT JOIN cat_recursos recu on recu.cve_recurso = sat.cve_recurso
        LEFT JOIN cat_recibio_fertilizantes cat_fert on sat.cve_recibio_fertilizante = cat_fert.cve_recibio_fcertilizante
        left join condicion_socioeconomica eco on sat.cve_encuesta = eco.cve_encuesta
        ";
        
        $query = $this->db->query($sinllenar);
                
        return $query->getResultArray();

        }
}
