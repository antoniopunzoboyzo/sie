<?php
namespace App\Models\Padrones;

use CodeIgniter\Model;

class CondicionModelo extends Model
{
	protected $DBGroup          = 'default';
    
    public function guardar($ins,$del)
    {
        $this->db->query($del);
    	$condicion=0;
        if($this->db->query($ins)){
        	$condicion=1;
        }
        return $condicion;
    }

    public function checafoto($validaImg)
    {
        $result= $this->db->query($validaImg);
        
        return $result->getResultArray();
    }

    public function guardarimg($ins,$act)
    {
    	$condicion=0;
        if($this->db->query($ins))
        	$condicion=1;
        else
            $condicion=0;

        if($this->db->query($act))
        	$condicion=1;
        else
            $condicion=0;

        return $condicion;
    }

    public function pdfConsulta($folio=''){
        $consultaFolio="SELECT a.cve_encuesta,concat(a.nombre,' ',a.paterno,' ',a.materno) as nombrebene,a.curp,a.folio_estatal ,d.localidad,c.municipio, b.nombre,b.ddr 
        ,c.municipio,a.cve_municipio_unidad,a.cve_localidad_unidad, e.ejido,a.latitud,a.longitud,l.cedas,f.cve_recibio_fertilizante, f.cve_satisfecho,f.porque_satisfaccion,
        g.sembro_este_ciclo,h.cultivo ,i.cultivo as cultivo1,j.aplico_fertilizante,(case when j.porque_aplico_ferti is null then '' else j.porque_aplico_ferti end) as porqueaplicofert,
        f.cve_porcentaje,f.porque, k.observaciones_encuestador,j.formato_unico_entrega,j.formato_porque,j.reporte_hecho,foto1,foto2,mapa,substr(l.cedas,10,length(l.cedas)) as cedalugar,mejora_produccion,sugerencias,f.porque_satisfaccion,n.ddr_nombre,n.jefe_ddr,
        a.fecha_encuesta
        FROM agricultura.encuestadores b 
        inner join agricultor a on a.cve_encuestador=b.cve_encuestador 
        left join cat_municipios c on c.cve_mpioINEGI=a.cve_municipio_unidad and c.cve_entidad='16'
        left join cat_localidades d on c.cve_entidad='16' and a.cve_municipio_unidad=d.cve_mpioINEGI and d.cve_localidad=a.cve_localidad_unidad
        left join cat_ejidos e on a.ejido=e.cve_ejido
        left join satisfaccion_producto f on f.cve_encuesta=a.cve_encuesta
        left join datos_productivos g on g.folio=a.cve_encuesta
        left join cat_cultivos h on h.cve_cultivo=g.cultivo_actual_1
        left join cat_cultivos i on i.cve_cultivo=g.cultivo_actual_2
        left join datos_tecnologicos j on j.cve_encuesta=a.cve_encuesta
        left join condicion_socioeconomica k on k.cve_encuesta=a.cve_encuesta
        left join cat_cedas l on l.cve_ceda=f.cedas_nombre 
        left join evidencia_fotografica m on m.cve_encuesta=a.cve_encuesta
        left join cat_ddr n on n.cve_ddr=a.cve_ddr  where a.cve_encuesta='".$folio."'";
        return $this->db->query($consultaFolio)->getResultArray();
    }

}
?>